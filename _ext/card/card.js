// JavaScript Document
$(document).ready(function(e) {
    $("#_cLogout").click(function(){
		var ok = confirm("Confermi uscita dalla tua area personale?");
		if( ok ){
			document.location = '?fn_logout=1';	
		}
	});
	FNET.Init();
	FNET.loadData();
	//FNET.mainInfo();
});

var FNET = {
	sp: '_ext/scripts/fnet.php',
	card: 0,
	customer_id: 0,
	
	CardInfo: null, //I dati della card
	fullname: null, //nome+cognome
	
	Init: function(){
		this.sp = path_webroot + this.sp;
		//this.card = $("#card").val();
		this.customer_id = $("input[name='customer_id']").val();
	},
	
	mainInfo: function(){
		var This = this;
		var data = $("#cardForm").serialize();
		//alert(data)
		$.ajax({
			url: This.sp,
			data: data,
			type: 'POST',
			success: function(data){
				//alert(data);
				data = JSON.parse( data );
				This.CardInfo = data;
				This.fullname = data.customer.personalInfo.name + " " + data.customer.personalInfo.surname;
				
				$("*[data-id='fullname']").html( This.fullname );
				//$("*[data-id='identityCard']").html( data.customer.personalInfo.identityCard );
				$("*[data-id='identityCard']").html( '<span style="font-size: 16px;">' + data.customer.fidelyCode + '</span>' );
				$("*[data-id='pointsCharged']").html( data.punti_text );
				$("*[data-id='pointsCharged_input']").val( data.punti );
				$("*[data-id='ultimoUtilizzo']").html( data.data_ultimo_movimento );
			}
		});
	},
	
	loadData: function(){
		var This = this;
		var act = $("#cardForm input[name='act']").val();
		switch(act){
			case 'getCustomer':
				This.DatiPersonali();
				break;
			case 'movimenti':
				This.Movimenti();
				break;
		}
	},
	
	Movimenti: function( page ){
		var This = this;
		var f = $("#cardForm");
		
		var ap = f.find("input[name='actualPage']").val();
		ap = parseInt(ap);
		
		if( page=="back" ){
			var page = ap>0 ? ap-1 : ap;
		}else if( page=="next"){
			var page = ap+1;
		}
		
		f.find("input[name='actualPage']").val( page );
		f.find("input[name='initLimit']").val( (page*10) );
		var o = $("#_tableMovements > tbody");
		o.html('');
		
		var data = $("#cardForm").serialize();
		//alert(data)
		
		$.ajax({
			url: This.sp,
			type: 'POST',
			data: data,
			success: function(data){
				//alert(data);
				data = $.parseJSON(data);
				if( data.status ){
					//var totalPages = data.pagination.totalPages;
					//f.find("input[name='totalPages']").val( totalPages );
					var totalPages = 1;
					
					var nav = $("#MovimentiNav");
					nav.html('');
					if( data.data.movimenti && data.data.movimenti.length>0){
						if(totalPages>1){
							if( ap>0 ){
								var a = $("<a>");	
								a.attr("href","javascript:FNET.Movimenti('back');");
								a.addClass("btnMovimenti");
								a.html("PRECEDENTE");
								nav.append(a);
							}
						
							for(var i=0;i<totalPages;i++){
								var a = $("<a>");
								a.attr("href","javascript:FNET.Movimenti(" + i + ");");
								a.addClass("btnMovimenti");
								
								if( ap==i ){
									a.addClass("active");	
								}
								a.html( (i+1) );
								nav.append(a);
							}
							if( ap<totalPages-1){
								var a = $("<a>");	
								a.attr("href","javascript:FNET.Movimenti('next');");
								a.addClass("btnMovimenti");
								a.html("SUCCESSIVO");
								nav.append(a);
							}
						}
						
						if( ap==0 && data.data.movimenti.length==0 ){
							$("#_tableMovements").hide();
							$("#cardForm .noData").show();
						}else{
							$("#_tableMovements").show();
							$("#cardForm .noData").hide();
							for(var i in data.data.movimenti){
								var mov = data.data.movimenti[i];
								
								if( parseFloat(mov.caricati)>0 || parseInt(mov.scaricati)>0 || parseFloat(mov.totalMoney)>0 ){
									var row = $("<tr>");
									//Data
									var td = $("<td>");
									td.addClass("center");
									td.html( mov.data );
									row.append(td);
									
									var td = $("<td>");
									td.addClass("center");
									td.html( mov.ora );
									row.append(td);
									
									var td = $("<td>");
									td.addClass("center");
									if( parseFloat( mov.caricati )>0 ){
										var txt = "Carico Punti";
									}else if( parseFloat( mov.scaricati )>0 ){
										var txt = "Scarico Punti";
									}else{
										var txt = "-";
									}
									td.html( txt );
									row.append(td);
									
									var td = $("<td>");
									td.addClass("center");
									var txt = mov.shop_name;
									td.html( txt );
									row.append(td);
									
									var td = $("<td>");
									td.addClass("center");
									var txt = parseFloat( mov.totalMoney )>0 ? mov.importo : "-";
									td.html( txt );
									row.append(td);
									
									var td = $("<td>");
									td.addClass("center");
									var txt = parseFloat( mov.caricati )>0 ? mov.caricati : "-";
									td.html( txt );
									row.append(td);
									
									var td = $("<td>");
									td.addClass("center");
									var txt = parseFloat( mov.scaricati )>0 ? mov.scaricati : "-";
									td.html( txt );
									row.append(td);
									
									o.append(row);
								}
							}
						}
						$("#_tableMovements").removeClass('hide');
						$("#cardForm .NoData").addClass('hide');
					}else{
						$("#_tableMovements").addClass('hide');
						$("#cardForm .NoData").removeClass('hide');
					}
					
					//FINE CARICAMENTO ROW
				}else{
					vex.dialog.alert( data.msg );	
				}
			}
		});
	},
	
	DP_Combustibile: function(){
		var v = $("select[name='combustibile']").val();
		
		if( v==191 ){
			$("input[name='combustibile_altro']").removeClass("hide");
		}else{
			$("input[name='combustibile_altro']").val('').addClass("hide");	
		}
	},
	
	DP_Professione: function(){
		var v = $("select[name='professione']").val();
		
		if( v==185 ){//Altro
			$("input[name='professione_altro']").removeClass("hide");
		}else{
			$("input[name='professione_altro']").val('').addClass("hide");	
		}
	},
	
	DatiPersonaliSalva: function(){		
		var This = this;
		var o = $("#cardForm");
		o.find("input[name='act']").val('updateProfile');		
		
		var data = o.serialize();
		//data = data.replace(/=&/g,"= &");//per bug con il passaggio delle variabili, in caso di variabili vuote annulla le variabili successive, per tanto è necessario passare almeno uno spazio vuoto
		//alert(data);
		$("*[data-errorTxt='1']").css("color","black");
		$.ajax({
			url: This.sp,
			type: 'post',
			data: data,
			success: function( data ){
				//alert( data );
				data = JSON.parse( data );
				if( data.status ){
					o.find("input[name='act']").val('getCustomer');
					This.DatiPersonaliModifica(false);
					This.DatiPersonali();
				}else{
					var html = '<div style="color: #cc0000; text-align: center;">' + data.msg + '</div>';
					for(var i in data.errors){
						var e = data.errors[i];
						//alert(e);
						//html += "<li>" + e + "</li>";
						$("*[data-id='" + e + "_err']").css("color","red");
					}
					vex.dialog.alert( html );
				}
			}
		});
	},
	
	DatiPersonali: function(){
		var This = this;
		var o = $("#cardForm");
		o.find("input[name='az']").val('InfoCard');
		var data = o.serialize();
		$.ajax({
			url: This.sp,
			data: data,
			type: 'POST',
			success: function(data){
				//alert( data );
				data = JSON.parse( data );
				
				if( data.status ){
					data = data.data;				
					o.find("div[data-id='name']").html( data.customer.name );
					o.find("input[name='name']").val( data.customer.name );
					
					o.find("div[data-id='surname']").html( data.customer.surname );
					o.find("input[name='surname']").val( data.customer.surname );

					o.find("div[data-id='birthdate']").html( data.customer.data_nascita );
					if( data.customer.data_nascita.length>0 ){
						var birthday = data.customer.data_nascita.split("/");
						o.find("select[name='birthdate_d']").val( birthday[0] );
						o.find("select[name='birthdate_m']").val( birthday[1] );
						o.find("select[name='birthdate_y']").val( birthday[2] );
					}
					
					o.find("div[data-id='gender']").html( data.customer.gender );
					if( data.customer.gender.length>0 && data.customer.gender!="N" ){
						o.find("select[name='gender']").val( data.customer.gender );
					}
					
					o.find("div[data-id='address']").html( data.customer.address );
					o.find("input[name='address']").val( data.customer.address );
					
					o.find("div[data-id='addressNumber']").html( data.customer.addressNumber );
					o.find("input[name='addressNumber']").val( data.customer.addressNumber );
					
					o.find("div[data-id='zip']").html( data.customer.zip );
					o.find("input[name='zip']").val( data.customer.zip );
					
					o.find("div[data-id='telephoneContactData']").html( data.customer.telephoneContactData );
					o.find("input[name='telephoneContactData']").val( data.customer.telephoneContactData );
					
					o.find("div[data-id='mobileContactData']").html( data.customer.mobileContactData );
					o.find("input[name='mobileContactData']").val( data.customer.mobileContactData );
					
					o.find("div[data-id='mailContactData']").html( data.customer.mailContactData );
					o.find("input[name='mailContactData']").val( data.customer.mailContactData );
					
					o.find("div[data-id='regione']").html( data.customer.Regione );
					if( data.customer.GeoLevel1>0 ){
						This.setGeoSelect(o,'GeoLevel1',data.customer.Regioni,data.customer.GeoLevel1);
					}
					
					o.find("div[data-id='provincia']").html( data.customer.Provincia );
					if( data.customer.GeoLevel1>0 ){
						This.setGeoSelect(o,'GeoLevel2',data.customer.Province,data.customer.GeoLevel2);
					}
					o.find("div[data-id='comune']").html( data.customer.Comune );
					if( data.customer.GeoLevel2>0 ){
						This.setGeoSelect(o,'GeoLevel3',data.customer.Comuni,data.customer.GeoLevel3);
					}
					/*o.find("div[data-id='localita']").html( data.customer.Localita );
					if( data.customer.GeoLevel4>0 ){
						This.setGeoSelect(o,'GeoLevel4',data.customer.Localitas,data.customer.GeoLevel4);
					}*/
					
					
					//Professione
					/*for(var i in data.customer.customerDynamicFields){
						var v = data.customer.customerDynamicFields[i];
						if( v.id==127 ){//Professione
							var tmp = $("select[name='professione']");
							tmp.val( v.value );
							This.DP_Professione();
							if( v.value==185 ){
								var note = data.customer.notes;
								//Professione (altro): web - 
								var t1 = 'Professione (altro): ';
								var text = note.substring( note.indexOf(t1)+t1.length );
								text = text.substring( 0, text.indexOf(" - ") );
								$("input[name='professione_altro']").val( text );
							}else{
								var text = tmp.find(":selected").text();	
							}
							$("div[data-id='professione']").html( text );
						}
					}*/
	
					var ck = data.customer.privacy.usedForPromotions==true ? true : false;
					o.find("input[name='privacy1']").attr("checked",ck);
					var ck = data.customer.privacy.usedForStatistics==true ? true : false;
					o.find("input[name='privacy2']").attr("checked",ck);
				
					//alert("END");
				}else{//data.status = 0;
				}
			}
		});
	},
	
	setGeoSelect: function(objForm,selName,list,id_selected){
		var rSelect = objForm.find("select[name='" + selName +  "']").get(0);
		rSelect.options.length=0;
		
		var iSel=0;
		if( !id_selected ){
			var opt = document.createElement('option');
			opt.text = '-';
			opt.value = 0;
			rSelect.add(opt,rSelect[iSel]);
			iSel++;
		}
		
		for(var i in list){
			//var opt = $('option');
			var opt = document.createElement('option');
			opt.text = list[i].name;
			opt.value = list[i].id;
			rSelect.add(opt,rSelect[iSel]);
			//rSelect.append(opt);
			iSel++;
		}
		if( id_selected>0 ){
			objForm.find("select[name='" + selName +  "']").val( id_selected );
		}
	},
	
	DatiPersonaliModifica: function(show){
		if( show ){
			$("#DP_ModificaBtn").css("display","none");
			$("#DP_AnnullaBtn").css("display","inline-block");
			$("#DP_SalvaBtn").css("display","inline-block");
			
			$("._Descr").hide();
			$("._inputData").show();
			$("input[name='privacy1']").attr("disabled",false);
			$("input[name='privacy2']").attr("disabled",false);
			$("input[name='privacy3']").attr("disabled",false);
		}else{
			$("#DP_ModificaBtn").css("display","inline-block");
			$("#DP_AnnullaBtn").css("display","none");
			$("#DP_SalvaBtn").css("display","none");
			
			$("._Descr").show();
			$("._inputData").hide();
			
			$("input[name='privacy1']").attr("disabled",true);
			$("input[name='privacy2']").attr("disabled",true);
			$("input[name='privacy3']").attr("disabled",true);
		}
	},
	
	updateProvince: function(id_form){
		var This = this;
		var o = $("#"+id_form);
		var id_regione = o.find("select[name='GeoLevel1']").val();
		var rSelect = o.find("select[name='GeoLevel2']").get(0);
		var x = o.find("select[name='GeoLevel3']").get(0);
		x.options.length=0;
		rSelect.options.length=0;
		if( id_regione>0 ){
			$.ajax({
				url: This.sp,
				type:'POST',
				data: "act=provinceList&req=ajax&id_regione=" + id_regione,
				success: function(data){
					//alert(data);
					data = $.parseJSON(data);
					rSelect.options.length=0;
					
					var iSel=0;
					var opt = document.createElement('option');
					opt.text = '-';
					opt.value = 0;
					rSelect.add(opt,rSelect[iSel]);
					iSel++;
					
					for(var i in data.data){
						var opt = document.createElement('option');
						opt.text = data.data[i].name;
						opt.value = data.data[i].id;
						rSelect.add(opt,rSelect[iSel]);
						iSel++;
					}
				}
			});
		}
	},
	
	updateComuni: function(id_form){
		var This = this;
		var o = $("#"+id_form);
		var id_provincia = o.find("select[name='GeoLevel2']").val();
		var rSelect = o.find("select[name='GeoLevel3']").get(0);
		rSelect.options.length=0;
		if( id_provincia>0 ){
			$.ajax({
				url: This.sp,
				type:'POST',
				data: "act=comuniList&req=ajax&id_provincia=" + id_provincia,
				success: function(data){
					//alert(data);
					data = $.parseJSON(data);
					rSelect.options.length=0;
					
					var iSel=0;
					var opt = document.createElement('option');
					opt.text = '-';
					opt.value = 0;
					rSelect.add(opt,rSelect[iSel]);
					iSel++;
					
					for(var i in data.data){
						var opt = document.createElement('option');
						opt.text = data.data[i].name;
						opt.value = data.data[i].id;
						rSelect.add(opt,rSelect[iSel]);
						iSel++;
					}
				}
			});
		}
	},
	
	Zone: function( level, id_sel1, id_sel2 ){
		var This = this;
		var idSel1 = id_sel1;
		var idSel2 = id_sel2;
		
		switch(level){
			case 2: //province
				var v = $("select[name='geoLevel1']").val();
				
				var o = $("select[name='geoLevel2']");
				o.html("");

				$.ajax({
					url: This.sp,
					type: 'POST',
					data: 'az=ProvinceSel&id_regione=' + v,
					success: function(data){
						//alert(data);
						data = JSON.parse( data );
						var id_sel = o.attr("data-id");
						for(var i in data.geoLevel){
							var opt = $("<option></option>");
							opt.text( data.geoLevel[i].name );
							opt.val( data.geoLevel[i].id );
							o.append( opt );							
							
						}
								
						if( idSel1>0 ){
							o.val(idSel1);
						}
						
						if( idSel2>0 ){
							This.Zone(3, idSel2);
						}else{
							This.Zone(3);
						}
					}
				});
				
				break;
			case 3: //comuni
				var v = $("select[name='geoLevel2']").val();
				//alert(v);
				var o = $("select[name='geoLevel3']");
				o.html("");
				
				$.ajax({
					url: This.sp,
					type: 'POST',
					data: 'az=ComuniSel&id_provincia=' + v,
					success: function(data){
						//alert(data);
						data = JSON.parse( data );
						for(var i in data.geoLevel){
							var opt = $("<option></option>");
							opt.text( data.geoLevel[i].name );
							opt.val( data.geoLevel[i].id );
							o.append( opt );
						}
						
						if( idSel1>0 ){
							o.val(idSel1);	
						}
					}
				});
				break;
		}
	},
	
	TP_add: function(punti){
		var o = $("#trasferimentoPuntiForm");
		var el = o.find("input[name='punti']");
		var v = el.val();
		v = v>0 ? v : 0;
		v = parseInt(v);
		v += punti;
		
		var x = o.find("input[name='limite_punti']").val();
		var residuo = x-v;
		alert(x+" "+v)
		if( residuo>=0){		
			el.val( v );
			$("*[data-id='punti_residui']").html( residuo );
		}else{
			vex.dialog.alert('Non hai punti sufficienti');
		}
	},
	
	TP_cancel: function(){
		var o = $("#trasferimentoPuntiForm");
		o.get(0).reset();
		$("*[data-id='punti_residui']").html( 0 );
	},
	
	TP_Trasferisci: function(){
		var This = this;
		
		var o = $("#trasferimentoPuntiForm");
		var numero_card = o.find("input[name='numero_card']").val();
		var punti = o.find("input[name='punti']").val();
		punti = parseInt( punti );
		if( numero_card.length!=14 ){
			vex.dialog.alert('Numero card non valido');	
		}else if( !(punti>0) ){
			vex.dialog.alert('Inserire i punti da trasferire');	
		}else{
			vex.dialog.confirm({
				message: 'CONFERMA TRAFERIMENTO?',
				callback: function(data){
					if( data ){
						var data = $("#trasferimentoPuntiForm").serialize();
						$.ajax({
							url: This.sp,
							type: 'POST',
							data: data,
							success: function(data){
								alert(data);
								data = JSON.parse(data);
								if( data.status ){
									This.TP_cancel();
								}else{
									vex.dialog.alert('');
								}
							}
						});
					}
				}
			});
		}
	}
};