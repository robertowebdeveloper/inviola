<?php
//CONFIGURAZIONE
$Customer = fnetAction('getCustomer',array("user"=>$_SESSION["_user"],"pw"=>$_SESSION["_userpw"]) );
$Customer = $Customer->data;

$_CardPar = array(
	"p" => $_GET["_p"],
	"path" => path_site . "_ext/card/",
	"menu_fired" => 0,
	"menu" => array("personalinfo"=>'Dati personali',"movements"=>'Lista movimenti',"prizes"=>'Premi',"exchanged_prizes"=>'Ordini'),
	"inc" => 'personalinfo',
	"balance" => $Customer->customer->balance_points,
	"balance_txt" => number_format($Customer->customer->balance_points,0,".","")
);
//print_r($_CardPar);
//echo "<pre>";print_r($Customer);
?>
<script type="text/javascript" src="{{root}}_ext/card/card.js"></script>

<div id="CardBody">
	<div id="_cWrapper">
        <nav id="_cMenu">
        	<a id="_cLogout" href="#"></a>
        	<ul>
				<?php
                foreach($_CardPar["menu"] as $k=>$v){
                    $fired = $_GET["_p"]==md5($k) || ($k=="personalinfo" && !isset($_GET["_p"]) ) ? ' class="_cfired"' : '';
					if( $_GET["_p"]==md5($k) ){
						$_CardPar["inc"] = $k;	
					}
                ?>
                    <li<?=$fired; ?>><a href="?_p=<?=md5($k); ?>"><?=$v; ?></a></li>
                <?php } ?>
            </ul>
            <div id="_cSubbar">
            	<span class="_cWelcome">
                	<?php if( strtolower($Customer->customer->gender)=="f" ){ ?><# Benvenuta #><?php }else{ ?><# Benvenuto #><?php } ?>
                   <b class="uppercase"><?="{$Customer->customer->name} {$Customer->customer->surname}"; ?></b>
                </span>
                <span class="_cBalance"><# Saldo punti #>: <b><?=$_CardPar["balance_txt"]; ?></b></span>
            </div>
		</nav>
        <div id="_cCont">
        	<?php include("_ext/card/{$_CardPar[inc]}.inc.php"); ?>
        </div>
	</div>
    
</div>
<div style="clear:both;"></div>