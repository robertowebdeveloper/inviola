<h1><# Lista Movimenti #></h1>
<br>
<form id="cardForm">
    <input type="hidden" name="act" value="movimenti">
    <input type="hidden" name="req" value="ajax">
    <input type="hidden" name="customer_id" value="<?=$Customer->customer->id; ?>">
    <input type="hidden" name="user" value="<?=$Customer->customer->userName; ?>">
    <input type="hidden" name="pw" value="<?=$Customer->customer->password; ?>">
    
    <input type="hidden" name="actualPage" value="0">
    <input type="hidden" name="initLimit" value="0">
    <input type="hidden" name="totalPages">
    
    <div class="NoData hide"><# Nessun movimento presente #></div>
    <table id="_tableMovements" class="_tableMovements hide">
        <thead>
            <tr>
                <th class="center"><# Data #></th>
                <th class="center"><# Ora #></th>
                <th><# Operazione #></th>
                <th class="center"><# Negozio #></th>
                <th><# Importo #></th>
                <th><# Punti Caricati #></th>
                <th><# Punti Scaricati #></th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
    
    <div class="right">
    	<div id="MovimentiNav"></div>
    </div>
    
</form>