<h1><# Profilo Personale #></h1>
<br>
<form id="cardForm">
    <input type="hidden" name="act" value="getCustomer">
    <input type="hidden" name="req" value="ajax">
    <input type="hidden" name="customer_id" value="<?=$Customer->customer->id; ?>">
    <input type="hidden" name="user" value="<?=$Customer->customer->userName; ?>">
    <input type="hidden" name="pw" value="<?=$Customer->customer->password; ?>">
    
    <div class="_tableData">
    	<div>
            <div class="_tD_row">
                <div class="_tD_tc _tD_tcLeft" data-id="name_err" data-errorTxt="1"><# Nome #></div>
               <div class="_tD_tc">
                    <div class="_Descr" data-id="name"></div>
                    <input type="text" class="_inputData" name="name">
               </div>
            </div>
            <div class="_tD_row">
                <div class="_tD_tc _tD_tcLeft" data-id="surname_err" data-errorTxt="1"><# Cognome #></div>
               <div class="_tD_tc">
                    <div class="_Descr" data-id="surname"></div>
                    <input type="text" class="_inputData" name="surname">
               </div>
            </div>
            
            <div class="_tD_row">
                <div class="_tD_tc _tD_tcLeft" data-id="birthdate_err" data-errorTxt="1"><# Data di nascita #></div>
               <div class="_tD_tc">
                    <div class="_Descr" data-id="birthdate"></div>
                    <div class="_inputData">
                        <select name="birthdate_d" style="width:auto;"><?php
                            for($i=1;$i<=31;$i++){
                                $x = str_pad($i,2,"0",STR_PAD_LEFT);
                                ?><option value="<?=$x; ?>"><?=$x; ?></option><?php	
                            }
                        ?></select>
                        -
                        <select name="birthdate_m" style="width:auto;"><?php
                            for($i=1;$i<=12;$i++){
                                $x = str_pad($i,2,"0",STR_PAD_LEFT);
                                ?><option value="<?=$x; ?>"><?=$x; ?></option><?php	
                            }
                        ?></select>
                        -
                        <select name="birthdate_y" style="width:auto;"><?php
                            $maxY = date("Y") - 18;
                            for($i=1900;$i<=$maxY;$i++){
                                $sel = $i==1960 ? ' selected' : "";
                                ?><option value="<?=$i; ?>"<?=$sel; ?>><?=$i; ?></option><?php	
                            }
                        ?></select>
                    </div>
                    
               </div>
            </div>
            
            <div class="_tD_row">
                <div class="_tD_tc _tD_tcLeft" data-id="gender_err" data-errorTxt="1"><# Sesso #></div>
               <div class="_tD_tc">
                    <div class="_Descr" data-id="gender"></div>
                    <div class="_inputData">
                        <select name="gender" style="width:auto;"><?php
                            $arr = array("M","F");
                            foreach($arr as $v){
                                ?><option value="<?=$v; ?>"><?=$v; ?></option><?php
                            }
                        ?></select>
                    </div>
                    
               </div>
            </div>
            
            <div class="_tD_row">
                <div class="_tD_tc _tD_tcLeft" data-id="address_err" data-errorTxt="1"><# Indirizzo #></div>
               <div class="_tD_tc">
                    <div class="_Descr" data-id="address"></div>
                    <input type="text" class="_inputData span6" name="address">
                    
               </div>
            </div>
            
            <div class="_tD_row">
                <div class="_tD_tc _tD_tcLeft"><# Numero Civico #></div>
               <div class="_tD_tc">
                    <div class="_Descr" data-id="addressNumber"></div>
                    <input type="text" class="_inputData span3" name="addressNumber">
                    
               </div>
            </div>
            
            <div class="_tD_row">
                <div class="_tD_tc _tD_tcLeft" data-id="zip_err" data-errorTxt="1"><# CAP #></div>
               <div class="_tD_tc">
                    <div class="_Descr" data-id="zip"></div>
                    <input type="text" class="_inputData span3" name="zip">                
               </div>
            </div>
            
            <input type="hidden" name="country" value="<?=$Customer->customer->country; ?>">
            <div class="_tD_row">
                <div class="_tD_tc _tD_tcLeft"><# Regione #></div>
               <div class="_tD_tc">
                    <div class="_Descr" data-id="regione"></div>
                    <select class="_inputData" name="GeoLevel1" onchange="FNET.updateProvince('cardForm');"></select>
               </div>
            </div>
            
            <div class="_tD_row">
                <div class="_tD_tc _tD_tcLeft"><# Provincia #></div>
               <div class="_tD_tc">
                    <div class="_Descr" data-id="provincia"></div>
                    <select class="_inputData" name="GeoLevel2" onchange="FNET.updateComuni('cardForm');"></select>
               </div>
            </div>
            
            <div class="_tD_row">
                <div class="_tD_tc _tD_tcLeft" data-id="comune_err" data-errorTxt="1"><# Comune #></div>
               <div class="_tD_tc">
                    <div class="_Descr" data-id="comune"></div>
                    <select class="_inputData" name="GeoLevel3"></select>
               </div>
            </div>
            
            <div class="_tD_row">
                <div class="_tD_tc _tD_tcLeft"><# Telefono #></div>
               <div class="_tD_tc">
                    <div class="_Descr" data-id="telephoneContactData"></div>
                    <input type="text" class="_inputData" name="telephoneContactData">
               </div>
            </div>
            
            <div class="_tD_row">
                <div class="_tD_tc _tD_tcLeft" data-id="mobileContactData_err" data-errorTxt="1"><# Cellulare #></div>
               <div class="_tD_tc">
                    <div class="_Descr" data-id="mobileContactData"></div>
                    <input type="text" class="_inputData" name="mobileContactData">
               </div>
            </div>
            
            <div class="_tD_row">
                <div class="_tD_tc _tD_tcLeft" data-id="mailContactData_err" data-errorTxt="1"><# E-mail #></div>
               <div class="_tD_tc">
                    <div class="_Descr" data-id="mailContactData"></div>
                    <input type="text" class="_inputData" name="mailContactData">
               </div>
            </div>
            
            <!--div class="_tD_row">
                <div class="_tD_tc _tD_tcLeft"><# Professione #></div>
               <div class="_tD_tc">
                    <div class="_Descr"><?=$Customer->customer->mailContactData; ?></div>
                    <div class="_inputData">
                        <select name="professione" data-id="127" onchange="FNET.DP_Professione();" style="width: 150px;"><?php
                            $arr = array(190=>'Imprenditore',
                                182=>'Libero professionista',
                                184=>'Artigiano',
                                187=>'Impiegato/a',
                                189=>'Operaio/a',
                                186=>'Casalinga',
                                183=>'Studente',
                                188=>'Pensionato/a',
                                185=>'Altro');
                            foreach($arr as $k=>$v){
                                ?><option value="<?=$k; ?>"><?=$v; ?></option><?php
                            }
                        ?></select>
                        &nbsp;&nbsp;&nbsp;
                        <input type="text" class="form-control hide" name="professione_altro" placeholder="Altro" style="display: inline-block; width: 250px;">
                    </div>
               </div>
            </div-->
        </div>
                
        <!--div>
            <input type="checkbox" name="privacy1" value="1" disabled checked>
            <span data-id="privacy1_err" data-errorTxt="1">Consenso dell'interessato per il trattamento dei dati al fine di informazione e promozione commerciale</span>
        </div>
        <div>
            <input type="checkbox" name="privacy2" value="1" disabled checked>
            Consenso dell'interessato per il trattamento dei dati al fine di informazione e promozione commerciale
        </div>
        <div>
            <input type="checkbox" name="privacy3" value="1" disabled checked>
            Consenso dell'interessato per il trattamento dei dati al fine di informazione e promozione commerciale
        </div-->
        <input type="hidden" name="privacy1" value="1"><input type="hidden" name="privacy2" value="1"><input type="hidden" name="privacy3" value="1">
        
    </div>
    <hr>
    <div class="BoxDatiPersonali">
            
            <div class="right">
                <br>
                <a id="DP_ModificaBtn" href="javascript:void(0);" onclick="FNET.DatiPersonaliModifica(true);" class="btn btn-primary">MODIFICA</a>
                <a id="DP_AnnullaBtn" href="javascript:void(0);" onclick="FNET.DatiPersonaliModifica(false);" class="btn btn-warning">ANNULLA</a>
                <a id="DP_SalvaBtn" href="javascript:void(0);" onclick="FNET.DatiPersonaliSalva();" class="btn btn-danger">SALVA</a>
                <br><br>
            </div>
        </div>
                    
    </div>
</form>