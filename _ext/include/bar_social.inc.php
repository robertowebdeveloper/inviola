<div class="row">
	<div class="col-md-1 col-sm-1 col-xs-1"></div>
    <div class="col-md-10">
        <div id="bigSocial">
            <a href="https://www.facebook.com/ACFFiorentina" target="_blank"><img src="{{theme}}img/stripe_socials/facebook.png" alt="" class="img-responsive"></a>
            <a href="https://twitter.com/acffiorentina" target="_blank"><img src="{{theme}}img/stripe_socials/twitter.png" alt="" class="img-responsive"></a>
            <a href="https://www.youtube.com/user/ViolaChannel" target="_blank"><img src="{{theme}}img/stripe_socials/you_tube.png" alt="" class="img-responsive"></a>
            <a href="https://www.flickr.com/photos/acffiorentina" target="_blank"><img src="{{theme}}img/stripe_socials/flickr.png" alt="" class="img-responsive"></a>
            <a href="http://instagram.com/acffiorentina" target="_blank"><img src="{{theme}}img/stripe_socials/istagram.png" alt="" class="img-responsive"></a>
            <a href="http://feeds.feedburner.com/Violachannel" target="_blank"><img src="{{theme}}img/stripe_socials/rss.png" alt="" class="img-responsive"></a>
            <a href="http://it.violachannel.tv/vc13-download.html" target="_blank"><img src="{{theme}}img/stripe_socials/download.png" alt="" class="img-responsive"></a>
        </div>
    </div>
</div>