<?php
if( $_SESSION['customer_id']>0 ){
	$q = "SELECT id FROM utenti WHERE customer_id = {$_SESSION['customer_id']} LIMIT 1";
	$id_utente = $S->cn->OF($q);
	
	//Elimina vecchi alert partite scadute
	$q = "DELETE ua.* FROM `{$S->_db_prefix}users_alert` ua INNER JOIN `{$S->_db_prefix}gf_match` gm ON ua.id_gf_match = gm.id WHERE gm.data<NOW()";
	$S->cn->Q($q);
	
	//Controlla se c'è quello legato alla prossima partita per il gioco formazione
	if( !class_exists("IndovinaFormazione") )
		require_once("_ext/scripts/class/IndovinaFormazione.class.php");
	if( !isset($IF) )
		$IF = new IndovinaFormazione( $S->cn , $S->_db_prefix );
		
	$next_match = $IF->getMatches('next',$id_utente);
	if( $next_match ){
        $types = array('game-if'=>'','game-ir'=>'_ir','game-im'=>'_im'); //if => indovina formazione, ir => indovina risultato, im => indovina marcatore

        foreach($types as $type => $field_giocata){
            $q = "SELECT ua.* FROM `{$S->_db_prefix}users_alert` ua WHERE ua.id_gf_match = {$next_match['id']} AND ua.id_utente = {$id_utente} AND `type` = '{$type}'";
            $alert_next_match = $S->cn->OQ($q);
            if( $alert_next_match==-1 && !$next_match['giocata' . $field_giocata] && $next_match["rimane"]["timestamp"]>0){
                $q = "INSERT INTO `{$S->_db_prefix}users_alert` (id_utente,id_gf_match,`type`) VALUES ({$id_utente},{$next_match['id']},'{$type}')";
                $this->cn->Q( $q );
            }else if($next_match['giocata' . $field_giocata] || $next_match['rimane']['timestamp']<0){
                $q = "DELETE FROM `{$S->_db_prefix}users_alert` WHERE id_utente={$id_utente} AND id_gf_match={$next_match['id']} AND `type`='{$type}'";
                $this->cn->Q( $q );
            }
        }
	}
	
	$q = "SELECT * FROM `{$S->_db_prefix}users_alert` WHERE id_utente = {$id_utente} AND closed IS NULL AND ( showed < close_after_showed || close_after_showed=0)";
	$list = $S->cn->Q($q,true);
	
	if( count($list)>0 ){ ?>
    	<form id="BarUserAlert_form">
        	<input type="hidden" name="az" value="closeUserAlert">
           <input type="hidden" name="id_utente" value="<?=$id_utente; ?>">
        	<input type="hidden" name="users_alert_ids" value="<?php
    	        $ids = array();
				foreach($list as $li){
					$ids[] = $li['id'];	
				}
				echo implode(",",$ids);
			?>">
        </form>
		<div id="BarUserAlert">
			<div class="container"><div class="row"><div class="col-md-12">
            	<a href="#" class="close glyphicon glyphicon-remove"></a>
				<ul class="Alert">
					<?php
                    foreach($list as $li){
						$q = "UPDATE `{$S->_db_prefix}users_alert` SET `showed` = `showed`+1, last_view = NOW() WHERE id = {$li['id']}";
						$S->cn->Q($q);
						?><li>
							<?php
                            if( $li["id_gf_match"]>0 ){
								$msg_match = $next_match["in_casa"] ? "Fiorentina - {$next_match['avversario']}" : "{$next_match['avversario']} - Fiorentina";
                                switch( $li['type'] ){
                                    case 'game-if':
                                        $msg = 'Non hai ancora giocato al gioco "<a href="{{url indovina-formazione}}">{{urlname indovina-formazione}}</a>" per la partita <b>' . $msg_match . '</b>. Hai tempo ancora ';
                                        break;
                                    case 'game-ir':
                                        $msg = 'Non hai ancora giocato al gioco "<a href="{{url indovina-risultato}}">{{urlname indovina-risultato}}</a>" per la partita <b>' . $msg_match . '</b>. Hai tempo ancora ';
                                        break;
                                    case 'game-im':
                                        $msg = 'Non hai ancora giocato al gioco "<a href="{{url indovina-primo-marcatore}}">{{urlname indovina-primo-marcatore}}</a>" per la partita <b>' . $msg_match . '</b>. Hai tempo ancora ';
                                        break;
                                    default:
                                        $msg = false;
                                        break;
                                }
                                if( $msg ){
                                    echo $IF->TimeToPlay($next_match, $msg);
                                }
                           }else{
							   echo $li['msg'];
							 }
							?>
                       </li><?php	
					}
					unset($IF);
					?>
			   </ul>
			</div></div></div>
		</div>
	<?php }
} ?>