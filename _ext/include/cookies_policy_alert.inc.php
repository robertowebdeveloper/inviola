<?php
if(
    !isset( $_SESSION['coap_hide'] ) //Se nella sessione l'utente ha deciso di non visualizzare più l'avviso
    &&
    (
        ( !$S->UserData && !isset($_COOKIE['in_coap']) ) //Se l'utente NON è loggato e non ha il cookie di approvazione
        ||
        ( isset($S->UserData) && $S->UserData['cookies_approved']==0 ) //Se l'utente è loggato e non ha ancora deciso sull'uso dei cookies
    )
){
?>
    <section id="CookiesPolicyAlert">
        <a href="#" class="Close" data-accept="1"><i class="glyphicon glyphicon-remove-circle"></i></a>
        <div class="container"><div class="col-md-12 col-sm-12 col-xs-12">
        	Questo sito utilizza i <a href="{{url cookies-policy}}">Cookies</a> per migliorare la tua esperienza di navigazione.&nbsp;&nbsp;<b>Continuando la navigazione e/o chiudendo questo avviso, confermi di aver letto l'informativa sull'utilizzo dei cookies</b>
            <?php if(false){ ?>&nbsp;&nbsp;&nbsp;
            <a href="#" class="choice" data-accept="1">Confermo</a><?php } ?>
        </div></div>
    </section>
<?php } ?>