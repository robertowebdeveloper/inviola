<div class="row"><div class="col-md-10 col-sm-10 col-xs-10 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
    <h2>
        <span class="Red"><?php
        if( $prevMatch["in_casa"] ){
            echo "Fiorentina - {$prevMatch['avversario']}";
        }else{
            echo "{$prevMatch['avversario']} - Fiorentina";
        }
        ?></span>
        <span class="IF_ora"><?=strftime("%d/%m/%Y %H:%M", strtotime($prevMatch["data"]) ); ?></span>
    </h2>
    <hr>       
</div><div class="col-md-1 col-sm-1 col-xs-1"></div></div>

<div class="row">
	<?php for($iCampo=0;$iCampo<2;$iCampo++){
		if( $iCampo==0 ){ //La tua formazione
			$field_formazione = "giocata";
			$title = "La tua formazione";
			$no_formazione = "Non hai partecipato a questa partita";
			$css_class = " Giocata";
		}else{ //Formazione vincente
			$title = "La formazione vincente";
			$field_formazione = "effettiva";
			$no_formazione = "La formazione titolare non &egrave; presente al momento";
			$css_class = " Effettiva";
		}
		?>
        <div class="col-md-6 col-sm-6 col-xs-12">
        	 <h3 class="IF_Title"><span><?=$title; ?></span></h3>
            <br>
            <div class="IndovinaFormazione_match<?=$css_class; ?>">
            	 <?php
				 if( $prevMatch["giocata"] && $prevMatch["effettiva"] && $iCampo==0 ){
					 for($i=1,$flag=true;$i<=11;$i++){
						 if( $prevMatch["giocata"]["id_giocatore{$i}"]!=$prevMatch["effettiva"]["id_giocatore{$i}"] ){
							$flag = false; 
						 }
					 }
					 //Controlla se hai vinto
					 if(
					 	$prevMatch["giocata"]["id_match"]==$prevMatch["effettiva"]["id_match"]
						&&
					 	$prevMatch["giocata"]["id_formazione"]==$prevMatch["effettiva"]["id_formazione"]
						&&
						$flag
					 ){//Vinto
						 ?><div class="StripeWinner Win"><span><# Congratulazioni hai vinto! #></span></div><?php
					 }else{ //Perso
						 ?><div class="StripeWinner Lose"><span><# Mi dispiace, per questa volta non hai indovinato.<br>Tenta nuovamente #></span></div><?php
					 }
				 }
				 ?>
                 
                <?php if( $prevMatch["id_banner"]>0 ){ echo $S->Banner( $prevMatch["id_banner"], "MainBanner" , false); ?>
                <?php }else{ ?><div class="MainBanner"><img src="{{theme}}img/indovina-formazione/banner-default.jpg" alt=""></div><?php } ?>
                
                <?php
                 //Calcolo la formazione
                 $index_formazione = 0;
                 for($i=0;$i<count($formazioni);$i++){
                    if($formazioni[$i]['id']==$prevMatch[ $field_formazione ]["id_formazione"]){
                        $index_formazione = $i;
                    }
                 }
                 ?>
                
                <div class="Campo f<?=$formazioni[ $index_formazione ]["formazione"]; ?>" data-formazione="<?=$formazioni[ $index_formazione ]["formazione"]; ?>">
                	<?php if($prevMatch[ $field_formazione ]){ ?>
                        <div class="BoxFormazione"><?=$formazioni[ $index_formazione ]["formazione"]; ?></div>
                        <div class="Player Portiere">
                           <?php
                            $item = $IF->getPlayer($prevMatch[ $field_formazione ]["id_giocatore1"]);
                            if($item["id_foto"]>0){
                                //$img = $S->Img($item["id_foto"], $S->_IMG_PARAMS["if_player"] );
                                $img = $S->pathFile($item["id_foto"]);
                            }else{
                                $img = "{$S->_path->theme}img/indovina-formazione/player.png";
                            }
                            $name = $IF->PlayerName($item);
                            ?>
                            <div>
                                <img src="<?=$img; ?>" alt="">
                                <span><?=$name; ?></span>
                            </div>
                       </div>
                       <?php for($i=2;$i<=11;$i++){
                            $item = $IF->getPlayer($prevMatch[ $field_formazione ]["id_giocatore{$i}"]);
                            if($item["id_foto"]>0){
                                //$img = $S->Img($item["id_foto"], $S->_IMG_PARAMS["if_player"] );
                                $img = $S->pathFile($item["id_foto"]);
                            }else{
                                $img = "{$S->_path->theme}img/indovina-formazione/player.png";
                            }
                            $name = $IF->PlayerName($item);
                           ?>
                        <div class="Player g<?=$i; ?>">
                            <div>
                                <img src="<?=$img; ?>" alt="">
                               <span><?=$name; ?></span>
                            </div>
                        </div>
                       <?php }
					}else{
					?>
                    	<div class="boxOverlay"><span><?=$no_formazione; ?></span></div>
                    <?php } ?>
                </div>
            </div>
        </div>
	<?php } ?>
</div>