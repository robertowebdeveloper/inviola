<div class="row"><div class="col-md-10 col-sm-10 col-xs-10 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
    <h2>
        <span class="Red"><?php
        if( $prevMatch["in_casa"] ){
            echo "Fiorentina - {$prevMatch['avversario']}";
        }else{
            echo "{$prevMatch['avversario']} - Fiorentina";
        }
        ?></span>
        <span class="IF_ora"><?=strftime("%d/%m/%Y %H:%M", strtotime($prevMatch["data"]) ); ?></span>
    </h2>
    <hr>       
</div><div class="col-md-1 col-sm-1 col-xs-1"></div></div>

<div class="row">
	<?php for($iCampo=0;$iCampo<2;$iCampo++){
		if( $iCampo==0 ){ //La tua formazione
            $field = 'giocata_im';
			$title = "Il tuo marcatore";
			$no_formazione = "Non hai partecipato a questa partita";
			$css_class = " Giocata";
		}else{ //Formazione vincente
			$title = "Il primo marcatore";
            $field = 'effettiva_im';
			$no_formazione = "Il primo marcatore ufficiale non &egrave; presente al momento";
			$css_class = " Effettiva";
		}
		?>
        <div class="col-md-6 col-sm-6 col-xs-12">
        	 <h3 class="IF_Title"><span><?=$title; ?></span></h3>
            <br>
            <div class="IndovinaPrimoMarcatore_match<?=$css_class; ?>">
            	 <?php
				 if( $prevMatch["giocata_im"] && $prevMatch["effettiva_im"] && $iCampo==0 ){
					 //Controlla se hai vinto
					 if(
					 	$prevMatch["giocata_im"]["id_match"]==$prevMatch["effettiva_im"]["id_match"]
						&&
					 	$prevMatch["giocata_im"]["id_giocatore"]==$prevMatch["effettiva_im"]["id_giocatore"]
					 ){//Vinto
						 ?><div class="StripeWinner Win"><span><# Congratulazioni hai vinto! #></span></div><?php
					 }else{ //Perso
						 ?><div class="StripeWinner Lose"><span><# Mi dispiace, per questa volta non hai indovinato.<br>Tenta nuovamente #></span></div><?php
					 }
				 }
                 ?>

                <?php if($prevMatch[$field]){
                    if($prevMatch[$field]["id_giocatore"]==0){ ?>
                        <div class="Marcatore NoMarcatore">
                            <img src="{{theme}}img/giochi/no-marcatore-btn.png" alt="">
                        </div>
                    <?php
                    }else{
                        $player = $IF->getPlayer($prevMatch[$field]["id_giocatore"]);
                        if ($player["id_foto"] > 0) {
                            $img = $S->pathFile($player["id_foto"]);
                        } else {
                            $img = $S->_path->theme . 'img/indovina-formazione/player.png';
                        }
                        $name = $player["nome"]." ".$player["cognome"];
                        ?>

                        <div class="Marcatore">
                            <img src="<?=$img; ?>"<?=$img_class; ?> alt="">
                            <span><?=$name; ?></span>
                        </div>

                        <?php
                    }
                    ?>
                <?php }else{
                ?>
                    <div class="boxOverlay"><span><?=$no_formazione; ?></span></div>
                <?php } ?>
            </div>
        </div>
	<?php } ?>
</div>