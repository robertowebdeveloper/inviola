<div class="row"><div class="col-md-10 col-sm-10 col-xs-10 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
    <h2>
        <span class="Red"><?php
        if( $prevMatch["in_casa"] ){
            echo "Fiorentina - {$prevMatch['avversario']}";
        }else{
            echo "{$prevMatch['avversario']} - Fiorentina";
        }
        ?></span>
        <span class="IF_ora"><?=strftime("%d/%m/%Y %H:%M", strtotime($prevMatch["data"]) ); ?></span>
    </h2>
    <hr>       
</div><div class="col-md-1 col-sm-1 col-xs-1"></div></div>

<div class="row">
	<?php
    $logo_avversario = $S->pathFile($prevMatch['id_logo_avversario']);
    for($iCampo=0;$iCampo<2;$iCampo++){
		if( $iCampo==0 ){ //La tua formazione
            $field = 'giocata_ir';
			$title = "Il tuo risultato";
			$no_formazione = "Non hai partecipato a questa partita";
			$css_class = " Giocata";
		}else{ //Formazione vincente
			$title = "Il risultato effettivo";
            $field = 'effettiva_ir';
			$no_formazione = "Il risultato ufficiale non &egrave; presente al momento";
			$css_class = " Effettiva";
		}
		?>
        <div class="col-md-6 col-sm-6 col-xs-12">
        	 <h3 class="IF_Title"><span><?=$title; ?></span></h3>
            <br>
            <div class="IndovinaRisultato_match<?=$css_class; ?>">
            	 <?php
				 if( $prevMatch[$field] && $prevMatch["effettiva_ir"] && $iCampo==0 ){
					 //Controlla se hai vinto
					 if(
					 	$prevMatch[$field]["id_match"]==$prevMatch["effettiva_ir"]["id_match"]
						&&
					 	$prevMatch[$field]["goal_fiorentina"]==$prevMatch["effettiva_ir"]["goal_fiorentina"]
						&&
                        $prevMatch[$field]["goal_avversario"]==$prevMatch["effettiva_ir"]["goal_avversario"]
					 ){//Vinto
						 ?><div class="StripeWinner Win"><span><# Congratulazioni hai vinto! #></span></div><?php
					 }else{ //Perso
						 ?><div class="StripeWinner Lose"><span><# Mi dispiace, per questa volta non hai indovinato.<br>Tenta nuovamente #></span></div><?php
					 }
				 }
				 ?>

                <?php if($prevMatch[$field]){ ?>
                    <div class="row">
                        <?php $arr = array(1,0);
                        foreach($arr as $in_casa){ ?>
                            <div class="col-md-6 col-sm-6 col-xs-6"><div class="boxSquadra">
                                <?php if($prevMatch['in_casa']==$in_casa){ ?>
                                    <img src="{{theme}}img/giochi/logo-fiorentina.png" alt="">
                                    <br>
                                    <h3><?=$prevMatch[$field]['goal_fiorentina']; ?><!--Fiorentina--></h3>
                                <?php }else{ ?>
                                    <img src="<?=$logo_avversario; ?>" alt="">
                                    <br>
                                    <h3><?=$prevMatch[$field]['goal_avversario']; ?><!--<?=$prevMatch['avversario']; ?>--></h3>
                                <?php } ?>
                            </div></div>
                        <?php } ?>
                    </div>
                <?php }else{
                ?>
                    <div class="boxOverlay"><span><?=$no_formazione; ?></span></div>
                <?php } ?>
            </div>
        </div>
	<?php } ?>
</div>