<?php if( $S->_isLogged ){ ?>
	<form id="logoutForm" enctype="application/x-www-form-urlencoded" method="post" action="{{url home}}">
    	<input type="hidden" name="logout" value="1">
    </form>
	<nav id="menuAreaRiservata"><ul>
       <li class="login"><a href="{{url la-mia-card}}"><i></i>
        	<?=strtolower($_SESSION["gender"])=="f" ? 'Benvenuta' : 'Benvenuto'; ?> <?=$_SESSION["name"]; ?>
        </a></li>
        <li class="logout"><a href="#" onclick="System.Logout();"><i></i></a></li>
    </ul></nav>
<?php }else{ ?>
	<nav id="menuAreaRiservata"><ul>
    	<li class="registrati"><a href="{{url hai-una-card}}"><i></i><# Registrati #></a></li
        ><li class="login">
        	<a href="#"><i></i><# Login #></a>
            <div class="submenu">
            	<?php
				if( isset($_SESSION["redirect_post_login"]) ){
					$url = $_SESSION["redirect_post_login"];	
				}else{
					$url = $S->Url('home');	
				}
				?>
            	<form id="loginForm" enctype="application/x-www-form-urlencoded" method="post" action="<?=$url; ?>"><!-- action="{{url la-mia-card}}" -->
                	<input type="hidden" name="az" value="login">
                   <input type="hidden" id="loginData" name="loginData">
                	<label><# E-mail #>
                   	<input type="text" name="login" class="inputText">
                   </label>
                   <label><# Password #>
                           <input type="password" name="pw" class="inputText">
                   </label>

                    <?php
                    //Modificare se si vuole riabilitare il sì/no sull'accettazione dei cookies
                    //$in_coap = isset($_COOKIE['in_coap']) ? $_COOKIE['in_coap'] : 0;
                    $in_coap = 2;
                    if($in_coap!=2){
                        $onchange = $in_coap==0 ? " onchange=\"System.RestaConnessoAlert('Per utilizzare questa funzione &egrave; necessario approvare l\'uso dei cookies');\"" : '';
                        $val = $in_coap==0 ? 0 : 1;
                        ?>
                       <div class="restaConnesso">
                           <label>
                                <input type="checkbox" name="resta_connesso" value="<?=$val; ?>"<?=$onchange; ?>>
                                <# Resta connesso #>
                            </label>
                       </div>
                    <?php } ?>
                   <div class="bottom"><div class="row">
                   	<div class="col-md-8 col-sm-8 col-xs-12"><a href="{{url recupera-password}}"><# Hai dimenticato la password? #></a></div>
                    	<div class="col-md-4 col-sm-4 col-xs-12 right">
                        	<img id="loginLoader" src="{{theme}}img/loaders/2.gif" alt="" class="hide">
                        	<a id="loginEntra" href="#" class="miniBtn" onclick="System.Login('login');"><# Entra #></a>
                       </div>
                   </div></div>
               </form>
            </div>
        </li>
    </ul></nav>
<?php } ?>