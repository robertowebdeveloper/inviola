<?php
$avatar_upload = false;
if( isset($_FILES["avatar"]) && strlen($_FILES["avatar"]["tmp_name"])>0 ){
	$avatar_upload = true;
	
	$info = getimagesize($_FILES["avatar"]["tmp_name"]);
	if( $info[2]==1 || $info[2]==2 || $info[2]==3 ){ //1=>GIF, 2=>JPG, 3=>PNG
		$tmp = explode(".",$_FILES["avatar"]["name"]);
		$ext = strtolower( array_pop($tmp) );
		
		$q = "INSERT INTO `{$S->_db_prefix}file` (`name`,`size`,`type`,`ext`,`temp`) VALUES ('{$_FILES[avatar][name]}',{$_FILES[avatar][size]},'{$_FILES[avatar][type]}','{$ext}',1)";
		$S->cn->Q($q);
		$id_file = $S->cn->last_id;
		$name = "{$id_file}.{$ext}";
		
		$subdir = substr($name,0,1);
		$pathdir = "_public/file/{$subdir}";
		$pathdir_here = "{$pathdir}";
		if( !file_exists($pathdir_here) ){
			@mkdir( $pathdir_here );	
		}
		
		if( !move_uploaded_file($_FILES["avatar"]["tmp_name"], "{$pathdir_here}/{$name}" ) ){
			$q = "DELETE * FROM `{$S->_db_prefix}file` WHERE id = {$id_file}";
			$S->cn->Q($q);
			$avatar_upload_status = false;
			$avatar_msg = "Errore nel caricamento dell'immagine";
		}else{
			$q = "UPDATE utenti SET id_avatar = {$id_file} WHERE customer_id = {$_SESSION[customer_id]}";
			$S->cn->Q($q);
			$avatar_upload_status = true;
		}	
	}else{
		$avatar_upload_status = false;
		$avatar_msg = "Formato immagine non valido";
	}
}else if( isset($_POST["eliminaAvatar"]) ){
	/*$q = "SELECT id_avatar FROM utenti WHERE customer_id = {$_SESSION[customer_id]}";
	$id_avatar = $S->cn->OF($q);
	$S->DeleteFile($id_avatar);*/
	$q = "UPDATE utenti SET id_avatar = NULL WHERE customer_id = {$_SESSION[customer_id]}";
	$S->cn->Q($q);
}
?>
<script type="text/javascript">
var Modifica = function(show){
	if( show ){
		$(".modNo").hide();
		$(".modSi").show();
		$("input[name='privacy1']").removeAttr('disabled');
		$("input[name='privacy2']").removeAttr('disabled');
	}else{
		$(".modNo").show();
		$(".modSi").hide();
		$("input[name='privacy1']").attr('disabled',true);
		$("input[name='privacy2']").attr('disabled',true);
	}
};
</script>
<div class="bgWhite">
	<?php //print_r($S->Page); ?>
    <div class="row"><div class="col-md-10 col-sm-10 col-xs-10 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
        <h1><?=$S->Page["name"]; ?></h1>

        <div class="row">
            <div class="col-md-8 col-sm-8 col-xs-12">
                <form id="avatarForm" enctype="multipart/form-data" method="post" action="{{url il-mio-profilo}}">
                    <div><div class="relative">
                            <?php $avatar = $S->getAvatar(true);
                            if($avatar){ ?>
                                <div id="avatarCont">
                                    <img src="<?=$avatar; ?>" alt="Avatar" style="width: 96px; height: 96px; border:1px solid #999;">
                                    <# Il tuo Avatar #>&nbsp;&nbsp;&nbsp;
                                        <br>
                                        <a href="#" onclick="System.deleteAvatar();"><# Elimina Avatar #></a>
                                </div>
                                <input type="hidden" name="eliminaAvatar" value="1">
                            <?php }else{ ?>
                                <div>
                                    <img src="{{theme}}img/avatar-big.png" width="96" alt="" class="pull-left" style="margin-right: 8px;">
                                    <div class="relative">
                                        <br>
                                        <b class="font13"><# Inserisci il tuo Avatar #></b>
                                        <img src="{{theme}}img/icons/add.png" alt="" style="width: 16px; margin: -2px 0 0 8px;">
                                        <input type="file" name="avatar" id="avatarInput" onchange="$('#avatarForm').submit();">
                                    </div>
                                    <?php if(0){ ?><span class="Red"><# Guadagna 100 punti sulla tua card #></span><?php } ?><br>
                                </div>
                                <br class="clearfix">
                                <span style="font-size:13px;"><# (Formato JPG,PNG,GIF) #></span>
                                <?php if($avatar_upload && !$avatar_upload_status){ ?><div class="Red"><?=$avatar_msg; ?></div><?php } ?>
                                <br><br>
                            <?php } ?>
                        </div></div>
                </form>
            </div>
            <div class="col-md-4 co-sm-4 col-xs-12"><div class="text-right">
                    <br><br>
                    <a href="#" onclick="System.openCambiaPW();" class="Button"><# Cambia Password #></a>
            </div></div>
        </div>

        <div class="clearfix"></div>
        <form id="passwordForm" enctype="application/x-www-form-urlencoded" method="post" action="#" role="form" class="form-inline">
            <input type="hidden" name="customer_id" value="<?=$_SESSION["customer_id"]; ?>">
            <input type="hidden" name="az" value="fnet">
            <input type="hidden" name="sub_az" value="modifyPw">
            <div id="passwordChange">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <label><# Password attuale #></label><br>
                        <input type="password" name="old_pw" class="form-control" autocomplete="off">
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <label><# Nuova password #></label><br>
                        <input type="password" name="new_pw" class="form-control" autocomplete="off">
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <label><# Conferma password #></label><br>
                        <input type="password" name="new_pw2" class="form-control" autocomplete="off">
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <br>
                        <input type="button" class="Button" value="<# Modifica #>" onclick="System.cambiaPW();">
                        <img id="pwLoader" class="hide" src="{{theme}}img/loaders/3.gif" alt="">
                    </div>
                </div>
                <div class="row"><div class="col-md-12 col-sm-12 col-xs-12"><div id="pwMsg" class="center"><span class="Title Red"></span></div></div></div>
            </div>
        </form>

        
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
               <form id="profiloForm" enctype="application/x-www-form-urlencoded" method="post" action="{{url la-mia-card}}">
               	<input type="hidden" name="az" value="fnet">
               	<input type="hidden" name="sub_az" value="updateProfile">
                	<input type="hidden" name="customer_id" value="<?=$S->_customer_id; ?>">
                   <input type="hidden" name="dataNascita">
                   <input type="hidden" id="url-reload" name="url" value="{{url il-mio-profilo}}">
                   <div class="tableCard"><table class="table table-bordered">
                   	<?php
						$postdata = array("az"=>'infocardByCustomer',"customer_id"=>$_SESSION["customer_id"]);
						$user = $S->FNET($postdata);
						$user = json_decode($user,false);
						$profilo = $user->data;
						//echo "<!--<pre>";print_r($profilo);echo "-->";
						?>
                   	<tr>
                    		<td class="profiloTable_left"><span data-error="name"><# Nome #></span></td>
                          <td>
						  		<div class="row"><div class="col-md-12 col-sm-12 col-xs-12"><span class="modNo"><?=$profilo->personalInfo->name; ?></span></div></div>
                              <div class="row"><div class="col-md-8 col-sm-8 col-xs-12"><span class="modSi"><input type="text" name="name" value="<?=$profilo->personalInfo->name; ?>" class="form-control"></span></div></div>
                          </td>
                    	</tr>
                       <tr>
                    		<td class="profiloTable_left"><span data-error="surname"><# Cognome #></span></td>
                          <td>
						  		<div class="row"><div class="col-md-12 col-sm-12 col-xs-12"><span class="modNo"><?=$profilo->personalInfo->surname; ?></span></div></div>
                              <div class="row"><div class="col-md-8 col-sm-8 col-xs-12"><span class="modSi"><input type="text" name="surname" value="<?=$profilo->personalInfo->surname; ?>" class="form-control"></span></div></div>
                          </td>
                    	</tr>
                       <tr>
                    		<td class="profiloTable_left"><span data-error="dataNascita"><# Data di nascita #></span></td>
                          <td>
						  		<div class="row"><div class="col-md-12 col-sm-12 col-xs-12"><span class="modNo"><?=$profilo->personalInfo->dataNascita_label; ?></span></div></div>
                              <div class="row"><div class="col-md-8 col-sm-8 col-xs-12"><span class="modSi">
                              	<?php
									list($d,$m,$y) =  explode("/",$profilo->personalInfo->dataNascita);
									?>
                              	<select name="birthday_d" class="form-control" style="width:auto; display:inline-block;">
                                		<?php
                                     for($i=1;$i<=31;$i++){
											$sel = $i==$d ? ' selected' : '';
											?><option value="<?=$i; ?>"<?=$sel; ?>><?=$i; ?></option><?php
										}
										?>      
                                	</select>
                                  /
                              	<select name="birthday_m" class="form-control" style="width:auto; display:inline-block;">
                                		<?php
                                     for($i=1;$i<=12;$i++){
											$sel = $i==$m ? ' selected' : '';
											?><option value="<?=$i; ?>"<?=$sel; ?>><?=$i; ?></option><?php
										}
										?>      
                                	</select>
                                  /
                              	<select name="birthday_y" class="form-control" style="width:auto; display:inline-block;">
                                		<?php
                                     for($i=1900;$i<=date("Y");$i++){
											$sel = $i==$y ? ' selected' : '';
											?><option value="<?=$i; ?>"<?=$sel; ?>><?=$i; ?></option><?php
										}
										?>      
                                	</select>
                              </span></div></div>
							</td>
                    	</tr>
                       <tr>
                    		<td class="profiloTable_left"><span data-error="address"><# Indirizzo #></span></td>
                          <td>
						  		<div class="row"><div class="col-md-12 col-sm-12 col-xs-12"><span class="modNo"><?=$profilo->personalInfo->address; ?></span></div></div>
                              <div class="row"><div class="col-md-8 col-sm-8 col-xs-12"><span class="modSi"><input type="text" name="address" value="<?=$profilo->personalInfo->address; ?>" class="form-control"></span></div></div>
                          </td>
                    	</tr>
                       <tr>
                    		<td class="profiloTable_left"><span data-error="addressNumber"><# N. Civico #></span></td>
                          <td>
						  		<div class="row"><div class="col-md-12 col-sm-12 col-xs-12"><span class="modNo"><?=$profilo->personalInfo->addressNumber; ?></span></div></div>
                              <div class="row"><div class="col-md-2 col-sm-4 col-xs-12"><span class="modSi"><input type="text" name="addressNumber" value="<?=$profilo->personalInfo->addressNumber; ?>" class="form-control"></span></div></div>
                          </td>
                    	</tr>
                       <tr>
                    		<td class="profiloTable_left"><span data-error="zip"><# CAP #></span></td>
                          <td>
                              <div class="row"><div class="col-md-12 col-sm-12 col-xs-12"><span class="modNo"><?=$profilo->personalInfo->zip; ?></span></div></div>
                              <div class="row"><div class="col-md-2 col-sm-4 col-xs-12"><span class="modSi"><input type="text" name="zip" value="<?=$profilo->personalInfo->zip; ?>" class="form-control onlyNumber"></span></div></div>
                          </td>
                    	</tr>
                       <tr>
                       	<td class="profiloTable_left"><# Residenza #></td>
                        	<td>
                            	<div class="row"><div class="col-md-12 col-sm-12 col-xs-12"><span class="modNo"><?php
								if( $profilo->personalInfo->residente=='italia' ){
									echo "Italia";
								}else{
									echo "Estero";
								}
								?></span></div></div>
                              <div class="row"><div class="col-md-8 col-sm-8 col-xs-12"><span class="modSi">
                              	<select name="residente" onchange="System.RegResidenza('profiloForm');">
                                		<?php
										$arr = array("italia"=>"Italia","estero"=>"All&prime;estero");
										foreach($arr as $k=>$v){
											$sel = $k==$profilo->personalInfo->residente ? ' selected' : '';
											?><option value="<?=$k; ?>"<?=$sel; ?>><# <?=$v; ?> #></option><?php
										}
										?>
                                	</select>
                              </span></div></div>
                           </td>
                       </tr>
                       <tr class="ResEsteroFields<?=$profilo->personalInfo->residente=='italia' ? ' hide' : ''; ?>">
                       	<td class="profiloTable_left"><span data-error="citta_estero"><# Citt&agrave; #></span></td>
                        	<td>
                              <div class="row"><div class="col-md-12 col-sm-12 col-xs-12"><span class="modNo"><?=$profilo->personalInfo->cittaEstero; ?></span></div></div>
                              <div class="row"><div class="col-md-8 col-sm-8 col-xs-12"><span class="modSi">
                                	<input type="text" name="citta_estero" class="form-control" value="<?=$profilo->personalInfo->cittaEstero; ?>">
                              </span></div></div>
                          </td>
                       </tr>
                       <tr class="ResItaliaFields<?=$profilo->personalInfo->residente=='italia' ? '' : ' hide'; ?>">
                    		<td class="profiloTable_left"><span data-error="regione"><# Regione #></span></td>
                          <td>
                              <div class="row"><div class="col-md-12 col-sm-12 col-xs-12"><span class="modNo"><?=$S->geoLevelName( $profilo->personalInfo->geoLevel1 , 1 ); ?></span></div></div>
                              <div class="row"><div class="col-md-8 col-sm-8 col-xs-12"><span class="modSi">
                                	<select name="geoLevel1" class="form-control" style="width:auto;" onchange="setGeoLevel(2,'profiloForm','geoLevel');">
										<option value="0">-</option>
										<?php
										$q = "SELECT * FROM `{$S->_db_prefix}geolevels` WHERE level=1 ORDER BY name ASC";
										$list = $S->cn->Q($q,true);
										foreach($list as $v){
											$sel = $v["id_ext"]==$profilo->personalInfo->geoLevel1 ? ' selected' : '';
											?><option value="<?=$v["id_ext"]; ?>"<?=$sel; ?>><?=$v["name"]; ?></option><?php
										}
									?></select>
                              </span></div></div>
                          </td>
                    	</tr>
                       <tr class="ResItaliaFields<?=$profilo->personalInfo->residente=='italia' ? '' : ' hide'; ?>">
                    		<td class="profiloTable_left"><span data-error="provincia"><# Provincia #></span></td>
                          <td>
                          		<div class="row"><div class="col-md-12 col-sm-12 col-xs-12"><span class="modNo"><?=$S->geoLevelName( $profilo->personalInfo->geoLevel2 , 2 ); ?></span></div></div>
                              <div class="row"><div class="col-md-8 col-sm-8 col-xs-12"><span class="modSi">
                                	<select name="geoLevel2" class="form-control" style="width:auto;" onchange="setGeoLevel(3,'profiloForm','geoLevel');">
										<option value="0">-</option>
										<?php
										$q = "SELECT * FROM `{$S->_db_prefix}geolevels` WHERE level=2 AND fatherId = {$profilo->personalInfo->geoLevel1} ORDER BY name ASC";
										$list = $S->cn->Q($q,true);
										foreach($list as $v){
											$sel = $v["id_ext"]==$profilo->personalInfo->geoLevel2 ? ' selected' : '';
											?><option value="<?=$v["id_ext"]; ?>"<?=$sel; ?>><?=$v["name"]; ?></option><?php
										}
									?></select>
                              </span></div></div>
                          </td>
                       <tr class="ResItaliaFields<?=$profilo->personalInfo->residente=='italia' ? '' : ' hide'; ?>">
                    		<td class="profiloTable_left"><span data-error="comune"><# Comune #></span></td>
                          <td>
                          		<div class="row"><div class="col-md-12 col-sm-12 col-xs-12"><span class="modNo"><?=$S->geoLevelName( $profilo->personalInfo->geoLevel3 , 3 ); ?></span></div></div>
                              <div class="row"><div class="col-md-8 col-sm-8 col-xs-12"><span class="modSi">
                                	<select name="geoLevel3" class="form-control" style="width:auto;" onchange="setGeoLevel(4,'profiloForm','geoLevel');">
										<option value="0">-</option>
										<?php
										$q = "SELECT * FROM `{$S->_db_prefix}geolevels` WHERE level=3 AND fatherId = {$profilo->personalInfo->geoLevel2} ORDER BY name ASC";
										$list = $S->cn->Q($q,true);
										foreach($list as $v){
											$sel = $v["id_ext"]==$profilo->personalInfo->geoLevel3 ? ' selected' : '';
											?><option value="<?=$v["id_ext"]; ?>"<?=$sel; ?>><?=$v["name"]; ?></option><?php
										}
									?></select>
                              </span></div></div>
                          </td>
                    	</tr>
                       <tr class="ResItaliaFields<?=$profilo->personalInfo->residente=='italia' ? '' : ' hide'; ?>">
                    		<td class="profiloTable_left"><span data-error="localita"><# Località #></span></td>
                          <td>
                              <div class="row"><div class="col-md-12 col-sm-12 col-xs-12"><span class="modNo"><?=$profilo->personalInfo->geoLevel4>0 ? $S->geoLevelName( $profilo->personalInfo->geoLevel4 , 4 ) : '-'; ?></span></div></div>
                              <div class="row"><div class="col-md-8 col-sm-8 col-xs-12"><span class="modSi">
                                	<select name="geoLevel4" class="form-control" style="width:auto;">
										<option value="0">-</option>
										<?php
										$q = "SELECT * FROM `{$S->_db_prefix}geolevels` WHERE level=4 AND fatherId = {$profilo->personalInfo->geoLevel3} ORDER BY name ASC";
										$list = $S->cn->Q($q,true);
										foreach($list as $v){
											$sel = $v["id_ext"]==$profilo->personalInfo->geoLevel3 ? ' selected' : '';
											?><option value="<?=$v["id_ext"]; ?>"<?=$sel; ?>><?=$v["name"]; ?></option><?php
										}
									?></select>
                              </span></div></div>
                          </td>
                    	</tr>
                       <tr>
                    		<td class="profiloTable_left"><span data-error="gender"><# Sesso #></span></td>
                          <td>
                              <div class="row"><div class="col-md-12 col-sm-12 col-xs-12"><span class="modNo"><?=strtolower($profilo->personalInfo->gender)=="f" || strtolower($profilo->personalInfo->gender)=="m" ? $profilo->personalInfo->gender : '-'; ?></span></div></div>
                              <div class="row"><div class="col-md-8 col-sm-8 col-xs-12"><span class="modSi">
                              	<select name="gender">
                                		<?php $arr = array("M","F"); 
										foreach($arr as $g){
											$sel = $g==strtoupper($profilo->personalInfo->gender) ? ' selected' : '';
											?><option value="<?=$g; ?>"<?=$sel; ?>><?=$g; ?></option><?php
										}
										?>
                                	</select>
                              </span></div></div>
                          </td>
                    	</tr>
                       <tr>
                    		<td class="profiloTable_left"><span data-error="luogoDiNascita"><# Luogo di Nascita #></span></td>
                          <td>
<div class="row"><div class="col-md-12 col-sm-12 col-xs-12"><span class="modNo"><?=$profilo->personalInfo->luogoDiNascita; ?></span></div></div>
                              <div class="row"><div class="col-md-8 col-sm-8 col-xs-12"><span class="modSi"><input type="text" name="luogoDiNascita" value="<?=$profilo->personalInfo->luogoDiNascita; ?>" class="form-control"></span></div></div>
                          </td>
                    	</tr>
                       <tr>
                    		<td class="profiloTable_left"><span data-error="identityCard"><# Codice Fiscale #></span></td>
                          <td>
                              <div class="row"><div class="col-md-12 col-sm-12 col-xs-12"><span class="modNo"><?=$profilo->personalInfo->identityCard; ?></span></div></div>
                              <div class="row"><div class="col-md-8 col-sm-8 col-xs-12"><span class="modSi"><input type="text" name="identityCard" value="<?=$profilo->personalInfo->identityCard; ?>" class="form-control"></span></div></div>
                          </td>
                    	</tr>
                       <tr>
                    		<td class="profiloTable_left"><span data-error="idProfessione"><# Professione #></span></td>
                          <td>
                              <div class="row"><div class="col-md-12 col-sm-12 col-xs-12"><span class="modNo"><?=$profilo->personalInfo->professione_label; ?></span></div></div>
                              <div class="row"><div class="col-md-8 col-sm-8 col-xs-12"><span class="modSi">
                              	<select name="idProfessione">
                                		<option value="0">-</option>
                                      <?php
									  	include("_ext/scripts/list_professioni.inc.php");
										foreach($list as $k=>$v){
											$sel = $k==$profilo->personalInfo->idProfessione ? ' selected' : '';
											?><option value="<?=$k; ?>"<?=$sel; ?>><?=$v; ?></option><?php	
										}
									  	?>
                                	</select>
                              </span></div></div>
                          </td>
                    	</tr>
                       <tr>
                    		<td class="profiloTable_left"><span data-error="mailContactData"><# E-Mail #></span></td>
                          <td>
                          		<div class="row"><div class="col-md-12 col-sm-12 col-xs-12"><span class="modNo"><?=$profilo->personalInfo->mailContactData; ?></span></div></div>
                              <div class="row"><div class="col-md-8 col-sm-8 col-xs-12"><span class="modSi"><input type="text" name="mailContactData_pre" value="<?=$profilo->personalInfo->mailContactData; ?>" class="form-control" onfocus="System.changeProfileMail();"></span></div></div>
                              <div style="font-size: 11px;"><# L'indirizzo e-mail sarà l'user di accesso del sito #></div>
                              <input type="hidden" name="mailContactData" value="<?=$profilo->personalInfo->mailContactData; ?>">
                          </td>
                    	</tr>
                       <tr>
                    		<td class="profiloTable_left"><span data-error="mobileContactData"><# Cellulare #></span></td>
                          <td>
                              <div class="row"><div class="col-md-12 col-sm-12 col-xs-12"><span class="modNo"><?=$profilo->personalInfo->mobileContactData; ?></span></div></div>
                              <div class="row"><div class="col-md-8 col-sm-8 col-xs-12"><span class="modSi"><input type="text" name="mobileContactData_pre" value="<?=$profilo->personalInfo->mobileContactData; ?>" class="form-control onlyNumber" onfocus="System.changeProfileMobile();"></span></div></div>
                              <input type="hidden" name="mobileContactData" value="<?=$profilo->personalInfo->mobileContactData; ?>">
                          </td>
                    	</tr>
                       <tfoot>
                       	<tr>
                        		<td colspan="6"><span style="font-size: 12px;">
                                <br>
                                <input type="checkbox" name="privacy1" value="1" disabled <?=$profilo->personalInfo->privacy->usedForPromotions==1 ? ' checked' : ''; ?>>
                                <span data-error="privacy1"><!--# Consenso dell&prime;interessato per il trattamento dei dati al fine di informazione e promozione commerciale #--><# Acconsento al trattamento dei miei dati ai fini di comunicazioni commerciali #></span>
                                <br><br>
                                <input type="checkbox" name="privacy2" value="1" disabled <?=$profilo->personalInfo->privacy->usedForStatistics==1 ? ' checked' : ''; ?>>
                                <span data-error="privacy2"><!--# Consenso dell&prime;interessato per il trattamento dei dati degli effetti dell&prime;investigazione di mercato, analisi economiche e statistiche #--><# Acconsento al trattamento dei miei dati ai fini di profilazione #></span>
                                
                                <br><br>
                                <input type="hidden" name="privacy3" value="1">
                                <input type="checkbox" name="privacy3check" value="1" disabled checked>
                                <span data-error="privacy3"><# Ho letto e ben compreso quanto riportato a tergo del presente modello che ho compilato prestando attenzione a tutte le sue parti, accetto quanto riportato sul Regolamento InViola Card sul regolamento dell&rsquo;Operazione a premi "InViola Fidelity" a cui aderisco, inoltre, come previsto dall&rsquo;art. 26 comma 1 del D. Lgs. 196/2003, esprimo il consenso ai trattamenti dei dati sensibili eventualmente acquisiti nell&rsquo;ambito delle operazioni a premi. #></span>
                                
                                <br><br>
                                
                                <div class="modNo"><input type="button" class="Button" value="<# Modifica #>" onclick="Modifica(true);"></div>
                                <div class="modSi">
                                		<input type="button" class="Button Gray" value="<# Annulla #>" onclick="Modifica(false);">
                                      <input type="button" class="Button" value="<# Salva #>" onclick="System.salvaProfilo();">
                                      <img id="saveLoader" class="hide" src="{{theme}}img/loaders/3.gif" alt="">
                                </div>
                              </span></td>
                        	</tr>
                       </tfoot>
                   </table></div>
               </form>

               <div class="clearfix"></div>
               <br>

            </div>
        </div>
        
    </div><div class="col-md-1 col-sm-1 col-xs-1"></div></div>
	<br /><br />
</div>