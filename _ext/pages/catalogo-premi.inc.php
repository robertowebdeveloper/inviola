<?php
if( isset($_GET["id"]) && $_GET["id"]>0 && isset($_GET["wl"]) ){
	if($_GET["wl"]>0){
		$az = 'wishlist_add';
	}else if($_GET["wl"]<0){
		$az = 'wishlist_remove';
	}else{
		$az = false;
	}
	if( $az ){
		$postdata = array("az"=>$az,"customer_id"=>$_SESSION["customer_id"],"prize_id"=>$_GET["id"]);
		$S->FNET($postdata);
	}
}

/*
$q = "SELECT `data` FROM `{$S->_db_prefix}fidely_cache` WHERE page='elenco-premi' LIMIT 1";
$premi = $S->cn->OF($q);
$premi = json_decode($premi,false);
$premi = $premi->data;*/

if( isset($_SESSION["customer_id"]) && strlen($_SESSION["customer_id"])>0 ){
	$postdata = array("az"=>'premi_elenco','customer_id'=>$_SESSION["customer_id"]);
	$cache_on = false;
}else{
	$postdata = array("az"=>'premi_elenco','customer_id'=>$S->P('service-card-customer-id') );
	$cache_on = true;
}

//CACHE
$cache_file = "_public/cache/prizes-list.dat";
$cache_time = filemtime($cache_file);
if( $cache_time+(24*60*60) > time() && $cache_on){
	$premi = file_get_contents($cache_file);
}else{
	$premi = $S->FNET($postdata);
	$fp = @fopen( $cache_file,"w");
	fwrite($fp,$premi);
	fclose($fp);
}

$premi = json_decode( $premi );
$premi = $premi->data;
//echo "<!-- <pre>";print_r($premi);echo "-->";

?>
<div class="bgWhite"><div class="row"><div class="col-md-1 col-sm-1 col-xs-1"></div><div class="col-md-10 col-sm-10 col-xs-10">
	<h1>Catalogo Premi</h1>
    
    <form id="catalogoPremi" enctype="application/x-www-form-urlencoded" method="get" action="{{url catalogo-premi}}" class="bgWhite form-horizontal" role="form">
    	<input type="hidden" name="s" value="1">
    	<div class="Title2">Cerca il premio che preferisci</div>
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
                <select name="cat" class="form-control">
                    <option value="0">TUTTE</option>
                    <?php
					 $q = "SELECT * FROM `premi_categorie` WHERE 1 ORDER BY id ASC";
					 $list = $S->cn->Q($q,true);
                    //$arr = array(1=>"PREMI EMOZIONALI",2=>"GARA",3=>"ALLENAMENTO",5=>"STADIO",4=>"TEMPO LIBERO",8=>"HI TECH",7=>"UFFICIO &amp; SCUOLA",6=>"CASA",9=>"KIDS",10=>"VAUCHERS");
                    foreach($list as $row){
						$sel = $row["category_id"]==$_GET["cat"] ? ' selected' : '';
                        ?><option value="<?=$row["category_id"]; ?>"<?=$sel; ?>><?=$row["name"]; ?></option><?php
                    }
                    ?>
                </select>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-6">
                <select name="punti_da" class="form-control">
                    <option value="">DA PUNTI</option>
                    <?php $arr = array(0,100,500,1000,2000,5000,10000,20000,50000,75000,100000);
                    foreach( $arr as $x){
                        $sel = $x==$_GET["punti_da"] && isset($_GET["punti_da"]) && strlen($_GET["punti_da"])>0 ? ' selected' : '';
                        ?>
                        <option value="<?=$x; ?>"<?=$sel; ?>><?=$x; ?></option><?php
                    }
                    ?>
                </select>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-6">
                <select name="punti_a" class="form-control">
                    <option value="">A PUNTI</option>
                    <?php $arr = array(100,500,1000,2000,5000,10000,20000,50000,75000,100000);
                    foreach( $arr as $x){
                        $sel = $x==$_GET["punti_a"] && isset($_GET["punti_a"]) && strlen($_GET["punti_a"])>0 ? ' selected' : '';
                        ?>
                        <option value="<?=$x; ?>"<?=$sel; ?>><?=$x; ?></option><?php
                    }
                    ?>
                </select>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
            	<input type="submit" value="CERCA" class="Button" />
            </div>
        </div>
    </form>
    <br>
</div><div class="col-md-1 col-sm-1 col-xs-1"></div></div></div>

<?php if($_GET["id"]>0 ){ //singolo premio
	foreach($premi as $x){
		if($x->id==$_GET["id"]){
			$item = $x;	
		}
	}

	echo $S->FormRichiestaPremio($item->id,$item->prizeCode);
	?>
	<div class="Box3"><div class="Box3bg"><div class="row">
    	<div class="col-md-1 col-sm-1 hidden-xs"></div>
        <div class="col-md-5 col-sm-4 col-xs-12">
        	<div class="premioImgBig">
            	<img src="<?=$item->image; ?>" class="img-responsive" style="margin:0 auto;" />
                <div class="Shadow"></div>
            </div>
        </div>
        <div class="col-md-5 col-sm-5 col-xs-12"><div class="premioBig">
        	<h2><?=$item->name; ?></h2>
            <div class="descr"><?=$item->description; ?></div>
            <br><br>
            <div class="infoPunti">
            	<?php if( isset($_SESSION["customer_id"]) ){ ?>
                    <div class="wishlist_bar">
                       <?php if($item->in_wishlist){ ?>
                            <a href="<?=$S->getUrl('catalogo-premi',$item->id); ?>&wl=-1"><img src="{{theme}}img/buttons/wishlist_added.png" alt=""></a>
                       <?php }else{ ?>
                            <a href="<?=$S->getUrl('catalogo-premi',$item->id); ?>&wl=1"><img src="{{theme}}img/buttons/wishlist_add.png" alt=""></a>
                       <?php } ?>
                   </div>
               <?php } ?>
                   
            	<?php if( isset($_SESSION["customer_id"]) && $item->saldo < $item->points ){
					$mancano = $item->points - $item->saldo;
					$perc = $item->saldo*100/$item->points;
					$perc = round($perc);
					$perc = $perc>100 ? 100 : $perc;
					?>
                	<div class="hr"></div>
                   <div class="PuntiMancanti">Per questo premio occorrono <span class="Red"><?=$item->points; ?> punti</span> &middot; ti mancano <span class="Red"><?=$mancano; ?> punti</span></div>
                   <div class="BarPunti"><div style="width: <?=$perc; ?>%;<?=$item->saldo==0 ? ' display: none;' : ''; ?>"></div></div>
               <?php } ?>
            	<div class="hr"></div>
                	<div class="row">
                    	<div class="col-md-6 col-sm-6 col-xs-6">
		                	<div class="Punti"><?=$item->points; ?> PUNTI</div>
                       </div>
                       <div class="col-md-6 col-sm-6 col-xs-6"><div class="right">
                       	<?php if( isset($_SESSION["customer_id"]) && $item->saldo >= $item->points ){ ?>
			                   <a href="#" onclick="System.richiediPremio(<?=$item->id; ?>);" class="Button ButtonPunti"><# Richiedi Premio #></a>
                           <?php } ?>
                       </div></div>
                   </div>
                <div class="hr"></div>
            </div>
        </div></div>
        <div class="col-md-1 col-sm-1 col-xs-1 hidden-xs"></div>
    </div></div></div>
    
    <br><br>
    
    <div class="row"><div class="col-md-1 col-sm-1 col-xs-1"></div><div class="col-md-10 col-sm-10 col-xs-10">
    	<div class="Title2 uppercase"><# Potrebbe interessarti anche #><br /><br /></div>
        <script type="text/javascript">
		$(document).ready(function(e) {
            $("#premiInteressarti > div").bxSlider({
				pager: false,
			});
        });
		</script>
        <div id="premiInteressarti"><div class="row">
        	<?php
			$list = array();
			foreach($premi as $item){
				if($item->id!=$_GET["id"]){
					$list[] = $item;
				}
			}
			$list = array_chunk($list,4);
			foreach($list as $div){ ?><div>
            	<?php foreach($div as $item){
					echo $S->FormRichiestaPremio($item->id,$item->prizeCode);
					?>
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="itemPremio" style="margin:0;">
                            <div class="Cont">
                                <div class="Titolo"><div><?=$item->name; ?></div></div>
                                <div class="Foto">
                                	<div class="bollinoPunti hidden-xs">
                                        <b><?=$item->points; ?></b><br>
                                      <# Punti #>
                                  </div>
                                	<img src="<?=$item->image; ?>" class="img-responsive" />
                                </div>
                                <div>
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tr>
                                                <td width="50%">
                                                <?php if( isset($_SESSION["customer_id"]) && $item->saldo >= $item->points ){ ?><a href="#" onclick="System.richiediPremio(<?=$item->id; ?>);" class="button4 Gold"><# Richiedi Premio #></a>
                                                <?php }else{ ?><div class="button4 Gray"><# Richiedi Premio #></div><?php } ?>
                                             </td>
                                                <td width="50%"><a href="<?=$S->getUrl('catalogo-premi', $item->id); ?>" class="button4">Scopri di pi&ugrave;</a></td>
                                            </tr>  
                                      </table>
                                </div>
                            </div>
                            <div class="Shadow"></div>
                        </div>
                    </div>
                    <?php	
				}
				?></div><?php
			}
			?>
        </div></div>
    </div><div class="col-md-1 col-sm-1 col-xs-1"></div></div>
    
<?php }else{ ?>
	<script type="text/javascript"><!--
	$(document).ready(function(e) {
        loadPrizes.Start();
    });
	var loadPrizes = {
		iLoaded: 0,
		n: null,
		
		prize4page: 12,
		
		Start: function(){
			var This = this;
			if( !this.n ){
				this.n = $(".itemPremio").length;	
			}
			
			var n = this.iLoaded + this.prize4page > this.n ? this.n : this.iLoaded + This.prize4page;
			for(var i=0;this.iLoaded<n;this.iLoaded++,i++){
				var o = $("div[data-premio='1'][data-n='" + this.iLoaded + "']");
				o.removeClass('hide').css("opacity","0")
				setTimeout( This.animateItem , i*300 , o);
			}
		},
		
		animateItem: function(item){
			item.animate({
				opacity : 1
			},{
				duration: 500,
				easing: 'linear'
			});
			item.attr("data-show","1");
		}
	};
	$(document).scroll(function(e){
		if( System.element_in_scroll("#elemForScroll") ){
			loadPrizes.Start();
		}
	});
	--></script>
    <div class="Box2"><div class="row"><?php
		$iItem=0;
		/*
		$tmp = array();
		for($i=0;$i<16;$i++)
			$tmp[] = $premi[$i];
		
		$premi = $tmp;*/
		foreach($premi as $item){
		    $show = true;
            if( $_GET["cat"]>0 && $item->category->id!=$_GET["cat"] ){
                $show = false;
            }
           			
            if( strlen($_GET["punti_da"])>0 && strlen($_GET["punti_a"])>0 && $_GET["punti_da"]>$item->points && $_GET["punti_a"]<$item->points ){
                $show=false;
            }else if( strlen($_GET["punti_da"])>0 && $_GET["punti_da"]>$item->points  ){
                $show=false;
            }else if( strlen($_GET["punti_a"])>0 && $_GET["punti_a"]<$item->points  ){
                $show=false;
            }
            if( $show ){
				 echo $S->FormRichiestaPremio($item->id,$item->prizeCode);
                ?>
                <div class="col-md-3 col-sm-3 col-xs-6 hide" data-premio="1" data-n="<?=$iItem; ?>" data-show="0">
                    <div class="itemPremio">
                        <div class="Cont">
                            <div class="Titolo"><div class="tc"><?=$item->name; ?></div></div>
                            <div class="Foto">
                            	<div class="bollinoPunti">
                                	<b><?=$item->points; ?></b><br>
                                  <# Punti #>
                              </div>
                              <?php if( false ){ ?>
	                            	<img src="<?=$item->image; ?>" class="img-responsive">
                              <?php }else{
								  	//echo $item->image;
								  	?>
	                              <img src="<?=$S->fnetImage($item->image,"prize_thumb"); ?>" class="img-responsive">
                              <?php } ?>
                            </div>
                            <div>
                            		<table cellpadding="0" cellspacing="0" border="0" width="100%">
                                  	<tr>
                                    		<td width="50%">
                                            <?php if( isset($_SESSION["customer_id"]) && $item->saldo >= $item->points ){ ?><a href="#" onclick="System.richiediPremio(<?=$item->id; ?>);" class="button4 Gold"><# Richiedi Premio #></a>
                                            <?php }else{ ?><div class="button4 Gray"><# Richiedi Premio #></div><?php } ?>
                                         </td>
                                    		<td width="50%"><a href="<?=$S->getUrl('catalogo-premi', $item->id); ?>" class="button4"><# Scopri di pi&ugrave; #></a></td>
                                    	</tr>  
                                  </table>
                            </div>
                        </div>
                        <div class="Shadow"></div>
                    </div>
                </div>
                <?php	
				 $iItem++;
            }
        }
    ?></div></div>
    <div id="elemForScroll"></div>
<?php } ?>