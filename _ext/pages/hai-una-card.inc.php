<script type="text/javascript"><!--
$(document).ready(function(e) {
    $("#boxCardExample > div").bxSlider({
		mode: 'fade',
		pager: false,
		controls: false,
		auto: true
	});
});
--></script>
<div class="bgWhite">
    <div class="row"><div class="col-md-10 col-sm-10 col-xs-10 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
        <br><br>
        <div id="HaiUnaCard" class="boxRegistrati">
        	<div class="row">
            	<div class="col-md-6 col-sm-6 hidden-xs"><a href="#" class="tab fired"><# Hai gi&agrave; una InViola Card? #></a></div>
            	<div class="col-md-6 col-sm-6 col-xs-12"><a href="{{url non-hai-una-card}}" class="tab"><# Non possiedi una card? #> <img src="{{theme}}img/arrow_white.png" alt="" class="arrow"></a></div>
               <div class="col-xs-12 visible-xs"><a href="#" class="tab fired"><# Hai gi&agrave; una InViola Card? #></a></div>
            </div>
            <div class="row">
            	<div class="col-md-12 col-sm-12 col-xs-12"><div class="boxRegistratiCont">
                	<br>
                	<form id="haicardForm" enctype="application/x-www-form-urlencoded" method="post" action="#">
                    	<input type="hidden" name="az" value="fnet">
                    	<input type="hidden" name="sub_az" value="infocard">
                    	<div class="row">
                        	<div class="col-md-8 col-sm-8 col-xs-8">
                            <br>
                            <span class="Title2 Viola"><# INSERISCI IL NUMERO E SCOPRI I CONTENUTI ESCLUSIVI RISERVATI AI MEMBER #></span>
                            <br><br><br>
                            <div class="row">
	                            <div class="col-md-6 col-sm-6 col-xs-6"><input type="text" name="fidelyCode" placeholder="<# INSERISCI QUI IL TUO NUMERO #>" class="form-control"></div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                		<a href="#" onclick="System.VerificaCard();" class="Button Big"><# Vai #></a>
                                      <img id="loaderLogin" class="hide" src="{{theme}}img/loaders/3.gif" alt="">
                                </div>
                            </div>
                        	</div>
                          <div class="col-md-4 col-sm-4 col-xs-4"><div id="boxCardExample">
                          		<br>
                          		<span class="uppercase Viola" style="padding-left: 20px;">Dove trovare il codice</span>
                              <div>
                              	<img src="{{theme}}img/registrati/card1.png" alt="">
                                	<img src="{{theme}}img/registrati/card2.png" alt="">
                              </div>
                          </div></div>
						</div>
                   </form>
                   <form id="cardIncompletaForm" method="post" enctype="application/x-www-form-urlencoded" action="{{url non-hai-una-card}}">
                   	<input type="hidden" name="registraCard" value="1">
                    	<input type="hidden" name="fidelyCode">
                   </form>
               </div></div>
               <div class="col-md-12 col-sm-12 col-xs-12"><br><img src="{{theme}}img/registrati/esempi-card.jpg" class="img-responsive" alt="" style="margin: 0 auto;"></div>
            </div>
        </div>
        
        <div id="inserisciPassword" class="boxRegistrati"><div class="row"><div class="col-md-12 col-sm-12 col-xs-12">
        	<form id="enterPasswordForm" enctype="application/x-www-form-urlencoded" method="post" action="{{url la-mia-card}}" class="boxRegistratiCont">
            	<input type="hidden" name="az" value="login">
            	<input type="hidden" name="login">
	        	<h2 class="Viola"><# Inserisci la tua password #></h2>
               <br>
               <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                    	<input type="password" name="pw" placeholder="<# INSERISCI QUI LA TUA PASSWORD #>" class="form-control">
                       <br>
                       <span class="dimenticato_pw"><# Hai dimenticato la password? #> <a href="{{url recupera-password}}"><# Clicca qui #></a></span>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                            <a id="entraPw" href="#" onclick="System.Login('haiunacard');" class="Button Big"><# Verifica password #></a>
                   	     <img id="loaderPw" class="hide" src="{{theme}}img/loaders/3.gif" alt="">
                    </div>
                </div>
            </form>
        </div></div></div>
        
    </div><div class="col-md-1 col-sm-1 col-xs-1"></div></div>
	<br /><br />
</div>