{{slideshow slide-home}}

<br>
<div class="row">
	<div class="col-md-3 col-sm-6 col-xs-6">
    	<div id="boxNews" class="boxHome">
        	<h3>News</h3>
           <div class="box">
           		<?php
				$q = "SELECT * FROM `{$S->_db_prefix}news_promo` WHERE deleted IS NULL
				AND (show_from IS NULL OR show_from <= NOW() )
				AND (show_to IS NULL OR NOW() <= show_to )
				ORDER BY `date` DESC LIMIT 3
				";
				$list = $S->cn->Q($q,true);
				?>
               <ul>
               	<?php foreach($list as $li){
						if( empty($li["description_short"]) ){
							$descr = strip_tags( $li["description"] );
						}else{
							$descr = $li["description_short"];
						}
						$descr = substr($descr,0,75)."&hellip;";
						?>
                		<li><a href="<?=$S->getUrl('news',$li["id"]); ?>"><?=$descr; ?></a></li>
                	<?php } ?>
                   <li class="allNews"><a href="{{url news}}"><# LEGGI TUTTE LE NEWS #></a></li>
               </ul>
           </div>
           <div class="shadow"><img src="{{theme}}img/shadow_store.png" alt="" class="img-responsive"></div>
        </div>
    </div>
    
    <div class="col-md-3 col-sm-6 col-xs-6">
    	<div id="boxInviolacard" class="boxHome">
        	<h3>Inviola Card</h3>
           <div class="box">
           		<# IL CIRCUITO<br>INVIOLA:<br>COME ACCUMULARE<br>PUNTI #>
               <a href="{{url inviola-card}}" class="Button"><# Per saperne di pi&ugrave; #></a>
           </div>
           <div class="shadow"><img src="{{theme}}img/shadow_store.png" alt="" class="img-responsive"></div>
        </div>
    </div>
    
    <div class="col-md-3 col-sm-6 col-xs-6">
    	<div class="boxHome">
        	<h3><# Punti InViola #></h3>
           <div class="box">
           		<img src="{{theme}}img/box_home/map.jpg" class="img-responsive" alt="" style="margin: 0 auto; padding-top: 10px;">
               <a href="{{url negozi}}" class="Button"><# Scopri #></a>
           </div>
           <div class="shadow"><img src="{{theme}}img/shadow_store.png" alt="" class="img-responsive"></div>
        </div>
    </div>
    
    <div class="col-md-3 col-sm-6 col-xs-6">
    	<div class="boxHome">
        	<h3><# Premi ed emozioni #></h3>
           <div class="box">
           		<img src="{{theme}}img/box_home/premi.jpg" class="img-responsive" alt="" style="margin: 0 auto; padding-top: 10px;">
               <a href="{{url catalogo-premi}}" class="Button"><# Catalogo Premi #></a>
           </div>
           <div class="shadow"><img src="{{theme}}img/shadow_store.png" alt="" class="img-responsive"></div>
        </div>
    </div>
    
</div>

<br><br>
<div class="row"><div class="col-md-12"><a href="{{url scopri-app}}"><img src="{{theme}}img/box_home/banner_app.png" alt="" class="img-responsive" style="margin: 0 auto;"></a></div></div>
<br>

<div class="row">
	<div class="col-md-3 col-sm-6 col-xs-6">
    	<div class="boxHome">
        	<h3><# Richiedi la card #></h3>
           <div class="box">
           		<img src="{{theme}}img/box_home/richiedi-card.png" alt="" class="img-responsive">
               <a href="{{url non-hai-una-card}}" class="Button"><# Richiedila #></a>
           </div>
           <div class="shadow"><img src="{{theme}}img/shadow_store.png" alt="" class="img-responsive"></div>
        </div>
    </div>
    
    <div class="col-md-3 col-sm-6 col-xs-6">
    	<div class="boxHome">
        	<h3><# Facebook #></h3>
           <div class="box">
           		<img src="{{theme}}img/box_home/f.png" alt="" class="img-responsive">
               <a href="https://www.facebook.com/ACFFiorentina" target="_blank" class="Button"><# Accedi #></a>
           </div>
           <div class="shadow"><img src="{{theme}}img/shadow_store.png" alt="" class="img-responsive"></div>
        </div>
    </div>
    
    <div class="col-md-3 col-sm-6 col-xs-6">
    	<div id="boxVideo" class="boxHome"> <!-- boxVideo -->
        	<h3><# Premium Video #></h3>
           <div class="box">
           		<ul>
                	<?php
					$q = "SELECT * FROM `fn_downloads` WHERE type='videopremium' ORDER BY `order` DESC LIMIT 3";
					$list = $S->cn->Q($q,true);
					$iVideo=1;
					foreach($list as $row){
						$descr = substr($row["label"],0,40)."&hellip;";
					?>
	               	<li><a href="<?=$S->getUrl('video-premium',$row["id"]); ?>"><img src="{{root}}_public/file/video/video<?=$iVideo; ?>.png" width="80" style="float: left; margin-right: 5px;" alt=""><?=$descr; ?></a></li>
                   <?php
                   	$iVideo++; 
				   } ?>
                   <li class="allNews"><a href="{{url video-premium}}"><# GUARDA TUTTI I VIDEO #></a></li>
               </ul>
           </div>
           <div class="shadow"><img src="{{theme}}img/shadow_store.png" alt="" class="img-responsive"></div>
        </div>
    </div>
    
    <div class="col-md-3 col-sm-6 col-xs-6">
    	<div id="boxPoll" class="boxHome">
        	<h3><# Sondaggio del mese #></h3>
           <div class="box">
           		<?php
				$q = "SELECT * FROM `{$S->_db_prefix}polls` WHERE `from` <= NOW() AND NOW() <= `to` AND enable_on=1 AND deleted IS NULL ORDER BY id ASC LIMIT 1";
				$poll = $S->cn->OQ($q);
				
				$txt = $poll["question"];
				$txt = strlen($txt)>60 ? substr($txt,0,59)."&hellip;" : $txt;
				echo $txt;
				?>
               <a href="{{url sondaggio-mese}}" class="Button"><# Partecipa #></a>
           </div>
           <div class="shadow"><img src="{{theme}}img/shadow_store.png" alt="" class="img-responsive"></div>
        </div>
    </div>
    
</div>