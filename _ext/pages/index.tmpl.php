<?php
include("_ext/scripts/setuser.inc.php");
if( 
	!(isset($_SESSION["customer_id"]) && strlen($_SESSION["customer_id"]))
	&&
	$S->Page["auth_required"]==1
	//!in_array($S->Page["k"],array('home','inviola-card','come-funziona','dove-accumulare-punti','catalogo-premi','entra-circuito','area-partner','news','negozi','scopri-app','hai-una-card','non-hai-una-card','riservato','recupera-password','concorso','modulistica','tim-inviola'))
){
	$_SESSION["redirect_post_login"] = $_SERVER["REQUEST_URI"];
	header("Location: " . $S->getUrl('riservato') );exit;
}
//echo "<pre>";print_r($S);exit;
?><!doctype html>
<html lang="<?=$S->_lang; ?>">
<head>
<meta charset="UTF-8">
<?=$S->Head(); ?>
<?php if( strpos($_SERVER['HTTP_USER_AGENT'],'Googlebot/')>0 ){ echo '<meta name="viewport" content="width=device-width, initial-scale=1.0">'; } ?>
<link href='http://fonts.googleapis.com/css?family=Pathway+Gothic+One' rel='stylesheet' type='text/css'>
<?php switch( $S->Page["k"] ){
	case "negozi":
		?><script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script><?php
		break;
}
?>
<?php if( site=="live" ){ ?>
    <script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-25610020-2', 'auto');
	  ga('send', 'pageview');
	
	</script>
<?php } ?>
<link href="{{root}}_ext/card/style.css" rel="stylesheet" type="text/css">
<link href="{{theme}}layout.css?v=1.9" rel="stylesheet" type="text/css">
</head>

<body<?=empty($S->Page["css_class"]) ? "" : ' class="' . $S->Page["css_class"] . '"'; ?>>
<div id="menuLoginOverlay"></div>
<div id="Site">
	<?php include("_ext/include/banner_bg.inc.php"); ?>
	<div id="barTop"><div class="container"><div class="row">
        <div class="col-md-6 col-sm-6 hidden-xs"><nav id="menuSites"><ul>
            <li class="violachannel"><a href="http://it.violachannel.tv"><i></i><# Viola Channel #></a></li>
            <li class="store"><a href="http://www.fiorentinastore.com/" target="_blank"><i></i><# Store #></a></li>
            <li class="inviola"><i></i><# InViola #></li>
        </ul></nav></div>
        <div class="col-md-6 col-sm-6 col-xs-12"><div class="relative"><?php include("_ext/include/menu_area_riservata.inc.php"); ?></div></div>
    </div></div></div>
    
    <?php include("_ext/include/bar_user_alert.inc.php"); ?>
    
    <div class="container"><div id="Wrapper">
    	<header>
            <nav id="Menu" class="row hidden-xs">
                <div class="col-md-4 col-sm-4 col-xs-4"><a href="{{url home}}"><img id="Logo" src="{{theme}}img/logo.png" alt="InViola Card" class="img-responsive"></a></div>
                <ul>
                    <li class="col-md-1 col-sm-1 hasSubmenu">
                    <a href="#">{{urlname download}}</a>
                        <div class="submenu" style="width: 140px;"><ul>
                            <li><a href="{{url cover-facebook}}">{{urlname cover-facebook}}</a></li>
                            <li><a href="{{url wallpaper}}">{{urlname wallpaper}}</a></li>
                            <li><a href="{{url suonerie}}">{{urlname suonerie}}</a></li>
                            <li><a href="{{url cori}}">{{urlname cori}}</a></li>
                       </ul></div>
                   </li>
                    <li class="col-md-2 col-sm-2 hasSubmenu">
                        <a href="#">{{urlname entra-in-azione}}</a>
                       <div class="submenu"><ul>
                        <li><a href="{{url indovina-formazione}}">{{urlname indovina-formazione}}</a></li>
					   <li><a href="{{url indovina-primo-marcatore}}">{{urlname indovina-primo-marcatore}}</a></li>
                        <li><a href="{{url indovina-risultato}}">{{urlname indovina-risultato}}</a></li>
                        <li><a href="{{url inviaci-foto}}">{{urlname inviaci-foto}}</a></li>
                        <li><a href="{{url sondaggio-mese}}">{{urlname sondaggio-mese}}</a></li>
                       <li><a href="{{url opta}}">{{urlname opta}}</a></li>
                       </ul></div>
                   </li>
                   <li class="col-md-1 col-sm-1 hasSubmenu">
                    <a href="#">{{urlname giochi}}</a>
                        <div class="submenu" style="width: 200px;"><ul>
                            <li><a href="{{url disegni-per-bambini}}">{{urlname disegni-per-bambini}}</a></li>
                            <li><a href="{{url trova-parole}}">{{urlname trova-parole}}</a></li>
                            <li><a href="{{url cruci-viola}}">{{urlname cruci-viola}}</a></li>
                            <li><a href="{{url trova-differenze}}">{{urlname trova-differenze}}</a></li>
                            <li><a href="{{url unisci-i-punti}}">{{urlname unisci-i-punti}}</a></li>
                       </ul></div>
                   </li>
                    <li class="col-md-2 col-sm-2 hasSubmenu">
                        <a href="#">{{urlname immagini-video}}</a>
                       <div class="submenu"><ul>
                        <li><a href="{{url video-highlights}}">{{urlname video-highlights}}</a></li>
                        <li><a href="{{url video-partiteintegrali}}">{{urlname video-partiteintegrali}}</a></li>
                        <li><a href="{{url immagini-premium}}">{{urlname immagini-premium}}</a></li>
                        <li><a href="{{url video-premium}}">{{urlname video-premium}}</a></li>
                        <li><a href="{{url inviaci-foto}}"><# Foto dei Tifosi #></a></li>
                       </ul></div>
                   </li>
                   <li class="col-md-2 col-sm-2 hasSubmenu">
                    <a href="#">{{urlname inviola-card}}</a>
                        <div class="submenu"><ul>
                        <li><a href="{{url la-mia-card}}">{{urlname la-mia-card}}</a></li>
                        <li><a href="{{url il-mio-profilo}}">{{urlname il-mio-profilo}}</a></li>
                        <li><a href="{{url i-miei-premi}}">{{urlname i-miei-premi}}</a></li>
                        <li><a href="{{url wishlist}}">{{urlname wishlist}}</a></li>
                        <li><a href="{{url trasferisci-punti}}">{{urlname trasferisci-punti}}</a></li>
                        <li><a href="{{url inviola-card}}"><# Come Funziona #></a></li>
                        <li><a href="{{url negozi}}"><# Punti Inviola #></a></li>
                        <li><a href="{{url catalogo-premi}}">{{urlname catalogo-premi}}</a></li>
                       </ul></div>
                   </li>
                </ul>
            </nav>
            <nav id="MenuMobile" class="visibile-xs hidden-lg hidden-md hidden-sm">
            	<div class="row">
                     <div class="col-xs-4">
                        <a href="{{url home}}"><img id="LogoMobile" src="{{theme}}img/logo.png" alt="InViola Card" class="img-responsive"></a>
                    </div>
                    <div class="col-xs-8"><div class="text-right">
                        <a href="#" class="btn-navbar" onclick="Mobile.OpenMenu();"><span></span><span></span><span></span></a>
                    </div></div>
                </div>                
                <ul id="MenuMobileList">
					<?php
                    //$arr = array("cover-facebook","wallpaper","suonerie","sondaggio-mese","disegni-per-bambini","trova-parole","cruci-viola","trova-differenze","unisci-i-punti","immagini-video","immagini-premium","video-premium","la-mia-card","il-mio-profilo","i-miei-premi","wishlist","trasferisci-punti");
					$arr = array("indovina-formazione","sondaggio-mese","immagini-video","video-premium","la-mia-card","il-mio-profilo","i-miei-premi","wishlist","trasferisci-punti");
                    foreach($arr as $link){
						$fired = $link==$S->Page["k"] ? ' class="fired"' : "";
						?><li<?=$fired; ?>><a href="{{url <?=$link; ?>}}">{{urlname <?=$link; ?>}}</a></li><?php
                    }
                    ?>
                </ul>
            </nav>
        </header>
        
        <section id="Schema"><?php $S->LoadSchema(); ?></section>
        
        <?php include("_ext/include/bar_social.inc.php");  ?>
        <?php //if( in_array($S->Page["k"],array('home')) ){ include("_ext/include/bar_banner_prefooter.inc.php"); } ?>
        <br>
    </div></div>
    <!-- End Wrapper -->
    <div class="container"><footer id="Footer">
    	<?php $list = $S->ListBanners('footer',array("limit"=>12) );
            if( count($list)>0 ){ ?>
	        <div class="row"><div class="col-md-12 col-sm-12 col-xs-12">
                <div class="bannerFooter"><?php
					  $width = round( 100/count($list) );
                    foreach($list as $banner){
                        ?><div class="bannerFooterItem"><?=$S->Banner($banner["id"]); ?></div><?php
                    }
                    ?>
                </div>
			</div></div>
        <?php } ?> 
        
        <?php $list = $S->ListBanners('footer2',array("limit"=>12) );
            if( count($list)>0 ){ ?>
            <div class="row"><div class="col-md-12 col-sm-12 col-xs-12">                
                <div class="bannerFooter"><?php
                    foreach($list as $banner){
                        ?><div class="bannerFooterItem" style="padding: 0 10px;"><?=$S->Banner($banner["id"]); ?></div><?php
                    }
                    ?>
                </div>
            </div></div>
            <br><br>
		<?php } ?>
        
        <div class="row">
           <div class="col-md-4 col-sm-4 col-xs-4 colFooter">
                <h4><# InViola Card #></h4>
               <ul>
                <li><a href="{{url inviola-card}}"><# Come funziona #></a></li>
                <li><a href="{{url negozi}}"><# Dove acculumare punti #></a></li>
                <li><a href="{{root}}docs/InViola - REGOLAMENTO INVIOLA CARD.pdf" target="_blank"><# Regolamento #></a></li>
                <li><a href="{{url catalogo-premi}}">{{urlname catalogo-premi}}</a></li>
               </ul>
           </div>
           <div class="col-md-4 col-sm-4 col-xs-4 colFooter">
               <h4><# Il Circuito #></h4>
               <ul>
                    <li><a href="{{url entra-circuito}}">{{urlname entra-circuito}}</a></li>
                    <li><a href="{{url modulistica}}">{{urlname modulistica}}</a></li>
                    <!--li><a href="{{url area-partner}}">{{urlname area-partner}}</a></li-->
               </ul>
           </div>
           <div class="col-md-4 col-sm-4 col-xs-4 colFooter colFooter_last">
           		<h4><# Contatti #></h4>
               <ul>
               	<li><# SERVIZIO CLIENTI #></li>
               	<li>
                		<a href="tel:055571259" class="visible-xs"><# Tel. #> 055571259</a>
                       <span class="hidden-xs"><# Tel. #> 055571259</span>
                    </li>
               </ul>
           </div>
		</div>

       
       <div class="row Info"><div class="col-md-12 col-sm-12 col-xs-12"><div class="center">
           <a href="{{url home}}"><img src="{{theme}}img/logo_footer.png" alt="InViola Card"></a>
           <br>
           2013. ACF Fiorentina. Tutti i diritti riservati.<br>
           Iscritto al registro della Stampa Periodica del Tribunale di Firenze n. 5667 in data 28 giugno 2008. Num. Lic. SIAE 1263/I/1336<br>
           ACF Fiorentina S.p.A. Viale M. Fanti 4, 50137 Firenze - Italia; Capitale Sociale &euro; 7.500.000,00 i.v. Registro Imprese, Codice Fiscale e Partita IVA 05248440488 - REA 532212
           <br><br>
           <a href="http://www.nxscom.com/" target="_blank"><img src="{{theme}}img/logo-nexus.png" alt="Loyalty Partner NexusCom" class="img-responsive" style="margin: 0 auto;"></a>
           <br>
        </div></div></div>
    </footer></div>
</div>

<?php include("_ext/include/cookies_policy_alert.inc.php"); ?>

<?=$S->EndScript(); ?>
<script type="text/javascript" src="{{theme}}main.js"></script>

<?php if( in_array($S->Page["k"],array("video-highlights","video-partiteintegrali")) ){
	/*<script id="mediaCenterScriptId" type="text/javascript" data-clientid="acffiorentina" src="//acffiorentina-4me.weebo.it/static/app/widgets/templates/media-center/1.0/js/media-center.js"></script>*/
    if( $S->video_preroll){ ?><script id="mediaCenterScriptId" type="text/javascript" data-clientid="acffiorentina" src="{{root}}_ext/js/4me/1.0/media-center-no-auto-play.js"></script><?php
	}else{ ?><script id="mediaCenterScriptId" type="text/javascript" data-clientid="acffiorentina" src="{{root}}_ext/js/4me/1.0/media-center.js"></script><?php
	}
} ?>
</body>
</html>