<script type="text/javascript" src="{{theme}}indovina_formazione.js"></script>
<?php
if( isset($_GET["id"]) && $_GET["id"]>0 ){
	$id_match = $_GET["id"];	
}else{
	$id_match = false;
}

//Cerca id utente e se ha già votato
//$_SESSION["customer_id"] = 717988; //test
$q = "SELECT * FROM utenti WHERE customer_id = {$_SESSION['customer_id']}";
$user = $S->cn->OQ($q);
$id_utente = $user['id'];
//echo $_SESSION["customer_id"];
$user = json_decode($user['fidely_data'],false);
//echo "<pre>";print_r($user);exit;

require_once("_ext/scripts/class/IndovinaFormazione.class.php");
$IF = new IndovinaFormazione( $S->cn , $S->_db_prefix);
if( site!='live' ){
	$IF->Ore_margine = 4;
}
$Stagione = $IF->StagioneInCorso();

$formazioni = $IF->getFormazioni();
$portieri = $IF->getPlayers(true);
$giocatori = $IF->getPlayers(false);

$Match = $IF->getMatches('next',$id_utente);
$Match = $Match=="-1" ? false : $Match;

//echo "<pre>";print_r($Match);
$IF_enable = $Match["rimane"]["timestamp"]>$IF_ore_margine*3600 ? true : false;

$index_formazione_sel = 0;
$IF_giocata = false;
if( $Match["giocata"] ){
	for($i=0;$i<count($formazioni);$i++){
		if( $formazioni[$i]["id"] == $Match["giocata"]["id_formazione"] ){
			$index_formazione_sel = $i;	
			$IF_giocata = true;
		}
	}
}

$IF_can_modify = $IF_giocata && $IF_enable ? true : false;

?>
<div class="bgWhite">
    <div class="row"><div class="col-md-10 col-sm-10 col-xs-10 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
        <h1><?=$S->Page["name"]; ?></h1>

        <!--a href="#" id="IndovinaFormazione-OpenClassifica-btn" class="Button" onclick="IndovinaFormazione.OpenClassifica();">Classifica Vincitori<span class="glyphicon glyphicon-chevron-down"></span></a-->
        <a href="#" id="IndovinaFormazione-OpenClassifica-btn" onclick="IndovinaFormazione.OpenClassifica();">
        	<span class="std"></span>
            <span class="hover"></span>
        </a>
        <div id="IndovinaFormazione-Classifica">
        	<div class="Title"><div>Classifica</div></div>
        	<table class="table table-striped table-bordered">
            	<thead>
                	<tr>
                    	<th width="80" class="text-center">Posizione</th>
                      <th>Nome</th>
                      <th width="80" class="text-center">Vinte</th>
                      <th width="120" class="text-center">Premi</th>
                   </tr>
                </thead>
            	<?php
				$list = $IF->Classifica($Stagione["id"]);
				$pos = 1;
				$premi = array('Visita centro sportivo','Stadium tour','Un giorno da reporter','Buono sconto da 25&euro;<br>Fiorentina Store','Sciarpa in raso');
				foreach($list as $r){
					$userClassifica = $S->UserInfo(NULL , $r->user["customer_id"] , true);
				?>
                	<tr>
                    	<td class="text-center"><b><?=$pos; ?>.</b></td>
                    	<td class="text-left">
                        	<?php $img = $r->user["id_avatar"] ? $S->Img($r->user["id_avatar"],array("w"=>90,"h"=>90,"m"=>"square") ) : $S->_path->theme . "img/avatar.png"; ?>
                          <img src="<?=$img; ?>" class="Avatar img-responsive" alt="">
							<?=$userClassifica->customer->personalInfo->name." ".$userClassifica->customer->personalInfo->surname; ?>
                       </td>
                       <td class="text-center"><?=$r->vittorie; ?></td>
                       <td class="text-center"><?php
                       	if($pos<=5){
								?><img src="{{theme}}img/indovina-formazione/classifica/premio<?=$pos; ?>.png" alt="" width="60">
                                <br>
                                <?php
                                echo $premi[ ($pos-1) ];
							}else{
								echo "-";	
							}
					    ?></td>
                   </tr>
                <?php
					$pos++;
				}
				?>
            </table>
            <hr>
        </div>
        
        <div class="pageStd" style="padding-right: 130px;"><?=$S->Page["html"]; ?></div>
        
        <?php if( !$id_match ){
			if($Match){ ?>
                <h2>
                    <span class="Red"><?php
                    if( $Match["in_casa"] ){
                        echo "Fiorentina - {$Match['avversario']}";
                    }else{
                        echo "{$Match['avversario']} - Fiorentina";
                    }
                    ?></span>
                    <span class="IF_ora"><?=strftime("%d/%m/%Y %H:%M", strtotime($Match["data"]) ); ?></span>
                </h2>
                <?php if( $IF_enable && !$IF_giocata ){ ?>
                    <div class="pageStd">
                    	<?=$IF->TimeToPlay($Match); ?>
                    </div>
                <?php }else if( $IF_enable && $IF_giocata ){ ?>
                    <div class="pageStd">
                    	<br>
                    	<?php if( empty($Match["giocata"]["updated"]) ){ ?>
	                    	Hai scelto la formazione per questa partita il giorno: <?=strftime("<b>%d/%m/%Y</b> alle <b>%H:%M</b>", strtotime($Match["giocata"]["timestamp"]) );//c'è una differenza di 90 minuti con l'ora del database ?>
						<?php }else{ ?>
                        	Hai modificato la formazione il <?=strftime("<b>%d/%m/%Y</b> alle <b>%H:%M</b>", strtotime($Match["giocata"]["updated"]) );//c'è una differenza di 90 minuti con l'ora del database ?>
                       <?php } ?>
                    </div>
                <?php }else if( !$IF_enable && $IF_giocata){
                    $tmp = !empty($Match['giocata']['updated']) ? $Match['giocata']['updated'] : $Match['giocata']['timestamp'];
                    ?>
                    <div class="pageStd"><br>Hai scelto la formazione per questa partita il giorno: <?=strftime("<b>%d/%m/%Y</b> alle <b>%H:%M</b>", strtotime( $tmp ) );//c'è una differenza di 90 minuti con l'ora del database ?></div>
                <?php }else if( !$IF_enable && !$IF_giocata){ ?>
                    <div class="pageStd"><br><em>Ci dispiace, non puoi pi&ugrave; giocare per questa partita. &Egrave; possibile giocare solo fino a <?=$IF->Ore_margine; ?> ore prima dell&lsquo;inizio della partita</em></div>
                <?php } ?>
                   
                <hr>
            <?php }else{ ?>
                <h2 class="Red"><# Al momento non ci sono partite in calendario #></h2>
            <?php }
		}
		?>
    </div><div class="col-md-1 col-sm-1 col-xs-1"></div></div>
    
    <form id="changeForm" action="{{url indovina-formazione}}"></form>
    <?php if($Match && !$id_match){ ?><div class="row"><div class="col-md-12 col-sm-12 col-xs-12"><form id="IndovinaFormazioneForm" onSubmit="return false;">
    	<input type="hidden" name="id_utente" value="<?=$id_utente; ?>">
        <input type="hidden" name="az" value="IndovinaFormazioneSave">
        <input type="hidden" name="id_match" value="<?=$Match["id"]; ?>">
        <input type="hidden" name="id_formazione" value="<?=$formazioni[ $index_formazione_sel ]["id"]; ?>">
        <input type="hidden" name="redirect" value="<?=$_SERVER['REQUEST_URI']; ?>">
        
    	<div id="IndovinaFormazione">
        	<?php if( $Match["id_banner"]>0 ){ echo $S->Banner( $Match["id_banner"], "MainBanner" , false); ?>
            <?php }else{ ?><div class="MainBanner"><img src="{{theme}}img/indovina-formazione/banner-default.jpg" alt=""></div><?php } ?>
            
        	<div class="Campo f<?=$formazioni[ $index_formazione_sel ]["formazione"]; ?>" data-formazione="<?=$formazioni[ $index_formazione_sel ]["formazione"]; ?>">
            	<div id="ScegliFormazione">
                	<div class="pad">
                    	<# Benvenuto #><br>
                       <b><?=$user->customer->personalInfo->name; ?>&nbsp;</b>
                   </div>
                   <div class="hr"></div>
                   <div class="pad">
                   	<?php if($IF_giocata){ ?>
                    		<div class="text-center"><# Hai scelto il modulo #></div>
                    	<?php }else{ ?>
	                   	<div><# Scegli il tuo modulo #></div>
                       <?php } ?>
                       <div class="formazione_panel">
                       	<?php if(!$IF_giocata || $IF_can_modify){
								$class_add = $IF_can_modify ? ' hide' : '';
								?>
                        		<a href="#" class="formazione_btn_left<?=$class_add; ?>"></a>
                        		<a href="#" class="formazione_btn_right<?=$class_add; ?>"></a>
                          <?php } ?>
                          
                          <div class="formazioni" data-id="<?=$formazioni[ $index_formazione_sel ]["id"]; ?>" data-formazione="<?=$formazioni[ $index_formazione_sel ]["formazione"]; ?>" data-index-formazione-sel="<?=$index_formazione_sel; ?>"><ul>
                          		<?php
								
								$tmp = array();
								$tmp[] = $formazioni[ count($formazioni)-1 ];
								foreach($formazioni as $fo){
									$tmp[] = $fo;
								}
								$tmp[] = $formazioni[ $index_formazione_sel ];
								
								$iLi=0;
								foreach($tmp as $fo){
								?><li data-index="<?=$iLi; ?>" data-id="<?=$fo["id"]; ?>" data-formazione="<?=$fo["formazione"]; ?>"><?=$fo["formazione"]; ?></li><?php
									$iLi++;
								}
								unset($tmp,$iLi,$fo);
                          		?>
                          </ul></div>
                       </div>
                   </div>
                   <div class="hr"></div>
                   <div class="text-center">
	                   <?php if(!$IF_giocata){ ?>
	                   	<button class="conferma" data-action="conferma"><# Conferma #></button>
                       <?php }else if($IF_can_modify){ ?>
                       	<button class="conferma hide" data-action="conferma"><# Conferma #></button>
                       	<button class="conferma" data-action="modifica"><# Modifica #></button>
                        	<a href="{{url indovina-formazione}}" class="annulla hide"><# Annulla #></a>
                       <?php }else{ ?>
                       	<img src="{{theme}}img/logo-inviola-card.png" alt="" style="width: 80%; margin-top: 10px;">
                       <?php } ?>
	               </div>
	            </div>
                
            	<div class="Player Portiere">
                	<?php
				   	if( $IF_giocata ){
						$item = $IF->getPlayer($Match["giocata"]["id_giocatore1"]);
						if($item["id_foto"]>0){
							//$img = $S->Img($item["id_foto"], $S->_IMG_PARAMS["if_player"] );
							$img = $S->pathFile($item["id_foto"]);
						}else{
							$img = "{$S->_path->theme}img/indovina-formazione/player.png";
						}
						$name = $IF->PlayerName($item);
						$id_giocatore_tmp = $Match["giocata"]["id_giocatore1"];
					}else{
						$img = "{$S->_path->theme}img/indovina-formazione/player_unsel.png";
						$name = 'Seleziona';
						$id_giocatore_tmp = 0;
					}
					
					$disable_player_button = $IF_enable && !$IF_giocata ? ' class="enable"' : '';
				    ?>
                   
                   <input type="hidden" name="id_giocatore1" value="<?=$id_giocatore_tmp; ?>">
                   <a href="#" data-index="1"<?=$disable_player_button; ?>>
                   	<img src="<?=$img; ?>" alt="">
                    	<span><?=$name; ?></span>
                   </a>
               </div>
               <?php for($i=2;$i<=11;$i++){
				   if( $IF_giocata ){
						$item = $IF->getPlayer($Match["giocata"]["id_giocatore{$i}"]);
						if($item["id_foto"]>0){
							//$img = $S->Img($item["id_foto"], $S->_IMG_PARAMS["if_player"] );
							$img = $S->pathFile($item["id_foto"]);
						}else{
							$img = "{$S->_path->theme}img/indovina-formazione/player.png";
						}
						$name = $IF->PlayerName($item);
						$id_giocatore_tmp = $Match["giocata"]["id_giocatore{$i}"];
					}else{
						$img = "{$S->_path->theme}img/indovina-formazione/player_unsel.png";
						$name = 'Seleziona';
						$id_giocatore_tmp = 0;
					}
				   ?>
               	<div class="Player g<?=$i; ?>">
                		<input type="hidden" name="id_giocatore<?=$i; ?>" value="<?=$id_giocatore_tmp; ?>">
                       <a href="#" data-index="<?=$i; ?>"<?=$disable_player_button; ?>>
                       	<img src="<?=$img; ?>" alt="">
                        	<span><?=$name; ?></span>
                       </a>
                	</div>
               <?php } ?>
            </div>
            
            <?php if($IF_enable && (!$IF_giocata || $IF_can_modify) ){ ?>
                <div id="boxPlayersOverlay"></div>
                <div id="boxPlayers">
                     <div class="Portieri hide"><div class="row">
                        <?php
                        foreach($portieri as $item){
                            if($item["id_foto"]>0){
                                //$img = $S->Img($item["id_foto"], $S->_IMG_PARAMS["if_player"] );
                                $img = $S->pathFile($item["id_foto"]);
                            }else{
                                $img = $S->_path->theme . 'img/indovina-formazione/player.png';
                            }
                            ?>
                           <a href="#" class="PlayerBtn col-md-3 col-sm-3" data-id="<?=$item["id"]; ?>" data-name="<?=$IF->PlayerName($item); ?>">
	                            <img src="<?=$img; ?>" alt="">
                                <span><?=$IF->PlayerName($item); ?></span>
                           </a>
                           <?php
                        }
                        ?>
                    </div></div>
                    <div class="Giocatori hide"><div class="row">
                        <?php
                        foreach($giocatori as $item){
                            if($item["id_foto"]>0){
                                //$img = $S->Img($item["id_foto"], $S->_IMG_PARAMS["if_player"] );
                                $img = $S->pathFile($item["id_foto"]);
                            }else{
                                $img = $S->_path->theme . 'img/indovina-formazione/player.png';
                            }
                            ?>
                           <a href="#" class="PlayerBtn col-md-3 col-sm-3" data-id="<?=$item["id"]; ?>" data-name="<?=$IF->PlayerName($item); ?>">
	                            <img src="<?=$img; ?>" alt="">
                                <span><?=$IF->PlayerName($item); ?></span>
                            </a>
                           <?php
                        }
                        ?>
                    </div></div>
                </div>
			<?php } //boxPlayersOverlay e boxPlayers ?>
            
        </div>
    </form></div></div><?php } ?>
    
    <?php
	if( !$id_match ){ 
		$prevMatch = $IF->getMatches('prev',$id_utente);
		if( $prevMatch!="-1" && $prevMatch ){ ?>
			<hr>
			<h3 class="text-center"><# L&lsquo;ultima partita #></h3>
			<?php
			include("_ext/include/indovina-formazione-match.inc.php");
		}
	}else{ //Partita selezionata da archivio
		$prevMatch = $IF->getMatches($id_match,$id_utente);
        if( $prevMatch ){
			?><h3 class="text-center"><?=$prevMatch["in_casa"] ? "Fiorentina - {$prevMatch['avversario']}" : "{$prevMatch['avversario']} - Fiorentina";	?></h3><?php
			include("_ext/include/indovina-formazione-match.inc.php");
		}
	}
	?>
    
    <?php
	$archivioMatch = $IF->getMatches('list',$id_utente);
	if( is_array($archivioMatch) && count($archivioMatch)>1 ){
		if( !$id_match ){
			unset($archivioMatch[0]);
		}
		?>
    	<hr>
        <div class="row">
        	<div class="col-md-1"></div>
           <div class="col-md-10">
                <table class="table table-striped">
                    <thead>
                        <tr>
	                        <th width="70%" class="uppercase"><# Archivio Partite #></th>
                            <th></th>
                       </tr>
                    </thead>
                    <tbody>
                        <?php foreach($archivioMatch as $item){
							if($item["id"]!=$id_match){
							?>
                            <tr>
	                            <td>
                                    <a href="{{url indovina-formazione}}?id=<?=$item["id"]; ?>" class="uppercase block"><?php
                                        if($item["in_casa"]){
                                            echo "Fiorentina - {$item['avversario']}";
                                        }else{
                                            echo "{$item['avversario']} - Fiorentina";
                                        }
                                    ?></a>
                                </td> 
                               <td>
                                <?=utf8_encode( strftime("%A %d %B %Y", strtotime($item["data"]) ) ); ?>
                               </td>
                           </tr>
                        <?php }
						}
						?>
                    </tbody>
                </table>
			</div>
            <div class="col-md-1"></div>
		</div>
    <?php } ?>
    
    <hr>
    
    <div class="container"><div class="row"><div class="col-md-10 col-md-offset-1"><section id="regolamento">
    	<h3 class="text-center">Regolamento</h3>
        <br>
        <p class="text-justify">Seleziona il modulo, scegli i giocatori che scenderanno in campo e partecipa al gioco!</p>        
        <p class="text-justify;">Se indovini la formazione vinci<strong><span style="color: red; font-size: 15px;">100 PUNTI SULLA TUA INVIOLA CARD</span></strong>** per avvicinarti sempre di pi&ugrave; al premio che desideri!&nbsp;<a href="{{url premi}}" target="_blank"><span style="text-decoration: underline;">Clicca qui</span></a> per scoprire il catalogo premi tutto firmato ACF Fiorentina!</p>
        
        <p class="text-justify;">La formazione potr&agrave; essere inserita fino a <strong><span style="color: red;"><?=$IF->Ore_margine; ?> ore prima</span></strong> dell'inizio della partita. Una volta inserita, la formazione potrà essere modificata fino allo scadere del tempo disponibile per l’inserimento. Per scoprire se hai vinto collegati alla tua area riservata ed accedi a questa pagina, la formazione ufficiale verr&agrave; inserita il giorno successivo alla gara. Potrai controllare i punti accreditati in caso di vincita sulla pagina "La Mia Card".&nbsp;&nbsp;</p>
        <p class="text-justify;">Controlla la <strong><span style="color: red; font-size: 15px;">CLASSIFICA</span></strong> dei vincitori e scopri se sei fra coloro che potranno vincere i bellissimi <strong><span style="color: red; font-size: 15px;">PREMI ESCLUSIVI ACF FIORENTINA</span></strong> al termine della stagione sportiva 2014/15! Una speciale classifica che a fine anno ci dir&agrave; chi &egrave; stato l&rsquo;utente che pi&ugrave; di tutti ha indovinato le scelte di mister Montella!***</p>
        
        <p class="text-justify;" style="font-size: 11px; line-height: 13px;">*La formazione vincente si basa SOLO ED ESCLUSIVAMENTE su quella ufficiale diramata dalla societ&agrave; ai media accreditati alla gara il giorno della partita e NON FA RIFERIMENTO a nessun canale televisivo.&nbsp;La formazione sar&agrave; valida soltanto se vengono indovinate le posizioni corrette dei giocatori al momento del fischio di inizio.<br />** I punti possono variare di gara in gara in base alla partita.<br />*** In caso di parit&agrave; fra pi&ugrave; utenti viene considerata la data di inserimento come variabile univoca, l&rsquo;utente che prima inserisce la formazione sar&agrave; avanti a tutti gli altri utenti con pari punti.</p>
        
        <p>&nbsp;</p>
    </section></div></div></div>
    
</div>