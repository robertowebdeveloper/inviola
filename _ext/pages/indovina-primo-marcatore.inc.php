<script type="text/javascript" src="{{theme}}indovina_primo_marcatore.js"></script>
<?php
if( isset($_GET["id"]) && $_GET["id"]>0 ){
	$id_match = $_GET["id"];	
}else{
	$id_match = false;
}

//Cerca id utente e se ha già votato
//$_SESSION["customer_id"] = 717988; //test
$q = "SELECT * FROM utenti WHERE customer_id = {$_SESSION['customer_id']}";
$user = $S->cn->OQ($q);
$id_utente = $user['id'];
//echo $_SESSION["customer_id"];
$user = json_decode($user['fidely_data'],false);
//echo "<pre>";print_r($user);exit;

require_once("_ext/scripts/class/IndovinaFormazione.class.php");
require_once("_ext/scripts/class/IndovinaPrimoMarcatore.class.php");
$IF = new IndovinaPrimoMarcatore( $S->cn , $S->_db_prefix);
if( site!='live' ){
	$IF->Ore_margine = 4;
}
$Stagione = $IF->StagioneInCorso();

$Match = $IF->getMatches('next',$id_utente);
$Match = $Match=="-1" ? false : $Match;

//echo "<pre>";print_r($Match);exit;
$IF_enable = $Match["rimane"]["timestamp"]>$IF_ore_margine*3600 ? true : false;

$index_formazione_sel = 0;
$IF_giocata = false;
if( $Match["giocata_im"] ){
    $IF_giocata = true;
}

$IF_can_modify = $IF_giocata && $IF_enable ? true : false;

?>
<div class="bgWhite">
    <div class="row"><div class="col-md-10 col-sm-10 col-xs-10 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
        <div id="IM_vs"><?php
        $name1 = $Match['in_casa'] ? 'Fiorentina' : $Match['avversario'];
        $name2 = $Match['in_casa'] ? $Match['avversario'] : 'Fiorentina';
        ?><b><?=$name1; ?></b> <span>vs</span> <b><?=$name2; ?></b></div>
        <h1><?=$S->Page["name"]; ?></h1>

        <a href="#" id="IndovinaPrimoMarcatore-OpenClassifica-btn" onclick="IndovinaPrimoMarcatore.OpenClassifica();" class="hide">
        	<span class="std"></span>
            <span class="hover"></span>
        </a>
        <div id="IndovinaPrimoMarcatore-Classifica" class="hide">
        	<div class="Title"><div>Classifica</div></div>
        	<table class="table table-striped table-bordered">
            	<thead>
                	<tr>
                    	<th width="80" class="text-center">Posizione</th>
                      <th>Nome</th>
                      <th width="80" class="text-center">Vinte</th>
                      <th width="120" class="text-center">Premi</th>
                   </tr>
                </thead>
            	<?php
				$list = $IF->Classifica($Stagione["id"]);
				$pos = 1;
				$premi = array('Visita centro sportivo','Stadium tour','Un giorno da reporter','Buono sconto da 25&euro;<br>Fiorentina Store','Sciarpa in raso');
				foreach($list as $r){
					$userClassifica = $S->UserInfo(NULL , $r->user["customer_id"] , true);
				?>
                	<tr>
                    	<td class="text-center"><b><?=$pos; ?>.</b></td>
                    	<td class="text-left">
                        	<?php $img = $r->user["id_avatar"] ? $S->Img($r->user["id_avatar"],array("w"=>90,"h"=>90,"m"=>"square") ) : $S->_path->theme . "img/avatar.png"; ?>
                          <img src="<?=$img; ?>" class="Avatar img-responsive" alt="">
							<?=$userClassifica->customer->personalInfo->name." ".$userClassifica->customer->personalInfo->surname; ?>
                       </td>
                       <td class="text-center"><?=$r->vittorie; ?></td>
                       <td class="text-center"><?php
                       	if($pos<=5){
								?><img src="{{theme}}img/indovina-formazione/classifica/premio<?=$pos; ?>.png" alt="" width="60">
                                <br>
                                <?php
                                echo $premi[ ($pos-1) ];
							}else{
								echo "-";	
							}
					    ?></td>
                   </tr>
                <?php
					$pos++;
				}
				?>
            </table>
            <hr>
        </div>
        
        <div class="pageStd" style="padding-right: 130px;"><?=$S->Page["html"]; ?></div>
    </div></div>

    <div class="row"><div class="col-md-10 col-sm-10 col-xs-12 col-md-offset-1 col-sm-offset-1">
        <?php if( $IF_enable && !$IF_giocata ){ ?>
            <div class="pageStd">
                <button type="button" class="Button hide" id="IM_modify-btn">Modifica</button>
                <?=$IF->TimeToPlay($Match); ?>
            </div>
        <?php }else if( $IF_enable && $IF_giocata ){ ?>
            <div class="pageStd">
                <?php if($IF_can_modify){ ?>
                    <button type="button" class="Button" id="IM_modify-btn">Modifica</button>
                <?php } ?>
                <br>
                <?php if( empty($Match["giocata_im"]["updated"]) ){ ?>
                    Hai scelto il giocatore per questa partita il giorno: <?=strftime("<b>%d/%m/%Y</b> alle <b>%H:%M</b>", strtotime($Match["giocata_im"]["timestamp"]) ); ?>
                <?php }else{ ?>
                    Hai modificato il giocatore il <?=strftime("<b>%d/%m/%Y</b> alle <b>%H:%M</b>", strtotime($Match["giocata_im"]["updated"]) ); ?>
                <?php } ?>
            </div>
        <?php }else if( !$IF_enable && $IF_giocata){
            $tmp = !empty($Match['giocata_im']['updated']) ? $Match['giocata_im']['updated'] : $Match['giocata_im']['timestamp'];
            ?>
            <div class="pageStd"><br>Hai scelto il giocatore per questa partita il giorno: <?=strftime("<b>%d/%m/%Y</b> alle <b>%H:%M</b>", strtotime( $tmp ) ); ?></div>
        <?php }else if( !$IF_enable && !$IF_giocata){
            ?><div class="pageStd"><br><em>Ci dispiace, non puoi pi&ugrave; giocare per questa partita. &Egrave; possibile giocare solo fino a <?=$IF->Ore_margine; ?> ore prima dell&lsquo;inizio della partita</em></div><?php
        }
        ?>
        <br>
    </div></div>

    <div class="row">
        <div class="col-md-12">
            <?php if( !$id_match ){
                if($Match){
                    $logo_avversario = $S->pathFile($Match['id_logo_avversario']);
                    ?>
                    <div id="IndovinaPrimoMarcatore"><form id="IM_form" data-game-enable="<?=$IF_enable ? 1 : 0; ?>">
                        <input type="hidden" name="az" value="IndovinaPrimoMarcatoreSave">
                        <input type="hidden" name="id_utente" value="<?=$id_utente; ?>">
                        <input type="hidden" name="id_match" value="<?=$Match['id']; ?>">
                        <input type="hidden" name="id_giocatore" value="NULL"><?php //NULL -> non selezionato, 0 -> nessun giocatore ?>
                        <div class="row">
                            <?php
                            $players = $IF->getAllPlayers();
                            foreach($players as $player){
                                if($player["id_foto"]>0){
                                    $img = $S->pathFile($player["id_foto"]);
                                }else{
                                    $img = $S->_path->theme . 'img/indovina-formazione/player.png';
                                }

                                $class = '';
                                if( $Match['giocata_im'] && strlen($Match['giocata_im']['id_giocatore'])>0 ){
                                    $class = $Match['giocata_im']['id_giocatore']==$player['id'] ? ' selected' : ' unselected';
                                }else if(!$IF_enable){
                                    $class = ' disable';
                                }
                            ?>
                                <div class="col-md-3 col-sm-3 col-xs-4"><a href="#" class="IM_Player<?=$class; ?>" data-id="<?=$player['id']; ?>" data-name="<?=$IF->PlayerName($player); ?>">
                                    <img src="<?=$img; ?>" alt="" class="img-responsive">
                                    <span><?=$IF->PlayerName($player); ?></span>
                                </a></div>
                            <?php } ?>

                            <?php
                            $class = '';
                            if( $Match['giocata_im'] && strlen($Match['giocata_im']['id_giocatore'])>0 ){
                                $class = $Match['giocata_im']['id_giocatore']==0 ? ' selected' : ' unselected';
                            }else if( !$IF_enable ){
                                $class = ' disable';
                            }
                            ?>
                            <div class="col-md-3 col-sm-3 col-xs-4"><a href="#" class="IM_Player no-player<?=$class; ?>" data-id="0" data-name="Nessun marcatore">
                                <img src="{{theme}}img/giochi/no-marcatore-btn.png" alt="" class="img-responsive">
                                <span>Nessun marcatore</span>
                            </a></div>
                        </div>

                        <div class="row">
                            <div class="col-md-12"><br><br></div>
                        </div>

                    </form></div>
                    <?php
                }else{
                    ?><h2 class="Red text-center"><# Al momento non ci sono partite in calendario #></h2><?php
                }
            }
            ?>
        </div>
    </div>

    <?php
	if( !$id_match ){ 
		$prevMatch = $IF->getMatches('prev',$id_utente);
		if( $prevMatch!="-1" && $prevMatch && strtotime($prevMatch['data'])>mktime(0,0,0,5,5,2015) ){ ?>
			<hr>
			<h3 class="text-center"><# L&lsquo;ultima partita #></h3>
			<?php
			include("_ext/include/indovina-primo-marcatore.inc.php");
		}
	}else{ //Partita selezionata da archivio
		$prevMatch = $IF->getMatches($id_match,$id_utente);
		if( $prevMatch ){
			?><h3 class="text-center"><?=$prevMatch["in_casa"] ? "Fiorentina - {$prevMatch['avversario']}" : "{$prevMatch['avversario']} - Fiorentina";	?></h3><?php
			include("_ext/include/indovina-primo-marcatore.inc.php");
		}
	}
	?>
    
    <?php
	$archivioMatch = $IF->getMatches('list',$id_utente);
	if( false && is_array($archivioMatch) && count($archivioMatch)>1 ){
		if( !$id_match ){
			unset($archivioMatch[0]);
		}
		?>
    	<hr>
        <div class="row">
        	<div class="col-md-1"></div>
           <div class="col-md-10">
                <table class="table table-striped">
                    <thead>
                        <tr>
	                        <th width="70%" class="uppercase"><# Archivio Partite #></th>
                            <th></th>
                       </tr>
                    </thead>
                    <tbody>
                        <?php foreach($archivioMatch as $item){
							if($item["id"]!=$id_match){
							?>
                            <tr>
	                            <td>
                                    <a href="{{url indovina-formazione}}?id=<?=$item["id"]; ?>" class="uppercase block"><?php
                                        if($item["in_casa"]){
                                            echo "Fiorentina - {$item['avversario']}";
                                        }else{
                                            echo "{$item['avversario']} - Fiorentina";
                                        }
                                    ?></a>
                                </td> 
                               <td>
                                <?=utf8_encode( strftime("%A %d %B %Y", strtotime($item["data"]) ) ); ?>
                               </td>
                           </tr>
                        <?php }
						}
						?>
                    </tbody>
                </table>
			</div>
            <div class="col-md-1"></div>
		</div>
    <?php } ?>

    <hr>
    
    <div class="container"><div class="row"><div class="col-md-10 col-md-offset-1"><section id="regolamento">
    	<h3 class="text-center">Regolamento</h3>
        <br>
        <p class="text-justify">Seleziona il giocatore che segnerà il primo goal della Viola e partecipa al gioco!</p>
        <p class="text-justify;">Se indovini il pirmo marcatore vinci <strong><span style="color: red; font-size: 15px;">30 PUNTI SULLA TUA INVIOLA CARD</span></strong>* per avvicinarti sempre di pi&ugrave; al premio che desideri!&nbsp;<a href="{{url premi}}" target="_blank"><span style="text-decoration: underline;">Clicca qui</span></a> per scoprire il catalogo premi tutto firmato ACF Fiorentina!</p>
        
        <p class="text-justify;">Il primo marcatore potrà essere selezionato fino a <strong><span style="color: red;"><?=$IF->Ore_margine; ?> ore prima</span></strong> Una volta inserito, il giocatore potrà essere modificato fino allo scadere del tempo disponibile per l’inserimento. Per scoprire se hai vinto collegati alla tua area riservata ed accedi a questa pagina, il risultato finale verrà inserito il giorno successivo alla gara. Potrai controllare i punti accreditati in caso di vincita sulla pagina "La Mia Card".&nbsp;&nbsp;</p>

        <p class="text-justify;" style="font-size: 12px; line-height: 14px;">
            *Il primo marcatore è il giocatore che segna il primo goal. In caso di supplementari dopo uno 0-0 sarà valido l’eventuale primo marcatore viola nei tempi supplementari. Non sono considerati i rigori.
            <br>
            I punti accreditati ai vincitori possono variare di gara in gara in base alla partita.
        </p>
        
        <p>&nbsp;</p>
    </section></div></div></div>

</div>
