<script type="text/javascript" src="{{theme}}indovina_risultato.js"></script>
<?php
if( isset($_GET["id"]) && $_GET["id"]>0 ){
	$id_match = $_GET["id"];	
}else{
	$id_match = false;
}

//Cerca id utente e se ha già votato
//$_SESSION["customer_id"] = 717988; //test
$q = "SELECT * FROM utenti WHERE customer_id = {$_SESSION['customer_id']}";
$user = $S->cn->OQ($q);
$id_utente = $user['id'];
//echo $_SESSION["customer_id"];
$user = json_decode($user['fidely_data'],false);
//echo "<pre>";print_r($user);exit;

require_once("_ext/scripts/class/IndovinaFormazione.class.php");
require_once("_ext/scripts/class/IndovinaRisultato.class.php");
$IF = new IndovinaRisultato( $S->cn , $S->_db_prefix);
if( site!='live' ){
	$IF->Ore_margine = 4;
}
$Stagione = $IF->StagioneInCorso();

$Match = $IF->getMatches('next',$id_utente);
$Match = $Match=="-1" ? false : $Match;

//echo "<pre>";print_r($Match);
$IF_enable = $Match["rimane"]["timestamp"]>$IF_ore_margine*3600 ? true : false;

$index_formazione_sel = 0;
$IF_giocata = false;
if( $Match["giocata_ir"] ){
    $IF_giocata = true;
}

$IF_can_modify = $IF_giocata && $IF_enable ? true : false;

?>
<div class="bgWhite">
    <div class="row"><div class="col-md-10 col-sm-10 col-xs-10 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
        <h1><?=$S->Page["name"]; ?></h1>

        <a href="#" id="IndovinaFormazione-OpenClassifica-btn" onclick="IndovinaRisultati.OpenClassifica();" class="hide">
        	<span class="std"></span>
            <span class="hover"></span>
        </a>
        <div id="IndovinaRisultati-Classifica" class="hide">
        	<div class="Title"><div>Classifica</div></div>
        	<table class="table table-striped table-bordered">
            	<thead>
                	<tr>
                    	<th width="80" class="text-center">Posizione</th>
                      <th>Nome</th>
                      <th width="80" class="text-center">Vinte</th>
                      <th width="120" class="text-center">Premi</th>
                   </tr>
                </thead>
            	<?php
				$list = $IF->Classifica($Stagione["id"]);
				$pos = 1;
				$premi = array('Visita centro sportivo','Stadium tour','Un giorno da reporter','Buono sconto da 25&euro;<br>Fiorentina Store','Sciarpa in raso');
				foreach($list as $r){
					$userClassifica = $S->UserInfo(NULL , $r->user["customer_id"] , true);
				?>
                	<tr>
                    	<td class="text-center"><b><?=$pos; ?>.</b></td>
                    	<td class="text-left">
                        	<?php $img = $r->user["id_avatar"] ? $S->Img($r->user["id_avatar"],array("w"=>90,"h"=>90,"m"=>"square") ) : $S->_path->theme . "img/avatar.png"; ?>
                          <img src="<?=$img; ?>" class="Avatar img-responsive" alt="">
							<?=$userClassifica->customer->personalInfo->name." ".$userClassifica->customer->personalInfo->surname; ?>
                       </td>
                       <td class="text-center"><?=$r->vittorie; ?></td>
                       <td class="text-center"><?php
                       	if($pos<=5){
								?><img src="{{theme}}img/indovina-formazione/classifica/premio<?=$pos; ?>.png" alt="" width="60">
                                <br>
                                <?php
                                echo $premi[ ($pos-1) ];
							}else{
								echo "-";	
							}
					    ?></td>
                   </tr>
                <?php
					$pos++;
				}
				?>
            </table>
            <hr>
        </div>
        
        <div class="pageStd" style="padding-right: 130px;"><?=$S->Page["html"]; ?></div>
    </div></div>

    <div class="row"><div class="col-md-10 col-sm-10 col-xs-12 col-md-offset-1 col-sm-offset-1">
        <?php if( $IF_enable && !$IF_giocata ){ ?>
            <div class="pageStd">
                <?=$IF->TimeToPlay($Match); ?>
            </div>
        <?php }else if( $IF_enable && $IF_giocata ){ ?>
            <div class="pageStd">
                <br>
                <?php if( empty($Match["giocata_ir"]["updated"]) ){ ?>
                    Hai scelto il risultato per questa partita il giorno: <?=strftime("<b>%d/%m/%Y</b> alle <b>%H:%M</b>", strtotime($Match["giocata_ir"]["timestamp"]) );//c'è una differenza di 90 minuti con l'ora del database ?>
                <?php }else{ ?>
                    Hai modificato il risultato il <?=strftime("<b>%d/%m/%Y</b> alle <b>%H:%M</b>", strtotime($Match["giocata_ir"]["updated"]) );//c'è una differenza di 90 minuti con l'ora del database ?>
                <?php } ?>
            </div>
        <?php }else if( !$IF_enable && $IF_giocata){
            $tmp = !empty($Match['giocata_ir']['updated']) ? $Match['giocata_ir']['updated'] : $Match['giocata_ir']['timestamp'];
            ?>
            <div class="pageStd"><br>Hai scelto il risultato per questa partita il giorno: <?=strftime("<b>%d/%m/%Y</b> alle <b>%H:%M</b>", strtotime( $tmp ) ); ?></div>
        <?php }else if( !$IF_enable && !$IF_giocata){
            ?><div class="pageStd"><br><em>Ci dispiace, non puoi pi&ugrave; giocare per questa partita. &Egrave; possibile giocare solo fino a <?=$IF->Ore_margine; ?> ore prima dell&lsquo;inizio della partita</em></div><?php
        }
        ?>
        <br>
    </div></div>

    <div class="row">
        <div class="col-md-12">
            <?php if( !$id_match ){
                if($Match){
                    $logo_avversario = $S->pathFile($Match['id_logo_avversario']);
                    ?>
                    <div id="IndovinaRisultato"><form id="IR_form">
                        <input type="hidden" name="az" value="IndovinaRisultatoSave">
                        <input type="hidden" name="id_utente" value="<?=$id_utente; ?>">
                        <input type="hidden" name="id_match" value="<?=$Match['id']; ?>">
                        <div class="row">
                            <div class="col-md-3 col-sm-3">
                                <?php
                                //echo "<pre>";print_r($Match);exit;
                                ?>
                                <div class="LogoSx">
                                    <?php if($Match['in_casa']){ ?>
                                        <img src="{{theme}}img/giochi/logo-fiorentina.png" alt="" class="img-responsive">
                                        <br>
                                        <h3>Fiorentina</h3>
                                    <?php }else{ ?>
                                        <img src="<?=$logo_avversario; ?>" alt="" class="img-responsive">
                                        <br>
                                        <h3><?=$Match['avversario']; ?></h3>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php
                            $class_btn = $IF_giocata || !$IF_enable ? ' disable' : '';
                            ?>
                            <div class="col-md-3 col-sm-3">
                                <div class="Goal Goal0">
                                    <?php
                                    $goal = $Match['in_casa'] ? $Match["giocata_ir"]["goal_fiorentina"] : $Match["giocata_ir"]["goal_avversario"];
                                    $goal = $goal>0 ? $goal : 0;
                                    ?>
                                    <a href="#" class="up<?=$class_btn; ?>" data-up="0"></a>
                                    <i><?=$goal; ?></i>
                                    <input type="hidden" name="goal0" value="<?=$goal; ?>">
                                    <a href="#" class="down<?=$class_btn; ?>" data-down="0"></a>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <div class="Goal Goal1">
                                    <?php
                                    $goal = !$Match['in_casa'] ? $Match["giocata_ir"]["goal_fiorentina"] : $Match["giocata_ir"]["goal_avversario"];
                                    $goal = $goal>0 ? $goal : 0;
                                    ?>
                                    <a href="#" class="up<?=$class_btn; ?>" data-up="1"></a>
                                    <i><?=$goal; ?></i>
                                    <input type="hidden" name="goal1" value="<?=$goal; ?>">
                                    <a href="#" class="down<?=$class_btn; ?>" data-down="1"></a>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <div class="LogoDx">
                                    <?php if(!$Match['in_casa']){ ?>
                                        <img src="{{theme}}img/giochi/logo-fiorentina.png" alt="" class="img-responsive">
                                        <br>
                                        <h3>Fiorentina</h3>
                                    <?php }else{ ?>
                                        <img src="<?=$logo_avversario; ?>" alt="" class="img-responsive">
                                        <br>
                                        <h3><?=$Match['avversario']; ?></h3>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12"><div class="text-center">
                                <?php
                                if( $IF_enable && !$IF_giocata  ) {
                                    $class_conferma = '';
                                    $class_modifica = ' hide';
                                }else if( $IF_can_modify ) {
                                    $class_conferma = ' hide';
                                    $class_modifica = '';
                                }else {
                                    $class_conferma = $class_modifica = ' hide';
                                }
                                ?>

                                <button type="submit" data-id="submit-btn" class="Button Big<?=$class_conferma; ?>">Conferma</button>
                                <button type="button" data-id="modify-btn" class="Button Big<?=$class_modifica; ?>">Modifica</button>
                            </div></div>
                        </div>

                    </form></div>
                    <?php
                }else{
                    ?><h2 class="Red text-center"><# Al momento non ci sono partite in calendario #></h2><?php
                }
            }
            ?>
        </div>
    </div>

    <?php
	if( !$id_match ){ 
		$prevMatch = $IF->getMatches('prev',$id_utente);
		if( $prevMatch!="-1" && $prevMatch && strtotime($prevMatch['data'])>mktime(0,0,0,5,5,2015)){ ?>
			<hr>
			<h3 class="text-center"><# L&lsquo;ultima partita #></h3>
			<?php
			include("_ext/include/indovina-risultato-match.inc.php");
		}
	}else{ //Partita selezionata da archivio
		$prevMatch = $IF->getMatches($id_match,$id_utente);
		if( $prevMatch ){
			?><h3 class="text-center"><?=$prevMatch["in_casa"] ? "Fiorentina - {$prevMatch['avversario']}" : "{$prevMatch['avversario']} - Fiorentina";	?></h3><?php
			include("_ext/include/indovina-risultato-match.inc.php");
		}
	}
	?>
    
    <?php
	$archivioMatch = $IF->getMatches('list',$id_utente);
	if( false && is_array($archivioMatch) && count($archivioMatch)>1 ){
		if( !$id_match ){
			unset($archivioMatch[0]);
		}
		?>
    	<hr>
        <div class="row">
        	<div class="col-md-1"></div>
           <div class="col-md-10">
                <table class="table table-striped">
                    <thead>
                        <tr>
	                        <th width="70%" class="uppercase"><# Archivio Partite #></th>
                            <th></th>
                       </tr>
                    </thead>
                    <tbody>
                        <?php foreach($archivioMatch as $item){
							if($item["id"]!=$id_match){
							?>
                            <tr>
	                            <td>
                                    <a href="{{url indovina-formazione}}?id=<?=$item["id"]; ?>" class="uppercase block"><?php
                                        if($item["in_casa"]){
                                            echo "Fiorentina - {$item['avversario']}";
                                        }else{
                                            echo "{$item['avversario']} - Fiorentina";
                                        }
                                    ?></a>
                                </td> 
                               <td>
                                <?=utf8_encode( strftime("%A %d %B %Y", strtotime($item["data"]) ) ); ?>
                               </td>
                           </tr>
                        <?php }
						}
						?>
                    </tbody>
                </table>
			</div>
            <div class="col-md-1"></div>
		</div>
    <?php } ?>

    <hr>
    
    <div class="container"><div class="row"><div class="col-md-10 col-md-offset-1"><section id="regolamento">
    	<h3 class="text-center">Regolamento</h3>
        <br>
        <p class="text-justify">Seleziona il risultato della partita e partecipa al gioco!</p>
        <p class="text-justify;">Se indovini il risultato finale vinci <strong><span style="color: red; font-size: 15px;">40 PUNTI SULLA TUA INVIOLA CARD</span></strong>* per avvicinarti sempre di più al premio che desideri! <a href="{{url premi}}">Clicca qui</a> per scoprire il catalogo premi tutto firmato ACF Fiorentina!</p>
        <p class="text-justify;">La formazione potr&agrave; essere inserita fino a <strong><span style="color: red;"><?=$IF->Ore_margine; ?> ore prima</span></strong> dell'inizio della partita. Una volta inserita, la formazione potrà essere modificata fino allo scadere del tempo disponibile per l’inserimento. Per scoprire se hai vinto collegati alla tua area riservata ed accedi a questa pagina, la formazione ufficiale verr&agrave; inserita il giorno successivo alla gara. Potrai controllare i punti accreditati in caso di vincita sulla pagina "La Mia Card".&nbsp;&nbsp;</p>

        <p class="text-justify;" style="font-size: 12px; line-height: 14px;">*Per risultato finale si intende quello a fine partita, inclusi gli eventuali supplementari.<br>
            Non sono considerati i rigori.
            I punti accreditati ai vincitori possono variare di gara in gara in base alla partita.
        </p>
        
        <p>&nbsp;</p>
    </section></div></div></div>
</div>