<div class="bgWhite">
	<?php //print_r($S->Page); ?>
    <div class="row"><div class="col-md-10 col-sm-10 col-xs-10 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
        <h1><?=$S->Page["name"]; ?></h1>
        <?php if( $subtext ){ ?>
        	<h3 class="Gray uppercase"><?=$S->W($subtext); ?></h3>
            <br>
        <?php } ?>
        
        <div class="row">
        	 <?php
			 /*
			 $arr = array(
			 	"Modello Richiesta Card Gold"=>"InViola - MODELLO RICHIESTA CARD GOLD.pdf",
				"Regolamento InViola Card<br>Annesso Gold"=>"InViola - REGOLAMENTO INVIOLA CARD_ANNESSO GOLD.pdf",
				"Regolamento InViola Card"=>"InViola - REGOLAMENTO INVIOLA CARD.pdf",
				"Provvedimento Garante<br>protezione dati personali"=>"Provvedimento Garante per le protezione dei dati Personali.pdf"
			 );*/
			 $arr = array();
			 $arr[] = array("text"=>"Regolamento utilizzo Inviola Card", 'pdf'=>"Regolamento utilizzo Inviola Card.pdf","subtext"=>false);
			 $arr[] = array("text"=>"Modello richiesta Inviola Card Gold", 'pdf'=>"Modello richiesta Inviola Card Gold.pdf","subtext"=>false);
			 $arr[] = array("text"=>"Modello richiesta Inviola Card<br>(fronte retro)", 'pdf'=>"Modello richiesta Inviola Card (fronte retro).pdf","subtext"=>"<b>IMPORTANTE</b>: il modulo deve essere stampato a colori");
			 $arr[] = array("text"=>"Provvedimento Garante per<br>la protezione dei dati Personali", 'pdf'=>"Provvedimento Garante per le protezione dei dati Personali_10nov.pdf","subtext"=>false);
			 $arr[] = array("text"=>"Informativa sui Cookies", 'pdf'=>"Informativa sui cookies.pdf","subtext"=>false);
			 foreach($arr as $row){
			 ?>
                <div class="col-md-3 col-sm-3 col-xs-12"><div class="relative center">
                    <div style="position: absolute; display: table; height: 3em; width: 100%; top: 150px; color:#63339e;"><div style="display: table-cell; text-align: center; vertical-align:middle;">
                        <?=$row["text"]; ?>
                    </div></div>
                    <img src="{{theme}}img/icon-pdf.png" class="img-responsive">
                    <br><br>
                    <a href="{{root}}docs/d.php?f=<?=$row["pdf"]; ?>" target="_blank" class="Button"><# Download #></a>
                    <br><br>
                    <?php if($row["subtext"]){ echo $row["subtext"]; }else{ ?><br><br><?php } ?>
                    <br>
                    <!--table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                        <td rowspan="2" width="70%" valign="middle" align="center"><span style="color:#909090;"></span></td>
                           <td width="30%"><a href="<?=$file; ?>" target="_blank" class="Button"><# Download #></a></td>
                        </tr>
                    </table-->
                    <br><br>
                </div></div>
			<?php } ?>
        </div>
        
    </div><div class="col-md-1 col-sm-1 col-xs-1"></div></div>
	<br /><br />
</div>