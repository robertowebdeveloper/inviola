<?php
$is_raccoglitore = true;

if( isset($_GET["id"]) && $_GET["id"]>0 ){
	$id = $_GET["id"];
	$q = "SELECT * FROM `{$this->_db_prefix}news_promo` WHERE id = {$id} AND deleted IS NULL AND
	( show_from IS NULL OR show_from <= NOW() )
	AND
	( show_to IS NULL OR NOW() <= show_to )";
	$Scheda = $this->cn->OQ($q);
	
	$is_raccoglitore = $Scheda==-1 ? true : false;
}

if( $is_raccoglitore ){
?>
	<script type="text/javascript"><!--
	$(document).ready(function(e) {
		System.loadNews();
	});
	--></script>
    <div class="bgWhite"><div class="row">
    	<div class="col-md-10 col-sm-10 col-xs-10 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
	        <h1><?=$S->Page["name"]; ?></h1>
    	    <div id="NewsCont" class="row" data-page="0"></div>
        	<div class="row"><div class="col-md-12"><div id="loaderNews" class="center"><img src="{{theme}}img/loaders/1.gif" alt=""></div></div>
	        <div class="row"><div class="col-md-12"><div id="NewsContNext" class="hide"><a class="Button Big" href="#" onclick="System.loadNews();"><# CARICA ALTRI #></a></div></div></div>
		    <br><br>
		</div>
        <div class="col-md-1 col-sm-1 col-xs-1"></div>
	</div></div>
    
<?php }else{ //NEWS SINGOLA ?>
	<div class="bgWhite"><div class="row">
    	<div class="col-md-10 col-sm-10 col-xs-10 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
        	<div class="bread1"><a href="{{url news}}">&laquo; {{urlname news}}</a></div>
        	<div class="News Scheda">
               <h1><?=$Scheda["title"]; ?></h1>
               <div class="time"><?=strftime("%d/%m/%Y %H:%M", strtotime($Scheda["date"]) ); ?></div>
               <div class="descr"><?=$Scheda["description"]; ?></div>
               <div class="social"><?=$S->SocialBar(0); ?></div>
               <br><br>
			</div>
		</div>
        <div class="col-md-1 col-sm-1 col-xs-1"></div>
	</div></div>
<?php } ?>