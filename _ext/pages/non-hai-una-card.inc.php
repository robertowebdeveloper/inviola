<?php
$NewCard = true;
if( isset($_POST["registraCard"]) && $_POST["registraCard"]==1 ){
	$postdata = array(
		"az" => "infocard",
		"fidelyCode" => $_POST["fidelyCode"]
	);
	$dati = $S->FNET($postdata);
	$dati = json_decode($dati);
	$dati = $dati->data;
	//echo "<pre>";print_r($dati);
	
	$NewCard = false;
}
?>
<script type="text/javascript"><!--
$(document).ready(function(e) {
    $("#boxCardExample > div").bxSlider({
		mode: 'fade',
		pager: false,
		controls: false,
		auto: true
	});
	
	//$("#inserisciPassword").slideDown(1000);
});
--></script>
<div class="bgWhite">
    <div class="row"><div class="col-md-10 col-sm-10 col-xs-10 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
        <br><br>
        <?php if($NewCard){ ?>
            <div class="boxRegistrati">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12"><a href="{{url hai-una-card}}" class="tab"><# Hai gi&agrave; una InViola Card? #> <img src="{{theme}}img/arrow_white.png" alt="" class="arrow"></a></div>
                    <div class="col-md-6 col-sm-6 hidden-xs"><a href="#" class="tab fired" style="padding-left:0;"><# Non possiedi una card? #></a></div>
                    <div class="visible-xs col-xs-12"><a href="#" class="tab fired"><# Non possiedi una card? #></a></div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12"><div class="boxRegistratiCont">
                        <br>
                       <img src="{{theme}}img/registrati/card3.png" alt="" class="pull-right img-responsive hidden-xs">
                       <h3 class="Title uppercase"><# REGISTRATI SUBITO! #></h3>
                       <h3 class="Title uppercase"><# TI VERRÀ ASSEGNATA UNA INVIOLA E-CARD CHE TI PERMETTERÀ DI ACCEDERE AI CONTENUTI ESCLUSIVI RISERVATI AI MEMBER! #></h3>
                       <br class="hidden-xs">
                       <hr class="visible-xs">
                       <# E se vorrai, potrai trasformare la tua E-Card in una InViola Card ed iniziare la tua raccolta punti!<br>
    Potrai trasformare la tua E-Card in una InViola Card fisica, recandoti presso uno dei punti ufficiali
    ACF Fiorentina*, oppure in una Inviola Card Virtuale, scaricando l'App Mobile InViola Card. <br><br>
    
    *I Fiorentina Store di via Dupré, del centro commerciale I Gigli e del Mercato Centrale di San Lorenzo, e il Fiorentina Point di viale Fanti. #>
                        <br><br>
                   </div></div>
                </div>
            </div>
		<?php } ?>
        
        <div class="boxRegistrati"><div class="row"><div class="col-md-12 col-sm-12 col-xs-12"><div class="boxRegistratiCont">
        	<form id="registraCardForm" enctype="application/x-www-form-urlencoded" method="post" action="#">
            	<input type="hidden" name="az" value="fnet">
                <input type="hidden" name="sub_az" value="checkDatiRegistrazione">
            	<input type="hidden" name="newcard" value="<?=$NewCard ? 1 : 0; ?>">
               <?php if(!$NewCard){ ?>
	               <input type="hidden" name="fidelyCode" value="<?=$dati->fidelyCode; ?>">
                   <input type="hidden" name="card" value="<?=$dati->card; ?>">
               <?php } ?>
                
           		<?php if($NewCard){ ?><h3><# COMPILA IL FORM DI ISCRIZIONE #></h3>
				<?php }else{ ?><br><h3><# REGISTRA I TUOI DATI PER POTER ACCEDERE #></h3><?php } ?>
                
               <# Prima di compilare il form leggere attentamente l&lsquo;Informativa Privacy #>: <a href="{{root}}docs/Informativa Privacy.pdf" target="_blank"><# leggi #></a>
               
               <br><br>
               <div class="row">
               	<div class="col-md-6 col-sm-6 col-xs-12">
                		<label data-error="name"><# NOME #></label><br>
                		<input type="text" name="name" class="form-control" value="<?=!$NewCard ? $dati->personalInfo->name : ''; ?>" placeholder="<# NOME #>">
                	</div>
                   <div class="col-md-6 col-sm-6 col-xs-12">
                		<label data-error="surname"><# COGNOME #></label><br>
                		<input type="text" name="surname" class="form-control" value="<?=!$NewCard ? $dati->personalInfo->surname : ''; ?>" placeholder="<# COGNOME #>">
                	</div>
               </div>
               <br>
               <div class="row">
               	<div class="col-md-6 col-sm-6 col-xs-12">
                		<?php
						if(!$NewCard){
							$tmp = $dati->personalInfo->birthdate;
							list($tmp) = explode("T",$tmp);
							list($tmp_y,$tmp_m,$tmp_d) = explode("-",$tmp);	
						}
						?>
                		<label data-error="dataNascita" class="uppercase"><# Data di Nascita #></label><br>
                       <input type="hidden" name="dataNascita">
                       <select name="birthday_d" class="form-control" style="width:auto; display: inline-block;">
                       		<option value="">-</option>
	                       	<?php for($i=1;$i<=31;$i++){
								$x = $i<10 ? "0{$i}" : $i;
								$sel = $i==intval($tmp_d) ? ' selected' : '';
								?>
                        		<option value="<?=$i; ?>"<?=$sel; ?>><?=$x; ?></option>
                        	<?php } ?>
                       </select>
                       /
                       <select name="birthday_m" class="form-control" style="width:auto; display: inline-block;">
                       		<option value="">-</option>
	                       	<?php for($i=1;$i<=12;$i++){
								$x = $i<10 ? "0{$i}" : $i;
								$sel = $i==intval($tmp_m) ? ' selected' : '';
								?>
                        		<option value="<?=$i; ?>"<?=$sel; ?>><?=$x; ?></option>
                        	<?php } ?>
                       </select>
                       /
                       <select name="birthday_y" class="form-control" style="width:auto; display: inline-block;">
                       		<option value="">-</option>
                       		<?php for($i=1900;$i<=date("Y");$i++){
								$x = $i<10 ? "0{$i}" : $i;
								/*if( $NewCard || empty($tmp_y) ){
									$sel = $i==date("Y")-35 ? ' selected' : '';
								}else{
									$sel = $i==intval($tmp_y) ? ' selected' : '';	
								}*/
								$sel = $i==intval($tmp_y) ? ' selected' : '';	
								?>
                        		<option value="<?=$i; ?>"<?=$sel; ?>><?=$x; ?></option>
                        	<?php } ?>
                       </select>
                	</div>
                   <div class="col-md-6 col-sm-6 col-xs-12">
                		<label data-error="gender"><# SESSO #></label><br>
                    	<select name="gender" class="form-control" style="width:auto; display: inline-block;">
                        	<option value="">-</option>
                        	<?php
							$arr = array("M","F");
							foreach($arr as $v){
								$sel = !$NewCard && $dati->personalInfo->gender==$v ? ' selected' : '';
								?><option value="<?=$v; ?>"<?=$sel; ?>><?=$v; ?></option><?php
							}
							?>
                       </select>
                   </div>
               </div>
               <br>
               <div class="row">
               	<div class="col-md-12 col-sm-12 col-xs-12">
                		<label><# RESIDENTE #></label><br>
                       <select class="form-control" name="residente" onchange="System.RegResidenza('registraCardForm');" style="width:auto;">
                       	<option value="italia"><# Italia #></option>
                        	<option value="estero"><# All&prime;estero #></option>
                       </select>
                	</div>
               </div>
               <div class="ResEsteroFields hide">
               	<br>
               	<div class="row">
                		<div class="col-md-12 col-sm-12 col-xs-12">
                        	<label data-error="citta_estero"><# CITT&Agrave; #></label><br>
                        	<input type="text" name="citta_estero" class="form-control">
                       </div>
					</div>
               </div>
               <div class="ResItaliaFields">
                   <br>
                   <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <label data-error="regione"><# REGIONE #></label><br>
                        <select name="geoLevel1" class="form-control" onchange="setGeoLevel(2,'registraCardForm','geoLevel');">
                                <option value="0"><# Seleziona Regione #></option>
                                <?php
                                $q = "SELECT * FROM `{$S->_db_prefix}geolevels` WHERE level=1 ORDER BY name ASC";
                                $list = $S->cn->Q($q,true);
                                foreach($list as $v){
                                    $sel = $v["id_ext"]==$dati->personalInfo->geoLevel1 ? ' selected' : '';
                                    ?><option value="<?=$v["id_ext"]; ?>"<?=$sel; ?>><?=$v["name"]; ?></option><?php
                                }
                            ?></select>
                        </div>
                    <div class="col-md-3 col-sm-3 col-xs-12">
                            <label data-error="provincia"><# PROVINCIA #></label><br>
                            <select name="geoLevel2" class="form-control" onchange="setGeoLevel(3,'registraCardForm','geoLevel');">
                                <option value="0">-</option>
                                    <?php
                                    $q = "SELECT * FROM `{$S->_db_prefix}geolevels` WHERE level=2 AND fatherId = {$dati->personalInfo->geoLevel1} ORDER BY name ASC";
                                    $list = $S->cn->Q($q,true);
                                    foreach($list as $v){
                                        $sel = $v["id_ext"]==$dati->personalInfo->geoLevel2 ? ' selected' : '';
                                        ?><option value="<?=$v["id_ext"]; ?>"<?=$sel; ?>><?=$v["name"]; ?></option><?php
                                    }
                                ?>
                           </select>
                        </div>
                       <div class="col-md-3 col-sm-3 col-xs-12">
                            <label data-error="comune"><# COMUNE #></label><br>
                            <select name="geoLevel3" class="form-control" onchange="setGeoLevel(4,'registraCardForm','geoLevel');">
                                <option value="0">-</option>
                                    <?php
                                    $q = "SELECT * FROM `{$S->_db_prefix}geolevels` WHERE level=3 AND fatherId = {$dati->personalInfo->geoLevel2} ORDER BY name ASC";
                                    $list = $S->cn->Q($q,true);
                                    foreach($list as $v){
                                        $sel = $v["id_ext"]==$dati->personalInfo->geoLevel3 ? ' selected' : '';
                                        ?><option value="<?=$v["id_ext"]; ?>"<?=$sel; ?>><?=$v["name"]; ?></option><?php
                                    }
                                ?>
                           </select>
                        </div>
                       <div class="col-md-3 col-sm-3 col-xs-12">
                            <label data-error="localita"><# LOCALIT&Agrave; #></label><br>
                            <select name="geoLevel4" class="form-control">
                                <option value="0">-</option>
                                    <?php
                                    $q = "SELECT * FROM `{$S->_db_prefix}geolevels` WHERE level=4 AND fatherId = {$dati->personalInfo->geoLevel3} ORDER BY name ASC";
                                    $list = $S->cn->Q($q,true);
                                    foreach($list as $v){
                                        $sel = $v["id_ext"]==$dati->personalInfo->geoLevel4 ? ' selected' : '';
                                        ?><option value="<?=$v["id_ext"]; ?>"<?=$sel; ?>><?=$v["name"]; ?></option><?php
                                    }
                                ?>
                           </select>
                        </div>
                   </div>
               </div>
               <br>
               <div class="row">
               	<div class="col-md-6 col-sm-6 col-xs-12">
                		<label data-error="luogoDiNascita"><# LUOGO DI NASCITA #></label><br>
                   	<input type="text" name="luogoDiNascita" class="form-control" value="<?=!$NewCard ? $dati->personalInfo->luogoDiNascita : ''; ?>" placeholder="<# LUOGO DI NASCITA #>">
                	</div>
               	<div class="col-md-6 col-sm-6 col-xs-12">
                		<label data-error="identityCard"><# CODICE FISCALE #></label><br>
                		<input type="text" name="identityCard" class="form-control" value="<?=!$NewCard ? $dati->personalInfo->identityCard : ''; ?>" placeholder="<# CODICE FISCALE #>">
                	</div>
               </div>
               <br>
               <div class="row">
               	<div class="col-md-6 col-sm-6 col-xs-12">
                		<label data-error="idProfessione"><# PROFESSIONE #></label><br>
                		<select name="idProfessione" class="form-control">
                          <option value="0"><# Seleziona Professione #></option>
                          <?php
                            include("_ext/scripts/list_professioni.inc.php");
                            foreach($list as $k=>$v){
                                $sel = $k==$dati->personalInfo->idProfessione ? ' selected' : '';
                                ?><option value="<?=$k; ?>"<?=$sel; ?>><?=$v; ?></option><?php	
                            }
                            ?>
                        </select>
                	</div>
                   <div class="col-md-6 col-sm-6 col-xs-12"></div>
               </div>
               <br>
               <div class="row">
               	<div class="col-md-6 col-sm-6 col-xs-12">
                		<label data-error="address"><# INDIRIZZO #></label><br>
                   	<input type="text" name="address" class="form-control" value="<?=!$NewCard ? $dati->personalInfo->address : ''; ?>" placeholder="<# INDIRIZZO #>">
                	</div>
               	<div class="col-md-3 col-sm-3 col-xs-12">
                		<label data-error="addressNumber"><# N. CIVICO #></label><br>
                		<input type="text" name="addressNumber" class="form-control" value="<?=!$NewCard ? $dati->personalInfo->addressNumber : ''; ?>" placeholder="<# N. CIVICO #>">
                	</div>
                   <div class="col-md-3 col-sm-3 col-xs-12">
                		<label data-error="zip"><# CAP #></label><br>
                		<input type="text" name="zip" maxlength="5" class="form-control onlyNumber" value="<?=!$NewCard ? $dati->personalInfo->zip : ''; ?>" placeholder="<# CAP #>">
                	</div>
               </div>
               <br>
               <div class="row">
               	<div class="col-md-6 col-sm-6 col-xs-12">
                		<label data-error="mobileContactData"><# CELLULARE #></label><br>
                   	<input type="text" name="mobileContactData" class="form-control onlyNumber" value="<?=!$NewCard ? $dati->personalInfo->mobileContactData : ''; ?>" placeholder="<# CELLULARE #>">
                	</div>
               	<div class="col-md-6 col-sm-6 col-xs-12">
                		<label data-error="mailContactData"><# E-MAIL #></label><br>
                		<input type="text" name="mailContactData" class="form-control" value="<?=!$NewCard ? $dati->personalInfo->mailContactData : ''; ?>" placeholder="<# E-MAIL #>">
                       <div class="Gray"><# L'indirizzo e-mail sarà l'user di accesso del sito #></div>
                	</div>
               </div>
               <br>
               <div class="row">
               	<div class="col-md-12 col-sm-12 col-xs-12">
                       <span data-error="privacy1" style="margin-left: 10px; display: inline-block; vertical-align: top; font-family:'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size: 12px;">
                       	<input type="checkbox" name="privacy1" value="1" checked>
                          <!--<# TRATTAMENTO  DATI PERSONALI #><br>-->
                          <# Acconsento al trattamento dei miei dati ai fini di comunicazioni commerciali #>
                       </span>
                       
                       <br><br>
                       
                       <span data-error="privacy2" style="margin-left: 10px; display: inline-block; vertical-align: top; font-family:'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:12px;">
                        	<input type="checkbox" name="privacy2" value="1" checked>
                          <!--<# TRATTAMENTO  DATI PERSONALI #><br>-->
                          <# Acconsento al trattamento dei miei dati ai fini di profilazione #>
                       </span>
                       
                       <br><br>
                       
                       <span data-error="privacy3" style="margin-left: 10px; display: inline-block; vertical-align: top; font-family:'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:12px;">
                        	<input type="checkbox" name="privacy3" value="1" checked>
                          <!--<# TRATTAMENTO  DATI PERSONALI #><br>-->
                          <# Ho letto e ben compreso quanto riportato a tergo del presente modello che ho compilato prestando attenzione a tutte le sue parti, accetto quanto riportato sul Regolamento InViola Card sul regolamento dell&rsquo;Operazione a premi "InViola Fidelity" a cui aderisco, inoltre, come previsto dall&rsquo;art. 26 comma 1 del D. Lgs. 196/2003, esprimo il consenso ai trattamenti dei dati sensibili eventualmente acquisiti nell&rsquo;ambito delle operazioni a premi. #>
                       </span>
                       
                	</div>
               </div>
               <br>
               <div class="right">
               	<a id="RegistraCard_btn" href="#" onclick="System.RegistraCard();" class="Button Big"><# Registrati #></a>
                	<img id="RegistraCard_loader" class="hide" src="{{theme}}img/loaders/3.gif" alt="">
               </div>
               <br>
           </form>
           <form id="successForm" enctype="application/x-www-form-urlencoded" method="get" action="{{url home}}"></form>
        </div></div></div></div>
        
        
        <div class="hide"><div id="CongratulazioniCard" class="boxRegistrati">
        	<div class="boxRegistratiCont">
                <img src="{{theme}}img/registrati/card3.png" alt="" class="pull-right img-responsive">
                <br>
                <h3 class="Title uppercase"><# Complimenti! #></h3>
                <br>
                <# Hai registrato correttamente i tuoi dati.<br>Ti abbiamo appena inviato una e-mail è stata inviata al tuo indirizzo, conferma i dati cliccando sul link di conferma per ottenere la tua nuova InViola Card. #>
                <br><br>
                <# Se non dovessi ricevere l&lsquo;email entro pochi minuti controlla che il tuo filtro anti-spam non l’abbia bloccata per errore e cercala nella cartella "posta indesiderata". #>
                <br><br>
            </div>
        </div></div>
        
    </div><div class="col-md-1 col-sm-1 col-xs-1"></div></div>
	<br /><br />
</div>