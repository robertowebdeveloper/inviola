<div class="bgWhite">
    <div class="row"><div class="col-md-10 col-sm-10 col-xs-10 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
        <br><br>
        <div class="boxRegistrati">
        	<div class="row">
            	<div class="col-md-12 col-sm-12 col-xs-12"><div class="tab fired"><# Recupera Password #></div></div>
            </div>
            <div class="row">
            	<div class="col-md-12 col-sm-12 col-xs-12"><div class="boxRegistratiCont">
                	<form id="recuperaPwForm" enctype="application/x-www-form-urlencoded" method="post" action="#">
                    	<input type="hidden" name="az" value="fnet">
                    	<input type="hidden" name="sub_az" value="recuperaPWFromEmail">
                    	<div class="row">
                        	<div class="col-md-12 col-sm-12 col-xs-12">
                                <span class="Title2 Viola"><# Inserisci il tuo indirizzo e-mail per recuperare la password #></span>
                                <br><br>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-6"><input type="text" name="email" placeholder="<# E-mail #>" class="form-control"></div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                    		<a href="#" onclick="System.RecuperoPW();" class="Button Big"><# Vai #></a>
	                                      <img id="loaderLogin" class="hide" src="{{theme}}img/loaders/3.gif" alt="">
                                    </div>
                                </div>
                                
                                <br>
                                <div id="msg" class="center Red"></div>
                                <br>
                        	</div>
						</div>
                   </form>
               </div></div>
            </div>
        </div>
        
    </div><div class="col-md-1 col-sm-1 col-xs-1"></div></div>
	<br /><br />
</div>