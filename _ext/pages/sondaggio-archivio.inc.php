<script type="text/javascript"><!--
var Polls = {
	opened: null,
	
	ms: 350,
	easingIn: 'easeInSine',
	easingOut: 'easeOutSine',	
	
	Open: function(id){
		var This = this;
		if(this.opened==id){
			this.Close(id);
		}else{
			if( this.opened>0 ){
				this.Close(id,function(){
					This.Open(id);
				});
			}else{
				$("#poll_" + id + " .answers").slideDown( this.ms, this.easingIn );
				this.opened = id;
			}
		}
	},
	
	Close: function(id,callback){
		$("#poll_" + id + " .answers").slideUp( this.ms, this.easingOut );
		this.opened = null;
		if( callback ){
			callback();	
		}
	}
};
--></script>
<div class="bgWhite" style="background:url({{root}}_ext/themes/default/img/logo_big_background2.png) right top no-repeat #FFF;"><div class="row"><div class="col-md-1 col-sm-1 col-xs-1"></div><div class="col-md-10 col-sm-10 col-xs-10">
	<h1><# Archivio sondaggi #></h1>
    
    <?php
	$qBase = "SELECT * FROM `{$S->_db_prefix}polls` WHERE
		enable_on=1 AND deleted IS NULL
		AND
		`to` < NOW()";
	$order = "ORDER BY `from` DESC";
	
	$q = $qBase . $order;
	$polls = $S->cn->Q($q,true);
	
	$iPoll=0;
	foreach($polls as $poll){
		$answers = array();
		$user_rate = false;
		$q = "SELECT * FROM `{$S->_db_prefix}polls_answers` WHERE id_poll = {$poll[id]}";
		$answers = $S->cn->Q($q,true);
		
		//Cerca id utente
		$q = "SELECT id FROM utenti WHERE customer_id = {$_SESSION[customer_id]}";
		$id_utente = $S->cn->OF($q);
		
		foreach($answers as $ans){		
			$q = "SELECT * FROM `{$S->_db_prefix}polls_answers_rate` WHERE id_answer={$ans[id]} AND id_user = {$id_utente}";
			$S->cn->Q($q);
			if( $S->cn->n>0 && !$user_rate){
				$user_rate = $ans["id"];
			}
		}
	
		if( count($answers)>0 ){
			//calcolo totale voti
			$q = "SELECT * FROM `{$S->_db_prefix}polls_answers_rate` r INNER JOIN `{$S->_db_prefix}polls_answers` a ON r.id_answer = a.id WHERE a.id_poll = {$poll[id]}";
			$S->cn->Q($q);
			$tot_rates = $S->cn->n;
		?>
        	<div class="row">
            	<div class="col-md-12 col-sm-12 col-xs-12">
                	<div id="poll_<?=$poll["id"]; ?>" class="Poll">
                        <div>
                        	<?=strftime("%d/%m/%Y", strtotime($poll["from"])); ?><br>
                          <a href="#" class="Title2" onclick="Polls.Open(<?=$poll["id"]; ?>);"><?=$poll["question"]; ?></a>
                        </div>
                        <div class="answers" style="display: none;">
							<?php foreach($answers as $ans){
                                $q = "SELECT * FROM `{$S->_db_prefix}polls_answers_rate` r WHERE r.id_answer = {$ans[id]}";
                                $S->cn->Q($q);
                                $rates = $S->cn->n;
                                $perc = round($rates*100/$tot_rates);
                                ?>
                                <span style="color:#747474; font-size: 20px;"><?=$ans["answer"]; ?></span>
                                <div>
                                    <div style=" width: 400px; display:inline-block;height:40px; border: 1px solid #9a9a9a;">
                                        <div style="width: <?=$perc; ?>%; <?=$perc>0 ? '' : ' display: none;'; ?> height: 38px; background-color: #462a9b;"></div>
                                    </div>
                                    <div style="display: inline-block; line-height: 40px; color:#462a9b; font-size: 30px; vertical-align: top; height: 40px; margin-left: 10px;"><?=$perc>0 ? "{$perc}%" : ''; ?></div>
                                </div>
                                <br />
                          <?php } ?>
                          
                          <?php if(!empty($poll["description"])){ ?>
                              <div class="code">
                                  <hr>
                                  <div><?=$poll["description"]; ?></div>
                              </div>
                          <?php } ?>
						</div>
                	</div>
				</div>
            </div>
            
		 <?php }else{ ?>
			<h1 class="Red"><# Spiacente, nessun sondaggio presente #></h1>
		<?php } ?>
	<?php
		$iPoll++;
	}
	?>
    <br>
</div><div class="col-md-1 col-sm-1 col-xs-1"></div></div></div>