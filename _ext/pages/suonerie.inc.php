<div class="bgWhite">
    <div class="row"><div class="col-md-10 col-sm-10 col-xs-10 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
        <h1><# SUONERIE #></h1>
        
        <div class="row">
			<?php
            $q = "SELECT * FROM `{$S->_db_prefix}downloads` WHERE type='suonerie' AND deleted IS NULL ORDER BY `order` ASC";
            $list = $S->cn->Q($q,true);
                foreach($list as $v){
					 list($type,$filename) = explode("|",$v["option"]);
                    switch($type){
                        case "mp3":
                            $img = "icon-mp3.png";
                            break;
                        case "m4r":
                            $img = "icon-m4r.png";
                            break;
                    }
             ?>
            <div class="col-md-3 col-sm-4 col-xs-4 center">
                 <div style="position: relative;"><img src="{{root}}_ext/themes/default/img/<?=$img; ?>" class="img-responsive">
                    <div style="position: absolute; width: 100%; color:  text-align: center;bottom: 20%; color:#63339e;">
                        <?=$v["label"]; ?>
                    </div>
                 </div><br>
                 <?php
				 $pathfile = $S->pathFile($v["id_file"]);
				 $pathfile = substr($pathfile,strlen("_public/")+1);
				 ?>
                <a href="{{root}}_public/d.php?f=<?=$pathfile; ?>&n=<?=$filename; ?>" target="_blank" class="Button">Download</a>
            </div>
			<?php } ?>
        </div>
        
    </div><div class="col-md-1 col-sm-1 col-xs-1"></div></div>
	<br /><br />
</div>