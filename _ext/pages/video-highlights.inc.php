<?php
$_video_section = isset($_video_section) ? $_video_section : 'highlights';
$_video_widget_id = isset($_video_widget_id) ? $_video_widget_id : 'd414c86990854031bfb5525cdb4a88a9';
$videoAds = $S->getVideoAds( $_video_section );
//print_r($videoAds);
?>
<div class="bgWhite">
	<?php //print_r($S->Page); ?>
    <div class="row"><div class="col-md-10 col-sm-10 col-xs-10 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
        <?php if(false){ ?><h1><?=$S->Page["name"]; ?></h1><?php } ?>
        
        <div class="pageStd">
			<?=$S->Page["html"]; ?>
           
           <div class="VideoMediaCenter">
	           <div data-type="MediaCenter" id="49411" data-widgetid="<?=$_video_widget_id; ?>"></div>
               <?php if($videoAds && $videoAds["type"]=='image'){
				   $S->video_preroll = false;
				   ?>
               	<div class="VideoAds-image" data-id="<?=$videoAds['id']; ?>" data-timing="<?=$videoAds['timing']; ?>">
                       <a href="#" class="VideoAds_close" onclick="System.VideoAds.Close(<?=$videoAds['id']; ?>);"><img src="{{theme}}img/icons/close16.png" alt=""></a>
                        <?php
                        if( !empty($videoAds["url"]) ){
                            $blank = $videoAds['blank']==1 ? ' target="_blank"' : '';
                            ?><a href="<?=$videoAds["url"]; ?>"<?=$blank; ?> onclick="javascript:ga('send', 'event', 'Link VideoAd-image: <?=$videoAds['url']; ?>', '<?=$videoAds['url']; ?>', '<?=$videoAds['url']; ?>', 1);"><?php
                        }
                        ?><img src="<?=$S->pathFile($videoAds["id_file"]); ?>" alt=""><?php
                        if( !empty($videoAds["url"]) ){
                            ?></a><?php
                        }
                        ?>
                   </div>
              <?php }else if($videoAds && $videoAds["type"]=="preroll"){
				  $S->video_preroll = true;
				  ?>
              		<div id="VideoPreroll" class="VideoAds-preroll" data-id="<?=$videoAds['id']; ?>" data-timing="<?=$videoAds['timing']; ?>">
                    	<div class="text-center"><?=$videoAds["ext_code"]; ?></div>
                       <?php if( !empty($videoAds["url"]) ){
                            $blank = $videoAds['blank']==1 ? ' target="_blank"' : '';
                            ?><a href="<?=$videoAds["url"]; ?>"<?=$blank; ?> onclick="javascript:ga('send', 'event', 'Link VideoAd-preroll: <?=$videoAds['url']; ?>', '<?=$videoAds['url']; ?>', '<?=$videoAds['url']; ?>', 1);"></a>                       <?php } ?>
                       <div class="AdBar">
                       	<a href="#">Skip Ad &raquo;</a>
                       	Pubblicit&agrave;
                       </div>
                   </div>
			   <?php } ?>
           </div>
        </div>
        
    </div><div class="col-md-1 col-sm-1 col-xs-1"></div></div>
	<br /><br />
</div>