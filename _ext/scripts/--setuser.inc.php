<?php
//NEL CASO DI REGISTRAZIONE DA LINK E-MAIL
if( isset($_GET["cf"]) && strlen($_GET["cf"])>0 ){
	$codice_conferma = base64_decode( $_GET["cf"] );
	$tmp = explode("&",$codice_conferma);
	$postdata = array();
	foreach($tmp as $tmp_v){
		list($k,$v) = explode("=",$tmp_v);
		if( $k!="az"){
			$postdata[	$k ] = $v;
		}
	}
	
	$q = "SELECT * FROM `utenti` WHERE `email` = '{$postdata['mailContactData']}'";
	$S->cn->Q($q);
	$nUtenti = $S->cn->n;
	if( isset($_GET["pre"]) ){
		echo $q;
		echo "<pre>";print_r($postdata);
		echo "nUtenti: {$nUtenti}";
		echo "</pre>";exit;	
	}
	if( $nUtenti==0 ){
		$postdata["az"] = 'registraCard';
		$res = $S->FNET($postdata);
		$res = json_decode($res);
	}
	if( isset($_GET["test"]) ){
		echo "<pre>";print_r($postdata);print_r($res);echo $codice_conferma;exit;
	}
	
	$_POST["login"] = $postdata["mailContactData"];
	$_POST["pw"] = $postdata["code"];
	$_POST["new_user"] = true; //fa apparire messaggio di benvenuto
}

//Controllo Cookie e auto-login
if( !isset($_SESSION["customer_id"]) && strlen($_COOKIE["rc_c_id"])>0 ){
	$str = substr($_COOKIE["rc_c_id"],0,32);
	$q = "SELECT * FROM utenti WHERE MD5(customer_id) = '{$str}' LIMIT 1";
	$U = $S->cn->OQ($q);
	if( $U!=-1 ){
		$_POST["login"] = $U["email"];
		$_POST["pw"] = $U["pw"];
		$_POST["resta_connesso"]=1;	
	}
}

if( isset($_POST["login"]) && strlen($_POST["login"])>0 ){ //PER LOGIN		
	$postdata = array(
		"az" => 'loginUtenteFull',
		"login" => $_POST["login"],
		"pw" => $_POST["pw"]
	);
	$res = $S->FNET($postdata);
	//echo $res;
	$res = json_decode($res);
	//echo "<pre>";print_r($res);exit;
	if( $res->status==1){
		$S->_isLogged = true;
		$_SESSION["customer_id"] = $res->data->customer->id;
		$_SESSION["card"] = $res->data->customer->card;
		$_SESSION["saldo"] = $res->data->customer->balance_points;
		$_SESSION["name"] = $res->data->customer->name;
		$_SESSION["surname"] = $res->data->customer->surname;
		$_SESSION["birthdate"] = $res->data->customer->birthdate;
		$_SESSION["gender"] = $res->data->customer->gender;
	}
	
	if( $_POST["resta_connesso"]==1 ){
		$str = substr(md5(time()),0,14);
		$expire = time() + (60*60*24*30); //scadenza a 1 mese
		setcookie('rc_c_id', md5($res->data->customer->id) . $str, $expire, path_webroot );
	}	
}else if( isset($_POST["logout"]) && $_POST["logout"]==1 ){
	unset($_SESSION["customer_id"]);
	$expire = time()-100000;
	setcookie('rc_c_id', '', $expire, path_webroot );//reset cookie
	$S->_isLogged = false;
}

if( isset($_SESSION["customer_id"]) ){
	$S->_customer_id = $_SESSION["customer_id"];
	$S->_isLogged = true;
	
	$q = "DELETE FROM `{$S->_db_prefix}fidely_cache` WHERE DATE_ADD(`timestamp`, INTERVAL 1 HOUR) < NOW()";
	$S->cn->Q($q);
	
	if( in_array($S->Page["k"],array("la-mia-card")) ){//Disabilito cache utente per alcune pagine
		$data = false;
	}else{	
		$q = "SELECT `data` FROM `{$S->_db_prefix}fidely_cache` WHERE session_id = '" . session_id() . "' AND customer_id = {$S->_customer_id} ORDER BY `timestamp` DESC LIMIT 1";
		$data = $S->cn->OF($q);
	}
	
	if( !$data || $S->Page['k']=='il-mio-profilo' ){
		$postdata = array(
			"az" => 'infocardByCustomer',
			"customer_id" => $S->_customer_id
		);
		
		$S->_infoUser = $S->FNET($postdata);
		if( isset($_GET["test"]) ){
			echo "<pre>";print_r($S->_infoUser);exit;
		}
		
		$q = "INSERT INTO `{$S->_db_prefix}fidely_cache` (session_id,customer_id,`data`) VALUES ('" . session_id() . "', {$S->_customer_id}, '{$S->_infoUser}')";
		$S->cn->Q($q);
	}else{
		$S->_infoUser = $data;
	}
	$S->_infoUser = json_decode( $S->_infoUser );
}else{
	$S->_customer_id = false;
	$S->_isLogged = false;
}

/*
dati a disposizione su risposta di LOGIN
stdClass Object
(
    [status] => 1
    [msg] => 
    [data] => stdClass Object
        (
            [answerCode] => 0
            [customer] => stdClass Object
                (
                    [id] => 32965
                    [campaign] => 282
                    [card] => 888888888888
                    [fidelyCode] => 0
                    [identityCard] => RSSMRA80A01D612Y
                    [category] => 422
                    [status] => 1
                    [name] => Mario
                    [surname] => Rossi
                    [gender] => M
                    [birthdate] => 1980-01-01T00:00:00.000+01:00
                    [userName] => mario.rossi@test.it
                    [password] => 75829
                    [pincode] => 1234
                    [expiration] => 2015-05-28T00:00:00.000+02:00
                    [flags] => stdClass Object
                        (
                            [flags] => 0
                            [activatedFromPointsCharge] => 
                            [nameChanged] => 
                            [failCardActivation] => 
                            [migratedFromFNet1] => 
                            [migratedFromFNet2] => 
                            [fatherExtension] => 
                            [activatedOfflineWithoutPoints] => 
                            [merged] => 
                            [imported] => 
                            [wonPointsByCompleteAllData] => 
                            [decrementCardStockByImport] => 
                            [notGivePointsOnActivationFromExternalComponent] => 
                        )

                    [privacy] => stdClass Object
                        (
                            [usedForPromotions] => 1
                            [usedForStatistics] => 1
                            [usedByOthers] => 
                        )

                    [cardType] => 0
                    [languageId] => 2
                    [pointsCharged] => 67283
                    [pointsUsed] => 3390
                    [pointsStatusCharged] => 0
                    [pointsStatusUsed] => 0
                    [pointsMLMCharged] => 0
                    [pointsMLMUsed] => 0
                    [creditsCharged] => 0
                    [creditsUsed] => 0
                    [creditsGiftCharged] => 0
                    [creditsGiftUsed] => 0
                    [rechargesCard] => 8
                    [usesCard] => 2
                    [lastMovement] => 2014-05-28T10:06:14.663+02:00
                    [mailContactData] => mario.rossi@test.it
                    [mobileContactData] => 3283417644
                    [address] => Via Buonvicini
                    [addressNumber] => 21
                    [zip] => 50132
                    [parentCustomerId] => 0
                    [percentajePointsParentCustomer] => 0
                    [percentajeCreditsParentCustomer] => 0
                    [mlmCustomerId] => 0
                    [geo_lat] => 43.7822939
                    [geo_long] => 11.2689223
                    [country] => 3
                    [geoLevel1] => 96
                    [geoLevel2] => 8281
                    [geoLevel3] => 56884
                    [geoLevel4] => 1198
                    [geoLevel5] => 0
                    [balance_points] => 63893
                    [balance_credits] => 0
                    [balance_gift_credits] => 0
                    [balance_status_points] => 0
                    [customerDynamicFields] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [id] => 189
                                    [value] => 268
                                )

                            [1] => stdClass Object
                                (
                                    [id] => 228
                                    [value] => Firenze
                                )

                        )

                )

            [campaign] => stdClass Object
                (
                    [id] => 282
                    [code] => 312
                    [name] => inVIOLA CARD
                    [imageURL] => files/dev/campaigns/282/LogoinVIOLA60.png
                    [amountLastMovements] => 10
                    [pointsTag] => Punti
                    [creditsTag] => Crediti
                    [amountPrizes] => 0
                    [amountDayNewsPrizes] => 30
                    [rights] => stdClass Object
                        (
                            [canModifyContactInformation] => 1
                            [canModifyPassword] => 1
                            [canChargePointsWithQRProducts] => 
                            [canChargePointsWithScratchoff] => 
                            [canExchangePrizes] => 1
                            [canChargePointsWithVoucher] => 
                        )

                    [operativeFlagsDTO] => stdClass Object
                        (
                            [flags] => 2525808
                            [useNameAsLoginInCustomersArea] => 
                            [useCardAsLoginInCustomersArea] => 
                            [useMailAsLoginInCustomersArea] => 1
                            [useCustomerSearchByDNI] => 
                            [useCustomerSearchByCard] => 1
                            [useCustomerSearchByBirthdate] => 1
                            [useCustomerSearchByCustomerID] => 1
                            [usePartialDeliverPrizes] => 
                            [useCentralizedIssueCards] => 
                            [useCustomerIDAsLoginInCustomersArea] => 
                            [showMinimumInfoinCustomerSearch] => 1
                            [askTicketInfoInSale] => 
                            [askBirthdateInReplaceCard] => 1
                            [askDNIInReplaceCard] => 
                            [showNetMovements] => 1
                            [useManualAmountInsteadProducts] => 
                            [useMultipleExchange] => 
                        )

                )

        )

)

--------------------

$_infoUser:
stdClass Object
(
    [status] => 1
    [msg] => 
    [data] => stdClass Object
        (
            [id] => 32965
            [campaignId] => 282
            [card] => 888888888888
            [fidelyCode] => 0
            [parentCustomerId] => 0
            [mlmCustomerId] => 0
            [registration_shop_id] => 1048
            [registration_net_id] => 1047
            [registration_shop_foreign_id] => 2821048
            [registration_net_foreign_id] => 0
            [personalInfo] => stdClass Object
                (
                    [identityCard] => RSSMRA80A01D612Y
                    [name] => Mario
                    [surname] => Rossi
                    [gender] => M
                    [birthdate] => 1980-01-01T00:00:00.000+01:00
                    [notes] => 
                    [userName] => mario.rossi@test.it
                    [privacy] => stdClass Object
                        (
                            [flags] => 3
                            [usedForPromotions] => 1
                            [usedForStatistics] => 1
                            [usedByOthers] => 
                            [canGetCurrentLocation] => 
                            [canComunicaVerification] => 
                        )

                    [mailContactData] => mario.rossi@test.it
                    [mobileContactData] => 3283417644
                    [telephoneContactData] => 
                    [faxContactData] => 
                    [address] => Via Buonvicini
                    [addressNumber] => 21
                    [addressPrefix] => 
                    [zip] => 50132
                    [country] => 3
                    [geoLevel1] => 96
                    [geoLevel2] => 8281
                    [geoLevel3] => 56884
                    [geoLevel4] => 1198
                    [geoLevel5] => 0
                    [facebookId] => 
                    [customerDynamicFields] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [id] => 189
                                    [value] => 268
                                )

                            [1] => stdClass Object
                                (
                                    [id] => 228
                                    [value] => Firenze
                                )

                        )

                    [ComuneSel_label] => Firenze
                    [dataNascita] => 1/1/1980
                    [dataNascita_label] => 01/01/1980
                    [professione_label] => Operaio/a
                    [idProfessione] => 268
                    [luogoDiNascita] => Firenze
                )

            [balanceData] => stdClass Object
                (
                    [category] => 422
                    [status] => 1
                    [cardType] => 2
                    [pointsCharged] => 67283
                    [pointsUsed] => 3390
                    [creditsCharged] => 0
                    [creditsUsed] => 0
                    [creditsGiftCharged] => 0
                    [creditsGiftUsed] => 0
                    [rechargesCard] => 8
                    [usesCard] => 2
                    [balance_points] => 63893
                    [balance_credits] => 0
                    [balance_gift_credits] => 0
                    [total_money_in_sale] => 67283
                    [paid_money_in_sale] => 67283
                    [pointsChargedCount] => 0
                    [pointsUsedCount] => 0
                    [creditsUsedCount] => 0
                    [creditsGiftUsedCount] => 0
                )

            [datiIncompleti] => 0
            [cellVerificato] => 1
        )

)
*/
?>