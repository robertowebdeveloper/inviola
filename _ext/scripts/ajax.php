<?php
$relative_path = "../../";
require("main.inc.php");

if(isset($_GET["az"])){
	$_POST = $_GET;	
}

$az = $_POST["az"];
echo Ajax($az);

$cn->Close();
unset($cn,$S);

function Ajax($az){
	global $cn,$S;
	
	$_db_prefix = db_prefix;
	$path_theme = path_webroot . "_ext/themes/" . $S->P("theme") . "/";
	
	switch($az){
        case 'cookies_privacy_alert_off':
            $_SESSION['coap_hide'] = 1;
            $_ret = array("status"=>1);
            break;
        case 'set_cookies_privacy':
            $cookies_approved = $_POST['cookies_approved'];
            $S->setCookiesApproved($cookies_approved);
            $_ret = array("status"=>1);

            break;
		case 'login':
			$login = $_POST["login"];
			$pw = $_POST["pw"];
			
			$errors = array();
			if( empty($login) ){
				$errors[] = "Inserire l'indirizzo e-mail";
			}else if( !filter_var($login, FILTER_VALIDATE_EMAIL) ){
				$errors[] = "Indirizzo e-mail non valido";
			}
			if( empty($pw) ){
				$errors[] = "Inserire password";
			}
			
			if( count($errors)==0 ){
				$_POST["sub_az"] = "loginUtenteFull";
				return Ajax('fnet');
			}else{
				$html = '<div class="Warning">';
				$html .= "<ul>";
				foreach($errors as $e){
					$html .= "<li>{$e}</li>";
				}
				$html .= "</ul></div>";
				$_ret = array("status"=>0,"msg"=>$html);
				usleep(500000);//mezzo secondo
			}
			break;
		case 'fnet':
			$postdata = array();
			foreach($_POST as $k=>$v){
				if( $k=="sub_az" ){
					$postdata["az"] = $v;
				}else if($k!="az"){
					$postdata[$k] = $v;	
				}
			}
			$res = $S->FNET( $postdata );
            $_ret = json_decode( $res );
			break;
		case 'closeUserAlert':
			$q = "UPDATE `{$_db_prefix}users_alert` SET closed = NOW() WHERE id_utente = {$_POST['id_utente']} AND id IN ({$_POST['users_alert_ids']})";
			$cn->Q($q);
			$_ret = array("status"=>1);
			break;
		case 'loadNews':
			$page = $_POST["page"];
			$items_x_page = 12;
			
			$lim1 = $page*$items_x_page;
			
			$qBase = "SELECT * FROM `{$_db_prefix}news_promo` WHERE deleted IS NULL AND
			( show_from IS NULL OR show_from < NOW() )
			AND
			( show_to IS NULL OR NOW() < show_to )
			ORDER BY `date` DESC";
			
			$q = $qBase . " LIMIT {$lim1},{$items_x_page}";
			
			$cn->Q($qBase);
			$tot = $cn->num_rows;
			
			$list = $cn->Q($q,true);
			$founded = count($list);
			/* for test 
			$new = array();
			for($i=0;$i<10;$i++){
				$new[] = $list[0];
			}
			$list = $new;
			/* end for test */
			
			$maxpages = ceil( $tot / $items_x_page );
			$end = $page>=$maxpages-1 ? 1 : 0;
			
			$html = array();
			
			foreach($list as $v){
				$html[] = '<div class="col-md-3 col-sm-3 col-xs-12"><div class="itemNews">';
				
					$html[] = '<div class="itemNewsCont"><a href="' . $S->getUrl('news',$v["id"]) . '">';
					$html[] = '<i></i>';
					
					$text = strlen($v["description_short"])>0 ? $v["description_short"] : strip_tags($v["description"]);
					$text = substr($text,0,200)."&hellip;";
					
					$html[] = '<div class="descr">'.$text.'</div>';
					$html[] = '<div class="bottom"	><div>
						<span class="iconNews"></span>
						' . strftime("%d.%m.%Y&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;%H:%M", strtotime( $v["date"]) ) . '
					</div></div>';
					
					$html[] = '</a></div>';
					
					$html[] = '<div class="shadow"><img src="' . $path_theme . '/img/box_shadow_news.png" class="img-responsive"></div>';
					
				$html[] = '</div></div>';
			}
			
			
			
			$_ret = array("status"=>1,"html"=>implode("",$html), "page"=>($page+1), "end"=>$end );
			break;
		case 'loadInviaciLeTueFoto':
			$page = $_POST["page"];
			$items_x_page = 12;
			
			$q = "SELECT * FROM `{$S->_db_prefix}users_gallery` WHERE approved IS NOT NULL GROUP BY id_user";
			$cn->Q($q);
			$tot = $cn->n;
			//paginazione
			$maxpages = ceil( $tot / $items_x_page );
			$end = $page>=$maxpages-1 ? 1 : 0;
			$page = $page>=$maxpages-1 ? $maxpages : $page;
			$lim1 = $page*$items_x_page;

			$q = "SELECT * FROM (
				SELECT * FROM `{$S->_db_prefix}users_gallery` WHERE approved IS NOT NULL ORDER BY approved DESC
				) AS ug
				GROUP BY id_user ORDER BY approved DESC
				LIMIT {$lim1},{$items_x_page}";
			$list = $cn->Q($q,true);
			
			$html = array();
			foreach($list as $item){
				//$avatar = $S->getAvatar(true,$item["id_user"]);
				$q = "SELECT * FROM utenti WHERE id = {$item['id_user']}";
				$v = $S->cn->OQ($q);
				$c_id = $v["customer_id"];
				$id_avatar = $v["id_avatar"]>0 ? $v["id_avatar"] : false;
				if( $fidely_data && $fidely_data->customer->personalInfo->name ){
					$v['name'] = $fidely_data->customer->personalInfo->name;
					$v['surname'] = $fidely_data->customer->personalInfo->surname;
				}else{
					$v["name"] = empty($v["name"]) ? "-" : $v["name"];
					$v["surname"] = empty($v["surname"]) ? "-" : $v["surname"];
				}
				
				$q = "SELECT COUNT(DISTINCT id) AS n FROM `{$S->_db_prefix}users_gallery` WHERE approved IS NOT NULL AND id_user = {$item['id_user']}";
				$n = $cn->OF($q);
				
				$html[] = '<div class="col-md-3 col-sm-4 col-xs-6"><div class="fotoUtentiItem">';
					if( $n>1 ){
						$html[] = '<div class="badge">' . $n . '</div>';	
					}
					$html[] = '<div class="foto"><a href="' . $S->getUrl('inviaci-foto',$item["id_user"]) . '"><img src="' . $S->Img($item["id_file"],array( "w"=>230,"h"=>130,"m"=>"square","bg"=>"FFF" ) ) . '" alt="" class="img-responsive"></a></div>';
					$html[] = '<div class="shadow"><img src="' . $path_theme . 'img/shadow_premio_img.png" class="img-responsive" alt=""></div>
					   <table width="100%" cellspacing="0" cellpadding="0">
							<tbody>
								<tr>
									<td width="20%">';
									
									if($id_avatar){
										$html[] = '<img src="' . $S->Img($id_avatar,array("w"=>45,"h"=>45,"m"=>"square") ) .'" class="img-responsive" alt="">';
									}else{
										$html[] = '<img src="' . $path_theme . 'img/avatar.png" class="img-responsive" alt="">';
									}
									
									$name = empty($v['name']) && empty($v['surname']) ? '-' : "{$v['name']} {$v['surname']}";
									$html[] = '
									</td>
								  	<td width="5%">&nbsp;</td>
									<td width="75%" valign="middle" bgcolor="#eee" align="left">
									  ' . $S->W('LE FOTO DI') . '<br>
									  <span class="Red uppercase">' . $name . '</span>
									</td>
								</tr>
								<tr>
							</tbody>
						</table>
					  <br><br>
				   </div></div>';
			}
			$_ret = array("status"=>1,"html"=>implode("",$html), "page"=>($page+1), "end"=>$end );
			break;
		case 'IndovinaFormazioneSave':
			$_errors = array();
			
			if( !($_POST["id_utente"]>0) ){
				$_errors[] = "Utente non riconosciuto";	
			}
			if( !($_POST["id_formazione"]>0) ){
				$_errors[] = "Formazione non riconosciuta";
			}
			
			if( !($_POST["id_match"]>0) ){
				$_errors[] = "Partita non riconosciuta";
			}
			for($i=1,$error_player=false;$i<=11;$i++){
				if( !($_POST["id_giocatore{$i}"]>0) ){
					$error_player = true;	
				}
			}
			if( $error_player ){
				$_errors[] = "Uno o pi&ugrave; giocatori non sono stati selezionati";	
			}
			
			if( count($_errors)>0 ){
				$html = '<div class="Warning">';
				$html .= "<ul>";
				foreach($_errors as $e){
					$html .= "<li>{$e}</li>";
				}
				$html .= "</ul></div>";
				$_ret = array("status"=>0,"msg"=>$html);
			}else{
				$q = "SELECT * FROM `{$_db_prefix}gf_formazione_match` WHERE id_utente={$_POST['id_utente']} AND id_match = {$_POST['id_match']}";
				$res = $cn->Q($q);
				
				if( $cn->n==0 ){
					$q = "INSERT INTO `{$_db_prefix}gf_formazione_match` (id_utente,id_match) VALUES ({$_POST['id_utente']},{$_POST['id_match']})";
					$cn->Q($q);
					$updated = "NULL";
				}else{
					$updated = "NOW()";	
				}
				$q = "UPDATE `{$_db_prefix}gf_formazione_match` SET
					updated = {$updated},
					id_formazione = {$_POST['id_formazione']}";
				for( $i=1;$i<=11;$i++){
					$q .= ", id_giocatore{$i} = " . $_POST["id_giocatore{$i}"];
				}
				$q .= " WHERE id_utente = {$_POST['id_utente']} AND id_match = {$_POST['id_match']}";
				$cn->Q($q);
				
				$_ret = array("status"=>1);
			}
			break;
        case 'IndovinaRisultatoSave':
            $_errors = array();

            if( !($_POST['id_utente']>0) ){
                $_errors[] = "Utente non riconosciuto";
            }
            if( !($_POST["id_match"]>0) ){
                $_errors[] = "Partita non riconosciuta";
            }

            if( count($_errors)>0 ){
                $html = '<div class="Warning">';
                $html .= "<ul>";
                foreach($_errors as $e){
                    $html .= "<li>{$e}</li>";
                }
                $html .= "</ul></div>";
                $_ret = array("status"=>0,"msg"=>$html);
            }else{
                $q = "SELECT * FROM `{$_db_prefix}gir_risultati` WHERE id_utente={$_POST['id_utente']} AND id_match = {$_POST['id_match']}";
                $res = $cn->Q($q);

                if( $cn->n==0 ){
                    $q = "INSERT INTO `{$_db_prefix}gir_risultati` (id_utente,id_match) VALUES ({$_POST['id_utente']},{$_POST['id_match']})";
                    $cn->Q($q);
                    $updated = "NULL";
                }else{
                    $updated = "NOW()";
                }

                $q = "SELECT in_casa FROM `{$_db_prefix}gf_match` WHERE id = {$_POST['id_match']}";
                $in_casa = $cn->OF($q);

                $goal_fiorentina = $in_casa ? $_POST['goal0'] : $_POST['goal1'];
                $goal_fiorentina = $goal_fiorentina>0 ? $goal_fiorentina : 0;
                $goal_avversario = !$in_casa ? $_POST['goal0'] : $_POST['goal1'];
                $goal_avversario = $goal_avversario>0 ? $goal_avversario : 0;

                $q = "UPDATE `{$_db_prefix}gir_risultati` SET
					updated = {$updated},
					goal_fiorentina = {$goal_fiorentina},
					goal_avversario = {$goal_avversario}

					WHERE id_utente = {$_POST['id_utente']} AND id_match = {$_POST['id_match']}";
                $cn->Q($q);

                $_ret = array("status"=>1);
            }

            break;
        case 'IndovinaPrimoMarcatoreSave':
            $_errors = array();

            if( !($_POST['id_utente']>0) ){
                $_errors[] = "Utente non riconosciuto";
            }
            if( !($_POST["id_match"]>0) ){
                $_errors[] = "Partita non riconosciuta";
            }
            if( $_POST['id_giocatore']=="NULL" ){
                $_errors[] = "Scelta non effettuata";
            }

            if( count($_errors)>0 ){
                $html = '<div class="Warning">';
                $html .= "<ul>";
                foreach($_errors as $e){
                    $html .= "<li>{$e}</li>";
                }
                $html .= "</ul></div>";
                $_ret = array("status"=>0,"msg"=>$html);
            }else{
                $q = "SELECT * FROM `{$_db_prefix}gim_risultati` WHERE id_utente={$_POST['id_utente']} AND id_match = {$_POST['id_match']}";
                $res = $cn->Q($q);

                if( $cn->n==0 ){
                    $q = "INSERT INTO `{$_db_prefix}gim_risultati` (id_utente,id_match) VALUES ({$_POST['id_utente']},{$_POST['id_match']})";
                    $cn->Q($q);
                    $updated = "NULL";
                }else{
                    $updated = "NOW()";
                }

                $q = "UPDATE `{$_db_prefix}gim_risultati` SET
					updated = {$updated},
					id_giocatore = {$_POST['id_giocatore']}

					WHERE id_utente = {$_POST['id_utente']} AND id_match = {$_POST['id_match']}";
                $cn->Q($q);

                $_ret = array("status"=>1,"id_giocatore"=>$_POST['id_giocatore']);
            }

            break;
		case 'UserInfoCache':
			$customer_id = $_POST["customer_id"];
			$postdata = array("az"=>"infocardByCustomer","customer_id"=>$customer_id);
			$res = $S->FNET( $postdata );
			$x = json_decode($res);
			//echo "<pre>";print_r($x);exit;
			if($x->status=="1"){
				$data = json_encode($x);
				$q = "UPDATE utenti SET `name` = '" . $x->data->personalInfo->name . "', `surname` = '" . $x->data->personalInfo->surname . "', `card` = '" . $x->data->card . "',  fidely_data = '" . addslashes($data) . "', last_sync_fidely = NOW() WHERE customer_id = {$customer_id}";
				$cn->Q($q);
				$status = 1;
			}else{
				$status = 0;
			}
			$_ret = array("status"=>$status,"answerCode"=>$x->answerCode);
			break;
	}
	return json_encode($_ret);
}
?>