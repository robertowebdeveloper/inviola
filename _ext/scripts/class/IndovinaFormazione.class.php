<?php
class IndovinaFormazione{
	public $Ore_margine = 20;
	
	public function __construct($cn,$_db_prefix){
		$this->cn = &$cn;
		$this->_db_prefix = $_db_prefix;
	}
	public function __destruct(){}
	
	public function PlayerName($item){
		$name = explode(" ",$item["nome"]);
		$shortname = array();
		foreach($name as $r){
			$shortname[] = substr($r,0,1) . ".";
		}
		$shortname = implode(" ",$shortname);
		
		return "{$shortname} {$item['cognome']}";
	}
	
	public function getFormazioni(){
		$q = "SELECT * FROM `{$this->_db_prefix}gf_formazioni` WHERE enable=1 ORDER BY `order` ASC";
		return $this->cn->Q($q,true);
	}
	
	public function getPlayers($is_portiere=false){
		$is_portiere = $is_portiere ? 1 : 0;
		$q = "SELECT * FROM `{$this->_db_prefix}gf_rosa` WHERE attivo=1 AND portiere={$is_portiere} AND deleted IS NULL ORDER BY `cognome` ASC";
		return $this->cn->Q($q,true);
	}
	
	public function getPlayer($id){
		$q = "SELECT * FROM `{$this->_db_prefix}gf_rosa` WHERE id={$id} LIMIT 1";
		return $this->cn->OQ($q,true);
	}
	
	public function TimeToPlay($Match,$msg=false){
		$giorni = $ore = $minuti = false;
		//print_r($Match["rimane"]);
		if($Match["rimane"]["giorni"]>1){
			$giorni = $Match["rimane"]["giorni"] . " giorni";
		}else if($Match["rimane"]["giorni"]==1){
			$giorni = "un giorno";
		}
		if($Match["rimane"]["ore"]>1){
			$ore = $Match["rimane"]["ore"] . " ore";
		}else if($Match["rimane"]["ore"]==1){
			$ore = "un'ora";
		}
		if($Match["rimane"]["minuti"]>1){
			$minuti = $Match["rimane"]["minuti"] . " minuti";
		}else if($Match["rimane"]["minuti"]==1){
			$minuti = "un minuto";
		}
		
		$txt = $msg ? $msg : 'Hai tempo per giocare ancora ';
		$txt .= $giorni ? $giorni : '';
		$txt .= $giorni && $ore && $minuti ? ", " : ( $giorni && ($ore || $minuti) ? " e " : "");
		$txt .= $ore ? $ore : '';
		$txt .= $ore && $minuti ? " e " : "";
		$txt .= $minuti ? $minuti : '';
		
		return $txt;
	}
	
	public function getMatches($what='list',$id_utente=false,$id_stagione=false){ //$what => 'list' (elenco match passati), 'next' => prossimo match, 'prev' => match precedente
		$q = "SELECT
		  main.*, IF(main.id_avversario>0 , teams.nome , main.avversario) AS avversario,
		  IF(main.id_avversario>0, teams.id_logo, 0) AS id_logo_avversario

		  FROM
		  `{$this->_db_prefix}gf_match` main
		  LEFT JOIN `{$this->_db_prefix}gf_teams` teams ON main.id_avversario = teams.id

		  WHERE main.deleted IS NULL AND main.attiva=1";
		if( $what=='next'):
			$q .= " AND main.`data`>=DATE_ADD(NOW(), INTERVAL 1 HOUR) ORDER BY `data` ASC LIMIT 1";
			$ris = $this->cn->OQ($q);
			$n = $ris=="-1" ? 0 : 1;
		elseif( $what=='prev' ):
			$q .= " AND main.`data`<DATE_ADD(NOW(),INTERVAL 1 HOUR) ORDER BY `data` DESC LIMIT 1";
			$ris = $this->cn->OQ($q);
			$n = $ris=="-1" ? 0 : 1;
		elseif( $what>0 ): //ID specifico
			$q .= " AND main.id={$what} LIMIT 1";
			$ris = $this->cn->OQ($q);
			$n = $ris=="-1" ? 0 : 1;
		else:
			$q .= " AND main.`data`<DATE_ADD(NOW(),INTERVAL 1 HOUR)";
			$q .= $id_stagione>0 ? " main.id_stagione={$id_stagione}" : "";
			$q .= " ORDER BY `data` DESC";
			$ris = $this->cn->Q($q,true);
			$n = $this->cn->n;
		endif;
		
		if( $n>0 ){
			if( $what=="next" ){
				$data_ts = strtotime($ris["data"]);
				$data_ts -= ($this->Ore_margine*3600); //Detrae le ore
				//$data_ts = strtotime("2014-12-19 17:00:00"); //for test
				$ris["rimane"] = $this->Manca($data_ts);
			}
			
			if( $what!="list" ){
				$tmp = array();
				$tmp[] = $ris;	
			}else{
				$tmp = $ris;	
			}

            foreach($tmp as $k=>$item){
				if( $id_utente ){
					//Formazione dell'utente
					$q = "SELECT
					fm.*, f.formazione
					
					FROM `{$this->_db_prefix}gf_formazione_match` fm INNER JOIN `{$this->_db_prefix}gf_formazioni` f ON fm.id_formazione = f.id
					WHERE
					fm.id_utente = {$id_utente} AND fm.id_match = {$item['id']}
					LIMIT 1";
					
					$x = $this->cn->OQ($q);
					$tmp[$k]["giocata"] = $x=="-1" ? false : $x;

                    //Indovina il risultato
                    $q = "SELECT * FROM `{$this->_db_prefix}gir_risultati` WHERE id_utente={$id_utente} AND id_match = {$item['id']} LIMIT 1";
                    $x = $this->cn->OQ($q);
                    $tmp[$k]["giocata_ir"] = $x=="-1" ? false : $x;

                    //Indovina il primo marcatore
                    $q = "SELECT * FROM `{$this->_db_prefix}gim_risultati` WHERE id_utente={$id_utente} AND id_match = {$item['id']} LIMIT 1";
                    $x = $this->cn->OQ($q);
                    $tmp[$k]["giocata_im"] = $x=="-1" ? false : $x;
				}else{
					$tmp[$k]["giocata"] = $tmp[$k]["giocata_ir"] = $tmp[$k]["giocata_im"] = false;
				}
				
				//Formazione reale
				$q = "SELECT fm.*, f.formazione FROM `{$this->_db_prefix}gf_formazione_match` fm INNER JOIN `{$this->_db_prefix}gf_formazioni` f ON fm.id_formazione = f.id
				WHERE
				fm.id_utente IS NULL AND fm.id_match = {$item['id']}
				LIMIT 1";
				$x = $this->cn->OQ($q);
				$tmp[$k]["effettiva"] = $x=="-1" ? false : $x;

                //Indovina il risultato reale
                $q = "SELECT * FROM `{$this->_db_prefix}gir_risultati` WHERE id_utente IS NULL AND id_match = {$item['id']} LIMIT 1";
                $x = $this->cn->OQ($q);
                $tmp[$k]["effettiva_ir"] = $x=="-1" ? false : $x;

                //Indovina il primo marcatore reale
                $q = "SELECT * FROM `{$this->_db_prefix}gim_risultati` WHERE id_utente IS NULL AND id_match = {$item['id']} LIMIT 1";
                $x = $this->cn->OQ($q);
                $tmp[$k]["effettiva_im"] = $x=="-1" ? false : $x;
				
				unset($q,$x,$item);
			}
			
			if( $what!="list" ){
				$ris = $tmp[0];	
			}else{
				$ris = $tmp;	
			}
			unset($tmp);
			
			return $ris;
		}else{
			return false;
		}
	}
	
	public function Manca($data_ts){
		$rimane = $data_ts - time();

		$giorni = floor($rimane/86400);
		$ore = floor(($rimane-$giorni*86400)/3600);
		$minuti = floor(($rimane-($giorni*86400+$ore*3600))/60);
		
		$secondi = $rimane-($giorni*86400 + $ore*3600 + $minuti*60);

		return array(
			"timestamp" => $rimane,
			"giorni" => $giorni,
			"ore" => $ore,
			"minuti" => $minuti,
			"secondi" => $secondi
		);
	}
	
	public function Classifica($id_stagione){
		$q = "SELECT fm.*
		FROM
		`{$this->_db_prefix}gf_formazione_match` fm
		INNER JOIN `{$this->_db_prefix}gf_match` m ON fm.id_match = m.id
		WHERE
		fm.id_utente IS NULL
		AND
		m.id_stagione = {$id_stagione}
		AND
		m.deleted IS NULL
		AND 
		fm.punti_assegnati=1
		AND fm.deleted IS NULL
		";
		$list = $this->cn->Q($q,true);
		$Utenti = array();
		
		
		//Cerca ed eliminare eventuali duplicati
		foreach($list as $row){
			$Duplicati = array();
			$q = "SELECT id,id_utente FROM `{$this->_db_prefix}gf_formazione_match` WHERE id_match = {$row['id_match']} AND id_utente>0";
			$utenti_tmp = $this->cn->Q($q,true);
			foreach($utenti_tmp as $utente_row){
				if( !isset($Duplicati[ $utente_row['id_utente'] ]) ){
					$Duplicati[ $utente_row['id_utente'] ] = array("n"=>1, "id_fm" => array() );
					$Duplicati[ $utente_row['id_utente'] ]["id_fm"][] = $utente_row["id"];
				}else{
					$Duplicati[ $utente_row['id_utente'] ]["n"]++;
					$Duplicati[ $utente_row['id_utente'] ]["id_fm"][] = $utente_row["id"];
				}
			}
			
			foreach($Duplicati as $r){
				if($r["n"]>1){
					for($i=0;$i<count($r["id_fm"])-1;$i++){
						$q = "UPDATE `{$this->_db_prefix}gf_formazione_match` SET deleted = NOW() WHERE id = " . $r["id_fm"][$i];
						$this->cn->Q($q);
						if( $_GET["t"]==1 ){
							echo "<pre>";print_r($q);	echo "</pre>";
						}	
					}
				}
			}
		}
		//FINE: Cerca ed eliminare eventuali duplicati
		
		foreach($list as $m){
			$q = "SELECT fm.id, fm.id_utente, u.customer_id, u.id_avatar
			FROM
			`{$this->_db_prefix}gf_formazione_match` fm INNER JOIN `utenti` u ON fm.id_utente=u.id
			WHERE fm.id_match={$m['id_match']} AND fm.id_formazione = {$m['id_formazione']}";
			for($i=1;$i<=11;$i++){
				$q .= " AND fm.id_giocatore{$i} = ". $m["id_giocatore{$i}"];
			}
			$q .= " AND fm.id_utente>0";
			
			$list_users = $this->cn->Q($q,true);
			
			if( $this->cn->n>0 ){
				foreach($list_users as $u){
					$q = "UPDATE `{$this->_db_prefix}gf_formazione_match` SET punti_vinti = 100 WHERE id={$u['id']}";
					$this->cn->Q($q);
					if( !isset($Utenti[ $u["id_utente"] ]) ){
						$Utenti[ $u["id_utente"] ] = new stdClass();
						$Utenti[ $u["id_utente"] ]->vittorie=0;
						$u["id_avatar"] = $u["id_avatar"]>0 ? $u["id_avatar"] : false;
						$Utenti[ $u["id_utente"] ]->user = $u;
					}
					$Utenti[ $u["id_utente"] ]->vittorie++;
				}
			}
		}
		unset($list,$list_users);
		
		/*if( $_GET["t"]==1 ){
			echo "<pre>";print_r($Utenti);
		}*/
		$Classifica = array();
		if( count($Utenti)>0 ){
			//$Utenti = array(1=>20,2=>21,6=>3,7=>3);//for debug			
			$posizione = 0;
			$last_vittorie = false;
			foreach($Utenti as $id_utente=>$obj_user){
				if( $obj_user->vittorie>0 && $obj_user->user["customer_id"]>0 ){
					$posizione++;
					
					$q = "SELECT COUNT(DISTINCT id) AS n FROM `{$this->_db_prefix}gf_formazione_match` WHERE id_utente = {$id_utente}";
					$obj_user->giocate = $this->cn->OF($q);
					
					$q = "SELECT SUM(punti_vinti) AS pv FROM `{$this->_db_prefix}gf_formazione_match` WHERE id_utente = {$id_utente}";
					$obj_user->punti_vinti = $this->cn->OF($q);
										
					$obj_user->precisione = round( 100*$obj_user->vittorie/$obj_user->giocate );
					$Classifica[] = $obj_user;
					$last_vittorie = $obj_user->vittorie;
					
				}
			}
		}
		//Ordinamento (ordine per vittorie e in caso di parità per precisione)
		$vittorie = $punti_vinti = array();
		foreach($Classifica as $o){
			$vittorie[] = $o->vittorie;
			$punti_vinti[] = $o->punti_vinti;	
		}
		array_multisort($vittorie,SORT_DESC, $punti_vinti, SORT_DESC, $Classifica);
		$Classifica = array_chunk($Classifica,10);
		$Classifica = $Classifica[0];
		
		//Fine ordinamento
		
		/*
		//Ordinamento (ordine per vittorie e in caso di parità per precisione)
		$vittorie = $precisione = array();
		foreach($Classifica as $o){
			$vittorie[] = $o->vittorie;
			$precisione[] = $o->precisione;	
		}
		array_multisort($vittorie,SORT_DESC, $precisione, SORT_DESC, $Classifica);
		//Fine ordinamento
		*/
		
		//echo "<pre>";print_r($Classifica);
		
		if( $_GET["t"]==1 ){
			echo "<pre>";print_r($Classifica);
		}
		return $Classifica;
			
	}
	
	public function StagioneInCorso(){
		$q = "SELECT * FROM `{$this->_db_prefix}gf_stagioni` WHERE 1 ORDER BY id DESC LIMIT 1";
		$o = $this->cn->OQ($q);
		return $o;	
	}
}
?>