<?php
class IndovinaPrimoMarcatore extends IndovinaFormazione{
    public function getAllPlayers(){
        $portieri = $this->getPlayers(true);
        $giocatori = $this->getPlayers(false);
        $all = array_merge($portieri,$giocatori);
        unset($portieri,$giocatori);
        return $all;
    }
}
?>