<?php
class Site{ //v.1.0.1 - 15/12/2014
	public $cn;
	public $_db_prefix;
	
	public $id_lang = NULL;
	public $_lang = '';
	public $Page = NULL;
	public $pagesTree = NULL;
	public $lang_in_url = NULL;
    public $UserData = NULL;
	
	public $_IMG_PARAMS = NULL;//Configurato da main.inc.php
	
	public function __construct($cn){
		$this->cn = &$cn;
		$this->_db_prefix = db_prefix;
		
		$this->start_time = $this->getMicroTime();
		
		/* Set paths */
		$this->_path = array();
		$this->_path["docroot"] = path_docroot;
		$this->_path["site"] = path_site;
		$theme = $this->P("theme");
		$this->_path["theme"] = path_webroot . "_ext/themes/{$theme}/";
		$this->_path = json_decode( json_encode($this->_path), false);//convert to object
		
		session_start();
		
		if( isset($_GET["doLang"]) ){
			$this->doLangsFile();	
		}
	}
	
	public function __destruct(){}
	
	public function pageTime($point=NULL){
		$time = $this->getMicroTime();
		return "<!-- {$point}: " . ( $time - $this->start_time ) . " -->\n";
	}
	
	public function Start(){
		//Libera cache
		if( isset($_GET["rc"]) ){
			header ("Expires: ".gmdate("D, d M Y H:i:s", ( time()-(24*60*60) ) ." GMT") );  
			header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");  
			header ("Cache-Control: no-cache, must-revalidate");  
			header ("Pragma: no-cache");
		}
		//riconoscimento pagina
		$_P = $_GET["_P"];
		$url_elements = explode("/",$_P);
		for($new=array(),$i=0;$i<count($url_elements);$i++){
			if(strlen($url_elements[$i])>0){
				$new[] = $url_elements[$i];
			}
		}
		
		$url_elements = $new;
		
		$lang_in_url = $this->P('lang_in_url');
		$this->lang_in_url = $lang_in_url==1 ? true : false;
				
		/* Set language */
		$lang = false;
		if( $this->lang_in_url && count($url_elements)>0 ){
			$q = "SELECT * FROM {$this->_db_prefix}langs WHERE `url` = '{$url_elements[0]}' LIMIT 1";
			$lang = $this->cn->OQ($q);
			if( $lang!=-1 ){
				$_SESSION["_lang"] = $this->_lang = $lang["url"];
				$this->id_lang = $lang["id"];
			}
		}
		if( isset($_SESSION["_lang"]) ){
			$q = "SELECT * FROM {$this->_db_prefix}langs WHERE is_default=1 LIMIT 1";
			$lang = $this->cn->OQ($q);
			if( $lang!=-1 ){
				$_SESSION["_lang"] = $this->_lang = $lang["url"];
				$this->id_lang = $lang["id"];
			}else{
				$_SESSION["_lang"] = $this->_lang = "it";
				$this->id_lang = 1;
			}
		}else{
			$_SESSION["_lang"] = $this->_lang = "it";
			$this->id_lang = 1;
			$lang["iso"] = "it_IT";	
		}
		/* END Set language */
		setlocale(LC_TIME, $lang["iso"]);
		
		/*
		Recognize and set Page
		*/
		$url_elements_page = $url_elements;
		if( $this->lang_in_url ){
			for($new=array(),$i=1;$i<count($url_elements_page);$i++){
				$new[] = $url_elements_page[$i];
			}
			$url_elements_page = $new;
		}
		$pages = array();
		if( count($url_elements_page)>0 ){
			for($id_parent=false, $level=0;$level<count($url_elements_page);$level++){
				$item = $url_elements_page[$level];
				$q = "SELECT * FROM {$this->_db_prefix}pages WHERE `url` = '{$item}'";
				$q .= $id_parent ? " AND id_parent = {$id_parent}" : " AND id_parent IS NULL";
				$q .= " LIMIT 1";
				$p = $this->cn->OQ($q);
				$p["html"] = $this->parseHTMLPage($p["html"]);
				$id_parent = $p["id"];
				$pages[] = $p;
			}
		}
		
		if( count($pages)==0 ){
			$q = "SELECT * FROM {$this->_db_prefix}pages WHERE is_home=1";
			$q .= $this->id_lang>0 ? " AND id_lang = {$this->id_lang}" : "";
			$q .= " LIMIT 1";
			$p = $this->cn->OQ($q);
			$p["html"] = $this->parseHTMLPage($p["html"]);
			$pages[] = $p;
		}
		$this->Page = array_pop($pages);
		$this->Page["BannerBg"] = $this->BannerBg($this->Page["id"]);
		$this->pagesTree = $pages;
		unset($pages);
		/*
		END Recognize and set Page
		*/
		
		ob_start('ob_gzhandler');
	}
	
	public function End(){
		$html = ob_get_contents();
		ob_end_clean();
		
		$html = $this->doHtml( $html );
		//Base path
		
		$html = str_replace("{{root}}", path_webroot , $html);
		//Theme Path
		$theme = $this->P("theme");
		$html = str_replace("{{theme}}", path_webroot . "_ext/themes/{$theme}/", $html);
		
		//This url
		$html = str_replace('{{urlthis}}', $this->Url( $this->Page["id"] ), $html);
		
		//Url
		$q = "SELECT * FROM {$this->_db_prefix}pages WHERE deleted IS NULL";
		$q .= $this->id_lang>0 ? " AND id_lang = {$this->id_lang}" : "";
		$list = $this->cn->Q($q,true);
		foreach($list as $item){
			//URL
			$html = str_replace('{{url ' . $item["id"] . '}}', $this->Url($item["id"]), $html);
			$html = str_replace('{{url ' . $item["k"] . '}}', $this->Url($item["id"]), $html);
			//NOME PAGINE
			$html = str_replace('{{urlname ' . $item["id"] . '}}', $item["name"], $html);
			$html = str_replace('{{urlname ' . $item["k"] . '}}', $item["name"], $html);
			
			//AGGIUNGE CLASSE CSS "_fired" A URL CORRISPODNENTE ALLA PAGINA IN CORSO
			if( $item["id"]==$this->Page["id"] ){
				$str = " _fired";
				$str_class = ' class="_fired"';
			}else{
				$str = $str_class = "";
			}
			$html = str_replace('{{urlfired ' . $item["id"] . '}}', $str, $html);
			$html = str_replace('{{urlfired ' . $item["k"] . '}}', $str, $html);
			$html = str_replace('{{urlfired_class ' . $item["id"] . '}}', $str_class, $html);
			$html = str_replace('{{urlfired_class ' . $item["k"] . '}}', $str_class, $html);
		}
		
		//Slideshow
		$q = "SELECT * FROM {$this->_db_prefix}blocks WHERE type='slideshow' AND enable=1 AND deleted IS NULL";
		$list = $this->cn->Q($q,true);
		foreach($list as $item){
			$html = str_replace('{{slideshow ' . $item["id"] . '}}', $this->SlideshowCode($item["id"]), $html);
			$html = str_replace('{{slideshow ' . $item["k"] . '}}', $this->SlideshowCode($item["id"]), $html);
		}
				
		echo $html;
	}
	
	private function parseHTMLPage($html){
		$html = str_replace("[*row*]", '<div class="row">',$html);
		$html = str_replace("[*/row*]", '</div>',$html);
		
		//WIDGETS
		$q = "SELECT * FROM `{$this->_db_prefix}widgets` WHERE enable_on=1";
		$list = $this->cn->Q($q,true);
		if( count($list)>0 ){
			foreach($list as $r){
				switch($r["k"]){
					case 'slide-foto-utenti':
						$code = array();
						$code[] = '<div class="col-md-' . $this->Widget_grid['md'] . ' col-sm-' . $this->Widget_grid['sm'] . ' col-xs-' . $this->Widget_grid['xs'] . '"><div id="boxSlideFotoUtenti" class="boxHome">';
							$code[] = '<h3>Foto Utenti</h3><div class="box">';
								$code[] = '<div class="slide">';
								$q = "SELECT * FROM 
									(SELECT * FROM `{$this->_db_prefix}users_gallery` WHERE approved IS NOT NULL AND `show`=1 ORDER BY `approved` DESC LIMIT 10) AS tb
									GROUP BY id_user";
								$photos = $this->cn->Q($q,true);
								foreach($photos as $ph){
									$img = $this->Img($ph["id_file"], array("w"=>230,"h"=>160,"bg"=>'fff','m'=>'prop') );
									$code[] = '<div class="slideItem"><img src="' . $img . '" alt=""></div>';
								}
								$code[] = '</div>';
								
								
								$code[] = '<a href="' . $this->getUrl('inviaci-foto') . '" class="Button">' . $this->W('Guardale tutte') . '</a>';
							$code[] = '</div>';//chiude "box"
						$code[] = '<div class="shadow"><img src="' . $this->_path->theme .'img/shadow_store.png" alt="" class="img-responsive"></div>';
						$code[] = '</div></div>';
						$html = str_replace("[*widget{$r['id']}*]", implode("\n",$code) , $html);
						break;
					case "news":
						$code = array();
						$code[] = '<div class="col-md-' . $this->Widget_grid['md'] . ' col-sm-' . $this->Widget_grid['sm'] . ' col-xs-' . $this->Widget_grid['xs'] . '"><div id="boxNews" class="boxHome">';
							$code[] = '<h3>News</h3><div class="box">';
							$q = "SELECT * FROM `{$this->_db_prefix}news_promo` WHERE deleted IS NULL
							AND (show_from IS NULL OR show_from <= NOW() )
							AND (show_to IS NULL OR NOW() <= show_to )
							ORDER BY `date` DESC LIMIT 3
							";
							$list = $this->cn->Q($q,true);
							$code[] = '<ul>';
							for($i=0;$i<count($list);$i++){
								$li = $list[$i];
								$hidden_xs = $i==count($list)-1 ? ' class="hidden-xs"' : '';
								if( empty($li["description_short"]) ){
									$descr = strip_tags( $li["description"] );
								}else{
									$descr = $li["description_short"];
								}
								$descr = substr($descr,0,75)."&hellip;";
								$code[] = '<li' . $hidden_xs . '><a href="' . $this->getUrl('news',$li["id"]) . '">' . $descr . '</a></li>';
							}
							$code[] = '<li class="allNews"><a href="' . $this->getUrl('news') .'">' . $this->W('LEGGI TUTTE LE NEWS') .'</a></li>';
							$code[] = '</ul>';
						$code[] = '</div>';
						$code[] = '<div class="shadow"><img src="' . $this->_path->theme .'img/shadow_store.png" alt="" class="img-responsive"></div>';
						$code[] = '</div></div>';
						$html = str_replace("[*widget{$r['id']}*]", implode("\n",$code) , $html);
						break;
					case "premium-video":
						$code = array();
						$code[] = '<div class="col-md-' . $this->Widget_grid['md'] . ' col-sm-' . $this->Widget_grid['sm'] . ' col-xs-' . $this->Widget_grid['xs'] . '"><div id="boxVideo" class="boxHome">';
							$code[] = '<h3>' . $this->W('Premium Video') . '</h3><div class="box">';
							$q = "SELECT * FROM `fn_downloads` WHERE type='videopremium' ORDER BY `order` DESC LIMIT 3";
							$list = $this->cn->Q($q,true);
							$code[] = '<ul>';
							for($i=0;$i<count($list);$i++){
								$row = $list[$i];
								$hidden_xs = $i==count($list)-1 ? ' class="hidden-xs"' : '';
								$descr = substr($row["label"],0,40)."&hellip;";
								$img = $this->Img($row["id_file"],array("w"=>80,"h"=>45,"m"=>'prop','bg'=>'fff'));
								$code[] = '<li' . $hidden_xs . '><a href="' . $this->getUrl('video-premium',$row["id"]) . '"><img src="' . $img . '" style="float: left; margin-right: 5px;" alt="">' . $descr . '</a></li>';
							}
							$code[] = '<li class="allNews"><a href="' . $this->getUrl('video-premium') .'">' . $this->W('GUARDA TUTTI I VIDEO') .'</a></li>';
							$code[] = '</ul>';
						$code[] = '</div>';
						$code[] = '<div class="shadow"><img src="' . $this->_path->theme .'img/shadow_store.png" alt="" class="img-responsive"></div>';
						$code[] = '</div></div>';
						$html = str_replace("[*widget{$r['id']}*]", implode("\n",$code) , $html);
						break;
					case "sondaggio":
						$code = array();
						$code[] = '<div class="col-md-' . $this->Widget_grid['md'] . ' col-sm-' . $this->Widget_grid['sm'] . ' col-xs-' . $this->Widget_grid['xs'] . '"><div id="boxPoll" class="boxHome">';
							$code[] = '<h3>' . $this->W('Sondaggio del mese') . '</h3><div class="box">';
							$q = "SELECT * FROM `{$this->_db_prefix}polls` WHERE `from` <= DATE_FORMAT(NOW(), '%Y-%m-%d') AND DATE_FORMAT(NOW(), '%Y-%m-%d') <= `to` AND enable_on=1 AND deleted IS NULL ORDER BY id ASC LIMIT 1";
							$poll = $this->cn->OQ($q);
							$txt = $poll["question"];
							$txt = strlen($txt)>60 ? substr($txt,0,59)."&hellip;" : $txt;
							$code[] = $txt;
							$code[] = '<a href="' . $this->getUrl('sondaggio-mese') . '" class="Button">' . $this->W('Partecipa') . '</a>';
						$code[] = '</div>';
						$code[] = '<div class="shadow"><img src="' . $this->_path->theme .'img/shadow_store.png" alt="" class="img-responsive"></div>';
						$code[] = '</div></div>';
						$html = str_replace("[*widget{$r['id']}*]", implode("\n",$code) , $html);
						break;
					default:
						$html = str_replace("[*widget{$r['id']}*]", $this->Widget($r["id"]), $html);
						break;
				}
			}
		}
		//FINE WIDGETS
		
		//BANNERS
		$zone = array('home');
		foreach($zone as $zona){
			$list = $this->ListBanners( $zona );
			if( count($list)>0 ){
				foreach($list as $r){
					$code = $this->Banner($r["id"]);
					switch( $zona ){
						case 'home':
							$code = '<div class="col-md-12"><div class="center">' . $code . '</div></div>';
							break;
					}
					$html = str_replace("[*banner_{$zona}_{$r['id']}*]", $code, $html);
				}
			}
		}
		//FINE BANNERS
		
		//Ripulisce da tutte le stringhe di templating non trovate.
		$flag = false;
		do{
			$pos1 = strpos($html,"[*");
			if($pos1===false){
				$flag = true;
			}else{
				$pos2 = strpos($html,"*]",$pos1)+2;
				$a = substr($html,0,$pos1);
				$b = substr($html,$pos2);
				$html = $a.$b;
			}
		}while(!$flag);
		
		return $html;
	}
	
	public function getVideoAds($section){
		$q = "SELECT * FROM `{$this->_db_prefix}video_ads` va WHERE
		va.`section`='{$section}'
		AND
		(va.show_from <= NOW() OR va.show_from IS NULL)
		AND
		(NOW() <= va.show_to OR va.show_to IS NULL)
		AND va.deleted IS NULL
		AND (
			(va.id_file>0 AND type='image')
			OR
			(LENGTH(va.ext_code) AND type='preroll')
		)
		
		ORDER BY va.id DESC LIMIT 1";
		
		$video = $this->cn->OQ($q);
		
		return $video=="-1" ? false : $video;
	}
	
	protected $Widget_grid = array(
		"lg" => 3,
		"md" => 3,
		"sm" => 6,
		"xs" => 6
	);
	public function Widget($id,$return_html=true){
		$w = intval($id)>0 ? " id={$id}" : " `k`='{$k}'";
		$q = "SELECT * FROM `{$this->_db_prefix}widgets` WHERE {$w}";
		$W = $this->cn->OQ($q);
		
		if( $return_html && $W!=-1){
			$html = array();
			$class = $W["id_file"]>0 ? 'boxHome' : 'widgetTextBox boxHome';
			$html[] = "<div class=\"col-md-{$this->Widget_grid['md']} col-sm-{$this->Widget_grid['sm']} col-xs-{$this->Widget_grid['xs']}\"><div class=\"{$class}\">";
				$html[] = "<h3>{$W['title']}</h3>";
				$html[] = '<div class="box">';
				
					if($W["id_file"]>0){
						$html[] = '<img src="' . $this->Img($W["id_file"],array("w"=>230,"h"=>240,"bg"=>'fff','m'=>'prop') ) . '" alt="" class="img-responsive">';
					}else{
						$html[] = $W["text"];
					}
					
					if( strlen($W["text_button"])>0 ){
						$a = '<a href="';
						if( strlen($W["url"])>0 ){
							$a .= $W["url"] . '"';
							if( $W["url_blank"]==1 ){
								$url_txt = str_replace("'","", str_replace('"','',$W['url']));
								$a .= " target=\"_blank\" onclick=\"javascript:ga('send', 'event', 'Link esterno: {$url_txt}', '{$W['url']}', '{$W['url']}', 1);\"";
							}
						}else{
							$a .= '#"';	
						}
						$a .= " class=\"Button\">{$W['text_button']}</a>";
						$html[] = $a;
					}
				
				$html[] = '</div>';
				$html[] = '<div class="shadow"><img src="' . $this->_path->theme . 'img/shadow_store.png" alt="" class="img-responsive"></div>';
			$html[] = '</div></div>';
			
			return implode("\n",$html);
		}else{
			return $W;	
		}
	}
	
	public function SlideshowCode($id){
		$q = "SELECT * FROM {$this->_db_prefix}blocks WHERE id = {$id}";
		$Slide = $this->cn->OQ($q);
		$code = array();
		$code[] = '<div id="slideshow-' . $Slide["k"] . '" class="slideshow">';
		$code[] = '<div>';
		
		$showed = isset($_SESSION["custom_id"]) ? 'logged' : 'guest';
		
		$q = "SELECT * FROM {$this->_db_prefix}slider_items WHERE id_block = {$id} AND enable=1 AND deleted IS NULL
			AND (show_from IS NULL OR show_from <= NOW() )
			AND (show_to IS NULL OR NOW() <= show_to )
			AND (showed='everyone' OR showed = '{$showed}')";
		$q .= " ORDER BY `order` ASC, id ASC";
		$list = $this->cn->Q($q,true);
		foreach($list as $item){
			$code[] = '<div>';
				if( !empty($item["url"]) ){
					if( $item["target_blank"]==1 ){
						$url_txt = str_replace("'","", str_replace('"','',$item['url']));
						$code[] = "<a href=\"{$item['url']}\" target=\"_blank\" onclick=\"javascript:ga('send', 'event', 'Link esterno: {$url_txt}', '{$item['url']}', '{$item['url']}', 1);\">";
					}else{
						$code[] = "<a href=\"{$item['url']}\">";
					}
				}
				$code[] = '<img src="' . $this->pathFile($item["id_file"]) . '" alt="">';
				if( !empty($item["url"]) ){
					$code[] = "</a>";
				}
			$code[] = '</div>';
		}
		$code[] = '</div>';
		$code[] = '</div>';
		
		
		return implode("\n",$code);
	}
	
	public function ListBanners( $zone, $par=array() ){
		//$par: array with "order" (asc/desc), "orderBy", "limit"
		$q = "SELECT * FROM {$this->_db_prefix}banners WHERE deleted IS NULL
			AND `zone` = '{$zone}'
			AND
			(show_from IS NULL OR show_from <= NOW() )
			AND
			(show_to IS NULL OR NOW() <= show_to )";
		if( isset($par["orderBy"]) ){
			$q .= " ORDER BY `{$par['orderBy']}`";
			$q .= isset($par["order"]) ? " {$par['order']}" : "ASC";
		}else{
			$q .= " ORDER BY `order` ASC, `id` ASC";
		}
		if( isset($par["limit"]) ){
			$q .= " LIMIT {$par['limit']}";
		}

		$list = $this->cn->Q($q,true);
		return $list;
	}
	
	public function Banner($id,$css_class=false,$margin_0_auto = true){
		$b = $this->BannerData($id);
		
		$str = "";
		if( !empty($b["url"]) ){
			$str .= "<a href=\"{$b['url']}\"";
			if( $b["target_blank"]==1 ){
				$str .= " onclick=\"javascript:ga('send', 'event', 'Link esterno: {$b['url_txt']}', '{$b['url']}', '{$b['url']}', 1);\" target=\"_blank\"";
			}
			$str .= ">";
		}
		
		$str .= "<img src=\"" . $b['img_src'] . "\" alt=\"\"";
		$css_class = $css_class ? "img-responsive {$css_class}" : "img-responsive";
		$str .= $css_class ? ' class="' . $css_class .'"' : '';
		$str .= $margin_0_auto ? " style=\"margin: 0 auto;\"" : '';
        $str .= '>';
		
		if( !empty($b["url"]) ){
			$str .= "</a>";	
		}
		return $str;
	}
	
	private function BannerData($id){
		$q = "SELECT * FROM `{$this->_db_prefix}banners` WHERE id = {$id}";
		$b = $this->cn->OQ($q);
		
		$BannerData = $b;
		$BannerData["url_txt"] = str_replace("'","", str_replace('"','',$b['url']));
		$BannerData["img_src"] = $this->pathFile($b["id_file"]);
		
		return $BannerData;
	}
	
	private function BannerBg($id_page){
		$q = "SELECT b.*
		FROM
		`{$this->_db_prefix}banners` b INNER JOIN `{$this->_db_prefix}banners_page` bp ON bp.id_banner = b.id
		WHERE bp.id_page={$id_page} AND b.enable=1 AND b.deleted IS NULL
		AND
		( b.show_from <= NOW() OR b.show_from IS NULL)
		AND
		( NOW() <= b.show_to OR b.show_to IS NULL)
		
		ORDER BY RAND() LIMIT 1;
		";
		
		$Banner = $this->cn->OQ($q);
		if( $Banner=="-1" ){
			return false;
		}else{
			return $this->BannerData($Banner['id']);
		}
	}
	
	public function getAvatar($fullpath=false,$id="auto"){
		if( $id=="auto" && !empty($_SESSION["customer_id"])){
			$q = "SELECT id_avatar FROM utenti WHERE customer_id = {$_SESSION['customer_id']}";
		}else if($id>0){
			$q = "SELECT id_avatar FROM utenti WHERE id = {$id}";
		}else{
			$q = false;	
		}
		
		if( $q ){
			$id_avatar = $this->cn->OF($q);
			if( $id_avatar>0 ){
				$avatar = $this->pathFile($id_avatar , $fullpath);
				return $avatar;
			}else{
				return false;	
			}
		}else{
			return false;	
		}
	}
	
	public function ListNews( $par ){
		//$par: array with "type", "order" (asc/desc), "orderBy", "page", "limit"
		$q = "SELECT * FROM {$this->_db_prefix}news_promo WHERE deleted IS NULL
			AND
			(show_from IS NULL OR show_from <= NOW() )
			AND
			(show_to IS NULL OR NOW() <= show_to )";
		if( strlen($par["type"])>0 ){
			$types = explode(",",$par["type"]);
			$new = array();
			foreach($types as $v){
				$new[] = "'{$v}'";
			}
			$q .= " AND type IN (" . implode(",",$new) . ")";
		}
		if( isset($par["orderBy"]) ){
			$q .= " ORDER BY `{$par['orderBy']}`";
			$q .= isset($par["order"]) ? " {$par['order']}" : "asc";
		}else{
			$q .= " ORDER BY `date` desc";
		}
		if( isset($par["limit"]) ){
			if( isset($par["page"]) ){
				$lim1 = $par["page"]*$par["limit"];	
			}else{
				$lim1 = 0;	
			}
			$q .= " LIMIT {$lim1},{$par['limit']}";
		}

		$list = $this->cn->Q($q,true);
		return $list;
	}
	
	public function pathFile($id_file,$full=true){
		$q = "SELECT * FROM {$this->_db_prefix}file WHERE id = {$id_file}";
		$f = $this->cn->OQ($q);
		if( $f==-1 ){
			return false;
		}else{
			$pathfile = $full ? path_webroot : "";
			$subdir = substr($id_file,0,1);
			$pathfile .= "_public/file/{$subdir}/{$id_file}";
			$pathfile .= empty($f["ext"]) ? "" : ".{$f['ext']}";
			return $pathfile;
		}
	}
	
	public function getUrl($page,$id=false){
		//$page -> key o id page
		if( !is_int($page) ){
			$q = "SELECT id FROM {$this->_db_prefix}pages WHERE `k` = '{$page}' LIMIT 1";
			$id_page = $this->cn->OF($q);
		}
		$url_base = $this->Url($id_page);
		
		if( $id>0 ){
			$url = $url_base . "?id={$id}";
		}else{
			$url = $url_base;	
		}
		
		return $url;
	}
	
	public function FormRichiestaPremio($prizeId,$prizeCode){
		$html = '<form id="richiestaPremioForm_' . $prizeId . '" enctype="application/x-www-form-urlencoded" method="post" action="' . $this->getUrl('i-miei-premi') . '">
			<input type="hidden" name="richiestaPremio" value="1">
			<input type="hidden" name="prize_id" value="' . $prizeId . '">
			<input type="hidden" name="prize_code" value="' . $prizeCode . '">
			<input type="hidden" name="qty" value="1">
		</form>';
		return $html;
	}
	
	public function Url($id_page){
		$url = array();
		$tmp = $id_page;
		do{
			$q = "SELECT * FROM {$this->_db_prefix}pages WHERE id = {$tmp}";
			$p = $this->cn->OQ($q);
			$url[] = $p["url"];
			if( $p["id_parent"]>0 ){
				$flag = false;
				$tmp = $p["id_parent"];
			}else{
				$flag = true;
			}
		}while(!$flag);
		
		if( $this->lang_in_url && $this->id_lang>0){
			$x = $this->Lang();
			if( $x ){
				$url[] = $x["url"];
			}
		}
		$url = array_reverse($url);
		
		$url = path_webroot . implode("/",$url);
		return $url;
	}
	
	private function Lang(){
		if( $this->id_lang>0 ){
			$q = "SELECT * FROM {$this->_db_prefix}langs WHERE id = {$this->id_lang} LIMIT 1";
			$url = $this->cn->OQ($q);
			return $url;
		}
		return false;
	}
	
	public function LoadTemplate(){
		$S = $this;
		$template = "{$this->_path->site}_ext/pages/{$this->Page['template']}";
		if( file_exists($template) && $this->Page!=-1 ){
			include($template);
		}else{
			header("Location: " . path_webroot);exit;
			//echo "Load template problem: <b>{$this->Page['template']}</b>";
		}	
	}
	
	public function LoadSchema(){
		$S = $this;
		$schema = "{$this->_path->site}_ext/pages/{$this->Page['file_inc']}";
		if( file_exists($schema) && $this->Page!=-1 ){
			include($schema);
		}else{
			echo "<!-- Schema {$this->Page['file_inc']} not found -->";
		}
	}
	
	public function geoLevelName($id_ext,$level){
		$q = "SELECT name FROM `{$this->_db_prefix}geolevels` WHERE id_ext = {$id_ext} AND `level`={$level} LIMIT 1";
		$name = $this->cn->OF($q);
		return $name;
	}
	
	public function Head(){
		$head = array();
		
		$q = "SELECT * FROM `{$this->_db_prefix}pages` WHERE is_home=1 AND deleted IS NULL";
		$q .= $this->id_lang>0 ? " AND id_lang={$this->id_lang}" : "";
		$q .= " LIMIT 1";
		$home = $this->cn->OQ($q);
		
		$tmp = strlen($this->Page["seo_title"])>0 ? $this->Page["seo_title"] : $home["seo_title"];
		if( strlen($tmp)>0 ){
			$head[] = "<title>{$tmp}</title>";
		}
		
		$tmp = strlen($this->Page["seo_keywords"])>0 ? $this->Page["seo_keywords"] : $home["seo_keywords"];
		if( strlen($tmp)>0 ){
			$head[] = '<meta name="keywords" content="' . $tmp . '">';
		}
		
		$tmp = strlen($this->Page["seo_description"])>0 ? $this->Page["seo_description"] : $home["seo_description"];
		if( strlen($tmp)>0 ){
			$head[] = '<meta name="description" content="' . $tmp . '">';
		}
		
		$tmp = $this->Page['meta'];
		if( strlen($tmp)>0 ){
			$head[] = $tmp;	
		}
		
		//FAVICON
		if( file_exists( path_site . '_ext/themes/default/favicons' ) ){
			$favicon_path = path_webroot . '_ext/themes/default/favicons/';
			$head[] = '<link rel="shortcut icon" href="' . $favicon_path . 'favicon.ico">
			<link rel="apple-touch-icon" sizes="57x57" href="' . $favicon_path . 'apple-touch-icon-57x57.png">
			<link rel="apple-touch-icon" sizes="114x114" href="' . $favicon_path . 'apple-touch-icon-114x114.png">
			<link rel="apple-touch-icon" sizes="72x72" href="' . $favicon_path . 'apple-touch-icon-72x72.png">
			<link rel="apple-touch-icon" sizes="144x144" href="' . $favicon_path . 'apple-touch-icon-144x144.png">
			<link rel="apple-touch-icon" sizes="60x60" href="' . $favicon_path . 'apple-touch-icon-60x60.png">
			<link rel="apple-touch-icon" sizes="120x120" href="' . $favicon_path . 'apple-touch-icon-120x120.png">
			<link rel="apple-touch-icon" sizes="76x76" href="' . $favicon_path . 'apple-touch-icon-76x76.png">
			<link rel="apple-touch-icon" sizes="152x152" href="' . $favicon_path . 'apple-touch-icon-152x152.png">
			<link rel="icon" type="image/png" href="' . $favicon_path . 'favicon-196x196.png" sizes="196x196">
			<link rel="icon" type="image/png" href="' . $favicon_path . 'favicon-160x160.png" sizes="160x160">
			<link rel="icon" type="image/png" href="' . $favicon_path . 'favicon-96x96.png" sizes="96x96">
			<link rel="icon" type="image/png" href="' . $favicon_path . 'favicon-16x16.png" sizes="16x16">
			<link rel="icon" type="image/png" href="' . $favicon_path . 'favicon-32x32.png" sizes="32x32">
			<meta name="msapplication-TileColor" content="#6550a2">
			<meta name="msapplication-TileImage" content="' . $favicon_path . 'mstile-144x144.png">
			<meta name="msapplication-config" content="' . $favicon_path . 'browserconfig.xml">';
		}
		//END FAVICON
		
		$head[] = '
		<!--[if lt IE 9]>
		<script src="' . path_webroot . '_ext/js/html5shiv/html5shiv.min.js" type="text/javascript"></script>
		<![endif]-->';
		
		list($a,$b) = explode("|", $this->P('framework_frontend') );
		$jquery = false;
		if($a=="jquery-1"):
			$jquery = true;
			$q = "SELECT `path` FROM {$this->_db_prefix}plugins WHERE `k`='{$a}' ORDER BY `version` DESC LIMIT 1";
			$tmp = $this->cn->OF($q);
			$head[] = '<script type="text/javascript" src="' . path_webroot . $tmp . '"></script>';
		elseif($a=="jquery-2"):
			$jquery = true;
			$q = "SELECT `path` FROM {$this->_db_prefix}plugins WHERE `k`='{$a}' ORDER BY `version` DESC LIMIT 1";
			$tmp = $this->cn->OF($q);
			$head[] = '<script type="text/javascript" src="' . path_webroot . $tmp . '"></script>';
		endif;
		
		if( $jquery ):
			$head[] = '<link href="' . path_webroot . '_ext/js/bxslider/jquery.bxslider.css" rel="stylesheet" media="screen">';
			$head[] = '<link href="' . path_webroot . '_ext/js/fancybox2/jquery.fancybox.css" rel="stylesheet" media="screen">';
			$head[] = '<link href="' . path_webroot . '_ext/js/fancybox2/helpers/jquery.fancybox-buttons.css" rel="stylesheet" media="screen">';
			$head[] = '<link href="' . path_webroot . '_ext/js/fancybox2/helpers/jquery.fancybox-thumbs.css" rel="stylesheet" media="screen">';
			$head[] = '<link href="' . path_webroot . '_ext/js/vex/css/vex.css" rel="stylesheet" media="screen">';
			$head[] = '<link href="' . path_webroot . '_ext/js/vex/css/vex-theme-default.css" rel="stylesheet" media="screen">';
		endif;
		
		$responsive_on = $this->P("responsive_on");
		$responsive_on = $responsive_on=="1" ? true : false;
		if( $responsive_on ){
			$head[] = '<meta name="viewport" content="width=device-width, initial-scale=1.0">';	
		}
		if($b=="bootstrap-2"){
			$q = "SELECT `path` FROM {$this->_db_prefix}plugins WHERE `k`='{$b}' ORDER BY `version` DESC LIMIT 1";
			$tmp = $this->cn->OF($q);
			$head[] = '<link href="' . path_webroot . $tmp . 'css/bootstrap.min.css" rel="stylesheet" media="screen">';
			if( $responsive_on ){
				$head[] = '<link href="' . path_webroot . $tmp . 'css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">';
			}
		}else if( $b=="bootstrap-3" ){
			$q = "SELECT `path` FROM {$this->_db_prefix}plugins WHERE `k`='{$b}' ORDER BY `version` DESC LIMIT 1";
			$tmp = $this->cn->OF($q);
			$head[] = '<link href="' . path_webroot . $tmp . 'css/bootstrap.min.css" rel="stylesheet" media="screen">';
			$head[] = '<link href="' . path_webroot . $tmp . 'css/bootstrap-theme.min.css" rel="stylesheet" media="screen">';
		}
		
		$head[] = '';
		return implode("\n",$head);
	}
	
	public function EndScript(){
		$end = array();
		$responsive_on = $this->P("responsive_on");
		$responsive_on = $responsive_on=="1" ? true : false;
		$theme = $this->P("theme");
		
		$end[] = '<script type="text/javascript"><!--
			var path_webroot = "' . path_webroot .'";
			var path_theme = "' . path_webroot . '_ext/themes/' . $theme . '/";
		--></script>';
		
		list($a,$b) = explode("|", $this->P('framework_frontend') );
		if( $b=="bootstrap-2"){
			$q = "SELECT `path` FROM {$this->_db_prefix}plugins WHERE `k`='{$b}' ORDER BY `version` DESC LIMIT 1";
			$tmp = $this->cn->OF($q);
			$end[] = '<script src="' . path_webroot . $tmp . 'js/bootstrap.min.js"></script>';
		}else if( $b=="bootstrap-3"){
			$q = "SELECT `path` FROM {$this->_db_prefix}plugins WHERE `k`='{$b}' ORDER BY `version` DESC LIMIT 1";
			$tmp = $this->cn->OF($q);
			$end[] = '<script src="' . path_webroot . $tmp . 'js/bootstrap.min.js"></script>';
		}
		
		$jquery = false;
		if( $a=="jquery-1" || $a=="jquery-2"){
			$jquery = true;
			$arr = array("jquery-easing","jquery-migrate","jquery-scrollto");
			foreach($arr as $k){
				$q = "SELECT `path` FROM {$this->_db_prefix}plugins WHERE `k`='{$k}' ORDER BY `version` DESC LIMIT 1";
				$tmp = $this->cn->OF($q);
				$end[] = '<script type="text/javascript" src="' . path_webroot . $tmp . '"></script>';
			}
		}
		
		if( $jquery ){
			$end[] = '<script type="text/javascript" src="' . path_webroot . '_ext/js/bxslider/jquery.bxslider.min.js"></script>';
			$end[] = '<script type="text/javascript" src="' . path_webroot . '_ext/js/bxslider/plugins/jquery.fitvids.js"></script>';
			$end[] = '<script type="text/javascript" src="' . path_webroot . '_ext/js/fancybox2/jquery.fancybox.pack.js"></script>';
			$end[] = '<script type="text/javascript" src="' . path_webroot . '_ext/js/fancybox2/helpers/jquery.fancybox-buttons.js"></script>';
			$end[] = '<script type="text/javascript" src="' . path_webroot . '_ext/js/fancybox2/helpers/jquery.fancybox-media.js"></script>';
			$end[] = '<script type="text/javascript" src="' . path_webroot . '_ext/js/fancybox2/helpers/jquery.fancybox-thumbs.js"></script>';
			$end[] = '<script type="text/javascript" src="' . path_webroot . '_ext/js/vex/js/vex.combined.min.js"></script>';
			$end[] = '<script type="text/javascript" src="' . path_webroot . '_ext/js/jquery.slimscroll.min.js"></script>';
		}
		
		$end[] = '';
		return implode("\n",$end);
	}
	
	public function P($parameter){ //parametri
		$q = "SELECT `val` FROM `{$this->_db_prefix}parameters` WHERE `k`='{$parameter}'";
		$val = $this->cn->OF( $q );
		return $val;
	}
	
	public function Img($id_file,$params){
		//$params => array("w","h","ref","bg","br","watermark","out")	
		$q = "SELECT * FROM `{$this->_db_prefix}file` WHERE id = {$id_file}";
		$F = $this->cn->OQ($q);
		
		$dir = substr($id_file,0,1);
		
		$path = path_webroot . "img/";
		//$path .= "{$id_file}.{$F['ext']}/";
		
		$par = "";
		foreach($params as $k=>$v){
			$par .= "{$k}_{$v}/";	
		}
		
		$path .= "{$id_file}-".md5($par).".{$F['ext']}/" . $par;
		
		$path .= "f_file/{$dir}/{$id_file}.{$F['ext']}";
		
		return $path;
	}
	
	public function fnetImage($fidely_pathImage,$type){
		$basepath = "img/";	
	
		$tmp = explode("/",$fidely_pathImage);
		$filename_tmp = array_pop($tmp);
		$ext_tmp = explode(".",$filename_tmp);
		$ext_tmp = array_pop($ext_tmp);
		
		$dir_tmp = array_pop($tmp);
		
		switch($type){
			case "prize_thumb":
				$filename_tmp = "prizes/{$dir_tmp}/{$filename_tmp}";
				$filesave_tmp = substr($dir_tmp,0,1) . "-" . md5($filename_tmp) . ".{$ext_tmp}";
				$path = "{$basepath}{$filesave_tmp}/w_" . ($this->_IMG_PARAMS["prize_thumb"]["w"]*2) ."/h_" . ($this->_IMG_PARAMS["prize_thumb"]["h"]*2) . "/m_prop/bg_fff/f_{$filename_tmp}";
				break;
		}
		
		return $path;	
	}
	
	public function SocialBar($type){
		/*$url = $this->Url( $this->Page["id"] );
		$share_url = "http://{$_SERVER['HTTP_HOST']}/{$url}";*/
		$share_url =  "http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
		switch($type){
			case 0://share
				$code = '<!-- facebook -->
				<a href="https://www.facebook.com/sharer/sharer.php?u=' . $share_url . '" target="_blank">
					<img src="{{theme}}img/socials/facebook.jpg" alt="condividi su facebook" style="vertical-align: top; margin-right: 15px;">
				</a>
	
				<!-- twitter -->
				<a href="https://twitter.com/share" class="twitter-share-button" data-lang="it">Tweet</a>
				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?\'http\':\'https\';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+\'://platform.twitter.com/widgets.js\';fjs.parentNode.insertBefore(js,fjs);}}(document, \'script\', \'twitter-wjs\');</script>';
				break;	
		}
		
		return $code;
	}
	
	public function UserInfo($id_utente=NULL,$customer_id=NULL,$insertJS=false){
		$q = "SELECT fidely_data, last_synch, customer_id FROM utenti WHERE ";
		if( $id_utente>0 ){
			$q .= " id = {$id_utente}";
		}else{
			$q .= " customer_id = {$customer_id}";
		}
		$User = $this->cn->OQ($q);
		
		if( $User!="-1" ){
			if( empty($User['fidely_data']) ){
				$postdata = array("az"=>"infocardByCustomer","customer_id"=>$customer_id);
				$u = $this->FNET($postdata);
			}else{
				$u = $User['fidely_data'];
			}
			if( $insertJS && 0){
				echo '<script type="text/javascript"><!--
					$(document).ready(function(){
						System.UserInfoCache(' . $User['customer_id'] . ');
					});
				--></script>';	
			}
			return json_decode($u);
		}else{
			return false;
		}
	}

    public function getUser($customer_id=NULL){
        if(!$customer_id){
            $customer_id = $_SESSION['customer_id'];
        }

        if($customer_id>0){
            $q = "SELECT * FROM utenti WHERE customer_id = {$customer_id}";
            $data = $this->cn->OQ($q);
            return $data;
        }
        return false;
    }

    public function setCookiesApproved($cookies_approved,$customer_id=NULL){
        $customer_id = !$customer_id ? $_SESSION['customer_id'] : $customer_id;

        $_SESSION['in_coap'] = $cookies_approved;
        if($cookies_approved==1) {
            setcookie('in_coap', $cookies_approved, time() + (5 * 365 * 24 * 60 * 60), path_webroot);
            if( $this->UserData ){
                $this->UserData['cookies_approved'] = 1;
            }
        }

        if($customer_id>0){
            $q = "UPDATE utenti SET cookies_approved = {$cookies_approved} WHERE customer_id = {$customer_id}";
            $this->cn->Q($q);
        }
    }

	public function FNET($postdata,$from='web'){
        $ch = curl_init();
		
		if( site=='locale' ):
			$url = "http://192.168.0.117" . path_webroot . "services/fnet3/inviola/dev.php";
		elseif(site=='stage'):
			$url = "http://{$_SERVER['HTTP_HOST']}" . path_webroot . "services/fnet3/inviola/dev.php";
		else:
			$url = "http://{$_SERVER['HTTP_HOST']}" . path_webroot . "services/fnet3/inviola/fnet.php";
		endif;

		curl_setopt($ch, CURLOPT_URL, $url );
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch,CURLOPT_TIMEOUT,1000);
		
		$postdata["auth"] = 'cWyp!A#D!N#1VY';
		$postdata["from"] = $from;
		$str = array();
		foreach($postdata as $k=>$v){
			$str[] = "{$k}={$v}";	
		}
		$str = implode("&",$str);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $str );
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		
		$res = curl_exec($ch);
		curl_close($ch);

		return $res;
	}
	
	private function doHtml( $html ){		
		//Traduzioni
		$nh = $html;
			
		$flag = false;
		$i=0;
		do{
			if( strpos($nh,"<# ")!==false ){
				$a = strpos($nh,"<# ");
				$b = strpos($nh," #>",$a);
				
				$txt = substr($nh, $a+3, $b-($a+3) );
				$txt_q = addslashes($txt);
				
				$replace = $this->W($txt_q);
				$html1 = substr($nh,0,$a);
				$html2 = substr($nh,$b+3);
				$nh = $html1 . $replace . $html2;
			}else{
				$flag = true;
			}
			$i++;
		}while(!$flag);
		
		//echo "<!-- doHtml counter: {$i} -->";
		
		return $nh;
	}
	
	public function W($word){
		$txt = false;
		$dir = $this->_path->docroot . "_ext/scripts/langs/";
		$q = "SELECT * FROM `{$this->_db_prefix}langs` WHERE id = {$this->id_lang}";
		$lang = $this->cn->OQ($q);
		$file = $dir . "labels-{$lang['url']}.inc.php";
		
		if( !is_int($word) && file_exists($file) ){			
			include( $file );
			
			if( isset($_labels[ $word ] ) ){
				$txt = $_labels[ $word ];
			}
		}
		
		if( !$txt ){
			if( is_int($word) ){
				$q = "SELECT * FROM {$this->_db_prefix}labels WHERE id = '{$word}' LIMIT 1";
			}else{
				$q = "SELECT * FROM {$this->_db_prefix}labels WHERE id_parent IS NULL AND label = '{$word}' LIMIT 1";
			}
			$row = $this->cn->OQ($q);
			$id_parent = $row==-1 ? false : $row["id"];
			
			if( $id_parent>0 ){
				$q = "SELECT `label` FROM `{$this->_db_prefix}labels` WHERE id_parent = {$id_parent} AND id_lang = {$this->id_lang} LIMIT 1";
				$txt = $this->cn->OF($q);
				$txt = $txt ? $txt : $word;
			}else if( !is_int($word) ){
				$q = "INSERT INTO `{$this->_db_prefix}labels` (id_lang,`label`) VALUES (1,'{$word}')";
				$this->cn->Q($q);
				$txt = $word;
			}
		}
		return stripslashes($txt);
	}
	
	public function doLangsFile(){
		$dir = $this->_path->docroot . "_ext/scripts/langs/";
		
		$q = "SELECT * FROM `{$this->_db_prefix}langs` WHERE 1";
		$langs = $this->cn->Q($q,true);

		foreach($langs as $lang){
			$file = $dir . "labels-{$lang['url']}.inc.php";
			
			if( !file_exists($dir) ){
				@mkdir($dir);
				@chmod($dir,0777);	
			}
			
			$fp = fopen($file,"w");
			$str = array();
			$str[] = '<?php';
			$str[] = '$_labels = array();';
			
			$q = "SELECT * FROM `{$this->_db_prefix}labels` WHERE id_parent IS NULL AND id_lang = {$lang['id']} GROUP BY `label`";
			$list = $this->cn->Q($q,true);
			foreach($list as $r){
				$q = "SELECT `label` FROM `{$this->_db_prefix}labels` WHERE id_parent = {$r['id']} AND id_lang = {$lang['id']}";
				$label = $this->cn->OF($q);
				$label = $label ? $label : $r["label"];
				
				$str[] = '$_labels[\'' . addslashes($r["label"]) . '\'] = \'' . $label . '\';';
			}
			
			$str[] = '?>';
			$str = implode("\n",$str);
			fwrite($fp,$str);
			fclose($fp);
		}
	}
	
	public function pr($data,$exit=true){
		echo "<pre>";print_r($data);echo "</pre>";
		if( $exit )
			exit;
	}
	
	public function getMicroTime(){
		list($usec, $sec) = explode(" ",microtime());
		return ((float)$usec + (float)$sec);
	}
}
?>