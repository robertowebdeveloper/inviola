<?php
session_start();
$relative_path = isset($relative_path) ? $relative_path : "";

date_default_timezone_set('Europe/Rome');

require("{$relative_path}module/nmefw.php");
$cn = new NE_mysql(0);
$q = "SET time_zone = 'Europe/Rome';";
$cn->Q($q);

require("{$relative_path}_ext/scripts/class/Site.class.php");
$S = new Site( $cn );

if( $_SESSION["customer_id"]>0 ){
	$_isLogged = true;
}else{
	$_isLogged = false;
}

$_IMG_PARAMS = array(
	"prize_thumb" => array("w"=>200,"h"=>150,"m"=>"prop","bg"=>"FFF")
);
$S->_IMG_PARAMS = $_IMG_PARAMS;
?>