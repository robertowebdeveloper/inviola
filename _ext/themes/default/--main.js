if( !window.console ){ window.console = { log: function(){} }; }
window.log = function(msg){ console.log(msg); };

// JavaScript Document
$(document).ready(function(e) {
	$("a[href='#']").click(function(e){
		e.preventDefault();
	});
	
	try{
		vex.defaultOptions.className = 'vex-theme-default';
	}catch(err){}
	
	var wrapper = $("#Wrapper");
	var footer = $("#Footer");
	if( wrapper.height()+footer.height() < $(document).height() ){
		//wrapper.height( $(document).height()-footer.height() );
	}
	
    $(".slideshow > div").each(function(index, element) {
		if( $(this).children("div").length>0 ){
			var autoStart = $(this).children("div").length>1 ? true : false;
			$(this).bxSlider({
				auto: autoStart,
				controls: false
			});	
			$(".slideshow").css("height","auto");
		}
    });
	
	$(".fancybox[data-rel]").each(function(index, element) {
        var rel = $(this).attr("data-rel");
		$(this).attr("rel",rel);
    });
	$(".fancybox").fancybox({
		openEffect: 'elastic',
		closeEffect: 'elastic'
	});	
	
	/*$("select[data-id='regioniList']").each(function(index, element) {
        setGeoLevel(1, $(this).attr("data-form") , $(this).attr("data-setname"));
    });*/
	
	System.Init();
	
	$('#loginForm input').keyup(function(e) {
		//alert(e.keyCode);
		if(e.keyCode == 13) {
			System.Login('login');
		}
	});
	
	//Controllo altezza
	var h = $("#Wrapper").innerHeight()+$("#Footer").innerHeight();
	if( h<$("#Site").innerHeight() ){
		var hTo = $("#Site").innerHeight();
		hTo = hTo - $("#Footer").innerHeight() - 38;
		//$("#Wrapper").height( hTo );
	}
	
	$("#loginForm input").click(function(){
		$("#menuAreaRiservata .login .submenu").addClass('active');
		$("#menuLoginOverlay").css("display","block");
		$("#menuLoginOverlay").click(function(){
			$("#menuAreaRiservata .login .submenu").removeClass('active');
			$("#menuLoginOverlay").css("display","none");
		});
	});
	
	if( $("#InviaciFotoGallery").length>0 ){
		System.loadInviaciFoto();
	}
	
	if( $("#boxSlideFotoUtenti").length>0 ){
		System.WidgetSlideFotoUtenti();
	}
	
	$(".abuse").each(function(index, element) {
        var a = '<a href="mailto:fiorentinapoint@acffiorentina.it?subject=Abuso su foto utenti InViola">Segnala un abuso</a>';
		$(this).html( a );
    });
});

var System = {
	sp: path_webroot + '_ext/scripts/ajax.php',
	timeout: 4*15000,
	
	Init: function(){
		var This = this;
		
		this.onlyNumber();
		this.VideoAds.Init();
		
		$("#BarUserAlert a.close").click(function(){
			var data = $("#BarUserAlert_form").serialize();
			log(data);
			$.ajax({
				url: This.sp,
				timeout: This.timeout,
				data: data,
				type: 'POST',
				complete: function(xhr,error){},
				success: function(data){
					log(data);
					data = $.parseJSON(data);
					if( data.status ){
						$("#BarUserAlert").animate({
							height: 0,
							opacity: 0,
							"overflow": "hidden"
						},{
							duration: 300,
							complete: function(){
								$(this).hide();	
							}
						});	
					}
				}
			});
		});
	},
	
	element_in_scroll: function(elem){
		var docViewTop = $(window).scrollTop();
		var docViewBottom = docViewTop + $(window).height();
	 
		var elemTop = $(elem).offset().top;
		var elemBottom = elemTop + $(elem).height();
	 
		return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
	},
	
	onlyNumber: function(){	
		$(".onlyNumber").keypress(function(evt) {
			//console.log(evt)
		   var charCode=(evt.which)?evt.which:event.keyCode;
		   if (charCode>31 && (charCode<48 || charCode>57))
			  return false;
		   return true;
		});
	},
	
	deleteAvatar: function(){
		vex.dialog.buttons.YES.text = 'OK';
		vex.dialog.buttons.NO.text = 'Annulla';
		vex.dialog.confirm({
		  message: 'Desideri eliminare il tuo avatar?',
		  callback: function(value) {
			  if( value ){
				  $("#avatarForm").submit();
			  }
		  }
		});	
	},
	
	changeProfileMail: function(){
		var This = this;
		
		vex.dialog.buttons.YES.text = 'OK';
		vex.dialog.buttons.NO.text = 'Annulla';
		vex.dialog.prompt({
			message: 'Inserisci il tuo nuovo indirizzo e-mail. Una e-mail con codice di conferma ti sarà inviata al nuovo indirizzo',
			placeholder: 'E-mail',
			callback: function(newmail){
				if(newmail && newmail.length>0){
					vex.dialog.alert('Invio e-mail di conferma in corso...');
					
					var f = $("#profiloForm");
					var oldmail = f.find("input[name='mailContactData']").val();
					var customer_id = f.find("input[name='customer_id']").val();
					$.ajax({
						type:'POST',
						url: This.sp,
						data: 'az=fnet&sub_az=modifyEmail&newmail=' + newmail + "&customer_id=" + customer_id,
						success: function(data){
							vex.close();
							//console.log(data);
							data = $.parseJSON(data);
							if( data.status ){
								vex.dialog.prompt({
									message: 'Una e-mail di conferma è stata inviata al tuo indirizzo e-mail.<br>Inserisci qui il codice presente nella mail per confermarla',
									placeholder: 'Codice di conferma',
									callback: function(code){
										if( code && code.length>0 && code==data.data.code ){
											f.find("input[name='mailContactData']").val( newmail 	);
											f.find("input[name='mailContactData_pre']").val( newmail 	);
										}else{					
											f.find("input[name='mailContactData_pre']").val( oldmail );
											vex.dialog.alert('Codice non valido');
										}
									}
								});								
							}else{
								/*if( data.errors==2 ){
									f.find("input[name='mailContactData_pre']").val( oldmail );
								}*/
								vex.dialog.alert( data.msg );		
							}
						}
					});		
				}
			}
		});
	},
	
	changeProfileMobile: function(){
		var This = this;
		
		vex.dialog.buttons.YES.text = 'OK';
		vex.dialog.buttons.NO.text = 'Annulla';
		vex.dialog.prompt({
			message: 'Inserisci il nuovo di cellulare. Una SMS con codice di conferma ti sarà inviata al nuovo numero',
			placeholder: 'Cellulare',
			callback: function(newcell){
				if(newcell && newcell.length>0){
					vex.dialog.alert('Invio SMS in corso...');
					
					var f = $("#profiloForm");
					var oldcell = f.find("input[name='mobileContactData']").val();
					var customer_id = f.find("input[name='customer_id']").val();
					$.ajax({
						type:'POST',
						url: This.sp,
						data: 'az=fnet&sub_az=modifyCell&newcell=' + newcell + "&customer_id=" + customer_id,
						success: function(data){
							vex.close();
							console.log("Success: " + data);
							data = $.parseJSON(data);
							if( data.status ){
								vex.dialog.prompt({
									message: 'Un SMS di conferma ti è stato inviato.<br>Inserisci qui il codice di conferma ricevuto',
									placeholder: 'Codice di conferma',
									callback: function(code){
										if( code && code.length>0 && code==data.data.code ){
											f.find("input[name='mobileContactData']").val( newcell );
											f.find("input[name='mobileContactData_pre']").val( newcell );
										}else{					
											f.find("input[name='mobileContactData_pre']").val( oldcell );
											vex.dialog.alert('Codice non valido');
										}
									}
								});								
							}else{
								/*if( data.errors==2 ){
									f.find("input[name='mailContactData_pre']").val( oldmail );
								}*/
								vex.dialog.alert( data.msg );		
							}
						}
					});		
				}
			}
		});
	},
	
	RecuperoPW: function(){
		var This = this;
		
		$("#loaderLogin").removeClass('hide');
		var f = $("#recuperaPwForm");
		$.ajax({
			url: This.sp,
			//timeout: This.timeout,
			type: 'POST',
			data: f.serialize(),
			complete: function(xhr,status){
				console.log(xhr+" "+status);
			},
			success: function(data){
				console.log(data);
				$("#loaderLogin").addClass('hide');
				data = $.parseJSON(data);
				if( data.status ){
					f.find("input[name='email']").val('');
					$("#msg").html(data.msg);
				}else{
					$("#msg").html(data.msg);
				}
			}
		});
	},
	
	VideoAds: {
		NV4ME_id_content: 'nv4me-player-container-49411',
		Init: function(){
			var This = this;
			
			if( $(".VideoAds-image").length>0 ){
				setTimeout(function(){
					This.Enable();
				},2000);
			}else if( $(".VideoAds-preroll").length>0 ){
				This.enableSubVideoButtons();
				This.VideoPreroll_show();
			}
		},
		
		enableSubVideoButtons: function(){ //Abilita i pulsanti dei video sotto, quando si clicca fa ripartire il pre-roll
			var This = this;
			
			if( NV4ME.widgets ){
				//Abilito la funzione sui video sotto
				 NV4ME.widgets.MediaCenter.api.elementClick = function(elementId){
					 //alert(elementId);
					 This.VideoPreroll_show();
					 NV4ME.widgets.MediaCenter.reproduceContent(elementId);
				 }
				/*NV4ME.widgets.MediaCenter.api.fourme_onContentComplete = function(contentUrl){
					alert(1);
				};*/
				//NV4ME.widgets.MediaCenter.api.fourme_onContentComplete('nv4me-player-container-49411');
			}else{
				setTimeout( function(){
					This.enableSubVideoButtons();
				},250);
			}
		},
		
		VideoPreroll_hided: false,
		idTime_endpreroll: 0,
		VideoPreroll_hide: function(){
			var This = this;
			
			if( !This.VideoPreroll_hided ){
				$("#VideoPreroll").fadeOut(400,function(){
					//$(this).html('');
					NV4ME.player.api.call( This.NV4ME_id_content,"play");
					
					if( This.idTime_endpreroll>0 ){
						clearTimeout( This.idTime_endpreroll );
					}
					This.VideoPreroll_hided = true;
				});
			}
		},
		
		VideoPreroll_show: function(){
			var This = this;
			This.VideoPreroll_hided = false;
			NV4ME.player.api.call( This.NV4ME_id_content,"pause");
			$("#VideoPreroll").fadeIn();
			$("#VideoPreroll").find(".AdBar > a").fadeOut(0);
			
			//Dopo 7 secondi appare "skyp ad"
			setTimeout(function(){
				$("#VideoPreroll").find(".AdBar > a").fadeIn().click( function(){
					This.VideoPreroll_hide();
				});
			},7000);
			
			if( This.idTime_endpreroll>0 ){
				clearTimeout( This.idTime_endpreroll );
			}
			var timing = parseInt( $("#VideoPreroll").attr("data-timing") );
			timing *= 1000;
			//Dopo i secondi di durata del preroll scompare
			This.idTime_endpreroll = setTimeout(function(){
				This.VideoPreroll_hide();
			}, timing);
		},
		
		Enable: function(){
			var This = this;
			$(".VideoAds-image").each(function(index,el){
				$(this).fadeIn(150);
				var id = $(this).attr("data-id");
				var timing = parseInt( $(this).attr("data-timing") );
				timing *= 1000; //da secondi a millisecondi
				if(timing>0){
					setTimeout(function(){
						This.Close(id);
					},timing);
				}
			});			
		},
		Close: function(id){
			$(".VideoAds-image[data-id='" + id + "']").fadeOut(250);
		}
	},
	
	VerificaCard_working: false,
	VerificaCard: function(){
		var This = this;
		
		if( !This.VerificaCard_working ){
			This.VerificaCard_working = true;
		
			$("#loaderLogin").removeClass('hide');
			var f = $("#haicardForm");
			$.ajax({
				url: This.sp,
				timeout: This.timeout,
				type: 'POST',
				data: f.serialize(),
				success: function(data){
					//console.log(data);
					This.VerificaCard_working = false;
					$("#loaderLogin").addClass('hide');
					data = $.parseJSON(data);
					if( data.status ){
						if(data.data.datiIncompleti){
							var card = f.find("input[name='fidelyCode']").val();
							var f2 = $("#cardIncompletaForm");
							f2.find("input[name='fidelyCode']").val( card );
							f2.submit();
						}else{
							$("#enterPasswordForm").find("input[name='login']").val(data.data.personalInfo.userName);
							$("#inserisciPassword").slideDown();
							$(document).scrollTop(500);
						}
					}else{
						vex.dialog.alert(data.msg);
					}
				}
			});
		}
	},
	
	openCambiaPW: function(e){
		$("#passwordChange").slideToggle(250);
	},
	cambiaPW: function(){
		var This = this;
		$("#pwMsg > span").html('').fadeOut();
		$("#pwLoader").removeClass('hide');
		$.ajax({
			type:'POST',
			timeout: This.sp,
			url: This.sp,
			data: $("#passwordForm").serialize(),
			success: function(data){
				$("#pwLoader").addClass('hide');
				//console.log(data);
				data = $.parseJSON(data);
				if( data.status ){
					$("#pwMsg > span").removeClass('Red').html( data.msg ).fadeIn();
					setTimeout(function(){
						$("#passwordChange").slideUp('slow');
					},3000);
				}else{
					$("#pwMsg > span").addClass('Red').html( data.msg ).fadeIn();
				}
			}
		});
	},
	
	salvaProfilo: function(){
		var This = this;
		$("*[data-error]").removeClass('Red');
		$("#saveLoader").removeClass('hide');

		var f = $("#profiloForm");
		var ds = f.find("select[name='birthday_d']").val()+"/"+f.find("select[name='birthday_m']").val()+"/"+f.find("select[name='birthday_y']").val();
		$("input[name='dataNascita']").val( ds );

		$.ajax({
			url: This.sp,
			timeout: This.timeout,
			type:'POST',
			data: f.serialize(),
			complete: function(xhr,status){
				//console.log(xhr+" "+status)
			},
			success: function(data){
				//console.log(data);
				$("#saveLoader").addClass('hide');
				data = $.parseJSON(data);
				if( data.status ){
					var url = $("#url-reload").val();
					//alert(url);
					document.location = url;
				}else{
					for(var i in data.errors){
						$("*[data-error='" + data.errors[i] + "']").addClass('Red');
					}
					vex.dialog.alert(data.msg);
				}
			}
		});
	},
	
	RegistraCard: function(){
		var This = this;
		$("#RegistraCard_btn").css('visibility','hidden');
		$("#RegistraCard_loader").removeClass('hide');
		$("*[data-error]").removeClass('Red');
		var f = $("#registraCardForm");
		
		f.find("input[name='dataNascita']").val( f.find("select[name='birthday_d']").val() + "/" + f.find("select[name='birthday_m']").val() + "/" + f.find("select[name='birthday_y']").val() );
		
		$.ajax({
			url: This.sp,
			type:'POST',
			data: f.serialize(),
			timeout: This.timeout,
			complete: function(xhr,status){
				if(status=="timeout"){
					$("#RegistraCard_btn").css('visibility','visible');
					$("#RegistraCard_loader").addClass('hide');
					vex.dialog.alert('Errore collegamento. Tentare nuovamente');
				}
			},
			success: function(data){
				//console.log(data);
				data = $.parseJSON(data);
				if( data.status ){
					f.find("input[name='sub_az']").val('inviaMailConfirm');			
					$.ajax({
						url: This.sp,
						timeout: This.timeout,
						data: f.serialize(),
						complete: function(xhr,status){
							if(status=="timeout"){
								$("#RegistraCard_btn").css('visibility','visible');
								$("#RegistraCard_loader").addClass('hide');
								vex.dialog.alert('Errore collegamento. Tentare nuovamente');
							}
						},
						success: function(data){
							//console.log(data);
							$("#RegistraCard_loader").addClass('hide');		
							//$("#CongratulazioniCard").slideDown(250);
							$.fancybox.open({
								href: '#CongratulazioniCard'
							},{
								autoSize: false,
								width: 600,
								autoHeight: true,
								type: 'inline',
								openEffect: 'fade',
								closeEffect: 'fade',
								afterClose: function(){
									$("#successForm").submit();	
								}
							});
							//$(document).scrollTop(500);
						}
					});
				}else{
					$("#RegistraCard_btn").css('visibility','visible');
					$("#RegistraCard_loader").addClass('hide');
					for(var i in data.errors){
						$("*[data-error='" + data.errors[i] +"']").addClass('Red');	
					}
					vex.dialog.alert(data.msg);	
				}
			}
		});
	},
	
	richiediPremio: function(id){
		vex.dialog.buttons.YES.text = 'OK';
		vex.dialog.buttons.NO.text = 'Annulla';
		vex.dialog.confirm({
		  message: 'Confermi richiesta premio?',
		  callback: function(value) {
			  if( value ){
				  $("#richiestaPremioForm_"+id).submit();
			  }
		  }
		});	
	},
	
	inviaciFotoScroll_enable: false,
	inviaciFotoLoading: false,
	loadInviaciFoto: function(){
		var This = this;
		
		if( !This.inviaciFotoScroll_enable ){
			$(document).scroll(function(e){
				if( System.element_in_scroll("#elemForScroll") && !This.inviaciFotoLoading ){
					This.inviaciFotoLoading  = true;
					This.loadInviaciFoto();
				}
			});
			This.inviaciFotoScroll_enable = true;
		}
		
		var div = $("#InviaciFotoGallery");
		var page = parseInt( div.attr("data-page") );
		page = page>0 ? page : 0;

		var loader = $("#loaderInviaciFoto");
		loader.fadeIn(250);
		
		$.ajax({
			url: This.sp,
			type: 'POST',
			data: 'az=loadInviaciLeTueFoto&page='+page,
			success: function(data){
				loader.fadeOut(500,'linear',function(){					
					//console.log(data);
					data = $.parseJSON(data);
					if( data.status ){
						div.append( data.html );
						div.attr("data-page",data.page);
						if( !data.end ){
							This.inviaciFotoLoading = false;		
						}
					}
					
				});
			}
		});
	},
	
	loadNews: function(){
		var This = this;
		var div = $("#NewsCont");
		var page = parseInt( div.attr("data-page") );
		
		$("#loaderNews").fadeIn(250);
		$("#NewsContNext").addClass('hide');
		
		$.ajax({
			url: This.sp,
			type: 'POST',
			data: 'az=loadNews&page='+page,
			success: function(data){
				$("#loaderNews").fadeOut(500,'linear',function(){
					$("#loaderNews").remove();	
					
					console.log(data);
					data = $.parseJSON(data);
					if( data.status ){
						div.append( data.html );
						div.attr("data-page",data.page);	
						if( !data.end ){
							$("#NewsContNext").removeClass('hide');
						}
					}
					
				});
			}
		});
	},
	
	Login: function( box ){
		var This = this;
		if( box=="login"){
			var loginLoader = $("#loginLoader");
			var loginEntra = $("#loginEntra");
			var f = $("#loginForm");
		}else if(box=="haiunacard"){
			var loginLoader = $("#loaderPw");
			var loginEntra = $("#entraPw");
			var f = $("#enterPasswordForm");
		}
		loginLoader.removeClass('hide');
		loginEntra.addClass('hide');
		
		var data = f.serialize();
		//console.log(data);
		$.ajax({
			url: This.sp,
			type: 'POST',
			timeout: This.timeout,
			data: data,
			complete: function(xhr,error){
				console.log(error);
			},
			success: function(data){
				//console.log(data);
				//alert(data);
				data = $.parseJSON(data);
				if( data.status ){
					f.submit();
				}else{
					loginEntra.removeClass('hide');
					loginLoader.addClass('hide');
					vex.dialog.alert(data.msg);
				}
			}
		});
	},
	
	Logout: function(){
		vex.dialog.buttons.YES.text = 'OK';
		vex.dialog.buttons.NO.text = 'Annulla';
		vex.dialog.confirm({
		  message: 'Vuoi disconnetterti?',
		  callback: function(value) {
			  if( value==1 ){
				  $("#logoutForm").submit();
			  }
		  }
		});	
	},
	
	RegResidenza: function(idForm){
		var f = $("#" + idForm);
		var res = f.find("select[name='residente']").val();
		if(res=="italia"){
			$(".ResItaliaFields").removeClass('hide');
			$(".ResEsteroFields").addClass('hide');
		}else{
			$(".ResItaliaFields").addClass('hide');
			$(".ResEsteroFields").removeClass('hide');
		}
	},
	
	UserInfoCache: function(customer_id){
		var This = this;
		$.ajax({
			type: 'POST',
			data: 'az=UserInfoCache&customer_id='+customer_id,
			url: This.sp,
			complete: function(xhr,error){
				//console.log(xhr);
				//console.log(error);
			},
			success: function(data){
				console.log(data);
			}
		});
	},
	
	WidgetSlideFotoUtenti: function(){
		$("#boxSlideFotoUtenti .slide").bxSlider({
			mode: 'fade',
			pause: 2500,
			easing: 'ease-in',
			pager: false,
			auto: true,
			controls: false
		});
	}
};


var setGeoLevel = function(level, id_form, name_set){
	var f = $("#" + id_form);
	var el = f.find("select[name='" + name_set+level + "']");
	var id_regione=id_provincia=id_comune=0;
	switch(level){
		default:
		case 1:
			var sub_az = "regioniList";
			var a = f.find("select[name='" + name_set + "2']");
			if( a.length>0 ){
				a = a.get(0);
				a.options.length=0;
				var opt = document.createElement('option');
				opt.text = 'Seleziona provincia...';
				a.disabled=true;
				a.add(opt,a[0]);
				var b = f.find("select[name='" + name_set + "3']");
				if( b.length>0 ){
					b = b.get(0);
					b.options.length=0;
					var opt = document.createElement('option');
					opt.text = 'Seleziona comune...';
					b.disabled=true;
					b.add(opt,b[0]);
					
					var c = f.find("select[name='" + name_set + "4']");
					if( c.length>0 ){
						c = c.get(0);
						c.options.length=0;
						var opt = document.createElement('option');
						opt.text = 'Seleziona località...';
						c.disabled=true;
						c.add(opt,c[0]);
					}
				}
			}
			break;
		case 2:
			var sub_az = "provinceList";
			var parent = f.find("select[name='" + name_set + "1']");
			var id_regione = parent.val();
			var b = f.find("select[name='" + name_set + "3']");
			if( b.length>0 ){
				b = b.get(0);
				b.options.length=0;
				var opt = document.createElement('option');
				opt.text = 'Seleziona comune...';
				b.disabled=true;
				b.add(opt,b[0]);
				
				var c = f.find("select[name='" + name_set + "4']");
				if( c.length>0 ){
					c = c.get(0);
					c.options.length=0;
					var opt = document.createElement('option');
					opt.text = 'Seleziona località...';
					c.disabled=true;
					c.add(opt,c[0]);
				}
			}
			break;
		case 3:
			var sub_az = "comuniList";
			var parent = f.find("select[name='" + name_set + "2']");
			var id_provincia = parent.val();
			var b = f.find("select[name='" + name_set + "3']");
			if( b.length>0 ){
				b = b.get(0);
				b.options.length=0;
				var opt = document.createElement('option');
				opt.text = 'Seleziona località...';
				b.disabled=true;
				b.add(opt,b[0]);
			}
			break;
		case 4:
			var sub_az = "localitaList";
			var parent = f.find("select[name='" + name_set + "3']");
			var id_comune = parent.val();
			break;
	}
		
	var sel_el = el.get(0);
	if( sel_el.options ){
		sel_el.options.length=1;
	}
	
	if( level<2 || level==2 && id_regione>0 || level==3 && id_provincia>0 || level==4 && id_comune>0){
		var sel_el = el.get(0);
		sel_el.options.length=0;
		var opt = document.createElement('option');
		opt.text = 'Loading...';
		sel_el.disabled=true;
		sel_el.add(opt,sel_el[0]);
		
		$.ajax({
			type: 'POST',
			url: path_webroot + '_ext/scripts/ajax.php',
			data: 'req=ajax&az=fnet&sub_az='+sub_az+'&id_regione='+id_regione+'&id_provincia='+id_provincia+'&id_comune='+id_comune,
			complete: function(data){},
			success: function(data){
				//console.log(data);
				data = $.parseJSON(data);
				if( data.status ){
					sel_el.disabled=false;
					sel_el.options.length=0;
					
					//Primo vuoto
					var opt = document.createElement('option');
					opt.text = '-';
					opt.value = 0;
					sel_el.add(opt,sel_el[i]);	
					
					for(var i=0;i<data.data.length;i++){
						var opt = document.createElement('option');
						opt.text = data.data[i].name;
						opt.value = data.data[i].id;
						sel_el.add(opt,sel_el[i+1]);	
					}
				}else{
					vex.dialog.alert(data.msg);
				}
			}
		});
	}
};

var Mobile = {
	OpenMenu: function(){
		$("#MenuMobileList").slideDown();	
	}
};

var richiediTessera = {
	Send: function(id_form){
		var f = $("#" + id_form);
		f.find("*[data-error]").removeClass("Red");
		$.ajax({
			type: 'POST',
			url: path_webroot + '_ext/scripts/fnet.php',
			data: "act=checkDatiRegistrazione&" + f.serialize(),
			success: function(data){
				//alert(data);
				data = $.parseJSON(data);
				if( data.status ){
					$.ajax({
						type: 'POST',
						url: path_webroot + '_ext/scripts/fnet.php',
						data: "act=registraCard&" + f.serialize(),
						success: function(data){
							console.log(data);
							data = $.parseJSON(data);
							if( data.status ){
								f.get(0).reset();
							}
							vex.dialog.alert("<h5>" + data.msg + "</h5>");
						}
					});					
					
				}else{
					for(var i in data.errors){
						var e = data.errors[i];
						f.find("*[data-error='" + e + "']").addClass('Red');
					}
					vex.dialog.alert("<h5>" + data.msg + "</h5>");
				}
			}
		});
	}
};

var SendForm = function(id_form){
	var f = $("#" + id_form);
	var data = f.serialize();

	$.ajax({
		url: path_webroot + '_ext/scripts/ajax.php',
		type: 'POST',
		data: data,
		complete: function(data){
			//alert(data.status)
		},
		success: function(data){
			//console.log(data);
			data = $.parseJSON(data);
			if( data.status ){
				f.get(0).reset();
			}
			vex.dialog.alert(data.msg);
		}
	});
};

var setStoreMap = function(id_map, lat,lng,zoom){
	var pos = new google.maps.LatLng(lat,lng);
	
	var myOptions = {
		 zoom: zoom,
		 center: pos,
		 mapTypeId: google.maps.MapTypeId.ROADMAP
	  };
	
	  var map = new google.maps.Map(document.getElementById( id_map ), myOptions);
	  
	  var marker = new google.maps.Marker({
		  position: pos,
		  map: map,
		  title: ''
	  });
};