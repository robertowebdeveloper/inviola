// JavaScript Document
$(document).ready(function(e) {
	IndovinaFormazione.Init();
	//$('.Giocatori').jScrollPane();
});

var IndovinaFormazione = {
	x_formazione: 0,
	width_formazione_li: null,
	moving: false,
	
	indexMaxFormazione:null,
	indexFormazione: 0,
	
	Init: function(){
		var This = this;
		
		this.width_formazione_li = $(".formazioni").width();
		this.x_formazione = -this.width_formazione_li;
		this.indexFormazione = 1;
		this.indexMaxFormazione = $(".formazioni > ul > li").length-1;
		
		setInterval(function(){
			This.updateRimanenza();
		},60*1000);
		
		$("#IndovinaFormazione .conferma[data-action='conferma']").click(function(){
			This.Send();
		});
		$("#ScegliFormazione .conferma[data-action='modifica']").click(function(){
			var o = $("#IndovinaFormazione");
			o.find(".conferma[data-action='conferma']").removeClass('hide');
			$(this).addClass('hide');
			o.find('.formazione_btn_left').removeClass('hide');
			o.find('.formazione_btn_right').removeClass('hide');
			o.find('.annulla').removeClass('hide');
			o.find('.Player > a').addClass('enable');
		});
		
		$(".formazione_panel .formazione_btn_left").click(function(e){
			e.preventDefault();
			This.changeFormazione(-1,'auto');
		});
		$(".formazione_panel .formazione_btn_right").click(function(e){
			e.preventDefault();
			This.changeFormazione(1,'auto');
		});
		
		$("#IndovinaFormazione .Player > a").click(function(){
			if( $(this).hasClass('enable') ){
				var index = $(this).attr("data-index");
				This.setPlayer( index );
			}
		});
		
		$("#boxPlayers .PlayerBtn").click(function(){//Click sul pulsante del giocatore all'interno del Box
			var id = $(this).attr("data-id");
			id = parseInt(id);
			var img = $(this).find("img").attr("src");
			var name = $(this).attr("data-name");
			
			var o = $(".Player a[data-index='" + This.index_player + "']");
			o.children("img").attr("src",img);
			o.children("span").html( name );
			
			$("input[name='id_giocatore" + This.index_player + "']").val(id);
			
			This.openBoxPlayers(false,false);
		});
		$("#boxPlayersOverlay").click(function(){
			This.openBoxPlayers(false,false);
		});
		
		//POSIZIONA LA FORMAZIONE CORRETTA
		var index_formazione_sel = parseInt( $(".formazioni").attr('data-index-formazione-sel') );
		for(var i=0;i<index_formazione_sel;i++){
			this.changeFormazione(1,0);
		}
	},
	
	updateRimanenza: function(){},
	
	speedChange: 1100,
	changeFormazione: function(dir,speed){
		var This = this;
		
		speed = speed=='auto' ? This.speedChange : speed;
		
		if( !this.moving ){
			this.moving = true;

			if( dir>0 ){
				var x_end = this.x_formazione - this.width_formazione_li;
				this.indexFormazione++;
				this.indexFormazione = this.indexFormazione>this.indexMaxFormazione ? 0 : this.indexFormazione;
			}else{
				var x_end = this.x_formazione + this.width_formazione_li;
				this.indexFormazione--;
				this.indexFormazione = this.indexFormazione<0 ? this.indexMaxFormazione : this.indexFormazione;
			}
			
			This.PlayerShow(false);

			$(".formazioni > ul").animate({
				left: x_end+"px"
			},{
				duration: speed,
				easing: 'easeInOutCirc',
				complete: function(){
					if( This.indexFormazione==0 ){
						var x = -( This.width_formazione_li*(This.indexMaxFormazione-1) );
						$(this).css("left", x+"px");
						This.indexFormazione = This.indexMaxFormazione-1;
						This.x_formazione = x;
					}else if( This.indexFormazione==This.indexMaxFormazione ){
						var x = -This.width_formazione_li;
						$(this).css("left", x+"px");
						This.indexFormazione = 1;
						This.x_formazione = x;
					}else{
						This.x_formazione = x_end;
					}
					This.moving = false;
					
					var li = $(this).children("li[data-index='" + This.indexFormazione + "']");
					var id_formazione = li.attr("data-id");
					var formazione = li.attr("data-formazione");
										
					This.setFormazione(id_formazione,formazione);
					This.PlayerShow(true);
				}
			});
		}
	},
	setFormazione: function(id_formazione,formazione){
		var o = $("#IndovinaFormazione .Campo");
		var formazione_old = o.attr("data-formazione");
		o.removeClass("f"+formazione_old);
		o.addClass("f"+formazione).attr("data-formazione",formazione);
		
		$("#IndovinaFormazione .formazioni").attr("data-id",id_formazione).attr("data-formazione",formazione);
		$("input[name='id_formazione']").val( id_formazione );
	},
	
	PlayerShow: function(show){
		var This = this;
		var divider = 11;
		
		var fn = function(index,show){
			var className = index==1 ? 'Portiere' : ('g'+index);
			var speed = This.speedChange/divider;
			if( show ){
				var op = 1;
				var ease = 'easeOutCirc';
			}else{
				var op = 0;
				var ease = 'easeInCirc';
			}
			
			$(".Player." + className).stop().animate({
				opacity: op
			},{
				easing: ease,
				duration: speed
			});
			if(index<11){
				setTimeout(function(){
					fn(index+1,show);
				}, ( speed/3 ) );
			}
		};
		fn(1,show);
	},
	
	index_player: null, //indice del giocatore da cambiare
	setPlayer: function(index){
		index = parseInt(index);
		var This = this;
		
		this.index_player = index;
		
		if(index==1){ //Portiere
			This.openBoxPlayers(true,true);
		}else{
			This.openBoxPlayers(true,false);
		}
	},
	
	boxPlayers_show: false,
	openBoxPlayers: function(show,portiere){
		var This = this;
		var duration = 600;

		var o = $("#boxPlayers");
		if( show ){
			
			o.find(".Portieri").addClass("hide");
			o.find(".Giocatori").addClass("hide");		
			if( portiere ){
				o.find(".Portieri").removeClass("hide");
				var top = 300;
			}else{
				var box = o.find(".Giocatori");
				box.removeClass("hide");
				if( !box.hasClass("slimScrollEnable") ){
					box.addClass("slimScrollEnable").children(".row").slimScroll({
						height: '560px',
						alwaysVisible: true
					});
				}
				var top = 70;
			}
			
			
			$("#boxPlayersOverlay").fadeIn( duration );
			o.css({
				"display" : "block",
				"opacity" : 0
			}).animate({
				top: top+'px',
				opacity: 1
			},{
				duration: duration
			});
		}else{
			this.index_player = null;
			$("#boxPlayersOverlay").fadeOut( duration );
			o.animate({
				top: '0px',
				opacity: 0
			},{
				duration: duration,
				complete: function(){
					$(this).css("display","none");	
				}
			});
		}
	},
	
	Send: function(){
		var giocatori = new Array();
		for(var i=1,error=false;i<=11;i++){
			var id = $("input[name='id_giocatore" + i + "']").val();
			id = parseInt( id );
			if( !(id>0) ){
				error = true;
			}
		}
		//alert(giocatori);		
		if( error ){
			vex.dialog.alert('<b>Attenzione:</b> non hai selezionato tutti i giocatori per poter confermare la formazione.');
		}else{
			vex.dialog.buttons.YES.text = 'Conferma';
			vex.dialog.buttons.NO.text = 'Modifica';
			vex.dialog.confirm({
			  message: 'Formazione completata. Vuoi confermarla?',
			  callback: function(value) {
				  if( value ){
					  $.fancybox.showLoading();
					  var id_formazione = $("#IndovinaFormazione .formazioni").attr("data-id");
					  var data = $("#IndovinaFormazioneForm").serialize();
					  //alert(data);

					  $.ajax({
						 url: System.sp,
						 timeout: System.timeout,
						 type: 'POST',
						 data: data,
						 complete: function(xhr,error){
							 $.fancybox.hideLoading();
							 console.log(xhr);
							 console.log(error);
						},
						 success: function(data){
							console.log(data);
							data = $.parseJSON(data);
							if( data.status ){
								$("#changeForm").submit();
							}else{
								vex.dialog.alert( data.msg );
							}
						 }
					  });
				  }
			  }
			});	
		}
	},
	
	OpenClassifica: function(){
		var btn = $("#IndovinaFormazione-OpenClassifica-btn");
		if( btn.attr("data-opened")=="1" ){
			btn.attr("data-opened",0);
			btn.children(".glyphicon").removeClass("glyphicon-chevron-up").addClass("glyphicon-chevron-down");
		}else{
			btn.attr("data-opened",1);
			btn.children(".glyphicon").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-up");
		}
		this.classificaOpened = !this.classificaOpened;
		$("#IndovinaFormazione-Classifica").slideToggle();
	}
};