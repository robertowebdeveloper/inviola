// JavaScript Document
$(document).ready(function(e) {
	IndovinaPrimoMarcatore.Init();
});

var IndovinaPrimoMarcatore = {
    box: null,

	Init: function(){
		var This = this;

        this.box = $("#IndovinaPrimoMarcatore");
        var h = this.box.height();
        if( !this.box.hasClass("slimScrollEnable") ){
            this.box.addClass("slimScrollEnable").slimScroll({
                height: h+"px",
                alwaysVisible: true
            });
        }

        var gameEnable = $("#IM_form").attr("data-game-enable");

        if( gameEnable=="1" ) {
            $(".IM_Player").click(function (e) {
                e.preventDefault();
                var btn = $(this);
                if (!btn.hasClass('unselected') && !btn.hasClass('selected')) {
                    var id = btn.attr("data-id");
                    $("#IM_form input[name='id_giocatore']").val(id);
                    This.enablePlayer(id);
                    This.Submit();
                }
            });
            $("#IM_modify-btn").click(function (e) {
                This.enableModify();
            });
        }
	},

    enableModify: function(){
        this.disablePlayers();
        $("#IM_modify-btn").addClass('hide  ');
    },

    enablePlayer: function( id_giocatore ){
        $(".IM_Player").addClass('unselected');
        $(".IM_Player[data-id='" + id_giocatore + "']").removeClass('unselected').addClass('selected');
    },

    disablePlayers: function(){
        $(".IM_Player").removeClass('selected').removeClass('unselected');
    },

    Submit: function(e){
        var This = this;
        //e.preventDefault();
        vex.dialog.buttons.YES.text = 'Conferma';
        vex.dialog.buttons.NO.text = 'Annulla';
        vex.dialog.confirm({
            message: 'Confermi?',
            callback: function(value) {
                if( value ){
                    $.fancybox.showLoading();
                    var data = $("#IM_form").serialize();
                    //alert(data);

                    $.ajax({
                        url: System.sp,
                        timeout: System.timeout,
                        type: 'POST',
                        data: data,
                        complete: function(xhr,error){
                            $.fancybox.hideLoading();
                            console.log(xhr);
                            console.log(error);
                        },
                        success: function(data){
                            console.log(data);
                            data = $.parseJSON(data);
                            if( data.status ){
                                document.location = document.location.href;
                                $("#IM_modify-btn").removeClass('hide');
                            }else{
                                This.disablePlayers();
                                vex.dialog.alert( data.msg );
                            }
                        }
                    });
                }else{
                    This.disablePlayers();
                }
            }
        });
    }
};