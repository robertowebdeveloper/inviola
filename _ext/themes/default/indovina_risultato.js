// JavaScript Document
$(document).ready(function(e) {
	IndovinaRisultato.Init();
});

var IndovinaRisultato = {
	Init: function(){
		var This = this;

        $("a[data-up]").click(function(e){
            e.preventDefault();
            var el = $(this).attr("data-up");
            This.changeGoal(el,'up');
        });
        $("a[data-down]").click(function(e){
            e.preventDefault();
            var el = $(this).attr("data-down");
            This.changeGoal(el,'down');
        });

        $("#IndovinaRisultato").submit(function(e){
           This.Submit(e);
        });

        $(".Button[data-id='modify-btn']").click(function(e){
            This.enableModify();
        });
	},

    changeGoal: function(el,w){
        var goal_el = $("input[name='goal" + el + "']");
        var goal = goal_el.val();
        goal = parseInt(goal);
        if(w=='up'){
            goal++;
        }else{
            goal = goal>0 ? goal-1 : 0;
        }
        goal_el.val(goal);
        $(".Goal" + el + " i").html(goal);
    },

    enableModify: function(){
        $(".up , .down").removeClass('disable');
        $(".Button[data-id='modify-btn']").addClass('hide');
        $(".Button[data-id='submit-btn']").removeClass('hide');
    },

    Submit: function(e){
        e.preventDefault();
        vex.dialog.buttons.YES.text = 'Conferma';
        vex.dialog.buttons.NO.text = 'Modifica';
        vex.dialog.confirm({
            message: 'Confermi?',
            callback: function(value) {
                if( value ){
                    $.fancybox.showLoading();
                    var data = $("#IR_form").serialize();
                    //alert(data);

                    $.ajax({
                        url: System.sp,
                        timeout: System.timeout,
                        type: 'POST',
                        data: data,
                        complete: function(xhr,error){
                            $.fancybox.hideLoading();
                            console.log(xhr);
                            console.log(error);
                        },
                        success: function(data){
                            console.log(data);
                            data = $.parseJSON(data);
                            if( data.status ){
                                document.location = document.location.href;
                            }else{
                                vex.dialog.alert( data.msg );
                            }
                        }
                    });
                }
            }
        });
    }
};