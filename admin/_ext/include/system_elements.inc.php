<div id="Overlay"></div>
<div id="OverlayLoader"><img src="_ext/img/loader2.gif" style="vertical-align: middle; margin-right: 10px;"><?=$S->L('Caricamento in corso'); ?>&hellip;</div>

<div id="ModalBox" class="modal hide">
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Info</h3>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <a href="#" onclick="$('#ModalBox').modal('toggle');" class="btn"><?=$S->L('Chiudi'); ?></a>
      </div>
</div>
