$(document).ready(function(e) {
    System.Init();
});

var System = {
	sp: null, //services path
	
	Init: function(){
		var This = this;
		
		this.sp = _PARAMETRI["basedir"] + 'services.php';

		This.setMain();
		$(window).resize( This.setMain );
		
		//Inizializzo a href per ordinamento
		$("table.table a[data-order]").each(function(index, element) {
            $(this).attr("href",'javascript:void(0);').click(function(){
				Search.setOrder( $(this).attr("data-order") );
			});
        });
		//FINE Inizializzo a href per ordinamento
	},
	
	Overlay_status: false,
	Loader_status : false,
	Overlay: function(show,className,show_loader){
		var ts = 250;
		
		var o = $("#Overlay");
		o.removeClass();
		o.addClass("Overlay_"+className);
		
		if( show && !this.Overlay_status ){
			var easing = 'easeInSine';
			
			var hPage = $(document).height();
			o.css("height",  hPage+"px" );
			o.fadeIn(ts,easing);
			this.Overlay_status = true;
			
			if( show_loader ){
				var o = $("#OverlayLoader");
				var w = o.width();
				var h = o.height();
				
				var ml = parseInt( w/2);
				var mt = parseInt( h/2);
				mt *= -1;
				mt += $(document).scrollTop();
				o.css({
					left:	'50%',
					"margin-left" : "-"+ml+"px",
					top: '50%',
					"margin-top"	:	mt+"px"
				}).fadeIn(ts,easing);
				this.Loader_status = true;
			}
			
		}else if(!show && this.Overlay_status){
			var easing = 'easeOutSine';
			o.fadeOut(ts,easing);
			this.Overlay_status = false;
			if( this.Loader_status ){
				$("#OverlayLoader").fadeOut(ts,easing);
				this.Loader_status = false;
			}
		}
	},
	
	setMain: function(){
		var wPage = $(window).width();
		var hPage = $(window).height();
		var hMain = $("#Main").height();
		
		var o = $("#Main");
		var w = wPage - $("#Left").width();
		if( hPage>hMain ){
			var h = hPage -= $("header").height();
		}else{
			var h = hMain;
		}
		o.css({
			position: 'absolute',
			width: w+"px",
			//height: h+"px"
		});
	},
	
	Confirm: function(txt,callback){
		var ok = confirm(txt);
		if(	ok ){
			eval(callback+"();");
		}
	}
};

var Search = {
	idForm: 'listForm',
	
	Loader: function(show){
		var list = $("#List");
		var loader = $("#ListLoader");
		
		if( show ){
			list.hide();
			loader.fadeIn();
		}else{
			list.fadeIn();
			loader.hide();
		}
	},
	
	setOrder: function(campo){
		var o = $("#" + this.idForm + " input[name='order']");
		var dir = $("#" + this.idForm + " input[name='order_dir']");
		
		var v = o.val();
		//alert(v+" "+campo+" "+dir.val())
		if( v==campo && dir.val()=="asc"){
			dir.val("desc");
		}else{
			dir.val("asc");
		}
		
		o.val( campo );
		this.Start();
	},
	
	selectAll: function(){
		var f = $("#" + this.idForm);
		var ck = f.find("input[name='selectAll']").is(":checked");
		f.find("input[name^='delete']").each(function(index, element) {
			var c = $(this).get(0).checked = ck;
        });
	},
	
	Delete: function(txt){
		var This = this;
		var ok = System.Confirm(txt, "Search.DeleteConfirm" );
	},
	DeleteConfirm: function(){
		var This = this;
		System.Overlay(true,'whitenoise',true);
		var f = $("#" + this.idForm);
		
		$.ajax({
			url: System.sp,
			data: f.serialize()+"&delete=1",
			type: 'POST',
			success: function(data){
				System.Overlay(false);
				if(data.status){
					This.Start();
				}
			}
		});
	},
	
	Start: function(){
		var This = this;
		var f = $("#" + this.idForm);
		This.Loader(true);
		$.ajax({
			type: 'POST',
			url: System.sp,
			data: f.serialize(),
			success: function(data){
				//alert(data);
				This.Loader(false);
				//alert(data.status)
				if(data.status>0){
					$("#List > tbody").html( data.list );
					$("#List *[data-name='risultati']").html( data.n );
					
					if( data.n==0 ){
						$("#List").hide();
						$("#noResult").show();
					}else{
						$("#List").show();
						$("#noResult").hide();
					}
					
					var order = data.order;// $("#List input[name='order']").val();
					var order_dir = data.order_dir; //$("#List input[name='order_dir']").val();
					
					$("#" + This.idForm + "  a.order").removeClass("orderAsc").removeClass("orderDesc");
					var classOrder = order_dir=="asc" ? "orderAsc" : "orderDesc";
					
					$("#" + This.idForm + " a[data-order='" + order + "']").addClass( classOrder );
					//alert(order+" "+classOrder)
					
					$("#" + This.idForm + " tr[data-url]").each(function() {
						var url = $(this).attr("data-url");
						$(this).find("td").each(function() {
                        	if( !$(this).hasClass("noClickable") ){
								$(this).click(function(){
									document.location = _PARAMETRI["basedir"] + url;
								});
							}
						});
                    });
				}else if(data.status<0){
					alert("Query: " + data.q);
				}else{
					alert(data.msg);
				}
			}
		});
	}
};

var Scheda = {
	idForm: 'saveForm',
	
	Load: function(){
		var This = this;
		
		var o = $("#"+this.idForm);
		$.ajax({
			type: 'POST',
			url: System.sp,
			data: o.serialize() + "&load=1",
			success: function(data){
				if(data.status){
					This.LoadData( data.fields );
				}else{
					//alert(data.msg);
				}
			}
		});
	},
	LoadData: function(fields){
		var o = $("#"+this.idForm);
		for(var i in fields){
			var field = o.find("*[name='"+i+"']");
			var type = field.attr("type");
			switch(type){
				case "checkbox":
					var ck = fields[i]>0;
					field.attr("checked",ck);
				case "password":
					break;
				default:
					field.val( fields[i] );
					break;
			}
		}
		
		this.enableUploadFile();
		
		if( this.loadData_callback ){
			this.loadData_callback(fields);
		}
	},
	
	SaveGoTo: null,//se settata e il salvataggio va a buon fine va al link indicato
	Save: function(){
		var This = this;
		System.Overlay(true,'whitenoise',true);
		var x = $("#"+this.idForm).serialize();
		
		$("textarea[data-tinymce='1']").each(function(index, element) {
            var id = $(this).attr("id");
			var txt = tinyMCE.get( id ).getContent();
			$(this).val( txt );
        });
		//alert($("#"+this.idForm).serialize())
		$.ajax({
			url: System.sp,
			type: 'POST',
			data: $("#"+this.idForm).serialize(),
			success: function(data){
				console.log(data);
				//alert(data);
				//alert(data.q)
				System.Overlay(false);	
				
				if( data.status ){
					This.LoadData(data.fields);
					if( This.SaveGoTo ){
						document.location = This.SaveGoTo;
					}
				}else{
					//alert(data.msg);
					$("#ModalBox .modal-body").html( data.msg );
					$("#ModalBox").modal({
						'backdrop'	: true	
					});
				}			
			}
		});
	},
	
	enableUploadFile: function(){
		var This = this;
		//Carica foto se ci sono
		if( $(".fotoUpload").length>0 ){
			var nLoaded = 0;
			$(".fotoUpload").each(function(index, element) {
				var id = $(this).find("input[name='foto']").attr("data-id");
				var id_file = $("input[name='" + id + "']").val();
				if( id_file>0 ){
					nLoaded++;	
				}
			});

			$(".fotoUpload").each(function(index, element) {
				var id = $(this).find("input[name='foto']").attr("data-id");
				var id_file = $("input[name='" + id + "']").val();
				if( id_file>0 ){
					$.ajax({
						url: System.sp,
						type: 'POST',
						data: "az=getPathFile&id_file="+id_file,
						success: function(data){
							if( data.status ){
								var showNext = index<nLoaded-1 ? false : true;
								This.loadFoto(id,id_file,data.path,showNext);
							}else{
								$("#ModalBox .modal-body").html( data.msg );
								$("#ModalBox").modal({
									'backdrop'	: true	
								});
							}
						}
					});
				}
            });
		}
		
		//abilito l'evento sul campo file		
		$(":file").change(function(){
			var filedata = $(this).get(0);
			var formdata = window.FormData ? new FormData() : false;
			
			var len = filedata.files.length, img, reader, file;
		
			for(var i=0;i<len;i++) {
				file = filedata.files[i];	
				if (window.FileReader) {
					reader = new FileReader();
					/*reader.onloadend = function(e) {
						showUploadedItem(e.target.result, file.fileName);
					};*/
					reader.readAsDataURL(file);
				}
				if (formdata) {
					formdata.append( $(this).attr("name") , file);
				}
			}
	
			formdata.append("az",'upload_foto');
			if( $(this).attr("data-type")=='all' ){
				formdata.append("control_type_file",'0');
			}else{
				formdata.append("control_type_file",'1');
			}
			//formdata.append("id", id);
			var id = $(this).attr("data-id");
			
			if (formdata) {
				$.ajax({
					url: System.sp,
					type: "POST",
					data: formdata,
					processData: false,
					contentType: false,
					xhr: function(){
						var myXhr = $.ajaxSettings.xhr();
						if(myXhr.upload){
							myXhr.upload.addEventListener('progress',function(e){
								//console.log( 'file_progress_' + id);
								if(e.lengthComputable){
									$('#file_progress_'+id).show().attr({value:e.loaded,max:e.total});
									//alert(e.loaded+" "+e.total);
									if(e.loaded==e.total){
										$('#file_progress_'+id).fadeOut(1000);	
									}
								}	
							},false);
						}
						return myXhr;
					},
					success: function(data) {
						//alert(data.status);
						if(data.status){
							This.loadFoto(id,data.id,data.path,true);
						}else{
							//alert(data.msg);	
							$("#ModalBox .modal-body").html( data.msg );
							$("#ModalBox").modal({
								'backdrop'	: true	
							});
						}
					},
					complete: function(res){},
					error: function(res) {}
				});
			}
		});	
	},
	
	deleteMainFile: function(id,multiFileOn){
		var ok = confirm('Confermi eliminazione foto?');
		if( ok ){
			$("input[name='" + id + "']").val('');
			$("#fotoProfilo_" + id + " .fotoProfiloImg").attr("href",'#');
			$("#fotoProfilo_" + id + " .fotoProfiloImg > img").attr("src",'').css("width","150px");
			$("#fotoProfilo_" + id).css('display','none');
			
			if( multiFileOn ){
				if( ! ( $(".fotoUpload-" + id).has('data-index') && $(".fotoUpload-" + id).has('data-idbase') ) ){//si possono caricare più files
					$(".fotoUpload-" + id).show();
				}
			}else{
				$(".fotoUpload-" + id).show();	
			}
		}
	},
	
	loadFoto: function(id,id_file,path,showNextUpload){
		$("input[name='" + id + "']").val(id_file);
		var x = $("#fotoProfilo_" + id + " .fotoProfiloImg");
		if( x.attr("data-type")=='all' ){
			$("#fotoProfilo_" + id + " .fotoProfiloImg").attr("href","../../../"+path);
			x.html( path );	
		}else{
			$("#fotoProfilo_" + id + " .fotoProfiloImg").attr("href",path);
			$("#fotoProfilo_" + id + " .fotoProfiloImg > img").attr("src",path).css("width","150px");
		}
		$("#fotoProfilo_" + id).css('display','inline-block');
		$(".fotoUpload-" + id).hide();
		
		if( showNextUpload && $(".fotoUpload-" + id).has('data-index') && $(".fotoUpload-" + id).has('data-idbase') ){//si possono caricare più files
			var index = $(".fotoUpload-" + id).attr('data-index');
			var idbase = $(".fotoUpload-" + id).attr('data-idbase');
			index = parseInt(index);
			index++;
			$(".fotoUpload-" + idbase + index).show();
		}
	}
};