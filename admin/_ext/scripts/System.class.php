<?php
class System{
	public function __construct(){
		global $cn,$_PARAMETRI,$_db_prefix;
		$this->cn = &$cn;
		$this->_dp = $_db_prefix;
		$this->_PARAMETRI = &$_PARAMETRI;
	}
	public function __destruct(){
		unset( $this->cn );	
	}
	
	public function Uri($path=""){
		return $this->_PARAMETRI["basedir"] . $path;
	}
	
	public function L($txt){
		return $txt;
	}
	public function Capitalize($txt){
		return strtoupper(substr($txt,0,1)) . strtolower(substr($txt,1));
	}
	
	public function P($key){
		$q = "SELECT `val` FROM {$this->_dp}parameters WHERE `k`='{$key}'";
		$v = $this->cn->OF($q);
		return $v;	
	}
	
	public function SingleFile($input_name,$type="photo",$multiupload=false){
		$code = array();
		$code[] = '<input type="hidden" name="' . $input_name . '">';
		
		$multiple = $multiupload ? ' multiple' : '';
		
		if( $type=="photo"){
			$code[] = '<div class="fotoUpload fotoUpload-' . $input_name . '">
				<a href="javascript:void(0);" class="btn"><input type="file" data-id="' . $input_name . '" name="foto"' . $multiple . '>' . $this->L('Carica File') . '</a>
					   <progress id="file_progress_'. $input_name .'" class="hide"></progress>
					</div>';
			$code[] = '<div id="fotoProfilo_'. $input_name .'" class="fotoProfilo">
					<a href="#" target="_blank" class="fotoProfiloImg"><img></a><br><br>
						<a href="javascript:void(0);" onclick="Scheda.deleteMainFile(\'' . $input_name .'\');" class="btn btn-danger">' . $this->L('Elimina') .'</a>
				   </div>';
		}else{
			$code[] = '<div class="fotoUpload fotoUpload-' . $input_name . '">
				<a href="javascript:void(0);" class="btn"><input type="file" data-id="' . $input_name . '" data-type="all" name="foto"' . $multiple . '>' . $this->L('Carica File') . '</a>
					   <progress id="file_progress_'. $input_name .'" class="hide"></progress>
					</div>';
			$code[] = '<div id="fotoProfilo_'. $input_name .'" class="fotoProfilo">
					<a href="#" target="_blank" class="fotoProfiloImg" data-type="all"></a><br><br>
						<a href="javascript:void(0);" onclick="Scheda.deleteMainFile(\'' . $input_name .'\',false);" class="btn btn-danger">' . $this->L('Elimina') .'</a>
				   </div>';
		}
		
		return implode("\n",$code);
	}
	
	public function MultiFile($input_name_base,$type="photo",$n=100){
		$ret = array();
		for($index=0;$index<$n;$index++){
			$input_name = $input_name_base . $index;
			$code = array();
			$code[] = '<input type="hidden" name="' . $input_name . '">';
			
			if( $type=="photo"){
				$hide = $index>0 ? ' hide' : '';
				$code[] = '<div class="fotoUpload fotoUpload-' . $input_name . $hide . '" data-index="' . $index . '" data-idbase="' . $input_name_base . '">
					<br>
					<a href="javascript:void(0);" class="btn"><input type="file" data-id="' . $input_name . '" name="foto">' . $this->L('Carica File') . '</a>
						   <progress id="file_progress_'. $input_name .'" class="hide"></progress>
						</div>';
				$code[] = '<div id="fotoProfilo_'. $input_name .'" class="fotoProfilo">
						<a href="#" target="_blank" class="fotoProfiloImg"><img></a><br><br>
							<a href="javascript:void(0);" onclick="Scheda.deleteMainFile(\'' . $input_name .'\',true);" class="btn btn-danger">' . $this->L('Elimina') .'</a>
					   </div>';
			}
			
			$ret[] = implode("\n",$code);
		}
		return implode("\n",$ret);
	}
	
	public function User(){
		$q = "SELECT * FROM {$this->_dp}admin WHERE `login`='{$_SESSION[user]}'";
		$u = $this->cn->OQ($q);
		if( $u!=-1 ){
			return $u;
		}
		return false;
	}
	
	public function Permessi($id_admin="auto",$id_sezione){
		if($id_admin=="auto"){
			$User = $this->User();
			$id_admin = $User["id"];
		}
		$q = "SELECT permission FROM {$this->_dp}admin_permissions WHERE id_admin = {$id_admin} AND id_section={$id_sezione} LIMIT 1";
		return $this->cn->OF($q);	
	}
}
?>