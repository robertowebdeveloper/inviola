<?php
require("_ext/scripts/main.inc.php");
if( isset($_REQUEST["logout"]) ){
	unset($_SESSION["user"]);		
}
?><!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title><?=$_Gruppo["title"]; ?></title>
<link href="_ext/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="all">
<link href="_ext/css/layout_login.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="_ext/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="_ext/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript" src="_ext/js/plugins/jquery.easing.min.js"></script>

<script type="text/javascript" src="_ext/js/main.js"></script>
<script type="text/javascript"><!--

var _PARAMETRI = new Array();
<?php foreach($_PARAMETRI as $k=>$v){ ?>_PARAMETRI['<?=$k; ?>'] = '<?=$v; ?>';<?php } ?>

var Login = function(){
	$("#Loader").show();
	$("#msg").html("");
	$.ajax({
		url: "services.php",
		type: "POST",
		data: $("#loginForm").serialize(),
		success: function(data){
			console.log(data);
			//alert(data)
			//data = $.parseJSON(data);
			$("#Loader").hide();
			if(data.status){
				$("#login_ajax").val(0);
				var x = $("#loginForm").get(0);
				x.onsubmit = null;
				$("#loginForm").attr("action","pdc.php").submit();
			}else{
				$("#msg").html( data.msg );	
			}
		}
	});
};
--></script>
</head>

<body>

<section id="Main">
	<div id="Login"><form id="loginForm" enctype="application/x-www-form-urlencoded" method="post" action="#" onsubmit="Login();return false;">
    	<input type="hidden" name="az" value="login">
        <input type="hidden" id="login_ajax" name="login_ajax" value="1">
    	<img id="Logo" src="_ext/img/logo.png"><br><br>
        <input type="text" id="lg" name="lg" placeholder="Nome utente">
        <input type="password" id="pw" name="pw" placeholder="Password">
        <div class="block">
	        <a href="javascript:void(0);" onclick="Login();" class="inputBtn">Login</a>
            <img src="_ext/img/login/loader.gif" id="Loader">
		</div>
        <br>
        <div id="msg"></div>
        
        <div style="overflow: hidden; height:0;"><input type="submit" value="s"></div>
	</form></div>
</section>

</body>
</html>
<?php
$cn->Close();
unset($cn);
?>