<?php
require("../../../module/nmefw.php");
$cn = new NE_mysql(0);

$q = rawurldecode( $_GET["q"] );
//echo "qui: ".$q;exit;
$ris = $cn->Q($q,true);
//print_r($ris);exit;
if( $_GET["format"]=="xls" ){
	$xls = new NE_Excel();
	$xls->Header("transazioni.xls","xls","attachment");
	$xls->BOF();
	
	$iRow=0;
	//RIGA INTESTAZIONE
	$iCol=0;
	$xls->WriteLabel($iRow,$iCol++,"ID Ordine");
	$xls->WriteLabel($iRow,$iCol++,"Negozio");
	$xls->WriteLabel($iRow,$iCol++,"Data");
	$xls->WriteLabel($iRow,$iCol++,"Codice Card");
	$xls->WriteLabel($iRow,$iCol++,"Numero Card");
	$xls->WriteLabel($iRow,$iCol++,"Cliente");
	$xls->WriteLabel($iRow,$iCol++,"Punti transati");
	$xls->WriteLabel($iRow,$iCol++,"Importo");
	$xls->WriteLabel($iRow,$iCol++,"Tipo di operazione");
	$xls->WriteLabel($iRow,$iCol++,"Categoria");
	$xls->WriteLabel($iRow,$iCol++,"Codice Prodotto");
	$xls->WriteLabel($iRow,$iCol++,"Prodotto");
	$xls->WriteLabel($iRow,$iCol++,"ID Prodotto");
	$xls->WriteLabel($iRow,$iCol++,"Quantita'");
	$iRow++;
	
	foreach($ris as $r){
		$nome_prodotto = $cod_prodotto = $categoria = "-";
		if($r["id_prodotto"]>0){
			$q = "SELECT p.*, c.nome AS cat FROM prodotti p INNER JOIN categorie c ON p.id_categoria = c.id WHERE p.id = {$r[id_prodotto]}";
			$Prod = $cn->OQ($q);
			$id_prodotto = $Prod["id"];
			$nome_prodotto = $Prod["nome"];
			$cod_prodotto = $Prod["codice"];
			$categoria = $Prod["cat"];
		}
		
		$iCol=0;
		
		$xls->WriteLabel($iRow,$iCol++, "#" . $r["id_ordine"]);
		$xls->WriteLabel($iRow,$iCol++, $r["negozio"] );
		$xls->WriteLabel($iRow,$iCol++, date("d/m/Y", strtotime($r["data_ordine"])) );
		$xls->WriteLabel($iRow,$iCol++, $r["icard"] );
		$xls->WriteLabel($iRow,$iCol++, $r["card_number"] );
		$xls->WriteLabel($iRow,$iCol++, $r["nome"] );
		$xls->WriteNumber($iRow,$iCol++, $r["punti_caricati"] );
		$xls->WriteNumber($iRow,$iCol++, number_format($r["prezzo"],2,".","") );
		$tipo_operazione = $r["punti_caricati"]<0 ? 'Scarico punti' : 'Carico punti';
		$xls->WriteLabel($iRow,$iCol++, $tipo_operazione );
		$xls->WriteLabel($iRow,$iCol++, $categoria );
		$xls->WriteLabel($iRow,$iCol++, $cod_prodotto );
		$xls->WriteLabel($iRow,$iCol++, $nome_prodotto );
		$xls->WriteLabel($iRow,$iCol++, "#{$id_prodotto}" );
		$xls->WriteLabel($iRow,$iCol++, $r["qta"] );
		$iRow++;	
	}
	
	$xls->EOF();
}else{
	$xls = new NE_Excel();
	$xls->Header("transazioni.csv","csv","attachment");
	
	$csv = array();
	$csv[] = "Ordine ID;Negozio;Data;Codice Card;Numero Card;Cliente;Punti transati;Importo;Tipo di operazione;Categoria;Codice Prodotto;Prodotto;ID Prodotto;Quantita';";
	
	foreach($ris as $r){
		$nome_prodotto = $cod_prodotto = $categoria = "-";
		if($r["id_prodotto"]>0){
			$q = "SELECT p.*, c.nome AS cat FROM prodotti p INNER JOIN categorie c ON p.id_categoria = c.id WHERE p.id = {$r[id_prodotto]}";
			$Prod = $cn->OQ($q);
			$id_prodotto = $Prod["id"];
			$nome_prodotto = $Prod["nome"];
			$cod_prodotto = $Prod["codice"];
			$categoria = $Prod["cat"];
		}
		
		$row = array();
		$row[] = "#{$r[id_ordine]}";
		$row[] = $r["negozio"];
		$row[] = date("d/m/Y", strtotime($r["data_ordine"]));
		$row[] = $r["icard"];
		$row[] = $r["card_number"];
		$row[] = $r["nome"];
		$row[] = $r["punti_caricati"];
		$row[] = number_format($r["prezzo"],2,".","");
		$tipo_operazione = $r["punti_caricati"]<0 ? 'Scarico punti' : 'Carico punti';
		$row[] = $tipo_operazione;
		$row[] = $categoria;
		$row[] = $cod_prodotto;
		$row[] = $nome_prodotto;
		$row[] = "#{$id_prodotto}";
		$row[] = $r["qta"];
		
		$csv[] = implode(";",$row);
	}
	echo implode("\n",$csv);
}

$cn->Close();
unset($cn,$xls,$q,$ris);
?>