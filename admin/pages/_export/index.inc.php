<form id="listForm">
	<input type="hidden" name="sessione" value="<?=session_id(); ?>">
    <input type="hidden" name="id_gruppo" value="1">
    
	<input type="hidden" name="az" value="esporta_transazioni">
    
    <h3><?=$S->L("Esporta i dati delle transazioni eseguite"); ?></h3>
    
    <div class="fl">
        <b><?=$S->L('Periodo'); ?></b><br>
        <?=$S->L('Da'); ?>
        <div id="datetimepicker1" class="input-append date">
            <input name="dal" data-format="dd/MM/yyyy" type="text" class="input-small"></input>
            <span class="add-on">
                <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
            </span>
        </div>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <?=$S->L('Al'); ?>
        <div id="datetimepicker2" class="input-append date">
            <input name="al" data-format="dd/MM/yyyy" type="text" class="input-small"></input>
            <span class="add-on">
                <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
            </span>
        </div>
          <script type="text/javascript"><!--
            $(function() {
                $('#datetimepicker1').datetimepicker({
                    pickTime: false
                });
                $('#datetimepicker2').datetimepicker({
                    pickTime: false
                });
            });
          --></script>
	</div>
    
    <div class="fl" style="margin-left: 50px;">
    	<b><?=$S->L('Tipo di operazione'); ?></b><br>
        <select name="tipo">
        	<option value="0"><?=$S->L('Tutte'); ?></option>
            <option value="carico"><?=$S->L('Carico punti'); ?></option>
            <option value="scarico"><?=$S->L('Scarico punti'); ?></option>
            <option value="richiesta_premi"><?=$S->L('Richiesta Premi'); ?></option>
        </select>
    </div>
    
    <div class="fl" style="margin-left: 50px;">
        <b><?=$S->L('Negozio'); ?></b><br>
        <select name="id_negozio">
            <option value="0">Tutti</option>
            <?php
            $q = "SELECT * FROM {$_db_prefix}stores WHERE 1 ORDER BY `name` ASC";
            $ris = $cn->Q($q,true);
            foreach($ris as $r){
                echo "<option value=\"{$r[id]}\">{$r[name]}</option>";
            }
            ?>
        </select>
    </div>
    
    <div class="cb"></div>
    <hr>
    
    <script type="text/javascript">
	var Export = function(format){
		$.ajax({
			url: System.sp,
			data: $("#listForm").serialize(),
			type: 'POST',
			success: function(data){
				//alert(data)
				if(data.status){
					$("input[name='dal']").val( data.dal );
					$("input[name='al']").val( data.al );
					//alert(data.qclear);
					if( data.n>0 ){
						$("#exportframe").attr("src","<?=$_PARAMETRI["basedir"]; ?>pages/export/export.php?q=" + data.q + "&format=" + format);
					}else{
						$("#ModalBox .modal-body").html("<?=$S->L('Nessuna transazione nel periodo indicato'); ?>");
						$("#ModalBox").modal({
							'backdrop'	: true	
						});
					}
				}else{
					$("#ModalBox .modal-body").html( data.msg );
					$("#ModalBox").modal({
						'backdrop'	: true	
					});
				}
			}
		});
	};
	</script>
    <iframe id="exportframe"></iframe>
    <div class="left">
    	<b><?=$S->L('Esporta'); ?></b>
    	<a href="javascript:void(0);" onclick="Export('xls');"><img src="_ext/img/icon/excel.png" style="margin-left: 30px;"></a>
        &nbsp;&nbsp;&nbsp;
    	<a href="javascript:void(0);" onclick="Export('csv');"><img src="_ext/img/icon/csv.png"></a>
    </div>
    
</form>