<?php
$id = $_GET["id"]>0 ? $_GET["id"] : 0;
//echo $id;
?>
<script type="text/javascript"><!--
$(document).ready(function(e) {
	Scheda.Load();
});
--></script>
<form id="saveForm">
	<input type="hidden" name="sessione" value="<?=session_id(); ?>">
    <input type="hidden" name="id" value="<?=$id; ?>">
	<input type="hidden" name="az" value="admin_save">
    
    <div>
    	<a href="<?=$S->Uri('sezioni/admin'); ?>" class="btn fr"><i class="icon-circle-arrow-left"></i> <?=$S->L('Indietro'); ?></a>
        <h3><?=$S->L("Scheda Utente"); ?></h3>
    </div>
    <hr>
    
    <div id="ListLoader" class="Loader1"></div>
    <table id="List" class="table table-striped">
    <tbody>
    	<tr>
        	<td width="200">
            	<label><?=$S->L("Nome"); ?>*</label>
            </td>
            <td><input type="text" name="name" class="input-xlarge"></td>
        </tr>
        <tr>
        	<td>
            	<label><?=$S->L("Login"); ?>*</label>
            </td>
            <td><input type="text" name="login" class="input-xlarge"></td>
        </tr>
        <tr>
        	<td>
            	<label><?=$S->L("Password"); ?>*</label>
            </td>
            <td>
            	<input type="password" name="pw" class="input-xlarge">
                <?php if($id>0){ ?>
                <span class="help-block"><?=$S->L('Compilare solo se si desidera modificare la password'); ?></span>
                <?php } ?>
            </td>
        </tr>
        <tr>
        	<td>
            	<label><?=$S->L("Conferma password"); ?></label>
            </td>
            <td><input type="password" name="pw2" class="input-xlarge"></td>
        </tr>
        <tr>
        	<td>
            	<label><?=$S->L("E-mail"); ?></label>
            </td>
            <td>
            	<input type="text" name="email" class="input-xlarge">
			</td>
        </tr>
        <tr>
        	<td>
            	<label for="enable"><?=$S->L("Utente attivo"); ?></label>
            </td>
            <td>
            	<input type="checkbox" id="enable" name="enable" value="1" checked>
			</td>
        </tr>
        <tr>
        	<td>PERMESSI</td>
            <td>
            	<table class="table table-striped" style="width: 500px; border: 1px solid #ccc;">
                	<thead>
                    	<th width="200">&nbsp;</th>
                        <th width="10" style="vertical-align: middle; text-align:center;">NO</th>
                        <th width="10" style="vertical-align: middle; text-align:center;">Sola lettura</th>
                        <th width="10" style="vertical-align: middle; text-align:center;">Lettura / Scrittura</th>
                    </thead>
                    <tbody>
                    	<?php
						$q = "SELECT * FROM {$_db_prefix}sections WHERE enable=1 ORDER BY `order` ASC";
						$ris = $cn->Q($q,true);
						foreach($ris as $row){
							$p = false;
							if( $id>0 ){
								$p = $S->Permessi($id,$row["id"]);
							}
							if( !$p ){
								$p="w";	
							}
						?>
                        <tr>
                        	<td><?=$row["name"]; ?></td>
                            <?php
							$arr = array("no","r","w");
							foreach($arr as $item){ ?>
	                            <td><label class="block center"><input type="radio" name="permessi_<?=$row["id"]; ?>" value="<?=$item; ?>"<?=$item==$p ? ' checked="checked"' : ""; ?>></label></td>
                            <?php } ?>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
        	<td>
            	<label for="configuration_on"><?=$S->L("Accesso Configurazione"); ?></label>
            </td>
            <td>
            	<input type="checkbox" id="configuration_on" name="configuration_on" value="1" checked>
			</td>
        </tr>
    </tbody>
    <tfoot>
    	<tr>
        	<td><span class="textLittle">*<?=$S->L("Campo obbligatorio"); ?></span></td>
            <td align="right">
            	<a class="btn btn-danger" href="javascript:void(0);" onclick="Scheda.Save();">Salva</a>
            </td>
        </tr>
    </tfoot>
    </table>
</form>