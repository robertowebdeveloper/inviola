<?php
$id_block = $_GET["id_block"];

$types = array(
	"cori" => 'Cori',
	"suonerie" => 'Suonerie',
	"cover-fb" => 'Cover Facebook',
	"cruci" => 'Cruci',
	"disegni-bambini" => 'Disegni per bambini',
	"immagini-premium" => 'Immagini Premium',
	"videopremium" => 'Video Premium',
	"trova-differenze" => 'Trova Differenze',
	"trova-parole" => 'Trova Parole',
	"unisci-i-punti" => 'Unisci i puntini'
);
?>
<script type="text/javascript"><!--
$(document).ready(function(e) {
    Search.Start();
});
--></script>
<div class="right">
    <form id="newForm" action="<?=$S->Uri('sezioni/area_download/scheda'); ?>" enctype="application/x-www-form-urlencoded" method="get">
        <input type="hidden" name="id" value="0">
        <select name="type" style="vertical-align: top;">
        	<?php
			foreach($types as $k=>$v){
            	?><option value="<?=$k; ?>"><?=$v; ?></option><?php
			}
			?>
        </select>
        <!--a href="<?=$S->Uri('sezioni/area_download/scheda?&id=0'); ?>" class="btn btn-primary"><i class="icon-plus icon-white"></i> <?=$S->L('Nuovo'); ?></a-->
        <a href="#" onclick="$('#newForm').submit();" class="btn btn-primary"><i class="icon-plus icon-white"></i> <?=$S->L('Nuovo'); ?></a>
    </form>
</div>
<hr>
<form id="listForm">
	<input type="hidden" name="sessione" value="<?=session_id(); ?>">
    <input type="hidden" name="order" value="id">
    <input type="hidden" name="order_dir" value="asc">
    
	<input type="hidden" name="az" value="downloads_list">
    
    <select name="type" style="vertical-align: top;" onchange="Search.Start();">
    	<option value="">-</option>
		<?php
        foreach($types as $k=>$v){
            ?><option value="<?=$k; ?>"><?=$v; ?></option><?php
        }
        ?>
    </select>
    
    <div id="noResult"><h5><?=$S->L('Nessun risultato trovato'); ?></h5></div>
    <div id="ListLoader" class="Loader1"></div>
    <table id="List" class="table table-hover table-striped">
    <thead>
    <tr>
    	<th width="30"><a href="#" data-order="id" class="order">#</a></th>
        <th width="150"><?=$S->L("Immagine"); ?></th>
        <th><?=$S->L("Titolo"); ?></th>
        <th width="80"><?=$S->L("Tipo"); ?></th>
        <th width="50"><div class="center"><input type="checkbox" name="selectAll" onchange="Search.selectAll();"></div></th>
    </tr>
    </thead>
    <tbody></tbody>
    <tfoot>
    	<tr>
        	<td colspan="3"><b><?=$S->L("Trovati"); ?>: <span data-name="risultati"></span></b></td>
           <td><div class="center"><a href="javascript:void(0);" onclick="Search.Delete('<?=$S->L("Confermi eliminazione?"); ?>');" class="btn"><i class="icon-trash"></i></a></div></td>
        </tr>
    </tfoot>
    </table>
</form>