<?php
$id = $_GET["id"]>0 ? $_GET["id"] : 0;
//echo $id;
$type = '';
if( $id==0 ){
	$type = $_GET["type"];	
}else{
	$q = "SELECT * FROM `{$S->_dp}downloads` WHERE id = {$id}";
	$Scheda = $S->cn->OQ($q);
	$type = $Scheda["type"];
}
?>
<script type="text/javascript"><!--
$(document).ready(function(e) {
	Scheda.Load();
});
--></script>
<form id="saveForm">
	<input type="hidden" name="sessione" value="<?=session_id(); ?>">
    <input type="hidden" name="id" value="<?=$id; ?>">
	<input type="hidden" name="az" value="downloads_save">
    
    <input type="hidden" name="type" value="<?=$type; ?>">
    
    <div>
    	<a href="<?=$S->Uri('sezioni/area_download/'); ?>" class="btn fr"><i class="icon-circle-arrow-left"></i> <?=$S->L('Indietro'); ?></a>
        <h3><?=$S->L("Area Download"); ?></h3>
    </div>
    <hr>
    
    <div id="ListLoader" class="Loader1"></div>
    
    <table id="List" class="table table-striped">
    <tbody>
    
        <?php if( in_array($type,array("videopremium","cover-fb","disegni-bambini","cruci","immagini-premium","trova-differenze","trova-parole","unisci-i-punti")) ){ ?>
            <tr>
                <td width="200"><label><?=$S->L('Titolo'); ?></label></td>
               <td>
                    <input type="text" name="label" class="input-xxlarge">
               </td> 
            </tr>
        <?php } ?>
        
    	<tr>
        	<td width="200"><label><?=$S->L('Immagine'); ?>*</label></td>
            <td>
            	<?=$S->SingleFile('id_file','photo'); ?>
            </td>
        </tr>
        
        <?php if( in_array($type,array("unisci-i-punti")) ){ ?>
            <tr>
                <td><label><?=$S->L('PDF'); ?>*</label></td>
                <td>
                    <?=$S->SingleFile('id_file_pdf','all'); ?>
                </td>
            </tr>
		<?php } ?>
        
        <?php if( in_array($type,array("suonerie","cori")) ){ ?>
            <tr>
                <td><label><?=$S->L('Tipo'); ?>*</label></td>
                <td>
                    <select name="tipo">
                    	<option value="mp3">MP3</option>
                       <option value="m4r">M4R</option>
                    </select>
                </td>
            </tr>
		<?php } ?>
                <?=$type; ?>
        <?php if( in_array($type,array("videopremium")) ){ ?>
            <tr>
                <td>
                    <label><?=$S->L("Codice"); ?></label>
                </td>
                <td>
                    <textarea name="option" class="input-xxlarge" rows="5" cols="45"></textarea>
                </td>
            </tr>
		<?php } ?>
        
        <tr>
            <td>
                <label><?=$S->L("Ordine"); ?></label>
            </td>
            <td>
                <input name="order" type="text" class="input-small">
            </td>
        </tr>

    	<!--tr>
        	<td>
            	<label for="enable"><?=$S->L("Attivo"); ?></label>
            </td>
            <td><input type="checkbox" id="enable" name="enable" value="1" checked></td>
        </tr-->
    </tbody>
    <tfoot>
    	<tr>
        	<td><span class="textLittle">*<?=$S->L("Campo obbligatorio"); ?></span></td>
            <td align="right">
            	<a class="btn btn-danger" href="javascript:void(0);" onclick="Scheda.Save();">Salva</a>
            </td>
        </tr>
    </tfoot>
    </table>
</form>