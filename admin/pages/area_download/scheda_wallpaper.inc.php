<?php
$id = $_GET["id"]>0 ? $_GET["id"] : 0;
//echo $id;
$type = '';
if( $id==0 ){
	$type = $_GET["type"];	
}else{
	$q = "SELECT * FROM `{$S->_dp}downloads` WHERE id = {$id}";
	$Scheda = $S->cn->OQ($q);
	$type = $Scheda["type"];
}
?>
<script type="text/javascript"><!--
$(document).ready(function(e) {
	Scheda.Load();
});
--></script>
<form id="saveForm">
	<input type="hidden" name="sessione" value="<?=session_id(); ?>">
    <input type="hidden" name="id" value="<?=$id; ?>">
	<input type="hidden" name="az" value="downloads_wallpapers_save">
    
    <input type="hidden" name="type" value="<?=$type; ?>">
    <input type="hidden" name="group" value="<?=$Scheda['group']; ?>">
    
    <div>
    	<a href="<?=$S->Uri('sezioni/area_download/'); ?>" class="btn fr"><i class="icon-circle-arrow-left"></i> <?=$S->L('Indietro'); ?></a>
        <h3><?=$S->L("Area Download"); ?></h3>
    </div>
    <hr>
    
    <div id="ListLoader" class="Loader1"></div>
    
    <table id="List" class="table table-striped">
    <tbody>
    
        <tr>
            <td width="200"><label><?=$S->L('Titolo'); ?></label></td>
           <td>
                <input type="text" name="label" class="input-xxlarge">
           </td> 
        </tr>
        
    	<tr>
        	<td width="200"><label><?=$S->L('Immagine 1024x768'); ?>*</label></td>
            <td>
            	<?=$S->SingleFile('id_file0','photo'); ?>
            </td>
        </tr>
        <tr>
        	<td width="200"><label><?=$S->L('Immagine 1280x1050'); ?>*</label></td>
            <td>
            	<?=$S->SingleFile('id_file1','photo'); ?>
            </td>
        </tr>
        <tr>
        	<td width="200"><label><?=$S->L('Immagine 1680x1050'); ?>*</label></td>
            <td>
            	<?=$S->SingleFile('id_file2','photo'); ?>
            </td>
        </tr>
        <tr>
        	<td width="200"><label><?=$S->L('Immagine 1920x1200'); ?>*</label></td>
            <td>
            	<?=$S->SingleFile('id_file3','photo'); ?>
            </td>
        </tr>
        
        <tr>
            <td>
                <label><?=$S->L("Ordine"); ?></label>
            </td>
            <td>
                <input name="order" type="text" class="input-small">
            </td>
        </tr>
    </tbody>
    <tfoot>
    	<tr>
        	<td><span class="textLittle">*<?=$S->L("Campo obbligatorio"); ?></span></td>
            <td align="right">
            	<a class="btn btn-danger" href="javascript:void(0);" onclick="Scheda.Save();">Salva</a>
            </td>
        </tr>
    </tfoot>
    </table>
</form>