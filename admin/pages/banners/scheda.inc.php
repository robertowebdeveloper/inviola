<?php
$id = $_GET["id"]>0 ? $_GET["id"] : 0;
//echo $id;
?>
<script type="text/javascript"><!--
$(document).ready(function(e) {
	Scheda.loadData_callback = function(fields){
		$("input[name^='id_pages[']").prop("checked",false);
		for(var i in fields.id_pages){
			$("input[name='id_pages[" + i + "]']").prop("checked",true);
		}
	};
	Scheda.Load();
	showZone();
});
var selAllPages = function(){
	var v = $("#id_all_pages").is(":checked");
	$("input[name^='id_pages[']").prop("checked",v);
};
var showZone = function(){
	var v = $("select[name='zone']").val();
	$("#id_pages_div").hide();
	
	if(v=="new"){
		$("#new_zone_div").show();	
	}else if(v.toLowerCase()=="background"){
		$("#id_pages_div").slideDown();
	}else{
		$("#new_zone_div").hide();
	}
};
--></script>
<form id="saveForm">
	<input type="hidden" name="sessione" value="<?=session_id(); ?>">
    <input type="hidden" name="id" value="<?=$id; ?>">
	<input type="hidden" name="az" value="banners_save">
    
    <div>
    	<a href="<?=$S->Uri('sezioni/banners'); ?>" class="btn fr"><i class="icon-circle-arrow-left"></i> <?=$S->L('Indietro'); ?></a>
        <h3><?=$S->L("Scheda Banner"); ?></h3>
    </div>
    <hr>
    
    <div id="ListLoader" class="Loader1"></div>
    
    <table id="List" class="table table-striped">
    <tbody>
    	<tr>
        	<td width="200"><label><?=$S->L('Immagine'); ?>*</label></td>
            <td>
            	<?=$S->SingleFile('id_file','photo'); ?>
            </td>
        </tr>
        <tr>
        	<td>
            	<label><?=$S->L('Titolo'); ?></label>
            </td>
            <td>
            	<input type="text" name="name" class="input-xxlarge">
            </td>
        </tr>
        <tr>
        	<td>
            	<label><?=$S->L('Box sul sito'); ?></label>
            </td>
            <td>
            	<select name="zone" onchange="showZone();">
                	<?php
					$q = "SELECT zone FROM {$S->_dp}banners WHERE deleted IS NULL GROUP BY zone ORDER BY zone ASC";
					$list = $cn->Q($q,true);
					foreach($list as $z){
						?><option value="<?=$z["zone"]; ?>"><?=$z["zone"]; ?></option><?php
					}                        
					?>
                   <option value="new">Nuova zona</option>
                </select>
                <div id="new_zone_div" class="hide"><input type="text" name="new_zone" class="input-large"></div>
            </td>
        </tr>
    	<tr>
        	<td>
            	<label><?=$S->L("Url"); ?></label>
            </td>
            <td>
              	<input type="text" name="url" class="input-xxlarge">
                <span class="help-block"><?=$S->L('formato assoluto o relativo'); ?></span>
            </td>
        </tr>
        
        <tr>
        	<td>
            	<label><?=$S->L("Apertura"); ?></label>
            </td>
            <td>
            	<select name="target_blank">
                	<option value="0">Nella pagina</option>
                    <option value="1" selected>Pagina esterna</option>
                </select>
            </td>
        </tr>
        
        <tr>
        	<td>
            	<label><?=$S->L('Ordine'); ?></label>
            </td>
            <td>
            	<input type="text" name="order" class="input-small">
               <span class="help-block"><?=$S->L('vuoto per automatico'); ?></span>
            </td>
        </tr>
        
        <tr>
        	<td><label><?=$S->L('Visibile dal'); ?></label></td>
            <td>
            	<div id="datetimepicker1" class="input-append date">
                    <input name="show_from" data-format="dd/MM/yyyy" type="text" class="input-small"></input>
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
            </td>
        </tr>
        <tr>
        	<td><label><?=$S->L('Visibile al'); ?></label></td>
            <td>
            	<div id="datetimepicker2" class="input-append date">
                    <input name="show_to" data-format="dd/MM/yyyy" type="text" class="input-small"></input>
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
                <script type="text/javascript"><!--
				$(function() {
					$('#datetimepicker1').datetimepicker({
						pickTime: false
					});
					$('#datetimepicker2').datetimepicker({
						pickTime: false
					});
				});
			  --></script>
            </td>
        </tr>
        
    	<tr>
        	<td>
            	<label for="enable"><?=$S->L("Attivo"); ?></label>
            </td>
            <td><input type="checkbox" id="enable" name="enable" value="1" checked></td>
        </tr>
        <tr>
        	<td><label><?=$S->L("Pagine allegate"); ?></label></td>
           <td>
           		<div>Selezionare solo per <b>Banner di sfondo pagina</b></div>
               <div id="id_pages_div" style="display: none;background-color: #f4f4f4; border-radius:12px; padding: 6px 12px;">
                   <div class="row">
                       <?php
                        $q = "SELECT id,name FROM `{$S->_dp}pages` WHERE deleted IS NULL";
                        $list = $S->cn->Q($q,true);
                        foreach($list as $l){
                        ?>
                        <div class="span3">
                            <label style="padding: 8px 0;">
                               <input type="checkbox" name="id_pages[<?=$l["id"]; ?>]" value="<?=$l["id"]; ?>">
                                <?=$l["name"]; ?>
                           </label>
                        </div>
                       <?php } ?>
                        <div class="span3">
                            <label style="padding: 8px 0;">
                               <input type="checkbox" id="id_all_pages" name="id_all_pages" value="1" onchange="selAllPages();">
                               <b>Tutte</b>
                           </label>
                        </div>
                    </div>
				</div>
           </td>
        </tr>
    </tbody>
    <tfoot>
    	<tr>
        	<td><span class="textLittle">*<?=$S->L("Campo obbligatorio"); ?></span></td>
            <td align="right">
            	<a class="btn btn-danger" href="javascript:void(0);" onclick="Scheda.Save();">Salva</a>
            </td>
        </tr>
    </tfoot>
    </table>
</form>