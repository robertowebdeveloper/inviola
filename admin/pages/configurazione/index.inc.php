<?php
$id = $_GET["id"]>0 ? $_GET["id"] : 0;
//echo $id;
?>
<script type="text/javascript"><!--
$(document).ready(function(e) {
	Scheda.Load();
});
var Cache = {
	Reset: function(type){
		var ok = confirm("Confermi reset cache?");
		if( ok ){
				
		}
	}
};
--></script>
<form id="saveForm">
	<input type="hidden" name="sessione" value="<?=session_id(); ?>">
    <input type="hidden" name="id" value="<?=$id; ?>">
	<input type="hidden" name="az" value="configurazione_save">
    
    <div>
        <h3><?=$S->L("Parametri Configurazione"); ?></h3>
    </div>
    <hr>
    
    <div id="ListLoader" class="Loader1"></div>
    <table id="List" class="table table-striped">
    <tbody>
    	<tr>
        	<td width="200">
            	<label><?=$S->L("Framework frontend"); ?></label>
            </td>
            <td>
            	<select name="pp_framework_frontend">
               	<option value="">Nessuno</option>
                	<option value="jquery-1">jQuery 1.x</option>
                   <option value="jquery-2">jQuery 2.x</option>
                   <option value="jquery-1|bootstrap-2" selected>jQuery 1.x &amp; Twitter Bootstrap 2.x</option>
                   <option value="jquery-2|bootstrap-3">jQuery 2.x &amp; Twitter Bootstrap 3.x</option>
               </select>
            </td>
        </tr>
        <tr>
        	<td>
            	<label><?=$S->L("Tema grafico"); ?></label>
            </td>
            <td>
            	<?php
				$list = glob(path_site . "_ext/themes/*");
				?>
            	<select name="pp_theme"><?php
					foreach($list as $item){
						if( file_exists($item . '/config.php') ){
							include($item . '/config.php');
							if( isset($_Theme["name"]) ){
								?><option value="<?=basename($item); ?>"><?=$_Theme["name"]; ?></option><?php
								unset($_Theme);
							}
						}
					}
				?></select>
            </td>
        </tr>
        <tr>
        	<td><label for="responsive_on"><?=$S->L('Responsive'); ?></label></td>
           <td>
           		<input type="checkbox" id="responsive_on" name="pp_responsive_on" value="1" checked>
           </td>
        </tr>
        <tr>
        	<td><label for="cache_pagine"><?=$S->L('Cache pagine'); ?></label></td>
           <td>
           		<input type="checkbox" id="cache_pagine" name="pp_cache_pagine" value="1" checked>
               <a href="javascript:void(0);" onclick="Cache.Reset('pagine');" class="btn btn-mini" style="margin-left: 30px;">Reset</a>
           </td>
        </tr>
        <tr>
        	<td><label for="cache_jscss"><?=$S->L('Cache Javascript/CSS'); ?></label></td>
           <td>
           		<input type="checkbox" id="cache_jscss" name="pp_cache_jscss" value="1" checked>
               <a href="javascript:void(0);" onclick="Cache.Reset('jscss');" class="btn btn-mini" style="margin-left: 30px;">Reset</a>
           </td>
        </tr>
        
        <tr>
        	<td rowspan="4"><b><?=$S->L('Webservices Terminal'); ?></b></td>
            <td>
            	<label><?=$S->L('Path'); ?></label><input type="text" name="pp_ws_terminal_path" class="input-xxlarge">
            </td>
        </tr>
        <tr>
        	<td>
            	<label><?=$S->L('Serial Number'); ?></label>
               <input type="text" name="pp_ws_terminal_sn" class="input-small">
            </td>
        </tr>
        <tr>
        	<td>
            	<label><?=$S->L('Username'); ?></label>
               <input type="text" name="pp_ws_terminal_username" class="input-large">
            </td>
        </tr>
        <tr>
        	<td>
            	<label><?=$S->L('Password'); ?></label>
               <input type="text" name="pp_ws_terminal_password" class="input-large">
            </td>
        </tr>
        
        <tr>
        	<td rowspan="4"><b><?=$S->L('Webservices Back Office'); ?></b></td>
            <td>
            	<label><?=$S->L('Path'); ?></label><input type="text" name="pp_ws_bo_path" class="input-xxlarge">
            </td>
        </tr>
        <tr>
        	<td>
            	<label><?=$S->L('Synchro Kind'); ?></label>
               <input type="text" name="pp_ws_bo_kind" class="input-small">
            </td>
        </tr>
        <tr>
        	<td>
            	<label><?=$S->L('Username'); ?></label>
               <input type="text" name="pp_ws_bo_username" class="input-large">
            </td>
        </tr>
        <tr>
        	<td>
            	<label><?=$S->L('Password'); ?></label>
               <input type="text" name="pp_ws_bo_password" class="input-large">
            </td>
        </tr>
        
        <tr>
        	<td><b><?=$S->L('Webservices Customer Area'); ?></b></td>
            <td>
            	<label><?=$S->L('Path'); ?></label><input type="text" name="pp_ws_ca_path" class="input-xxlarge">
            </td>
        </tr>
        
        <tr>
        	<td><?=$S->L('ID Campagna'); ?></td>
            <td>
            	<input type="text" name="pp_campaign_id" class="input-small">
			</td>
        </tr>
        <tr>
        	<td><?=$S->L('Codice Campagna'); ?></td>
            <td>
            	<input type="text" name="pp_campaign_code" class="input-small">
			</td>
        </tr>
        
        <tr>
        	<td><label><?=$S->L('Google Analytics'); ?></label></td>
            <td>
            	<input type="text" name="pp_ga" class="input-large">
                <span class="help-block"><?=$S->L('Codice Google Analytics'); ?> <b>UA-xxxxxxxx-x</b></span>
            </td>
        </tr>
        
        <tr>
        	<td><label><?=$S->L('Lingue'); ?></label></td>
           <td>
           		<?php
				$q = "SELECT * FROM {$_db_prefix}langs WHERE 1 ORDER BY `name` ASC";
				$list = $cn->Q($q,true);
				?>
               <ul><?php
               	foreach($list as $l){
					?><li><?=$l["name"]; ?> (<?=$l["status"]; ?>)</li><?php
					}
			   ?></ul>
              <a href="<?=$S->Uri('sezioni/configurazione/lingue'); ?>" class="btn"><?=$S->L('Gestisci'); ?></a>
           </td>
        </tr>
        
    </tbody>
    <tfoot>
    	<tr>
        	<td><span class="textLittle">*<?=$S->L("Campo obbligatorio"); ?></span></td>
            <td align="right">
            	<a class="btn btn-danger" href="javascript:void(0);" onclick="Scheda.Save();">Salva</a>
            </td>
        </tr>
    </tfoot>
    </table>
    <br><br>
</form>