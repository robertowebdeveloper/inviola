<?php
$id = $_GET["id"]>0 ? $_GET["id"] : 0;
//echo $id;
?>
<script type="text/javascript"><!--
$(document).ready(function(e) {
	Scheda.Load();
});
var Cache = {
	Reset: function(type){
		var ok = confirm("Confermi reset cache?");
		if( ok ){
				
		}
	}
};
--></script>
<form id="saveForm">
	<input type="hidden" name="sessione" value="<?=session_id(); ?>">
    <input type="hidden" name="id" value="<?=$id; ?>">
	<input type="hidden" name="az" value="lingue_save">
    
    <div>
    	<a href="<?=$S->Uri('sezioni/configurazione'); ?>" class="btn fr"><i class="icon-circle-arrow-left"></i> <?=$S->L('Indietro'); ?></a>
        <h3><?=$S->L("Parametri Configurazione :: Lingue"); ?></h3>
    </div>
    <hr>
    
    <div id="ListLoader" class="Loader1"></div>
    <table id="List" class="table table-striped">
    <thead>
    	<tr>
        	<th style="vertical-align: top;"><?=$S->L('Nome'); ?></th>
        	<th style="vertical-align: top;"><?=$S->L('Url'); ?></th>
            <th style="vertical-align: top;"><?=$S->L('ISO'); ?>
            	<span class="help-block"><?=$S->L('es. it_IT'); ?></span>
            </th>
            <th style="vertical-align: top;"><?=$S->L('Principale'); ?></th>
            <th style="vertical-align: top;"><?=$S->L('Right-to-Left'); ?></th>
            <th style="vertical-align: top;"><?=$S->L('Stato'); ?></th>
        </tr>
    </thead>
    <tbody>
    	<?php 
		$q = "SELECT * FROM {$_db_prefix}langs WHERE 1 ORDER BY `name` ASC";
		$list = $cn->Q($q,true);
		foreach($list as $l){ ?>
    		<tr>
                <td>
                    <input type="text" name="name[<?=$l["id"]; ?>]" value="<?=$l["name"]; ?>" class="input-large">
                </td>
                <td>
                	<input type="text" name="url[<?=$l["id"]; ?>]" value="<?=$l["url"]; ?>" class="input-small">
                </td>
                <td>
                    <input type="text" name="iso[<?=$l["id"]; ?>]" value="<?=$l["iso"]; ?>" class="input-small" maxlength="5">
                </td>
                <td style="text-align: center;">
                    <input type="radio" name="is_default" id="is_default[<?=$l["id"]; ?>]" value="<?=$l["id"]; ?>"<?=$l["is_default"]==1 ? ' checked' : ''; ?>>
                </td>
                <td style="text-align: center;">
                    <input type="checkbox" name="rtl[<?=$l["id"]; ?>]" value="1"<?=$l["rtl"]==1 ? ' checked' : ''; ?>>
                </td>
                <td>
                    <select name="status[<?=$l["id"]; ?>]">
                        <?php
                        $arr = array("enable"=>'Attiva',"only_backend"=>'Solo gestionale',"disable"=>'Disattiva');
                        foreach($arr as $k=>$v){
                            $sel = $k==$l["status"] ? ' selected' : "";
                            ?>
                           <option value="<?=$k; ?>"<?=$sel; ?>><?=$S->L($v); ?></option>
                           <?php
                        }
                        ?>
                   </select>
                </td>
            </tr>
        <?php } ?>
        <tr>
            <td>
                <input type="text" name="name[0]" class="input-large" placeholder="Nuova lingua">
            </td>
            <td>
                <input type="text" name="url[0]" value="" class="input-small">
            </td>
            <td>
                <input type="text" name="iso[0]" class="input-small" maxlength="5">
            </td>
            <td style="text-align: center;">
                <input type="radio" name="is_default" id="is_default[0]">
            </td>
            <td style="text-align: center;">
                <input type="checkbox" name="rtl[0]" value="1">
            </td>
            <td>
                <select name="status[0]">
                    <?php
                    $arr = array("enable"=>'Attiva',"only_backend"=>'Solo gestionale',"disable"=>'Disattiva');
                    foreach($arr as $k=>$v){
                        ?>
                       <option value="<?=$k; ?>"><?=$S->L($v); ?></option>
                       <?php
                    }
                    ?>
               </select>
            </td>
        </tr>
    </tbody>
    <tfoot>
    	<tr>
        	 <td><span class="textLittle">*<?=$S->L("Campo obbligatorio"); ?></span></td>
            <td colspan="4" align="right">
            	<a class="btn btn-danger" href="javascript:void(0);" onclick="Scheda.SaveGoTo='<?=$S->Uri('sezioni/configurazione/lingue'); ?>';Scheda.Save();">Salva</a>
            </td>
        </tr>
    </tfoot>
    </table>
</form>