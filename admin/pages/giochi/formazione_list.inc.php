<script type="text/javascript"><!--
$(document).ready(function(e) {
    Search.Start();
});
--></script>
<form id="listForm">
	<input type="hidden" name="sessione" value="<?=session_id(); ?>">
    <input type="hidden" name="order" value="data">
    <input type="hidden" name="order_dir" value="desc">
    
	<input type="hidden" name="az" value="formazione_list">
    
    <div class="pull-right">
    	<a href="<?=$S->Uri('sezioni/giochi'); ?>" class="btn btn-default"><i class="icon-circle-arrow-left"></i> <?=$S->L('Indietro'); ?></a>
    	<a href="<?=$S->Uri('sezioni/giochi/formazione_scheda?id=0'); ?>" class="btn btn-primary"><i class="icon-plus icon-white"></i> <?=$S->L('Nuova'); ?></a>
    </div>
    <select name="id_stagione" onchange="Search.Start();">
    	<?php
		$q = "SELECT * FROM `{$S->_dp}gf_stagioni` WHERE 1 ORDER BY id DESC";
		$list = $S->cn->Q($q);
		foreach($list as $li){
			$q = "SELECT * FROM `{$S->_dp}gf_match` WHERE id_stagione = {$li['id']}";
			$S->cn->Q($q);
			$sel = $S->cn->n>0 ? ' selected' : '';
			?><option value="<?=$li["id"]; ?>"<?=$sel; ?>><?=$li["nome"]; ?></option><?php
		}
		?>        
    </select>
    <hr>

    <div id="noResult"><h5><?=$S->L('Nessun risultato trovato'); ?></h5></div>
    <div id="ListLoader" class="Loader1"></div>
    <table id="List" class="table table-hover table-striped">
    <thead>
    <tr>
    	<th width="30"><a href="#" data-order="id" class="order">#</a></th>
        <th width="200"><a href="#" data-order="data" class="order"><?=$S->L("Data"); ?></a></th>
        <th><div class="left"><a href="#" data-order="cognome" class="order"><?=$S->L("Partita"); ?></a></div></th>
        <th width="80"><div class="left"><a href="#" data-order="attivo" class="order"><?=$S->L("Attivo"); ?></a></div></th>
        <th width="50"><div class="center"><input type="checkbox" name="selectAll" onchange="Search.selectAll();"></div></th>
    </tr>
    </thead>
    <tbody></tbody>
    <tfoot>
    	<tr>
        	<td colspan="4"><b><?=$S->L("Trovati"); ?>: <span data-name="risultati"></span></b></td>
           <td><div class="center"><a href="javascript:void(0);" onclick="Search.Delete('<?=$S->L("Confermi eliminazione?"); ?>');" class="btn"><i class="icon-trash"></i></a></div></td>
        </tr>
    </tfoot>
    </table>
</form>