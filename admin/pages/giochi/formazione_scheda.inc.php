<?php
$id = $_GET["id"]>0 ? $_GET["id"] : 0;
//echo $id;
$readonly = '';
if( $id>0 ){
	$q = "SELECT * FROM `{$S->_dp}gf_formazione_match` WHERE id_match = {$id} LIMIT 1";
	$formazione = $S->cn->OQ($q);
	if( $formazione["id_formazione"]>0 ){
		$readonly = " readonly";	
	}
}
?>
<script type="text/javascript"><!--
$(document).ready(function(e) {
	tinymce.init({
		selector: '#description',
		height: '300',
		language: 'it',
		/*
		plugins: "template image",
		//toolbar: "template image",
		image_list: "_ext/scripts/image_list.php"*/
		theme: "modern",
		plugins: [
			"advlist autolink lists link image charmap print preview hr anchor pagebreak",
			"searchreplace wordcount visualblocks visualchars code fullscreen",
			"insertdatetime media nonbreaking save table contextmenu directionality",
			"emoticons template paste textcolor moxiemanager"
		],
		toolbar1: "insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
		toolbar2: "link image | print preview code media | forecolor backcolor",
		image_advtab: true,
		//toolbar: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect forecolor backcolor | link unlink anchor | image media | print preview code",
		templates: [
			{title: 'Test template 1', content: 'Test 1'},
			{title: 'Test template 2', content: 'Test 2'}
		]

	});
	
	Scheda.loadData_callback = function(){
		cambiaFormazione();
	};
	Scheda.Load();
	
	
	$('.datetimepicker').datetimepicker({
		pickTime: false,
		pick12HourFormat: false
	});
	$('.datetimepicker_clock').datetimepicker({
		pickTime: true,
		pick12HourFormat: false,
		use24hours: true
	});
});

var SaveMatch = function(){
	Scheda.SaveGoTo='<?=$S->Uri('sezioni/giochi/formazione_list'); ?>';
	var id_formazione = $("input[name='id_formazione']").val();
	if( id_formazione>0 ){
		var ok = confirm("Salvando la formazione la partita verrà chiusa ed i punti attribuiti. Confermi salvataggio formazione?");
		if( ok ){
			Scheda.Save();
		}
	}else{
		Scheda.Save();	
	}
};
var cambiaFormazione = function(){
	var formazione = $("select[name='id_formazione'] option:selected").text();
	if( formazione=="-" ){
		$("#formazioneTipo").hide();
	}else{
		var url_base = '<?=str_replace("/sezioni/","/pages/",$S->Uri('sezioni/giochi/')); ?>img/formazione/';
		$("#formazioneTipo").show().attr("src",url_base + formazione + ".jpg");
	}
};
--></script>
<form id="saveForm">
	<input type="hidden" name="sessione" value="<?=session_id(); ?>">
    <input type="hidden" name="id" value="<?=$id; ?>">
	<input type="hidden" name="az" value="formazione_save">
    
    <div>
    	<a href="<?=$S->Uri('sezioni/giochi/formazione_list'); ?>" class="btn fr"><i class="icon-circle-arrow-left"></i> <?=$S->L('Indietro'); ?></a>
        <h3><?=$S->L("Scheda Partita"); ?></h3>
    </div>
    <hr>
    
    <div id="ListLoader" class="Loader1"></div>
    
    <table id="List" class="table table-striped">
    <tbody>
    	<tr>
        	<td><label><?=$S->L('Stagione'); ?></label></td>
            <td>
            	<select name="id_stagione">
                   <?php
				    $q = "SELECT * FROM `{$S->_dp}gf_stagioni` WHERE 1 ORDER BY id DESC";
					$ris = $S->cn->Q($q,true);
					foreach($ris as $r){
						?><option value="<?=$r['id']; ?>"><?=$r["nome"]; ?></option><?php
					}
				    ?>
                </select>
            </td>
        </tr>
    	<tr>
        	<td width="200"><label><?=$S->L('Data e orario'); ?>*</label></td>
           <td>
                <div class="datetimepicker_clock input-append date">
                    <input name="data" data-format="dd/MM/yyyy hh:mm" data-useseconds="false" type="text" class="input-medium"></input>
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
           </td>
        </tr>
        <tr>
        	<td><label><?=$S->L('Banner'); ?></label></td>
            <td>
            	<select name="id_banner">
                	<option value="NULL">Nessuno</option>
                   <?php
				    $q = "SELECT * FROM `{$S->_dp}banners` WHERE zone='formazione' AND deleted IS NULL AND enable=1";
					$ris = $S->cn->Q($q,true);
					foreach($ris as $r){
						?><option value="<?=$r['id']; ?>"><?=$r["name"]; ?> (#<?=$r['id']; ?>)</option><?php
					}
				    ?>
                </select>
            </td>
        </tr>
        <tr>
        	<td>
            	<label for="in_casa"><?=$S->L('In casa'); ?></label>
            </td>
            <td>
            	<input type="checkbox" id="in_casa" name="in_casa" value="1" checked>
            </td>
        </tr>
        <tr>
        	<td><label><?=$S->L('Squadra avversaria'); ?>*</label></td>
            <td>
            	<!--input type="text" name="avversario" class="input-xlarge"-->
                <select name="id_avversario">
                    <option value="0">Seleziona avversario&hellip;</option>
                    <?php
                    $q = "SELECT * FROM `{$S->_dp}gf_teams_categorie` WHERE 1 ORDER BY nome ASC";
                    $list = $S->cn->Q($q,true);
                    foreach($list as $cat){
                        ?><optgroup label="<?=$cat['nome']; ?>">
                            <?php
                            $q = "SELECT * FROM `{$S->_dp}gf_teams` WHERE id_categoria = {$cat['id']} ORDER BY nome ASC";
                            $teams = $S->cn->Q($q,true);
                            foreach($teams as $team){
                                ?><option value="<?=$team['id']; ?>"><?=$team['nome']; ?></option><?php
                            }
                            ?>
                        </optgroup><?php
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
        	<td colspan="2">
            	<script type="text/javascript"><!--
				var OpenContenuto = function(){
					$("#ContenutoDiv").slideToggle();
				};
				--></script>
            	<div class="center"><a href="javascript:void(0);" class="btn" onclick="OpenContenuto();">Contenuto <i class="icon-chevron-down"></i></a></div>
                <div id="ContenutoDiv" class="hide">
                	<br>
	            	<textarea id="description" name="description" data-tinymce="1"></textarea>
				</div>
            </td>
        </tr>
        <tr>
        	<td>
            	<label for="attiva"><?=$S->L('Attiva'); ?></label>
            </td>
            <td>
            	<input type="checkbox" id="attiva" name="attiva" value="1" checked>
            </td>
        </tr>
        <tr>
        	<td><label><?=$S->L('Numero partecipanti:'); ?></label></td>
            <td>
                <div class="row">
                    <div class="span2">
                        Formazione<br>
                        <input type="text" name="nr_partecipanti" class="input-small" readonly>
                    </div>
                    <div class="span2">
                        Indovina risultato<br>
                        <input type="text" name="nr_partecipanti_risultato" class="input-small" readonly>
                    </div>
                    <div class="span2">
                        Indovina marcatore<br>
                        <input type="text" name="nr_partecipanti_marcatore" class="input-small" readonly>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
        	<td><label><?=$S->L('Numero vincitori:'); ?></label></td>
            <td>
                <div class="row">
                    <div class="span2">
                        Formazione<br>
                        <input type="text" name="nr_vincitori" class="input-small" readonly><br>
                        <input type="text" name="perc_vincitori" class="input-small" readonly>
                    </div>
                    <div class="span2">
                        Indovina risultato<br>
                        <input type="text" name="nr_vincitori_risultato" class="input-small" readonly><br>
                        <input type="text" name="perc_vincitori_risultato" class="input-small" readonly>
                    </div>
                    <div class="span2">
                        Indovina marcatore<br>
                        <input type="text" name="nr_vincitori_marcatore" class="input-small" readonly><br>
                        <input type="text" name="perc_vincitori_marcatore" class="input-small" readonly>
                    </div>
                </div>
            </td>
        </tr>
        
        <tr>
        	<td colspan="2"><div class="center"><h4>Dati post-partita</h4></div></td>
        </tr>
        <tr>
            <td><label><?=$S->L('Goal Fiorentina'); ?></label></td>
            <td><input type="text" name="goal_fiorentina" class="input-small" value="0"></td>
        </tr>
        <tr>
            <td><label><?=$S->L('Goal Avversario'); ?></label></td>
            <td><input type="text" name="goal_avversario" class="input-small" value="0"></td>
        </tr>
        <tr>
            <td><label><?=$S->L('Primo marcatore fiorentina'); ?></label></td>
            <td>
                <select name="id_primo_marcatore">
                    <option value="0">Nessuno</option>
                    <?php
                    $q = "SELECT * FROM `{$S->_dp}gf_rosa` WHERE deleted IS NULL AND attivo=1";
                    $list = $S->cn->Q($q,true);
                    foreach($list as $r){
                        ?><option value="<?=$r['id']; ?>"><?=$r['nome']." ".$r['cognome']; ?></option><?php
                    }
                    ?>
                    <optgroup label="Giocatori non più attivi">
                        <?php
                        $q = "SELECT * FROM `{$S->_dp}gf_rosa` WHERE deleted IS NOT NULL OR attivo=0";
                        $list = $S->cn->Q($q,true);
                        foreach($list as $r){
                            ?><option value="<?=$r['id']; ?>"><?=$r['nome']." ".$r['cognome']; ?></option><?php
                        }
                        ?>
                    </optgroup>
                </select>
            </td>
        </tr>
        <tr>
        	<td><label><?=$S->L('Formazione'); ?></label></td>
            <td style="position:relative;">
            	<img id="formazioneTipo" style="position: absolute; display: none; right: 0px; border-radius: 12px; border: 2px solid rgba(255,255,255,.5); box-shadow: 0 0 5px rgba(0,0,0,.45); top: 0; min-width: 400px; max-width: 600px; width: 50%;">
            	<select name="id_formazione"<?=$readonly; ?> onchange="cambiaFormazione();">
                	<option value="NULL">-</option>
                	<?php
					$q = "SELECT * FROM `{$S->_dp}gf_formazioni` WHERE enable=1 ORDER BY `order` ASC";
					$list = $S->cn->Q($q,true);
					foreach($list as $r){
						?><option value="<?=$r['id']; ?>"><?=$r['formazione']; ?></option><?php
					}
					?>
                </select>
            </td>
        </tr>
        <tr>
        	<td><label><?=$S->L('Portiere'); ?></label></td>
            <td>
            	<select name="id_giocatore1"<?=$readonly; ?>>
                	<option value="NULL">-</option>
                	<?php
					$q = "SELECT * FROM `{$S->_dp}gf_rosa` WHERE portiere=1 AND attivo=1 ORDER BY `cognome` ASC";
					$list = $S->cn->Q($q,true);
					foreach($list as $r){
						?><option value="<?=$r['id']; ?>"><?="{$r['numero']}. {$r['nome']} {$r['cognome']}"; ?></option><?php
					}
					?>
                </select>
            </td>
        </tr>
        
        <?php
		$q = "SELECT * FROM `{$S->_dp}gf_rosa` WHERE portiere=0 AND attivo=1 ORDER BY `cognome` ASC";
		$list = $S->cn->Q($q,true);
		for($iRow=2;$iRow<=11;$iRow++){
			?>
        	<tr>
                <td><label><?=$S->L('Giocatore'); ?> <?=$iRow; ?></label></td>
                <td>
                    <select name="id_giocatore<?=$iRow; ?>"<?=$readonly; ?>>
                        <option value="NULL">-</option>
                        <?php
                        foreach($list as $r){
                            ?><option value="<?=$r['id']; ?>"><?="{$r['numero']}. {$r['nome']} {$r['cognome']}"; ?></option><?php
                        }
                        ?>
                    </select>
                </td>
            </tr>
		<?php
        } ?>        
    </tbody>
    <tfoot>
        <tr>
            <td colspan="2"><label><b>CONFERMA SALVATAGGIO</b>&nbsp;&nbsp;&nbsp;<input style="vertical-align: middle;" type="checkbox" name="conferma_salvataggio" value="1"></label></td>
        </tr>
    	<tr>
        	<td><span class="textLittle">*<?=$S->L("Campo obbligatorio"); ?></span></td>
            <td align="right">
            	<a class="btn btn-danger" href="javascript:void(0);" onclick="SaveMatch();">Salva</a>
            </td>
        </tr>
    </tfoot>
    </table>
</form>