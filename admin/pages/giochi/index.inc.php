<div>
	<h3>Giochi</h3>
    <hr>
    <div class="row">
        <div class="span2">
            <a href="<?=$S->Uri('sezioni/giochi/categorie_teams_list'); ?>" class="btn btn-default"><i class="icon-list"></i> Categorie Squadre</a>
        </div>
        <div class="span2">
            <a href="<?=$S->Uri('sezioni/giochi/teams_list'); ?>" class="btn btn-default"><i class="icon-th "></i> Gestione Squadre</a>
        </div>
        <div class="span2">
            <a href="<?=$S->Uri('sezioni/giochi/rosa_list'); ?>" class="btn btn-default"><i class="icon-user"></i> Gestione Rosa Giocatori</a>
        </div>
        
        <div class="span2">
            <a href="<?=$S->Uri('sezioni/giochi/stagioni_list'); ?>" class="btn btn-default"><i class="icon-calendar"></i> Gestione Stagioni</a>
        </div>
        
        <div class="span2">
            <a href="<?=$S->Uri('sezioni/giochi/formazione_list'); ?>" class="btn btn-default"><i class="icon-list"></i> Gestione Formazioni</a>
        </div>
    </div>
</div>