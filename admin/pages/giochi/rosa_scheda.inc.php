<?php
$id = $_GET["id"]>0 ? $_GET["id"] : 0;
//echo $id;
?>
<script type="text/javascript"><!--
$(document).ready(function(e) {
	Scheda.Load();
});
--></script>
<form id="saveForm">
	<input type="hidden" name="sessione" value="<?=session_id(); ?>">
    <input type="hidden" name="id" value="<?=$id; ?>">
	<input type="hidden" name="az" value="rosa_save">
    
    <div>
    	<a href="<?=$S->Uri('sezioni/giochi/rosa_list'); ?>" class="btn fr"><i class="icon-circle-arrow-left"></i> <?=$S->L('Indietro'); ?></a>
        <h3><?=$S->L("Scheda Giocatore"); ?></h3>
    </div>
    <hr>
    
    <div id="ListLoader" class="Loader1"></div>
    
    <table id="List" class="table table-striped">
    <tbody>
    	<tr>
        	<td width="200"><label><?=$S->L('Nome'); ?>*</label></td>
            <td>
            	<input type="text" name="nome" class="input-xlarge">
            </td>
        </tr>
        <tr>
        	<td><label><?=$S->L('Cognome'); ?>*</label></td>
            <td>
            	<input type="text" name="cognome" class="input-xlarge">
            </td>
        </tr>
    	<tr>
        	<td><label><?=$S->L('Numero maglia'); ?></label></td>
            <td>
            	<input type="text" name="numero" class="input-small" maxlength="3">
            </td>
        </tr>
    	<tr>
        	<td>
            	<label><?=$S->L('Foto'); ?>*</label>
            </td>
            <td>
				<?=$S->SingleFile('id_foto','photo'); ?>
            </td>
        </tr>
        <!--tr>
        	<td>
            	<label><?=$S->L('Foto Over'); ?>*</label>
            </td>
            <td>
				<?=$S->SingleFile('id_foto_over','photo'); ?>
                <span class="help-block">Immagine al passaggio del mouse</span>
            </td>
        </tr-->
        <tr>
        	<td>
            	<label for="portiere"><?=$S->L('Portiere'); ?></label>
            </td>
            <td>
            	<input type="checkbox" id="portiere" name="portiere" value="1">
            </td>
        </tr>
        <tr>
        	<td>
            	<label for="attivo"><?=$S->L('Attivo'); ?></label>
            </td>
            <td>
            	<input type="checkbox" id="attivo" name="attivo" value="1" checked>
            </td>
        </tr>
    </tbody>
    <tfoot>
    	<tr>
        	<td><span class="textLittle">*<?=$S->L("Campo obbligatorio"); ?></span></td>
            <td align="right">
            	<a class="btn btn-danger" href="javascript:void(0);" onclick="Scheda.Save();">Salva</a>
            </td>
        </tr>
    </tfoot>
    </table>
</form>