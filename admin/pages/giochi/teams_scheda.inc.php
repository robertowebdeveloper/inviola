<?php
$id = $_GET["id"]>0 ? $_GET["id"] : 0;
//echo $id;
?>
<script type="text/javascript"><!--
    $(document).ready(function(e) {
        Scheda.Load();
    });
    --></script>
<form id="saveForm">
    <input type="hidden" name="sessione" value="<?=session_id(); ?>">
    <input type="hidden" name="id" value="<?=$id; ?>">
    <input type="hidden" name="az" value="teams_save">

    <div>
        <a href="<?=$S->Uri('sezioni/giochi/teams_list'); ?>" class="btn fr"><i class="icon-circle-arrow-left"></i> <?=$S->L('Indietro'); ?></a>
        <h3><?=$S->L("Scheda Team"); ?></h3>
    </div>
    <hr>

    <div id="ListLoader" class="Loader1"></div>

    <table id="List" class="table table-striped">
        <tbody>
        <tr>
            <td width="200"><label><?=$S->L('Nome'); ?>*</label></td>
            <td>
                <input type="text" name="nome" class="input-xlarge">
            </td>
        </tr>
        <tr>
            <td><label><?=$S->L('Categoria'); ?></label></td>
            <td>
                <select name="id_categoria">
                    <option value="NULL">-</option>
                    <?php
                    $q = "SELECT * FROM `{$S->_dp}gf_teams_categorie` WHERE 1 ORDER BY nome ASC";
                    $list = $S->cn->Q($q);
                    foreach($list as $r){
                        ?><option value="<?=$r['id']; ?>"><?=$r['nome']; ?></option><?php
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>
                <label><?=$S->L('Logo'); ?>*</label>
            </td>
            <td>
                <?=$S->SingleFile('id_logo','photo'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <label for="attivo"><?=$S->L('Attivo'); ?></label>
            </td>
            <td>
                <input type="checkbox" id="attivo" name="attivo" value="1" checked>
            </td>
        </tr>
        </tbody>
        <tfoot>
        <tr>
            <td><span class="textLittle">*<?=$S->L("Campo obbligatorio"); ?></span></td>
            <td align="right">
                <a class="btn btn-danger" href="javascript:void(0);" onclick="Scheda.Save();">Salva</a>
            </td>
        </tr>
        </tfoot>
    </table>
</form>