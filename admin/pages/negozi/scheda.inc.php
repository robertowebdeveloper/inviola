<?php
$id = $_GET["id"]>0 ? $_GET["id"] : 0;
//echo $id;
$q = "SELECT * FROM `{$_db_prefix}stores` WHERE id = {$id}";
$Scheda = $cn->OQ($q);

$_status_platform = ' disabled';
?>
<script type="text/javascript"><!--
$(document).ready(function(e) {
	tinymce.init({
		selector: '#html',
		height: '300',
		language: 'it',
		/*
		plugins: "template image",
		//toolbar: "template image",
		image_list: "_ext/scripts/image_list.php"*/
		theme: "modern",
		style_formats: [
			{title: 'Headers', items: [
				{title: 'h1', block: 'h1'},
				{title: 'h2', block: 'h2'},
				{title: 'h3', block: 'h3'},
				{title: 'h4', block: 'h4'},
				{title: 'h5', block: 'h5'},
				{title: 'h6', block: 'h6'}
			]},
	
			{title: 'Blocks', items: [
				{title: 'p', block: 'p'},
				{title: 'div', block: 'div'},
				{title: 'pre', block: 'pre'}
			]},
	
			{title: 'Containers', items: [
				{title: 'section', block: 'section', wrapper: true, merge_siblings: false},
				{title: 'article', block: 'article', wrapper: true, merge_siblings: false},
				{title: 'blockquote', block: 'blockquote', wrapper: true},
				{title: 'hgroup', block: 'hgroup', wrapper: true},
				{title: 'aside', block: 'aside', wrapper: true},
				{title: 'figure', block: 'figure', wrapper: true}
			]},
			
			{title: 'Personalizzato', items: [
				{title: 'Title', block: 'span', classes: 'Title'},
				{title: 'TitleRed', block: 'span', classes: 'TitleRed'},
				{title: 'Title1', block: 'span', classes: 'Title1'},
				{title: 'Title2', block: 'span', classes: 'Title2'},
				{title: 'Title3', block: 'span', classes: 'Title3'},
				{title: 'subTitle', block: 'span', classes: 'subTitle'},
				{title: 'subTitle2', block: 'span', classes: 'subTitle2'},
			]}
		],
		plugins: [
			"advlist autolink lists link image charmap print preview hr anchor pagebreak",
			"searchreplace wordcount visualblocks visualchars code fullscreen",
			"insertdatetime media nonbreaking save table contextmenu directionality",
			"emoticons template paste textcolor moxiemanager"
		],
		toolbar1: "insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
		toolbar2: "link image | print preview code media | forecolor backcolor",
		image_advtab: true,
		//toolbar: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect forecolor backcolor | link unlink anchor | image media | print preview code",
		templates: [
			{title: 'Test template 1', content: 'Test 1'},
			{title: 'Test template 2', content: 'Test 2'}
		],
		
		//MOXIE MANAGER
		moxiemanager_path: '/produzione/_public/file/pages',
		moxiemanager_insert: true,
		moxiemanager_title: 'inol3 file manager',
		relative_urls: false,
		//document_base_url: 'http://www.xtracard.it/produzione/_public/file/pages',
		moxiemanager_fullscreen: false,
		remove_script_host: true

	});

	Scheda.Load();
});
--></script>
<form id="saveForm">
	<input type="hidden" name="sessione" value="<?=session_id(); ?>">
    <input type="hidden" name="id" value="<?=$id; ?>">
	<input type="hidden" name="az" value="negozio_save">
    
    <input type="hidden" name="fn_shopCategory1" />
    <input type="hidden" name="fn_shopCategory2" />
    
    <div>
    	<a href="<?=$S->Uri('sezioni/negozi'); ?>" class="btn fr"><i class="icon-circle-arrow-left"></i> <?=$S->L('Indietro'); ?></a>
        <h3><?=$S->L("Scheda Negozio"); ?></h3>
    </div>
    <hr>
    
    <div id="ListLoader" class="Loader1"></div>
    
    <table id="List" class="table table-striped">
    <tbody>
    	<tr>
        	<td width="200">
            	<label><?=$S->L("Nome"); ?>*</label>
            </td>
            <td>
              	<input type="text" name="fn_companyName" class="input-xxlarge"<?=$_status_platform; ?>>
            </td>
        </tr>
        <tr>
        	<td>
            	<label for="enable"><?=$S->L("Attivo"); ?></label>
            </td>
            <td><input type="checkbox" id="enable" name="enable" value="1" checked></td>
        </tr>
    	<tr>
        	<td>
            	<label><?=$S->L("Descrizione in piattaforma"); ?></label>
            </td>
            <td>
              	<input type="text" name="fn_description" class="input-xxlarge"<?=$_status_platform; ?>>
            </td>
        </tr>
    	<tr>
        	<td>
            	<label><?=$S->L("Prefisso Indirizzo"); ?></label>
            </td>
            <td>
              	<input type="text" name="fn_addressPrefix" class="input-large"<?=$_status_platform; ?>>
            </td>
        </tr>
    	<tr>
        	<td>
            	<label><?=$S->L("Indirizzo"); ?></label>
            </td>
            <td>
              	<input type="text" name="fn_address" class="input-xxlarge"<?=$_status_platform; ?>>
            </td>
        </tr>
    	<tr>
        	<td>
            	<label><?=$S->L("Numero Civico"); ?></label>
            </td>
            <td>
              	<input type="text" name="fn_addressNumber" class="input-small"<?=$_status_platform; ?>>
            </td>
        </tr>
    	<tr>
        	<td>
            	<label><?=$S->L("Regione"); ?></label>
            </td>
            <td>
              	<select name="fn_geoLevel1"<?=$_status_platform; ?>>
                	<?php $q = "SELECT id_ext,name FROM `{$_db_prefix}geolevels` WHERE level=1 ORDER BY name ASC";
					$list = $cn->Q($q,true);
					foreach($list as $v){
						?><option value="<?=$v["id_ext"]; ?>"><?=$v["name"]; ?></option><?php
					}
					?>
                </select>
            </td>
        </tr>
        <tr>
        	<td>
            	<label><?=$S->L("Provincia"); ?></label>
            </td>
            <td>
              	<select name="fn_geoLevel2"<?=$_status_platform; ?>>
                	<?php $q = "SELECT id_ext,name FROM `{$_db_prefix}geolevels` WHERE level=2 AND fatherId={$Scheda[fn_geoLevel1]} ORDER BY name ASC";
					$list = $cn->Q($q,true);
					foreach($list as $v){
						?><option value="<?=$v["id_ext"]; ?>"><?=$v["name"]; ?></option><?php
					}
					?>
                </select>
            </td>
        </tr>
        <tr>
        	<td>
            	<label><?=$S->L("Comune"); ?></label>
            </td>
            <td>
              	<select name="fn_geoLevel3"<?=$_status_platform; ?>>
                	<?php $q = "SELECT id_ext,name FROM `{$_db_prefix}geolevels` WHERE level=3 AND fatherId={$Scheda[fn_geoLevel2]} ORDER BY name ASC";
					$list = $cn->Q($q,true);
					foreach($list as $v){
						?><option value="<?=$v["id_ext"]; ?>"><?=$v["name"]; ?></option><?php
					}
					?>
                </select>
                <input type="hidden" name="fn_geoLevel4" />
                <input type="hidden" name="fn_geoLevel5" />
            </td>
        </tr>
        <tr>
        	<td>
            	<label><?=$S->L("Comune"); ?></label>
            </td>
            <td>
              	<input type="text" name="fn_zip" class="input-large"<?=$_status_platform; ?>>
            </td>
        </tr>
        <tr>
        	<td>
            	<label><?=$S->L("Latitudine"); ?></label>
            </td>
            <td>
              	<input type="text" name="fn_geoLat" class="input-xxlarge"<?=$_status_platform; ?>>
            </td>
        </tr>
        <tr>
        	<td>
            	<label><?=$S->L("Longitudine"); ?></label>
            </td>
            <td>
              	<input type="text" name="fn_geoLong" class="input-xxlarge"<?=$_status_platform; ?>>
            </td>
        </tr>
        <tr>
        	<td>
            	<label><?=$S->L("Persona di riferimento"); ?></label>
            </td>
            <td>
              	<input type="text" name="fn_contactName" class="input-xxlarge"<?=$_status_platform; ?>>
            </td>
        </tr>
        <tr>
        	<td>
            	<label><?=$S->L("Partita IVA"); ?></label>
            </td>
            <td>
              	<input type="text" name="fn_taxNumber" class="input-large"<?=$_status_platform; ?>>
            </td>
        </tr>
        <tr>
        	<td>
            	<label><?=$S->L("E-Mail"); ?></label>
            </td>
            <td>
              	<input type="text" name="fn_mail" class="input-xxlarge"<?=$_status_platform; ?>>
            </td>
        </tr>
        <tr>
        	<td>
            	<label><?=$S->L("Telefono"); ?></label>
            </td>
            <td>
              	<input type="text" name="fn_telephone" class="input-xxlarge"<?=$_status_platform; ?>>
            </td>
        </tr>
        <tr>
        	<td>
            	<label><?=$S->L("E-mail di contatto"); ?></label>
            </td>
            <td>
              	<input type="text" name="fn_contactEmail" class="input-xxlarge"<?=$_status_platform; ?>>
            </td>
        </tr>
        <tr>
        	<td>
            	<label><?=$S->L("Cellulare di contatto"); ?></label>
            </td>
            <td>
              	<input type="text" name="fn_contactMobile" class="input-xxlarge"<?=$_status_platform; ?>>
            </td>
        </tr>
        <tr>
        	<td>
            	<label><?=$S->L("Telefono di contatto"); ?></label>
            </td>
            <td>
              	<input type="text" name="fn_contactTelephone" class="input-xxlarge"<?=$_status_platform; ?>>
            </td>
        </tr>
        
        <tr>
        	<td colspan="2">
            	<script type="text/javascript"><!--
				var OpenContenuto = function(){
					$("#ContenutoDiv").slideToggle();
				};
				--></script>
            	<div class="center">
                	<a href="javascript:void(0);" class="btn" onclick="OpenContenuto();">Contenuto <i class="icon-chevron-down"></i></a>
               </div>
                
                <div id="ContenutoDiv" class="hide">
                	<br>
	            	<textarea id="html" name="html" data-tinymce="1"></textarea>
				</div>
            </td>
        </tr>
        <tr>
        	<td>
            	<label><?=$S->L("Ponderazione"); ?></label>
            </td>
            <td>
              	<?=$S->L('Euro'); ?> <input type="text" name="moneyValue" class="input-small">
                <?=$S->L('corrisponde a punti:'); ?> <input type="text" name="pointValue" class="input-small"> 
            </td>
        </tr>
        
        <tr>
        	<td><label><?=$S->L('Orario'); ?></label></td>
            <td>
            	<table class="table table-striped" style="width: 400px;">
                	<?php
						$arr = array("Lunedì","Martedì","Mercoledì","Giovedì","Venerdì","Sabato","Domenica");
						$i=1;
                    	foreach($arr as $day){
						?>
                    	<tr>
                        	<td><?=$S->L( $day ); ?></td>
                            <td>
                            	<input type="text" name="timetable<?=$i; ?>" class="input-large" />
                            </td>
                        </tr>
                    <?php
						$i++;
                    } ?>
                </table>
            </td>
        </tr>
        
        <!--tr>
        	<td>
            	<label><?=$S->L("Username Terminale (FNET)"); ?></label>
            </td>
            <td><input type="text" name="terminal_username" class="input-xlarge"></td>
        </tr>
        <tr>
        	<td>
            	<label><?=$S->L("Password Terminale (FNET)"); ?></label>
            </td>
            <td><input type="text" name="terminal_password" class="input-xlarge"></td>
        </tr>
        <tr>
        	<td>
            	<label><?=$S->L("Serial Number Terminal (FNET)"); ?></label>
            </td>
            <td><input type="text" name="terminal_sn" class="input-small"></td>
        </tr-->
        
        <tr>
        	<td><label><?=$S->L('Immagini'); ?></label></td>
            <td>
				<?=$S->MultiFile('id_file','photo',100); ?>
            </td>
        </tr>
        
        <tr>
        	<td>
            	<label><?=$S->L("Note interne"); ?></label>
                <span class="help-block">(<?=$S->L('Non visibili ad utente'); ?>)</span>
            </td>
            <td>
            	<textarea name="note" rows="5" class="input-xxlarge"></textarea>
			</td>
        </tr>
    </tbody>
    <tfoot>
    	<tr>
        	<td><span class="textLittle">*<?=$S->L("Campo obbligatorio"); ?></span></td>
            <td align="right">
            	<a class="btn btn-danger" href="javascript:void(0);" onclick="Scheda.SaveGoTo='<?=$S->Uri('sezioni/negozi'); ?>';Scheda.Save();">Salva</a>
            </td>
        </tr>
    </tfoot>
    </table>
</form>