<?php
$id = $_GET["id"]>0 ? $_GET["id"] : 0;
//echo $id;
?>
<script type="text/javascript"><!--
$(document).ready(function(e) {
	/*$("#html").tinymce({
			
	});*/
	tinymce.init({
		selector: '#description',
		height: '300',
		language: 'it',
		/*
		plugins: "template image",
		//toolbar: "template image",
		image_list: "_ext/scripts/image_list.php"*/
		theme: "modern",
		plugins: [
			"advlist autolink lists link image charmap print preview hr anchor pagebreak",
			"searchreplace wordcount visualblocks visualchars code fullscreen",
			"insertdatetime media nonbreaking save table contextmenu directionality",
			"emoticons template paste textcolor moxiemanager"
		],
		toolbar1: "insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
		toolbar2: "link image | print preview code media | forecolor backcolor",
		image_advtab: true,
		//toolbar: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect forecolor backcolor | link unlink anchor | image media | print preview code",
		templates: [
			{title: 'Test template 1', content: 'Test 1'},
			{title: 'Test template 2', content: 'Test 2'}
		]

	});

	Scheda.Load();
	
	
	$('.datetimepicker').datetimepicker({
		pickTime: false,
		pick12HourFormat: false
	});
	$('.datetimepicker_clock').datetimepicker({
		pickTime: true,
		pick12HourFormat: false,
		use24hours: true
	});
});
--></script>
<form id="saveForm">
	<input type="hidden" name="sessione" value="<?=session_id(); ?>">
    <input type="hidden" name="id" value="<?=$id; ?>">
	<input type="hidden" name="az" value="news_promo_save">
    <input type="hidden" name="id_user" value="<?=$User["id"]; ?>">
    
    <div>
    	<a href="<?=$S->Uri('sezioni/news_promo'); ?>" class="btn fr"><i class="icon-circle-arrow-left"></i> <?=$S->L('Indietro'); ?></a>
        <h3><?=$S->L("Scheda News & Promo"); ?></h3>
    </div>
    <hr>
    
    <div id="ListLoader" class="Loader1"></div>
    <table id="List" class="table table-striped">
    <tbody>
    	<tr>
        	<td width="200">
            	<label><?=$S->L("Titolo"); ?>*</label>
            </td>
            <td><input type="text" name="title" class="input-xxlarge"></td>
        </tr>
        <!--tr>
        	<td><label><?=$S->L('Tipo'); ?></label></td>
            <td>
            	<select name="type">
                	<option value="news"><?=$S->L('News'); ?></option>
                	<option value="promo"><?=$S->L('Promo'); ?></option>
                </select>
            </td>
        </tr-->
        
    	<tr>
        	<td width="200"><label><?=$S->L('Immagine per App Mobile'); ?>*</label></td>
            <td>
            	<?=$S->SingleFile('id_file','photo'); ?>
            </td>
        </tr>
        
        <tr>
        	<td><?=$S->L('Descrizione breve'); ?></td>
           <td>
           		<input type="hidden" name="type" value="news">
           		<textarea name="description_short" rows="4" class="input-xxlarge"></textarea>
           </td>
        </tr>
        
        <tr>
        	<td colspan="2">
            	<script type="text/javascript"><!--
				var OpenContenuto = function(){
					$("#ContenutoDiv").slideToggle();
				};
				--></script>
            	<div class="center"><a href="javascript:void(0);" class="btn" onclick="OpenContenuto();">Contenuto <i class="icon-chevron-down"></i></a></div>
                <div id="ContenutoDiv" class="hide">
                	<br>
	            	<textarea id="description" name="description" data-tinymce="1"></textarea>
				</div>
            </td>
        </tr>
        
        <tr>
        	<td><label><?=$S->L('Data'); ?></label></td>
           <td>
                <div class="datetimepicker_clock input-append date">
                    <input name="date" data-format="dd/MM/yyyy HH:mm" data-useseconds="false" type="text" class="input-medium"></input>
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
           </td>
        </tr>
        
        <tr>
        	<td><label><?=$S->L('Visibile'); ?></label></td>
           <td>	
           		<?=$S->L('dal'); ?>
                <div class="datetimepicker input-append date">
                    <input name="show_from" data-format="dd/MM/yyyy" type="text" class="input-small"></input>
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
                &nbsp;&nbsp;&nbsp;
           		<?=$S->L('al'); ?>
                <div class="datetimepicker input-append date">
                    <input name="show_to" data-format="dd/MM/yyyy" type="text" class="input-small"></input>
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
           </td>
        </tr>
        
    </tbody>
    <tfoot>
    	<tr>
        	<td><span class="textLittle">*<?=$S->L("Campo obbligatorio"); ?></span></td>
            <td align="right">
            	<a class="btn btn-danger" href="javascript:void(0);" onclick="Scheda.SaveGoTo='<?=$S->Uri('sezioni/news_promo'); ?>';Scheda.Save();">Salva</a>
            </td>
        </tr>
    </tfoot>
    </table>
</form>