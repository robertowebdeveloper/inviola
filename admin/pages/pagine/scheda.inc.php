<?php
$id = $_GET["id"]>0 ? $_GET["id"] : 0;
//echo $id;
?>
<script type="text/javascript"><!--
$(document).ready(function(e) {
	/*$("#html").tinymce({
			
	});*/
	tinymce.init({
		selector: '#html',
		height: '300',
		language: 'it',
		schema: 'html5',
		verify_html: false,
		/*
		plugins: "template image",
		//toolbar: "template image",
		image_list: "_ext/scripts/image_list.php"*/
		theme: "modern",
		style_formats: [
			{title: 'Headers', items: [
				{title: 'h1', block: 'h1'},
				{title: 'h2', block: 'h2'},
				{title: 'h3', block: 'h3'},
				{title: 'h4', block: 'h4'},
				{title: 'h5', block: 'h5'},
				{title: 'h6', block: 'h6'}
			]},
	
			{title: 'Blocks', items: [
				{title: 'p', block: 'p'},
				{title: 'div', block: 'div'},
				{title: 'pre', block: 'pre'}
			]},
	
			{title: 'Containers', items: [
				{title: 'section', block: 'section', wrapper: true, merge_siblings: false},
				{title: 'article', block: 'article', wrapper: true, merge_siblings: false},
				{title: 'blockquote', block: 'blockquote', wrapper: true},
				{title: 'hgroup', block: 'hgroup', wrapper: true},
				{title: 'aside', block: 'aside', wrapper: true},
				{title: 'figure', block: 'figure', wrapper: true}
			]},
			
			{title: 'Personalizzato', items: [
				{title: 'Title', block: 'span', classes: 'Title'},
				{title: 'TitleRed', block: 'span', classes: 'TitleRed'},
				{title: 'Title1', block: 'span', classes: 'Title1'},
				{title: 'Title2', block: 'span', classes: 'Title2'},
				{title: 'Title3', block: 'span', classes: 'Title3'},
				{title: 'subTitle', block: 'span', classes: 'subTitle'},
				{title: 'subTitle2', block: 'span', classes: 'subTitle2'},
			]}
		],
		plugins: [
			"advlist autolink lists link image charmap print preview hr anchor pagebreak",
			"searchreplace wordcount visualblocks visualchars code fullscreen",
			"insertdatetime media nonbreaking save table contextmenu directionality",
			"emoticons template paste textcolor moxiemanager"
		],
		toolbar1: "insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
		toolbar2: "link image | print preview code media | forecolor backcolor",
		image_advtab: true,
		//toolbar: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect forecolor backcolor | link unlink anchor | image media | print preview code",
		templates: [
			{title: 'Test template 1', content: 'Test 1'},
			{title: 'Test template 2', content: 'Test 2'}
		],
		
		//MOXIE MANAGER
		moxiemanager_path: '/_public/file/pages',
		moxiemanager_insert: true,
		moxiemanager_title: 'inol3 file manager',
		relative_urls: false,
		//document_base_url: 'http://inviola.violachannel.it/produzione/_public/file/pages',
		moxiemanager_fullscreen: false,
		remove_script_host: true

	});

	Scheda.Load();
});
--></script>
<form id="saveForm">
	<input type="hidden" name="sessione" value="<?=session_id(); ?>">
    <input type="hidden" name="id" value="<?=$id; ?>">
	<input type="hidden" name="az" value="pagine_save">
    <input type="hidden" name="id_user" value="<?=$User["id"]; ?>">
    
    <div>
    	<a href="<?=$S->Uri('sezioni/pagine'); ?>" class="btn fr"><i class="icon-circle-arrow-left"></i> <?=$S->L('Indietro'); ?></a>
        <h3><?=$S->L("Scheda Pagina"); ?></h3>
    </div>
    <hr>
    
    <div id="ListLoader" class="Loader1"></div>
    <table id="List" class="table table-striped">
    <tbody>
    	<tr>
        	<td width="200">
            	<label><?=$S->L("Nome"); ?>*</label>
            </td>
            <td><input type="text" name="name" class="input-xlarge"></td>
        </tr>
    	<tr>
        	<td>
            	<label><?=$S->L("Key"); ?>*</label>
            </td>
            <td>
            	<input type="text" name="k" class="input-large">
               <span class="help-block"><?=$S->L('Chiave per inserimento nel codice'); ?></span>
            </td>
        </tr>
        <tr>
        	<td><label for="auth_required"><?=$S->L('Pagina riservata'); ?></label></td>
            <td>
            	<input type="checkbox" id="auth_required" name="auth_required" value="1" checked>
            </td>
        </tr>
        <tr>
        	<td><label><?=$S->L('Lingua'); ?></label></td>
            <td>
            	<select name="id_lang">
                	<?php
					$q = "SELECT * FROM {$_db_prefix}langs WHERE status!='disable'";
					$ris = $cn->Q($q,true);
					foreach($ris as $r){
						echo "<option value=\"{$r[id]}\">{$r[name]}</option>";	
					}
					?>
                </select>
            </td>
        </tr>
        <tr>
        	<td><label><?=$S->L('Pagina genitore'); ?></label></td>
            <td>
            	<select name="id_parent">
                	<option value="NULL">-</option>
                	<?php
					$q = "SELECT * FROM {$_db_prefix}pages WHERE id!={$id} AND id_parent IS NULL";
					$ris = $cn->Q($q,true);
					foreach($ris as $r){
						echo "<option value=\"{$r[id]}\">{$r[name]}</option>";	
					}
					?>
                </select>
            </td>
        </tr>
        <tr>
        	<td><label for="is_home"><?=$S->L('Home Page'); ?></label></td>
            <td>
            	<input type="checkbox" id="is_home" name="is_home" value="1">
            </td>
        </tr>
    	<tr>
        	<td>
            	<label><?=$S->L("URL"); ?></label>
            </td>
            <td>
            	<input type="text" name="url" class="input-xxlarge">
            	<span class="help-block">Path relativo (es. <b>"chi-siamo"</b>)</span>
            </td>
        </tr>
        
    	<tr>
        	<td>
            	<label><?=$S->L("Meta-tag personalizzati"); ?></label>
            </td>
            <td>
            	<textarea name="meta" class="input-xxlarge" rows="3"></textarea>
            </td>
        </tr>
        <tr>
        	<td><label><?=$S->L('Classe Body CSS'); ?></label></td>
            <td>
            	<input type="text" name="css_class" class="input-xlarge">
            </td>
        </tr>
        <tr>
        	<td colspan="2">
            	<script type="text/javascript"><!--
				var OpenContenuto = function(){
					$("#ContenutoDiv").slideToggle();
				};
				--></script>
            	<div class="center">
                	<a href="javascript:void(0);" class="btn" onclick="OpenContenuto();">Contenuto <i class="icon-chevron-down"></i></a>
                   <br>
                   <span style="font-size: .8em;">(<?=$S->L('Non tutte le pagine prevedono il contenuto'); ?>)</span>
               </div>
                
                <div id="ContenutoDiv" class="hide">
                	<br>
	            	<textarea id="html" name="html" data-tinymce="1"></textarea>
				</div>
            </td>
        </tr>
        <tr>
        	<td><label><?=$S->L('Pagina di template'); ?></label></td>
            <td>
            	<select name="template">
                   <?php
				   	$files = glob("../_ext/pages/*.tmpl.php");
					foreach($files as $f){
						$f = basename($f);
						?><option value="<?=$f; ?>"><?=$f; ?></option><?php
					}
				   	?>
                </select>
                <span class="help-block">(<?=$S->L('Pagina PHP di riferimento nella cartella _ext/pages'); ?>)</span>
            </td>
        <tr>
        	<td><label><?=$S->L('Pagina di schema'); ?></label></td>
            <td>
            	<select name="file_inc">
                	<option value="">-</option>
                   <?php
				   	$files = glob("../_ext/pages/*.inc.php");
					foreach($files as $f){
						$f = basename($f);
						?><option value="<?=$f; ?>"><?=$f; ?></option><?php
					}
				   	?>
                </select>
                <span class="help-block">(<?=$S->L('Pagina PHP di riferimento nella cartella _ext/pages'); ?>)</span>
            </td>
        </tr>
        <tr>
        	<td>
            	<label><?=$S->L('Title'); ?> <sup><em>(SEO)</em></sup></label>
            </td>
            <td>
            	<input type="text" name="seo_title" class="input-xxlarge">
            </td>
        </tr>
        <tr>
        	<td>
            	<label><?=$S->L('Keywords'); ?> <sup><em>(SEO)</em></sup></label>
            </td>
            <td>
            	<input type="text" name="seo_keywords" class="input-xxlarge">
            </td>
        </tr>
        <tr>
        	<td>
            	<label><?=$S->L('Description'); ?> <sup><em>(SEO)</em></sup></label>
            </td>
            <td><textarea name="seo_description" cols="40" rows="4" class="input-xxlarge"></textarea></td>
        </tr>
    </tbody>
    <tfoot>
    	<tr>
        	<td><span class="textLittle">*<?=$S->L("Campo obbligatorio"); ?></span></td>
            <td align="right">
            	<a class="btn btn-danger" href="javascript:void(0);" onclick="Scheda.SaveGoTo='<?=$S->Uri('sezioni/pagine'); ?>';Scheda.Save();">Salva</a>
            </td>
        </tr>
    </tfoot>
    </table>
</form>