<?php
$id = $_GET["id"]>0 ? $_GET["id"] : 0;
//echo $id;
?>
<script type="text/javascript"><!--
$(document).ready(function(e) {
	tinymce.init({
		selector: "textarea[name='descr']",
		height: '200',
		language: 'it',
		/*
		plugins: "template image",
		//toolbar: "template image",
		image_list: "_ext/scripts/image_list.php"*/
		theme: "modern",
		/*plugins: [
			"advlist autolink lists link image charmap print preview hr anchor pagebreak",
			"searchreplace wordcount visualblocks visualchars code fullscreen",
			"insertdatetime media nonbreaking save table contextmenu directionality",
			"emoticons template paste textcolor moxiemanager"
		],
		toolbar1: "insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
		toolbar2: "link image | print preview code media | forecolor backcolor",
		image_advtab: true,*/
		//toolbar: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect forecolor backcolor | link unlink anchor | image media | print preview code",

	});
	
	LoadNegozi();
	Scheda.Load();
});
var LoadNegozi = function(){
	var ccn = $("#saveForm select[name='id_ccn']");
	var id_ccn = ccn.val();
	
	var o = $("#saveForm select[name='id_negozio']");
	$.ajax({
		type: 'POST',
		url: System.sp,
		data: 'az=options_negozi&id_ccn='+id_ccn,
		success: function(data){
			var opts = "<option value=\"NULL\">TUTTI (Promozione del Circuito)</option>";
			for(var i in data.list){
				opts += "<option value=\"" + data.list[i].v +"\">" + data.list[i].l + "</option>";
			}
			o.html(opts);
		}
	});
};
--></script>
<form id="saveForm">
	<input type="hidden" name="sessione" value="<?=session_id(); ?>">
    <input type="hidden" name="id" value="<?=$id; ?>">
	<input type="hidden" name="az" value="promozioni_save">
    <input type="hidden" name="id_gruppo" value="1">
    
    <div>
    	<a href="<?=$S->Uri('sezioni/promozioni'); ?>" class="btn fr"><i class="icon-circle-arrow-left"></i> <?=$S->L('Indietro'); ?></a>
        <h3><?=$S->L("Scheda Promozione"); ?></h3>
    </div>
    <hr>
    
    <div id="ListLoader" class="Loader1"></div>
    <table id="List" class="table table-striped">
    <tbody>
    	<tr>
        	<td width="200">
            	<label><?=$S->L("Titolo"); ?>*</label>
            </td>
            <td><input type="text" name="titolo" class="input-xxlarge"></td>
        </tr>
        <tr>
        	<td><label><?=$S->L('Zona / Quartiere'); ?></label></td>
            <td>
            	<select name="id_ccn" onchange="LoadNegozi();">
                	<?php
					$q = "SELECT * FROM ccn WHERE 1 ORDER BY nome ASC";
					$ris = $cn->Q($q,true);
					foreach($ris as $r){
						echo "<option value=\"{$r[id]}\">{$r[nome]}</option>";	
					}
					?>
                </select>
            </td>
        </tr>
        <tr>
        	<td><label><?=$S->L('Negozio'); ?></label></td>
            <td>
            	<select name="id_negozio"></select>
            </td>
        </tr>
    	<tr>
        	<td>
            	<label for="attivo"><?=$S->L("Attiva"); ?></label>
            </td>
            <td><input type="checkbox" id="attivo" name="attivo" value="1" checked></td>
        </tr>
        <tr>
        	<td>
            	<label><?=$S->L("Periodo"); ?></label>
            </td>
            <td>
            	<?=$S->L('Da'); ?>
                <div id="datetimepicker1" class="input-append date">
                    <input name="dal" data-format="dd/MM/yyyy" type="text" class="input-small"></input>
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <?=$S->L('Al'); ?>
                <div id="datetimepicker2" class="input-append date">
                    <input name="al" data-format="dd/MM/yyyy" type="text" class="input-small"></input>
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
                  <script type="text/javascript"><!--
                    $(function() {
                        $('#datetimepicker1').datetimepicker({
                            pickTime: false
                        });
                        $('#datetimepicker2').datetimepicker({
                            pickTime: false
                        });
                    });
                  --></script>
            </td>
        </tr>
        <tr>
        	<td><label><?=$S->L('Descrizione'); ?></label></td>
            <td>
            	<textarea name="descr" rows="4"></textarea>
            </td>
        </tr>        
    </tbody>
    <tfoot>
    	<tr>
        	<td><span class="textLittle">*<?=$S->L("Campo obbligatorio"); ?></span></td>
            <td align="right">
            	<a class="btn btn-danger" href="javascript:void(0);" onclick="Scheda.SaveGoTo='<?=$S->Uri('sezioni/promozioni'); ?>';Scheda.Save();">Salva</a>
            </td>
        </tr>
    </tfoot>
    </table>
</form>