<script type="text/javascript"><!--
$(document).ready(function(e) {
    Search.Start();
});
--></script>
<form id="listForm">
	<input type="hidden" name="sessione" value="<?=session_id(); ?>">
    <input type="hidden" name="order" value="name">
    <input type="hidden" name="order_dir" value="asc">
    
	<input type="hidden" name="az" value="slideshow_list">
    
    <div class="right">
    	<a href="<?=$S->Uri('sezioni/slideshow/scheda?id=0'); ?>" class="btn btn-primary"><i class="icon-plus icon-white"></i> <?=$S->L('Nuovo'); ?></a>
    </div>
    <hr>

    <div id="noResult"><h5><?=$S->L('Nessun risultato trovato'); ?></h5></div>
    <div id="ListLoader" class="Loader1"></div>
    <table id="List" class="table table-hover table-striped">
    <thead>
    <tr>
    	<th width="30"><a href="#" data-order="id" class="order">#</a></th>
        <th><a href="#" data-order="nome" class="order"><?=$S->L("Nome"); ?></a></th>
        <th width="80"><a href="#" data-order="k" class="order"><?=$S->L("Key"); ?></a></th>
        <th width="50"><div class="center"><a href="#" data-order="attivo" class="order"><?=$S->L("Attivo"); ?></a></div></th>
        <th width="50"><div class="center"><input type="checkbox" name="selectAll" onchange="Search.selectAll();"></div></th>
    </tr>
    </thead>
    <tbody></tbody>
    <tfoot>
    	<tr>
        	<td colspan="4"><b><?=$S->L("Trovati"); ?>: <span data-name="risultati"></span></b></td>
           <td><div class="center"><a href="javascript:void(0);" onclick="Search.Delete('<?=$S->L("Confermi eliminazione?"); ?>');" class="btn"><i class="icon-trash"></i></a></div></td>
        </tr>
    </tfoot>
    </table>
</form>