<?php
$id = $_GET["id"]>0 ? $_GET["id"] : 0;
//echo $id;
?>
<script type="text/javascript"><!--
$(document).ready(function(e) {
	Scheda.Load();
});
var SlideList = function(){
	var url = '<?=$S->Uri('sezioni/slideshow/slide_list'); ?>';
	var id = $("#saveForm").find("input[name='id']").val();
	id = parseInt(id);
	if( id>0 ){
		document.location = url + '?id_block=' + id;
	}else{
		$("#ModalBox .modal-body").html( '<?=$S->L('Salvare la scheda prima di inserire Slides'); ?>' );
		$("#ModalBox").modal({
			'backdrop'	: true	
		});
	}
};
--></script>
<form id="saveForm">
	<input type="hidden" name="sessione" value="<?=session_id(); ?>">
    <input type="hidden" name="id" value="<?=$id; ?>">
	<input type="hidden" name="az" value="slideshow_save">
    <input type="hidden" name="type" value="slideshow">
    
    <div>
    	<a href="<?=$S->Uri('sezioni/slideshow'); ?>" class="btn fr"><i class="icon-circle-arrow-left"></i> <?=$S->L('Indietro'); ?></a>
        <h3><?=$S->L("Slideshow"); ?></h3>
    </div>
    <hr>
    
    <div id="ListLoader" class="Loader1"></div>
    
    <table id="List" class="table table-striped">
    <tbody>
    	<tr>
        	<td width="200">
            	<label><?=$S->L("Nome"); ?>*</label>
            </td>
            <td>
              	<input type="text" name="name" class="input-xxlarge">
            </td>
        </tr>
        
        <tr>
        	<td width="200">
            	<label><?=$S->L("Key"); ?>*</label>
            </td>
            <td>
              	<input type="text" name="k" class="input-large">
                <span class="help-block"><?=$S->L('Chiave per inserimento nel codice dello Slideshow'); ?></span>
            </td>
        </tr>
        
    	<tr>
        	<td>
            	<label for="enable"><?=$S->L("Attivo"); ?></label>
            </td>
            <td><input type="checkbox" id="enable" name="enable" value="1" checked></td>
        </tr>
        
        <tr>
        	<td>
            	<label><?=$S->L("Slides"); ?></label>
            </td>
            <td>
            	<ul id="slides"></ul>
                <a href="javascript:void(0);" onclick="SlideList();" class="btn"><?=$S->L('Gestisci slides'); ?></a>
			</td>
        </tr>
    </tbody>
    <tfoot>
    	<tr>
        	<td><span class="textLittle">*<?=$S->L("Campo obbligatorio"); ?></span></td>
            <td align="right">
            	<a class="btn btn-danger" href="javascript:void(0);" onclick="Scheda.Save();">Salva</a>
            </td>
        </tr>
    </tfoot>
    </table>
</form>