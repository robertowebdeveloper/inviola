<?php
$id_block = $_GET["id_block"];
$id = $_GET["id"]>0 ? $_GET["id"] : 0;
//echo $id;
?>
<script type="text/javascript"><!--
$(document).ready(function(e) {
	Scheda.Load();
});
--></script>
<form id="saveForm">
	<input type="hidden" name="sessione" value="<?=session_id(); ?>">
    <input type="hidden" name="id" value="<?=$id; ?>">
	<input type="hidden" name="az" value="slideshow_slide_save">
    <input type="hidden" name="id_block" value="<?=$id_block; ?>">
    
    <div>
    	<a href="<?=$S->Uri('sezioni/slideshow/slide_list?id_block='.$id_block); ?>" class="btn fr"><i class="icon-circle-arrow-left"></i> <?=$S->L('Indietro'); ?></a>
        <h3><?=$S->L("Slideshow"); ?> :: <?=$S->L('Slide'); ?></h3>
    </div>
    <hr>
    
    <div id="ListLoader" class="Loader1"></div>
    
    <table id="List" class="table table-striped">
    <tbody>
    	<tr>
        	<td width="200"><label><?=$S->L('Immagine'); ?>*</label></td>
            <td>
            	<?=$S->SingleFile('id_file','photo'); ?>
            </td>
        </tr>
        <tr>
        	<td><label><?=$S->L('Ordine'); ?></label></td>
           <td>
           		<?php
				$q = "SELECT COUNT(DISTINCT id) AS n FROM `{$S->_dp}slider_items` WHERE id_block = {$id_block} AND deleted IS NULL";
				$n = $S->cn->OF($q);
				?>
           		<select name="order">
                	<option value="NULL">Auto</option>
                   <?php
					if( $id==0 ){
						$n++;
					}
					for($i=0;$i<$n;$i++){
						?><option value="<?=$i; ?>"><?=($i+1); ?></option><?php
					}
					?>
               </select>
           </td>
        </tr>
    	<tr>
        	<td>
            	<label><?=$S->L("Url"); ?></label>
            </td>
            <td>
              	<input type="text" name="url" class="input-xxlarge">
                <span class="help-block"><?=$S->L('formato assoluto o relativo'); ?></span>
            </td>
        </tr>
        
        <tr>
        	<td>
            	<label><?=$S->L("Apertura"); ?></label>
            </td>
            <td>
            	<select name="target_blank">
                	<option value="0">Nella pagina</option>
                    <option value="1">Pagina esterna</option>
                </select>
            </td>
        </tr>
        
        <tr>
        	<td>
            	<label><?=$S->L("Visibile a"); ?></label>
            </td>
            <td>
            	<select name="showed">
                	<option value="everyone">Qualsiasi utente</option>
                   <option value="logged">Solo utenti collegati</option>
                   <option value="guest">Solo utenti ospiti (non registrati)</option>
                </select>
            </td>
        </tr>
        
        <tr>
        	<td><label><?=$S->L('Visibile dal'); ?></label></td>
            <td>
            	<div id="datetimepicker1" class="input-append date">
                    <input name="show_from" data-format="dd/MM/yyyy" type="text" class="input-small"></input>
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
            </td>
        </tr>
        <tr>
        	<td><label><?=$S->L('Visibile al'); ?></label></td>
            <td>
            	<div id="datetimepicker2" class="input-append date">
                    <input name="show_to" data-format="dd/MM/yyyy" type="text" class="input-small"></input>
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
                <script type="text/javascript"><!--
				$(function() {
					$('#datetimepicker1').datetimepicker({
						pickTime: false
					});
					$('#datetimepicker2').datetimepicker({
						pickTime: false
					});
				});
			  --></script>
            </td>
        </tr>
        
    	<tr>
        	<td>
            	<label for="enable"><?=$S->L("Attivo"); ?></label>
            </td>
            <td><input type="checkbox" id="enable" name="enable" value="1" checked></td>
        </tr>
    </tbody>
    <tfoot>
    	<tr>
        	<td><span class="textLittle">*<?=$S->L("Campo obbligatorio"); ?></span></td>
            <td align="right">
            	<a class="btn btn-danger" href="javascript:void(0);" onclick="Scheda.Save();">Salva</a>
            </td>
        </tr>
    </tfoot>
    </table>
</form>