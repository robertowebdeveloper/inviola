<?php
$lang = $_GET["lang"];
if( empty($lang) ){
	$q = "SELECT id FROM {$_db_prefix}langs WHERE is_default=1 LIMIT 1";
	$lang = $cn->OF($q);
}
?><script type="text/javascript"><!--
$(document).ready(function(e) {});

var changeLang = function(){
	$("#langForm").submit();
};
--></script>
<form id="langForm" method="get" enctype="application/x-www-form-urlencoded" action="http://<?=$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]; ?>">
	<div>
    	<select name="lang" onchange="changeLang();" class="fr">
        	<?php
			$q = "SELECT * FROM `{$_db_prefix}langs` WHERE 1 ORDER BY name ASC";
			$ris = $cn->Q($q,true);
			foreach($ris as $r){
				$sel = $r["id"]==$lang ? ' selected="selected"' : "";
				echo "<option value=\"{$r[id]}\"{$sel}>{$r[name]}</option>";
			}
			?>
        </select>
        <h3><?=$S->L("Traduzione"); ?></h3>
    </div>
</form>
<form id="saveForm">
	<input type="hidden" name="sessione" value="<?=session_id(); ?>">
	<input type="hidden" name="az" value="translate_save">
    <input type="hidden" name="lang" value="<?=$lang; ?>">
    
    <hr>
    
    <div id="ListLoader" class="Loader1"></div>
    <table id="List" class="table table-striped">
    <thead>
    	<tr>
        	<th width="35%">Voce</th>
            <th width="65%"><?=$Lingua["nome"]; ?></th>
        </tr>
    </thead>
    <tbody>
    	<?php
		$q = "SELECT * FROM `{$_db_prefix}labels` WHERE id_parent IS NULL";
		$ris = $cn->Q($q,true);
		$iRow=0;
		foreach($ris as $r){
			$q = "SELECT * FROM `{$_db_prefix}labels` WHERE id_parent = {$r[id]} AND `id_lang`='{$lang}' LIMIT 1";
			$Word = $cn->OQ($q);
			if( $Word==-1 ){
				$q = "INSERT INTO `{$_db_prefix}labels` (id_parent,`id_lang`) VALUES ({$r[id]},'{$lang}')";
				$cn->Q($q);
				$Word = array( "id"=>$cn->last_id );
			}
			?>
            <tr>
            	<td><?=$r["label"]; ?></td>
                <td>
					<input type="hidden" name="idlabel-<?=$iRow; ?>" value="<?=$Word["id"]; ?>">
                    <?php if( strlen($r["label"])>45 ){ ?>
                    	<textarea name="label-<?=$iRow; ?>" rows="5" class="input-xxlarge"><?=$Word["label"]; ?></textarea>
                    <?php }else{ ?>
						<input type="text" name="label-<?=$iRow; ?>" class="input-xxlarge" value="<?=str_replace('"',"&quot;",$Word["label"]); ?>">
                    <?php } ?>
                </td>
            </tr>
		<?php
			$iRow++;
        } ?>
    </tbody>
    <tfoot>
    	<tr>
        	<td><span class="textLittle">*<?=$S->L("Campo obbligatorio"); ?></span></td>
            <td align="right">
            	<a class="btn btn-danger" href="javascript:void(0);" onclick="Scheda.Save();">Salva</a>
            </td>
        </tr>
    </tfoot>
    </table>
</form>