<script type="text/javascript"><!--
$(document).ready(function(e) {
    Search.Start();
});
--></script>
<form id="listForm">
	<input type="hidden" name="sessione" value="<?=session_id(); ?>">
    <input type="hidden" name="order" value="timestamp">
    <input type="hidden" name="order_dir" value="desc">
    
	<input type="hidden" name="az" value="users_gallery_list">
    
    
    
    <div class="right">
    	<select name="s" onchange="Search.Start();" style="vertical-align: top;">
            <option value="new">Nuovi</option>
            <option value="approvati">Approvati</option>
            <option value="respinti">Respinti</option>
        </select>
    	<!--a href="<?=$S->Uri('sezioni/news_promo/scheda?id=0'); ?>" class="btn btn-primary"><i class="icon-plus icon-white"></i> <?=$S->L('Nuova'); ?></a-->
    </div>
    <hr>

    <div id="noResult"><h5><?=$S->L('Nessun risultato trovato'); ?></h5></div>
    <div id="ListLoader" class="Loader1"></div>
    <table id="List" class="table table-hover table-striped">
    <thead>
    <tr>
    	<th width="30"><a href="#" data-order="id" class="order">#</a></th>
        <th width="200"><a href="#" data-order="title" class="order"><?=$S->L("Foto"); ?></a></th>
        <th width="80"><div class="left"><a href="#" data-order="type" class="order"><?=$S->L("Stato"); ?></a></div></th>
        <!--th width="50"><div class="center"><input type="checkbox" name="selectAll" onchange="Search.selectAll();"></div></th-->
    </tr>
    </thead>
    <tbody></tbody>
    <tfoot>
    	<tr>
        	<td colspan="3"><b><?=$S->L("Trovati"); ?>: <span data-name="risultati"></span></b></td>
            <!--td><div class="center"><a href="javascript:void(0);" onclick="Search.Delete('<?=$S->L("Confermi eliminazione?"); ?>');" class="btn"><i class="icon-trash"></i></a></div></td-->
        </tr>
    </tfoot>
    </table>
</form>