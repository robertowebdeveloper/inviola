<?php
$id = $_GET["id"]>0 ? $_GET["id"] : 0;
$q = "UPDATE `{$_db_prefix}users_gallery` SET `new`=0 WHERE id = {$id}";
$cn->Q($q);
$q = "SELECT * FROM `{$_db_prefix}users_gallery` WHERE id = {$id}";
$Scheda = $cn->OQ($q);

$q = "SELECT * FROM `{$_db_prefix}file` WHERE id = {$Scheda[id_file]}";
$F = $cn->OQ($q);
$filepath = "../../../_public/file/" . substr($F["id"],0,1) . "/{$F[id]}.{$F[ext]}";
//echo $id;
?>
<script type="text/javascript"><!--
$(document).ready(function(e) {
	//Scheda.Load();
});
--></script>
<form id="saveForm">
	<input type="hidden" name="sessione" value="<?=session_id(); ?>">
    <input type="hidden" name="id" value="<?=$id; ?>">
	<input type="hidden" name="az" value="users_gallery_save">
    <input type="hidden" name="id_user" value="<?=$User["id"]; ?>">
    
    <div>
    	<a href="<?=$S->Uri('sezioni/users_gallery'); ?>" class="btn fr"><i class="icon-circle-arrow-left"></i> <?=$S->L('Indietro'); ?></a>
        <h3><?=$S->L("Scheda Utente-Foto"); ?></h3>
    </div>
    <hr>
    
    <div id="ListLoader" class="Loader1"></div>
    <table id="List" class="table table-striped">
    <tbody>
    	<tr>
        	<td width="200">
            	<label><?=$S->L("Foto"); ?></label>
            </td>
            <td><img src="<?=$filepath; ?>" alt="" style="max-height: 250px;"></td>
        </tr>
        
        <tr>
        	<td><label><?=$S->L('Approvata'); ?></label></td>
           <td>	
           		<input type="checkbox" name="approved" value="1"<?=!empty($Scheda["approved"]) ? ' checked' : ''; ?>>
           </td>
        </tr>
        
    </tbody>
    <tfoot>
    	<tr>
        	<td><span class="textLittle">*<?=$S->L("Campo obbligatorio"); ?></span></td>
            <td align="right">
            	<a class="btn btn-danger" href="javascript:void(0);" onclick="Scheda.SaveGoTo='<?=$S->Uri('sezioni/users_gallery'); ?>';Scheda.Save();">Salva</a>
            </td>
        </tr>
    </tfoot>
    </table>
</form>