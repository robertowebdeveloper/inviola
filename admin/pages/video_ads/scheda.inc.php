<?php
$id = $_GET["id"]>0 ? $_GET["id"] : 0;
//echo $id;
?>
<script type="text/javascript"><!--
$(document).ready(function(e) {
	Scheda.loadData_callback = postType;
	Scheda.Load();
});
var postType = function(){
	var v = $("select[name='type']").val();
	$(".typeImage, .typeVideo").hide();
	if(v=="image"){
		$(".typeImage").show();
	}else{
		$(".typeVideo").show();
	}
};
--></script>
<form id="saveForm">
	<input type="hidden" name="sessione" value="<?=session_id(); ?>">
    <input type="hidden" name="id" value="<?=$id; ?>">
	<input type="hidden" name="az" value="video_ads_save">
    
    <div>
    	<a href="<?=$S->Uri('sezioni/video_ads'); ?>" class="btn fr"><i class="icon-circle-arrow-left"></i> <?=$S->L('Indietro'); ?></a>
        <h3><?=$S->L("Scheda Video AD"); ?></h3>
    </div>
    <hr>
    
    <div id="ListLoader" class="Loader1"></div>
    
    <table id="List" class="table table-striped">
    <tbody>
    	<tr>
        	<td width="200"><label><?=$S->L('Titolo'); ?></label></td>
            <td>
            	<input type="text" name="title" class="input-xxlarge">
            </td>
        </tr>
    	<tr>
        	<td><label><?=$S->L('Tipo'); ?></label></td>
            <td>
            	<select name="type" onchange="postType();">
                	<option value="image">Immagine</option>
                   <option value="preroll">Video pre-roll</option>
               </select>
            </td>
        </tr>
    	<tr>
        	<td>
            	<label class="typeImage"><?=$S->L('Immagine'); ?>*</label>
               <label class="typeVideo"><?=$S->L('Codice video'); ?>*</label>
            </td>
            <td>
            	<div class="typeImage"><?=$S->SingleFile('id_file','photo'); ?></div>
               <div class="typeVideo"><textarea name="ext_code" class="input-xxlarge" rows="18"></textarea></div>
            </td>
        </tr>
        <tr>
        	<td>
            	<label><?=$S->L('Durata'); ?></label>
            </td>
            <td>
            	<select name="timing_min" class="input-small">
                	<?php for($i=0;$i<=10;$i++){ ?><option value="<?=$i; ?>"><?=str_pad($i,2,"0",STR_PAD_LEFT); ?>'</option><?php } ?>
               </select>
               
            	<select name="timing_sec" class="input-small">
                	<?php for($i=0;$i<=59;$i++){ ?><option value="<?=$i; ?>"><?=str_pad($i,2,"0",STR_PAD_LEFT); ?>"</option><?php } ?>
                </select>
                <br>
                <span class="help-text">Il tempo 0'00" corrisponde ad infinito</span>
            </td>
        </tr>
        <tr>
        	<td>
            	<label><?=$S->L('Area sul sito'); ?></label>
            </td>
            <td>
            	<select name="section">
                   <option value="highlights">Highlights</option>
                   <option value="partite_integrali">Partite integrali</option>
                </select>
            </td>
        </tr>
    	<tr>
        	<td>
            	<label><?=$S->L("Url"); ?></label>
            </td>
            <td>
              	<input type="text" name="url" class="input-xxlarge">
                <span class="help-block"><?=$S->L('formato assoluto o relativo'); ?></span>
            </td>
        </tr>
        
        <tr>
        	<td>
            	<label><?=$S->L("Apertura"); ?></label>
            </td>
            <td>
            	<select name="blank">
                	<option value="0">Nella pagina</option>
                    <option value="1" selected>Pagina esterna</option>
                </select>
            </td>
        </tr>
        <tr>
        	<td><label><?=$S->L('Visibile dal'); ?></label></td>
            <td>
            	<div id="datetimepicker1" class="input-append date">
                    <input name="show_from" data-format="dd/MM/yyyy" type="text" class="input-small"></input>
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
            </td>
        </tr>
        <tr>
        	<td><label><?=$S->L('Visibile al'); ?></label></td>
            <td>
            	<div id="datetimepicker2" class="input-append date">
                    <input name="show_to" data-format="dd/MM/yyyy" type="text" class="input-small"></input>
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
                <script type="text/javascript"><!--
				$(function() {
					$('#datetimepicker1').datetimepicker({
						pickTime: false
					});
					$('#datetimepicker2').datetimepicker({
						pickTime: false
					});
				});
			  --></script>
            </td>
        </tr>
    </tbody>
    <tfoot>
    	<tr>
        	<td><span class="textLittle">*<?=$S->L("Campo obbligatorio"); ?></span></td>
            <td align="right">
            	<a class="btn btn-danger" href="javascript:void(0);" onclick="Scheda.Save();">Salva</a>
            </td>
        </tr>
    </tfoot>
    </table>
</form>