<?php
$id = $_GET["id"]>0 ? $_GET["id"] : 0;
//echo $id;
?>
<script type="text/javascript"><!--
$(document).ready(function(e) {
	Scheda.Load();
});
--></script>
<form id="saveForm">
	<input type="hidden" name="sessione" value="<?=session_id(); ?>">
    <input type="hidden" name="id" value="<?=$id; ?>">
	<input type="hidden" name="az" value="widgets_save">
    
    <div>
    	<a href="<?=$S->Uri('sezioni/widgets'); ?>" class="btn fr"><i class="icon-circle-arrow-left"></i> <?=$S->L('Indietro'); ?></a>
        <h3><?=$S->L("Scheda Widget"); ?></h3>
    </div>
    <hr>
    
    <div id="ListLoader" class="Loader1"></div>
    
    <table id="List" class="table table-striped">
    <tbody>
    	<tr>
        	<td width="200"><label><?=$S->L('Titolo'); ?></label></td> 
           <td>
           		<?php $max = 20; ?>
           		<input type="text" name="title" maxlength="<?=$max; ?>" class="input-large">
               <span class="help-block">(max <?=$max; ?> caratteri)</span>
           </td>
        </tr>
    	<tr>
        	<td><label><?=$S->L('Immagine di sfondo'); ?></label></td>
            <td>
            	<?=$S->SingleFile('id_file','photo'); ?>
               <span class="help-block">(Se inserita un'immagine di sfondo non sarà visibile il testo. 230x240 px)</span>
            </td>
        </tr>
        <tr>
        	<td>
            	<label><?=$S->L('Testo'); ?></label>
            </td>
            <td>
            	<?php $max=45; ?>
            	<input type="text" name="text" class="input-xxlarge" maxlength="<?=$max; ?>">
               <span class="help-block">(Se inserita un'immagine di sfondo non sarà visibile il testo. Max <?=$max; ?> caratteri)</span>
            </td>
        </tr>
        
        <tr>
        	<td>
            	<label><?=$S->L('Testo pulsante'); ?></label>
            </td>
            <td>
            	<?php $max=24; ?>
            	<input type="text" name="text_button" class="input-large" maxlength="<?=$max; ?>">
               <span class="help-block">(Max <?=$max; ?> caratteri)</span>
            </td>
        </tr>
        
    	<tr>
        	<td>
            	<label><?=$S->L("Url"); ?></label>
            </td>
            <td>
              	<input type="text" name="url" class="input-xxlarge">
               <span class="help-block"><?=$S->L('formato assoluto o relativo'); ?></span>
            </td>
        </tr>
        
        <tr>
        	<td>
            	<label><?=$S->L("Apertura"); ?></label>
            </td>
            <td>
            	<select name="url_blank">
                	<option value="0" selected>Nella pagina</option>
                   <option value="1">Pagina esterna</option>
                </select>
            </td>
        </tr>
                
        <?php if(false){ ?>
        <tr>
        	<td><label><?=$S->L('Visibile dal'); ?></label></td>
            <td>
            	<div id="datetimepicker1" class="input-append date">
                    <input name="show_from" data-format="dd/MM/yyyy" type="text" class="input-small"></input>
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
            </td>
        </tr>
        <tr>
        	<td><label><?=$S->L('Visibile al'); ?></label></td>
            <td>
            	<div id="datetimepicker2" class="input-append date">
                    <input name="show_to" data-format="dd/MM/yyyy" type="text" class="input-small"></input>
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
                <script type="text/javascript"><!--
				$(function() {
					$('#datetimepicker1').datetimepicker({
						pickTime: false
					});
					$('#datetimepicker2').datetimepicker({
						pickTime: false
					});
				});
			  --></script>
            </td>
        </tr>
        <?php } ?>
        
    	<tr>
        	<td>
            	<label for="enable_on"><?=$S->L("Attivo"); ?></label>
            </td>
            <td><input type="checkbox" id="enable_on" name="enable_on" value="1" checked></td>
        </tr>
    </tbody>
    <tfoot>
    	<tr>
        	<td><span class="textLittle">*<?=$S->L("Campo obbligatorio"); ?></span></td>
            <td align="right">
            	<a class="btn btn-danger" href="javascript:void(0);" onclick="Scheda.SaveGoTo='<?=$S->Uri('sezioni/widgets'); ?>';Scheda.Save();">Salva</a>
            </td>
        </tr>
    </tfoot>
    </table>
</form>