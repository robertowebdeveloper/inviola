<?php
$CHECK_LOGIN = true;
require("_ext/scripts/main.inc.php");

$User = $S->User();

list(,$sezione,$page) = explode("/",$_GET["P"]);
$page = empty($page) ? "index" : $page;
$Page = "{$sezione}/{$page}";
$page_inc = "pages/{$Page}.inc.php";
//$page_inc = file_exists( $page_inc ) ? $page_inc : "pages/dashboard.inc.php";
if( !file_exists($page_inc) ){
	$q = "SELECT id_section FROM `fn_admin_permissions` WHERE id_admin = {$User[id]} AND permission='w' LIMIT 1";
	$id_section = $S->cn->OF($q);
	$q = "SELECT `path` FROM `fn_sections` WHERE id = {$id_section}";
	$path = $S->cn->OF($q);
	header("Location: {$_PARAMETRI[basedir]}{$path}");
	//header("Location: {$_PARAMETRI[basedir]}sezioni/admin");
}
?><!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title><?=$S->P('pdc_title'); ?></title>
<link href="_ext/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="all">
<link href="_ext/css/layout.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="_ext/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="_ext/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript" src="_ext/js/plugins/jquery.easing.min.js"></script>
<script type="text/javascript" src="_ext/js/plugins/ui/js/jquery-ui-1.10.3.custom.min"></script>
<link href="_ext/js/plugins/ui/css/redmond/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="_ext/js/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<link href="_ext/js/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="_ext/js/tinymce/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="_ext/js/tinymce/js/tinymce/jquery.tinymce.min.js"></script>

<script type="text/javascript" src="_ext/js/main.js"></script>
<script type="text/javascript"><!--

var _PARAMETRI = new Array();
<?php foreach($_PARAMETRI as $k=>$v){ ?>_PARAMETRI['<?=$k; ?>'] = '<?=$v; ?>';<?php } ?>
$(document).ready(function(e) {});
--></script>
</head>

<body>
<?php include("_ext/include/system_elements.inc.php"); ?>

<header>
	<img id="Logo" src="_ext/img/logo.png">
	<ul id="headerButtons">
    	<?php if($User["configuration_on"]){ ?><li><a href="<?=$S->Uri('sezioni/configurazione'); ?>" class="btn btn-primary">Configurazione</a></li><?php } ?>
    	<li><a href="<?=$_PARAMETRI["basedir"]; ?>?logout=1" class="btn btn-primary"><i class="icon-off icon-white"></i></a></li>
    </ul>
</header>

<aside id="Left">
	<nav><ul id="Menu">
    	<?php include("_ext/include/leftmenu.inc.php"); ?>
    </ul></nav>
</aside>
<section id="Main"><div id="MainCont">
	<!-- <?=$page_inc; ?> -->
	<?php include( $page_inc ); ?>
    <!--<div style="height: 2000px;"></div>-->
</div></section>

</body>
</html>
<?php
$cn->Close();
unset($cn);
?>