<?php
$_DEBUG_MODE = true;
if( !$_DEBUG_MODE ){
	$CHECK_LOGIN = true;
}
require("_ext/scripts/main.inc.php");

$q = "SELECT id FROM {$_db_prefix}admin WHERE `session` = '{$_POST['session']}' ORDER BY `session_time` DESC LIMIT 1";
if( $_DEBUG_MODE ){
	$q = "SELECT id FROM {$_db_prefix}admin WHERE id = 1";
}
$id_user = $cn->OF($q);

$az = $_REQUEST["az"];

if( $id_user>0 || $az=="login"){
	$Services = new Services( $id_user );
	$Services->Exec( $az );
}else{
	die();
}

/*
$fp = fopen("logs/q.txt","w");
fwrite($fp,$q);
fclose($fp);
*/

class Services{
	public function __construct($id_user){
		global $cn,$S,$_db_prefix;
		$this->cn = &$cn;
		$this->S = &$S;
		$this->_dp = $_db_prefix;
		
		if( $id_user>0 ){
			$q = "SELECT * FROM {$_db_prefix}admin WHERE id = {$id_user}";
			$this->User = $this->cn->OQ($q);
		}
	}
	public function __desctruct(){
		unset( $this->cn );	
	}
	
	public function Exec($az,$return='json_header'){ //return=>'json_header','json','echo','return'
		switch($az){
			case "login":
				$pw = hash("sha512",$_POST["pw"]);
				
				$q = "SELECT * FROM {$this->_dp}admin WHERE `login` = '{$_POST['lg']}' AND `pw`='{$pw}'";
				$Login = $this->cn->OQ($q);
				
				if( $Login["id"]>0 ){
					$ret = array("status"=>1,"id"=>$Login["id"]);
				}else{
					$ret = array("status"=>0,"msg"=>"Accesso non consentito");
				}
				sleep(1);
				break;
			case "getPathFile":
				$id_file = $_POST["id_file"];
				
				$pathfile = $this->pathFile($id_file);
				if( $pathfile ){
					$ret = array("status"=>1,"path"=>$pathfile);
				}else{
					$ret = array("status"=>0,"msg"=>"Errore nel caricamento del file");
				}
				
				break;
			case "upload_foto":
				$info = getimagesize($_FILES["foto"]["tmp_name"]);
				$control_type_file = $_POST["control_type_file"]=="0" ? false : true;
				if(
					$info[2]==1 || $info[2]==2 || $info[2]==3 //1=>GIF, 2=>JPG, 3=>PNG
					|| !$control_type_file ){
					
					$tmp = explode(".",$_FILES["foto"]["name"]);
					$ext = strtolower( array_pop($tmp) );
					
					$q = "INSERT INTO `{$this->_dp}file` (`name`,`size`,`type`,`ext`,`temp`) VALUES ('{$_FILES['foto']['name']}',{$_FILES['foto']['size']},'{$_FILES['foto']['type']}','{$ext}',1)";
					$this->cn->Q($q);
					$id_file = $this->cn->last_id;
					$name = "{$id_file}.{$ext}";
					
					$subdir = substr($name,0,1);
					$pathdir = "_public/file/{$subdir}";
					$pathdir_here = "../{$pathdir}";
					if( !file_exists($pathdir_here) ){
						@mkdir( $pathdir_here );	
					}
					
					if( !move_uploaded_file($_FILES["foto"]["tmp_name"], "{$pathdir_here}/{$name}" ) ){
						$q = "DELETE * FROM `file` WHERE id = {$id_file}";
						$this->cn->Q($q);
						$ret = array("status"=>0,"msg"=>"Errore nell'upload del file");
					}else{
						$ret = array("status"=>1,"id"=>$id_file,"name"=>$_FILES["foto"]["name"],"path"=> path_webroot . "{$pathdir}/{$name}");
					}	
				}else{
					$ret = array("status"=>0,"msg"=>"Formato file non corretto (accettati: GIF,JPG,PNG)");
				}
				break;
			case "admin_list":
				if( $_POST["delete"]==1 ){
					$ids = array();
					foreach($_POST as $k=>$v){
						if( strpos($k,'delete_id')===0 && $v>0 && $v!=1){
							$ids[] = $v;
						}
					}
					if( count($ids)>0 ){
						$q = "UPDATE {$this->_dp}admin SET deleted = NOW() WHERE id!={$this->User['id']} AND id IN (".implode(",",$ids).")";
						$this->cn->Q($q);
						$ret = array("status"=>1);
					}else{
						$ret = array("status"=>0);
					}
				}else{
					$order = $_POST["order"];
					$order_dir = $_POST["order_dir"];
	
					$q = "SELECT * FROM {$this->_dp}admin WHERE 1 AND deleted IS NULL ORDER BY {$order} {$order_dir}";
					
					$ris = $this->cn->Q($q,true) or self::Error($q);
					$n = $this->cn->n;
					
					$list = array();
					foreach($ris as $r){
						$row = array();
						$row[] = "<tr class=\"Clickable\" data-url=\"sezioni/admin/scheda?id={$r['id']}\">";
						$row[] = "<td><div class=\"center\">{$r['id']}</div></td>";
						$row[] = "<td>{$r['nome']}</td>";
						$row[] = "<td>{$r['login']}</td>";
						$row[] = "<td>{$r['email']}</td>";
						$row[] = "<td class=\"noClickable\"><div class=\"center\"><input type=\"checkbox\" name=\"delete_id{$r['id']}\" value=\"{$r['id']}\"></div></td>";
						$row[] = '</tr>';
						
						$list[] = implode("",$row);
					}
					
					
					$ret = array("status"=>1,
						"n"=>$n,
						"list"=>$list,
						"order"=>$order,
						"order_dir"=>$order_dir
					);
				
					self::delay();
				}
				break;
			case "admin_save":
				$id = $_POST["id"];
				$Load = $_POST["load"]==1 ? true : false;
				
				$status = 0;
				$msg = "";
				
				if( !$Load ){
					//Controlli campi
					$errors = array();
					if( strlen($_POST["name"])==0 ){
						$errors[] = $this->S->L("Il campo Nome è necessario");
					}
					if( strlen($_POST["login"])==0 ){
						$errors[] = $this->S->L("Il campo Login è necessario");
					}else{
						$q = "SELECT * FROM {$this->_dp}admin WHERE `login`='{$_POST['login']}' AND id!={$id}";
						$this->cn->Q($q);
						if( $this->cn->n>0 ){
							$errors[] = $this->S->L("Il Login è già in uso da un altro utente");
						}
					}
					
					$pw = $_POST["pw"];
					if( $id==0 && empty($pw) ){
						$errors[] = $this->S->L("Password è necessaria");	
					}
					if( strlen($pw)>0 ){
						if( strlen($pw)<6 ){
							$errors[] = $this->S->L("La Password deve essere almeno di 6 caratteri");
						}else if($pw!=$_POST["pw2"]){
							$errors[] = $this->S->L("La Password di conferma non corrisponde");
						}else{
							$pw = hash("sha512",$pw);
						}
					}

					$email = $_POST["email"];
					if( strlen($email)>0 && !NE_Email::checkMail($email) ){
						$errors[] = $this->S->L("Indirizzo e-mail non valido");
					}else if(strlen($email)>0 ){
						$q = "SELECT * FROM {$this->_dp}admin WHERE id!={$id} AND email='{$email}'";
						$this->cn->Q($q);
						if( $this->cn->n>0){
							$errors[] = $this->S->L("Indirizzo E-mail già in uso da un altro utente");
						}
					}
					
					if( count($errors)>0){
						$msg = $this->toMsg($errors);
					}else{
						if( $id==0 ){
							$q = "INSERT INTO {$this->_dp}admin () VALUES ()";
							$this->cn->Q($q);
							$id = $this->cn->last_id;
						}
						
						$login = $_POST["login"];
						$login = str_replace(" ","",$login);
						$enable = isset($_POST["enable"]) ? 1 : 0;
						$configuration_on = isset($_POST["configuration_on"]) ? 1 : 0;
						$email = empty($email) ? "NULL" : "'{$email}'";
						$q = "UPDATE {$this->_dp}admin SET
							name = '".addslashes($_POST["name"])."',
							login = '{$login}',
							`email` = {$email},
							`configuration_on` = {$configuration_on},
							enable = {$enable}";
						if( $pw ){
							$q .= ", `pw` = '{$pw}'";
						}
						$q .= " WHERE id = {$id}";

						$this->cn->Q($q);
						
						$q = "DELETE FROM {$this->_dp}admin_permissions WHERE id_admin = {$id}";
						$this->cn->Q($q);
						foreach($_POST as $k=>$v){
							if(strpos($k,"permessi_")===0){
								list(,$id_sezione) = explode("_",$k);
								$q = "INSERT INTO {$this->_dp}admin_permissions (id_admin,id_section,permission) VALUES ({$id},{$id_sezione},'{$v}')";
								$this->cn->Q($q);
							}
						}
						
						$status=1;
					}
				}else{
					$status=1;
				}
				
				$dati = false;
				if( $Load && $id>0 || $id>0 ){
					$q = "SELECT * FROM {$this->_dp}admin WHERE id = {$id}";
					$dati = $this->cn->OQ( $q );
					$q = "SELECT * FROM {$this->_dp}admin_permissions WHERE id_admin = {$id}";
					$permessi = $this->cn->Q($q,true);
					$dati["permessi"] = $permessi;
				}
				
				$ret = array("status"=>$status,"msg"=>$msg,"fields"=>$dati);
				self::delay();
				break;
			case "pagine_save":
				$id = $_POST["id"];
				$Load = $_POST["load"]==1 ? true : false;
				
				$status = 0;
				$msg = "";
				
				$url = $this->parseURL($_POST["url"]);
				
				$q = "SELECT id FROM {$this->_dp}pages WHERE url = '{$url}' AND id!={$id}";
				$ck = $this->cn->OF($q);
				
				if( !$Load ){
					$errors = array();
					if( $ck>0 ){
						$errors[] = "URL già assegnato ad altra pagina";
					}
					
					$k = strtolower($_POST["k"]);
					$k = str_replace(" ","-",$k);
					if( strlen($k)>0 ){
						$q = "SELECT id FROM {$this->_dp}pages WHERE `k` = '{$k}' AND id_lang = {$_POST['id_lang']} AND id!={$id}";
						$this->cn->Q($q);
						if( $this->cn->n>0 ){
							$errors[] = "Key già utilizzata da un'altra pagina con la stessa lingua";
						}
					}
					
					if( count($errors)>0 ){
						$msg = $this->toMsg($errors);
					}else{
						if( $id==0 ){
							$q = "INSERT INTO {$this->_dp}pages (id_lang) VALUES ({$_POST['id_lang']})";
							$this->cn->Q($q);
							$id = $this->cn->last_id;
						}
						
						$id_parent = $_POST["id_parent"]>0 ? $_POST["id_parent"] : "NULL";
						$is_home = isset($_POST["is_home"]) ? 1 : 0;
						$auth_required = isset($_POST["auth_required"]) ? 1 : 0;
						$q = "UPDATE {$this->_dp}pages SET
							id_parent = {$id_parent},
							auth_required = {$auth_required},
							`k` = '{$k}',
							id_lang = {$_POST['id_lang']},
							is_home = {$is_home},
							css_class = '{$_POST['css_class']}',
							template = '{$_POST['template']}',
							file_inc = '{$_POST['file_inc']}',
							name = '".addslashes($_POST["name"])."',
							url = '{$url}',
							`meta` = '" . addslashes($_POST['meta']) . "',
							seo_title = '".addslashes($_POST["seo_title"])."',
							seo_keywords = '".addslashes($_POST["seo_keywords"])."',
							seo_description = '".addslashes($_POST["seo_description"])."',
							html = '".addslashes($_POST["html"])."'
							
							WHERE id = {$id}
							";
						$this->cn->Q($q);
						
						$status = 1;
					}
				}else{
					$status = 1;
				}
				
				$dati = false;
				if( $Load && $id>0 || $id>0 ){
					$q = "SELECT * FROM {$this->_dp}pages WHERE id = {$id}";
					$dati = $this->cn->OQ( $q );
				}
				
				$ret = array("status"=>$status,"msg"=>$msg,"fields"=>$dati);
				
				break;
			case "pagine_list":
				if( $_POST["delete"]==1 ){
					$ids = array();
					foreach($_POST as $k=>$v){
						if( strpos($k,"delete_id")===0 && $v>0){
							$ids[] = $v;
						}
					}
					if( count($ids)>0 ){
						$q = "UPDATE {$this->_dp}pages SET deleted = NOW() WHERE id IN (".implode(",",$ids).")";
						$this->cn->Q($q);
						$ret = array("status"=>1);
					}else{
						$ret = array("status"=>0);
					}
				}else{
					$order = $_POST["order"];
					$order_dir = $_POST["order_dir"];
	
					$q = "SELECT * FROM {$this->_dp}pages WHERE 1 AND deleted IS NULL";
										
					$q .= " ORDER BY {$order} {$order_dir}";
					
					$ris = $this->cn->Q($q,true);
					$n = $this->cn->n;
					
					$list = array();
					foreach($ris as $r){
						$row = array();
						$row[] = "<tr class=\"Clickable\" data-url=\"sezioni/pagine/scheda?id={$r['id']}\">";
						$row[] = "<td><div class=\"center\">{$r['id']}</div></td>";
						$row[] = "<td>{$r['k']}</td>";
						$row[] = "<td>{$r['name']}</td>";
						$row[] = "<td>{$r['url']}</td>";
						//$row[] = "<td class=\"noClickable\"><div class=\"center\"><input type=\"checkbox\" name=\"delete_id{$r['id']}\" value=\"{$r['id']}\"></div></td>";
						$row[] = '</tr>';
						
						$list[] = implode("",$row);
					}
					
					
					$ret = array("status"=>1,
						"n"=>$n,
						"list"=>$list,
						"order"=>$order,
						"order_dir"=>$order_dir
					);
				
					self::delay();
				}
				break;
			case "news_promo_save":
				$id = $_POST["id"];
				$Load = $_POST["load"]==1 ? true : false;
				
				$status = 0;
				$msg = "";
				
				if( !$Load ){
					$errors = array();
					
					if( empty($_POST["title"]) ){
						$errors[] = $this->S->L('Il campo Titolo è necessario');	
					}
					
					if( count($errors)>0 ){
						$msg = $this->toMsg($errors);
					}else{
						if( $id==0 ){
							$q = "INSERT INTO {$this->_dp}news_promo () VALUES ()";
							$this->cn->Q($q);
							$id = $this->cn->last_id;
						}
						
						list($a,$b) = explode(" ",$_POST["date"]);
						$a = strtotime( str_replace("/","-",$a) );
						$hour = "{$b}:00";
						$data = date("Y-m-d",$a) . " {$hour}";
						
						$id_file = $_POST["id_file"]>0 ? $_POST["id_file"] : "NULL";
						$q = "UPDATE {$this->_dp}news_promo SET
							`id_file` = {$id_file},
							`title` = '".addslashes($_POST["title"])."',
							`type` = '{$_POST['type']}',
							description_short = '".addslashes($_POST["description_short"])."',
							description = '".addslashes($_POST["description"])."',
							`date` = '{$data}',
							`show_from` = " . $this->toDate($_POST["show_from"],1) .",
							`show_to` = " . $this->toDate($_POST["show_to"],1) ."
							
							WHERE id = {$id}
							";
						$this->cn->Q($q);
						
						$status = 1;
					}
				}else{
					$status = 1;
				}
				
				$dati = false;
				if( $Load && $id>0 || $id>0 ){
					$q = "SELECT *,
					DATE_FORMAT(`date`,'%d/%m/%Y %H:%i') AS `date`,
					DATE_FORMAT(`show_from`,'%d/%m/%Y') AS `show_from`,
					DATE_FORMAT(`show_to`,'%d/%m/%Y') AS `show_to`
					FROM {$this->_dp}news_promo WHERE id = {$id}";
					$dati = $this->cn->OQ( $q );
				}
				
				$ret = array("status"=>$status,"msg"=>$msg,"fields"=>$dati);
				
				break;
			case "news_promo_list":
				if( $_POST["delete"]==1 ){
					$ids = array();
					foreach($_POST as $k=>$v){
						if( strpos($k,"delete_id")===0 && $v>0){
							$ids[] = $v;
						}
					}
					if( count($ids)>0 ){
						$q = "UPDATE {$this->_dp}news_promo SET deleted = NOW() WHERE id IN (".implode(",",$ids).")";
						$this->cn->Q($q);
						$ret = array("status"=>1);
					}else{
						$ret = array("status"=>0);
					}
				}else{
					$order = $_POST["order"];
					$order_dir = $_POST["order_dir"];
	
					$q = "SELECT
						*,
						IF( `date`=0,'-',
							DATE_FORMAT(`date`,'%d/%m/%Y %H:%i')
						) AS date_txt
						FROM {$this->_dp}news_promo WHERE 1 AND deleted IS NULL";
										
					$q .= " ORDER BY {$order} {$order_dir}";
					
					$ris = $this->cn->Q($q,true);
					$n = $this->cn->n;
					
					$list = array();
					foreach($ris as $r){
						$row = array();
						$row[] = "<tr class=\"Clickable\" data-url=\"sezioni/news_promo/scheda?id={$r['id']}\">";
						$row[] = "<td><div class=\"center\">{$r['id']}</div></td>";
						$row[] = "<td>{$r['title']}</td>";
						$row[] = "<td>{$r['date_txt']}</td>";
						$row[] = "<td>{$r['type']}</td>";
						$row[] = "<td class=\"noClickable\"><div class=\"center\"><input type=\"checkbox\" name=\"delete_id{$r['id']}\" value=\"{$r['id']}\"></div></td>";
						$row[] = '</tr>';
						
						$list[] = implode("",$row);
					}
					
					
					$ret = array("status"=>1,
						"n"=>$n,
						"list"=>$list,
						"order"=>$order,
						"order_dir"=>$order_dir
					);
				
					self::delay();
				}
				break;
			case "slideshow_save":
				$id = $_POST["id"];
				$type = $_POST["type"];
				$Load = $_POST["load"]==1 ? true : false;
				
				$status = 0;
				$msg = "";
				
				if( !$Load ){
					$errors = array();
					
					if( empty($_POST["name"]) ){
						$errors[] = $this->S->L('Il campo Nome è necessario');
					}
					if( empty($_POST["k"]) ){
						$errors[] = $this->S->L('Il campo Key è necessario');
					}
					$q = "SELECT * FROM {$this->_dp}blocks WHERE `k`='{$_POST['k']}' AND id!={$id}  AND deleted IS NULL";
					$this->cn->Q($q);
					if( $cn->n>0 ){
						$errors[] = $this->S->L('La Key scelta è già in uso');
					}
					
					if( count($errors)>0 ){
						$msg = $this->toMsg($errors);
					}else{
						if( $id==0 ){
							$q = "INSERT INTO {$this->_dp}blocks () VALUES ()";
							$this->cn->Q($q);
							$id = $this->cn->last_id;
						}
						
						$enable = isset($_POST["enable"]) ? 1 : 0;
						$q = "UPDATE {$this->_dp}blocks SET
							`k` = '" . addslashes($_POST["k"]) ."',
							`name` = '".addslashes($_POST["name"])."',
							`type` = '{$type}',
							enable = {$enable}
							
							WHERE id = {$id}
							";
						$this->cn->Q($q);
						
						$status = 1;
					}
				}else{
					$status = 1;
				}
				
				$dati = false;
				if( $Load && $id>0 || $id>0 ){
					$q = "SELECT *
					FROM {$this->_dp}blocks WHERE id = {$id}";
					$dati = $this->cn->OQ( $q );
				}
				
				$ret = array("status"=>$status,"msg"=>$msg,"fields"=>$dati);
				
				break;
			case "slideshow_list":
				if( $_POST["delete"]==1 ){
					$ids = array();
					foreach($_POST as $k=>$v){
						if( strpos($k,"delete_id")===0 && $v>0){
							$ids[] = $v;
						}
					}
					if( count($ids)>0 ){
						$q = "UPDATE {$this->_dp}blocks SET deleted = NOW() WHERE id IN (".implode(",",$ids).")";
						$this->cn->Q($q);
						$ret = array("status"=>1);
					}else{
						$ret = array("status"=>0);
					}
				}else{
					$order = $_POST["order"];
					$order_dir = $_POST["order_dir"];
	
					$q = "SELECT
						*
						FROM {$this->_dp}blocks WHERE type='slideshow' AND deleted IS NULL";
										
					$q .= " ORDER BY {$order} {$order_dir}";
					
					$ris = $this->cn->Q($q,true);
					$n = $this->cn->n;
					
					$list = array();
					foreach($ris as $r){
						$row = array();
						$row[] = "<tr class=\"Clickable\" data-url=\"sezioni/slideshow/scheda?id={$r['id']}\">";
						$row[] = "<td><div class=\"center\">{$r['id']}</div></td>";
						$row[] = "<td>{$r['name']}</td>";
						$row[] = "<td>{$r['k']}</td>";
						$attivo = $r["enable"]==1 ? "check.png" : "del.png";
						$row[] = "<td><div class=\"center\"><img src=\"_ext/img/icon/{$attivo}\" width=\"24\"></div></td>";
						$row[] = "<td class=\"noClickable\"><div class=\"center\"><input type=\"checkbox\" name=\"delete_id{$r['id']}\" value=\"{$r['id']}\"></div></td>";
						$row[] = '</tr>';
						
						$list[] = implode("",$row);
					}
					
					
					$ret = array("status"=>1,
						"n"=>$n,
						"list"=>$list,
						"order"=>$order,
						"order_dir"=>$order_dir
					);
				
					self::delay();
				}
				break;
			case "slideshow_slide_save":
				$id = $_POST["id"];
				$Load = $_POST["load"]==1 ? true : false;
				
				$status = 0;
				$msg = "";
				
				if( !$Load ){
					$errors = array();
					
					if( !($_POST["id_file"]>0) ) {
						$errors[] = $this->S->L('E\' necessario caricare un\'immagine');
					}
					
					if( count($errors)>0 ){
						$msg = $this->toMsg($errors);
					}else{
						if( $id==0 ){
							$q = "INSERT INTO `{$this->_dp}slider_items` (id_block,id_file) VALUES ({$_POST['id_block']},{$_POST['id_file']})";
							$this->cn->Q($q);
							$id = $this->cn->last_id;
						}
						
						$enable = isset($_POST["enable"]) ? 1 : 0;
						$q = "UPDATE {$this->_dp}slider_items SET
							id_file = {$_POST['id_file']},
							`url` = '" . addslashes($_POST["url"]) ."',
							target_blank = {$_POST['target_blank']},
							`show_from` = " . $this->toDate($_POST["show_from"]) .",
							`show_to` = " . $this->toDate($_POST["show_to"]) .",
							enable = {$enable},
							`order` = {$_POST['order']},
							`showed` = '{$_POST['showed']}'
							
							WHERE id = {$id}
							";
						$this->cn->Q($q);
						
						$status = 1;
					}
				}else{
					$status = 1;
				}
				
				$dati = false;
				if( $Load && $id>0 || $id>0 ){
					$q = "SELECT *
					FROM {$this->_dp}slider_items WHERE id = {$id}";
					$dati = $this->cn->OQ( $q );
				}
				
				$ret = array("status"=>$status,"msg"=>$msg,"fields"=>$dati);
				
				break;
			case "slideshow_slide_list":
				if( $_POST["delete"]==1 ){
					$ids = array();
					foreach($_POST as $k=>$v){
						if( strpos($k,"delete_id")===0 && $v>0){
							$ids[] = $v;
						}
					}
					if( count($ids)>0 ){
						$q = "UPDATE {$this->_dp}slider_items SET deleted = NOW() WHERE id IN (".implode(",",$ids).")";
						$this->cn->Q($q);
						$ret = array("status"=>1);
					}else{
						$ret = array("status"=>0);
					}
				}else{
					$order = $_POST["order"];
					$order_dir = $_POST["order_dir"];
	
					$q = "SELECT
						*
						FROM `{$this->_dp}slider_items` WHERE 1 AND deleted IS NULL";
										
					$q .= " ORDER BY `{$order}` {$order_dir}";
					
					$ris = $this->cn->Q($q,true);
					$n = $this->cn->n;
					
					$list = array();
					foreach($ris as $r){
						$row = array();
						$row[] = "<tr class=\"Clickable\" data-url=\"sezioni/slideshow/slide_scheda?id_block={$r['id_block']}&id={$r['id']}\">";
						$row[] = "<td><div class=\"center\">{$r['id']}</div></td>";
						$pathfile = $this->pathFile($r["id_file"]);
						$row[] = "<td><img src=\"{$pathfile}\" width=\"100\" class=\"img-polaroid\"></td>";
						$row[] = "<td><div class=\"center\">" . ($r["order"]>0 ? $r["order"] : "-") . "</div></td>";
						$attivo = $r["enable"]==1 ? "check.png" : "del.png";
						$row[] = "<td><div class=\"center\"><img src=\"_ext/img/icon/{$attivo}\" width=\"24\"></div></td>";
						$row[] = "<td class=\"noClickable\"><div class=\"center\"><input type=\"checkbox\" name=\"delete_id{$r['id']}\" value=\"{$r['id']}\"></div></td>";
						$row[] = '</tr>';
						
						$list[] = implode("",$row);
					}
					
					
					$ret = array("status"=>1,
						"n"=>$n,
						"list"=>$list,
						"order"=>$order,
						"order_dir"=>$order_dir
					);
				
					self::delay();
				}
				break;
			case "banners_save":
				$id = $_POST["id"];
				$Load = $_POST["load"]==1 ? true : false;
				
				$status = 0;
				$msg = "";
				
				if( !$Load ){
					$errors = array();
					
					if( !($_POST["id_file"]>0) ) {
						$errors[] = $this->S->L('E\' necessario caricare un\'immagine');
					}
					if( $_POST["zone"]=="new"){
						$zone = $_POST["new_zone"];	
					}else{
						$zone = $_POST["zone"];
					}
					if( empty($zone) ){
						$errors[] = $this->S->L('E\' necessario scegliere il box sul sito');
					}
					
					if( count($errors)>0 ){
						$msg = $this->toMsg($errors);
					}else{
						if( $id==0 ){
							$q = "INSERT INTO {$this->_dp}banners (id_file) VALUES ({$_POST['id_file']})";
							$this->cn->Q($q);
							$id = $this->cn->last_id;
						}
						
						$enable = isset($_POST["enable"]) ? 1 : 0;
						$order = $_POST["order"]>0 ? $_POST["order"] : "NULL";
						$q = "UPDATE {$this->_dp}banners SET
							id_file = {$_POST['id_file']},
							`zone` = '" . addslashes($zone) . "',
							`name` = '" . addslashes($_POST["name"]) . "',
							`url` = '" . addslashes($_POST["url"]) ."',
							target_blank = {$_POST[target_blank]},
							`show_from` = " . $this->toDate($_POST["show_from"]) .",
							`show_to` = " . $this->toDate($_POST["show_to"]) .",
							`order` = {$order},
							enable = {$enable}
							
							WHERE id = {$id}
							";
						$this->cn->Q($q);
						
						$q = "DELETE FROM `{$this->_dp}banners_page` WHERE id_banner = {$id}";
						$this->cn->Q($q);
						if( count($_POST["id_pages"])>0 && strtolower($zone)=="background" ){
							foreach($_POST["id_pages"] as $k=>$v){
								$q = "INSERT INTO 	`{$this->_dp}banners_page` (id_banner,id_page) VALUES ({$id},{$v})";
								$this->cn->Q($q);
							}
						}
						
						$status = 1;
					}
				}else{
					$status = 1;
				}
				
				$dati = false;
				if( $Load && $id>0 || $id>0 ){
					$q = "SELECT *
					FROM {$this->_dp}banners WHERE id = {$id}";
					$dati = $this->cn->OQ( $q );
					$q = "SELECT id_page FROM `{$this->_dp}banners_page` WHERE id_banner = {$id}";
					$list = $this->cn->Q($q,true);
					$dati["id_pages"] = array();
					foreach($list as $l){
						$dati["id_pages"][$l["id_page"]] = $l["id_page"];
					}
				}
				
				$ret = array("status"=>$status,"msg"=>$msg,"fields"=>$dati);
				
				break;
			case "banners_list":
				if( $_POST["delete"]==1 ){
					$ids = array();
					foreach($_POST as $k=>$v){
						if( strpos($k,"delete_id")===0 && $v>0){
							$ids[] = $v;
						}
					}
					if( count($ids)>0 ){
						$q = "UPDATE {$this->_dp}banners SET deleted = NOW() WHERE id IN (".implode(",",$ids).")";
						$this->cn->Q($q);
						$ret = array("status"=>1);
					}else{
						$ret = array("status"=>0);
					}
				}else{
					$order = $_POST["order"];
					$order_dir = $_POST["order_dir"];
	
					$q = "SELECT
						*
						FROM {$this->_dp}banners WHERE 1 AND deleted IS NULL";
										
					$q .= " ORDER BY {$order} {$order_dir}";
					
					$ris = $this->cn->Q($q,true);
					$n = $this->cn->n;
					
					$list = array();
					foreach($ris as $r){
						$row = array();
						$row[] = "<tr class=\"Clickable\" data-url=\"sezioni/banners/scheda?id={$r['id']}\">";
						$row[] = "<td><div class=\"center\">{$r['id']}</div></td>";
						$pathfile = $this->pathFile($r["id_file"]);
						$row[] = "<td><img src=\"{$pathfile}\" width=\"100\" class=\"img-polaroid\"></td>";
						$txt = empty($r["name"]) ? "-" : $r["name"];
						$row[] = "<td>{$txt}</td>";
						$attivo = $r["enable"]==1 ? "check.png" : "del.png";
						$row[] = "<td><div class=\"center\"><img src=\"_ext/img/icon/{$attivo}\" width=\"24\"></div></td>";
						$row[] = "<td class=\"noClickable\"><div class=\"center\"><input type=\"checkbox\" name=\"delete_id{$r['id']}\" value=\"{$r['id']}\"></div></td>";
						$row[] = '</tr>';
						
						$list[] = implode("",$row);
					}
					
					
					$ret = array("status"=>1,
						"n"=>$n,
						"list"=>$list,
						"order"=>$order,
						"order_dir"=>$order_dir
					);
				
					self::delay();
				}
				break;
			case "widgets_save":
				$id = $_POST["id"];
				$Load = $_POST["load"]==1 ? true : false;
				
				$status = 0;
				$msg = "";
				
				if( !$Load ){
					$errors = array();
					
					if( strlen($_POST["title"])==0 ){
						$errors[] = $this->S->L('Titolo necessario');
					}
					
					if( count($errors)>0 ){
						$msg = $this->toMsg($errors);
					}else{
						if( $id==0 ){
							$q = "INSERT INTO `{$this->_dp}widgets` () VALUES ()";
							$this->cn->Q($q);
							$id = $this->cn->last_id;
						}
						
						$enable_on = isset($_POST["enable_on"]) ? 1 : 0;
						$id_file = $_POST["id_file"]>0 ? $_POST["id_file"] : "NULL";
						$order = $_POST["order"]>0 ? $_POST["order"] : "NULL";
						$q = "UPDATE `{$this->_dp}widgets` SET
							`title` = '" . addslashes($_POST["title"]) . "',
							`text` = '" . addslashes($_POST["text"]) . "',
							`text_button` = '" . addslashes($_POST["text_button"]) . "',
							`url` = '" . addslashes($_POST["url"]) . "',
							`url_blank` = {$_POST['url_blank']},
							id_file = {$id_file},
							enable_on = {$enable_on}
							
							WHERE id = {$id}
							";
						$this->cn->Q($q);
						
						$status = 1;
					}
				}else{
					$status = 1;
				}
				
				$dati = false;
				if( $Load && $id>0 || $id>0 ){
					$q = "SELECT *
					FROM `{$this->_dp}widgets` WHERE id = {$id}";
					$dati = $this->cn->OQ( $q );
				}
				
				$ret = array("status"=>$status,"msg"=>$msg,"fields"=>$dati,"q"=>$q_update);
				
				break;
			case "widgets_list":
				if( $_POST["delete"]==1 ){
					$ids = array();
					foreach($_POST as $k=>$v){
						if( strpos($k,"delete_id")===0 && $v>0){
							$ids[] = $v;
						}
					}
					if( count($ids)>0 ){
						$q = "UPDATE `{$this->_dp}widgets` SET deleted = NOW() WHERE id IN (".implode(",",$ids).") AND lock_on=0";
						$this->cn->Q($q);
						$ret = array("status"=>1);
					}else{
						$ret = array("status"=>0);
					}
				}else{
					$order = $_POST["order"];
					$order_dir = $_POST["order_dir"];
	
					$q = "SELECT
						*
						FROM `{$this->_dp}widgets` WHERE 1 AND deleted IS NULL";
										
					$q .= " ORDER BY {$order} {$order_dir}";
					
					$ris = $this->cn->Q($q,true);
					$n = $this->cn->n;
					
					$list = array();
					foreach($ris as $r){
						$row = array();
						if( $r["lock_on"]==1 ){
							$row[] = "<tr>";
						}else{
							$row[] = "<tr class=\"Clickable\" data-url=\"sezioni/widgets/scheda?id={$r['id']}\">";
						}
						$row[] = "<td><div class=\"center\">{$r['id']}</div></td>";
						$row[] = "<td><div class=\"left\">{$r['title']}</div></td>";
						if( $r["id_file"]>0 ){
							$pathfile = $this->pathFile($r["id_file"]);
							$row[] = "<td><img src=\"{$pathfile}\" width=\"100\" class=\"img-polaroid\"></td>";
						}else{
							$row[] = "<td>-</td>";	
						}
						$attivo = $r["enable_on"]==1 ? "check.png" : "del.png";
						$row[] = "<td><div class=\"center\"><img src=\"_ext/img/icon/{$attivo}\" width=\"24\"></div></td>";
						if( $r["lock_on"]==1 ){
							$row[] = "<td>&nbsp;</td>";
						}else{
							$row[] = "<td class=\"noClickable\"><div class=\"center\"><input type=\"checkbox\" name=\"delete_id{$r['id']}\" value=\"{$r['id']}\"></div></td>";
						}
						$row[] = '</tr>';
						
						$list[] = implode("",$row);
					}
					
					
					$ret = array("status"=>1,
						"n"=>$n,
						"list"=>$list,
						"order"=>$order,
						"order_dir"=>$order_dir
					);
				
					self::delay();
				}
				break;
			case "prodotti_save":
				$id = $_POST["id"];
				$Load = $_POST["load"]==1 ? true : false;
				
				$status = 0;
				$msg = "";
				
				if( !$Load ){
					$errors = array();
					
					if( empty($_POST["title"]) ){
						$errors[] = $this->S->L('Il campo Titolo è necessario');	
					}
					
					if( count($errors)>0 ){
						$msg = $this->toMsg($errors);
					}else{
						if( $id==0 ){
							$q = "INSERT INTO {$this->_dp}news_promo () VALUES ()";
							$this->cn->Q($q);
							$id = $this->cn->last_id;
						}
						
						$q = "UPDATE {$this->_dp}news_promo SET
							`title` = '".addslashes($_POST["title"])."',
							description_short = '".addslashes($_POST["description_short"])."',
							description = '".addslashes($_POST["description"])."',
							`date` = " . $this->toDate($_POST["date"],1) .",
							`show_from` = " . $this->toDate($_POST["show_from"],1) .",
							`show_to` = " . $this->toDate($_POST["show_to"],1) ."
							
							WHERE id = {$id}
							";
						$this->cn->Q($q);
						
						$status = 1;
					}
				}else{
					$status = 1;
				}
				
				$dati = false;
				if( $Load && $id>0 || $id>0 ){
					$q = "SELECT *,
					DATE_FORMAT(`date`,'%d/%m/%Y') AS `date`,
					DATE_FORMAT(`show_from`,'%d/%m/%Y') AS `show_from`,
					DATE_FORMAT(`show_to`,'%d/%m/%Y') AS `show_to`
					FROM {$this->_dp}news_promo WHERE id = {$id}";
					$dati = $this->cn->OQ( $q );
				}
				
				$ret = array("status"=>$status,"msg"=>$msg,"fields"=>$dati);
				
				break;
			case "prodotti_list":
				if( $_POST["delete"]==1 ){
					$ids = array();
					foreach($_POST as $k=>$v){
						if( strpos($k,"delete_id")===0 && $v>0){
							$ids[] = $v;
						}
					}
					if( count($ids)>0 ){
						$q = "UPDATE {$this->_dp}news_promo SET deleted = NOW() WHERE id IN (".implode(",",$ids).")";
						$this->cn->Q($q);
						$ret = array("status"=>1);
					}else{
						$ret = array("status"=>0);
					}
				}else{
					$order = $_POST["order"];
					$order_dir = $_POST["order_dir"];
	
					$q = "SELECT
						*,
						IF( `date`=0,'-',
							DATE_FORMAT(`date`,'%d/%m/%Y')
						) AS date_txt
						FROM {$this->_dp}news_promo WHERE 1 AND deleted IS NULL";
										
					$q .= " ORDER BY {$order} {$order_dir}";
					
					$ris = $this->cn->Q($q,true);
					$n = $this->cn->n;
					
					$list = array();
					foreach($ris as $r){
						$row = array();
						$row[] = "<tr class=\"Clickable\" data-url=\"sezioni/news_promo/scheda?id={$r['id']}\">";
						$row[] = "<td><div class=\"center\">{$r['id']}</div></td>";
						$row[] = "<td>{$r['title']}</td>";
						$row[] = "<td>{$r['date_txt']}</td>";
						$row[] = "<td class=\"noClickable\"><div class=\"center\"><input type=\"checkbox\" name=\"delete_id{$r['id']}\" value=\"{$r['id']}\"></div></td>";
						$row[] = '</tr>';
						
						$list[] = implode("",$row);
					}
					
					
					$ret = array("status"=>1,
						"n"=>$n,
						"list"=>$list,
						"order"=>$order,
						"order_dir"=>$order_dir
					);
				
					self::delay();
				}
				break;
			case "lingue_save":
				//Verifica dei dati
				foreach($_POST as $k=>$v){
					foreach($v as $k2=>$v2){
						$_POST[$k][$k2] = trim($v2);
					}
				}
				
				$errors = array();
				foreach($_POST["name"] as $k=>$v){
					if($k>0){
						if( empty($_POST["name"][$k]) ){
							$errors[] = 'Campo nome necessario';
						}
						if( strlen($_POST["iso"][$k])!=5 ){
							$errors[] = 'Il campo ISO deve essere di 5 caratteri e nel formato xx_XX (es. it_IT)';
						}
						if( empty($_POST["url"][$k]) ){
							$errors[] = 'Campo url necessario';
						}
						
					}
					$q = "SELECT * FROM {$this->_dp}langs WHERE `url`='{$_POST['url']}'";
					$q .= $k>0 ? " AND id!={$k}" : "";
					$this->cn->Q($q);
					if( $this->cn->n>0 ){
						$errors[] = 'L\'url deve essere univoco per lingua';
					}
				}
				$status = count($errors)==0 ? 1 : 0;
				
				if( $status ){				
					foreach($_POST["name"] as $k=>$v){	
						$is_default = $_POST["is_default"]==$k ? 1 : 0;
						$rtl = $_POST["rtl"][$k]==1 ? 1 : 0;
						
						//$k==0 indica che è una lingua nuova
						if($k==0 && !empty($_POST["name"][$k]) && strlen($_POST["iso"][$k])==5){
							$q = "INSERT INTO {$this->_dp}langs (url,iso,is_default,rtl,`name`,`status`) VALUES ('{$_POST['url'][$k]}','{$_POST['iso'][$k]}',{$is_default},{$rtl},'{$_POST['name'][$k]}','{$_POST['status'][$k]}')";
							$this->cn->Q($q);
						}else if($k>0){
							$q = "UPDATE {$this->_dp}langs SET
								url = '{$_POST['url'][$k]}',
								iso = '{$_POST['iso'][$k]}',
								is_default = {$is_default},
								rtl = {$rtl},
								`name` = '{$_POST['name'][$k]}',
								`status` = '{$_POST['status'][$k]}'
								WHERE id = {$k}";
							$this->cn->Q($q);
						}
					}
				}else{
					$msg = "<ul>";
					foreach($errors	as $e){
						$msg .= "<li>{$e}</li>";
					}
					$msg .= "</ul>";
				}
				$ret = array("status"=>$status,"q"=>$q,"msg"=>$msg);
				break;
			case "configurazione_save":
				$Load = $_POST["load"]==1 ? true : false;
				
				if( !$Load ){
					$arr = $_POST;
					if( !isset($arr["pp_responsive_on"]) ){
						$arr["pp_responsive_on"] = 0;
					}
					if( !isset($arr["pp_cache_pagine"]) ){
						$arr["pp_cache_pagine"] = 0;
					}
					if( !isset($arr["pp_cache_jscss"]) ){
						$arr["pp_cache_jscss"] = 0;
					}
					
					foreach($arr as $k=>$v){
						if( strpos($k,"pp_")===0 ){
							$k = substr($k,3);
							$q = "SELECT * FROM {$this->_dp}parameters WHERE `k` = '{$k}'";
							$x = $this->cn->OQ($q);
							if( $x==-1 ){
								$q = "INSERT INTO {$this->_dp}parameters (`k`) VALUES ('{$k}')";
								$this->cn->Q($q);
							}
							$q = "UPDATE {$this->_dp}parameters SET `val` = '{$v}' WHERE `k` = '{$k}'";
							$this->cn->Q($q);
						}
					}
					
					//Checkbox
					$checkboxes = array("cache_pagine","cache_jscss");
					foreach($checkboxes as $c){
						$v = isset($_POST["pp_".$c]) ? 1 : 0;
						$q = "SELECT * FROM {$this->_dp}parameters WHERE `k` = '{$c}'";
						$x = $this->cn->OQ($q);
						if( $x==-1 ){
							$q = "INSERT INTO {$this->_dp}parameters (`k`) VALUES ('{$c}')";
							$this->cn->Q($q);
						}
						$q = "UPDATE {$this->_dp}parameters SET `val` = '{$v}' WHERE `k` = '{$c}'";
						$this->cn->Q($q);
					}
				}
				
				$dati = false;
				$q = "SELECT * FROM {$this->_dp}parameters WHERE 1";
				$list = $this->cn->Q($q,true);
				foreach($list as $v){
					$dati["pp_{$v['k']}"] = utf8_encode($v["val"]);
				}

				$ret = array("status"=>1,"fields"=>$dati);
				break;
			case "esporta_transazioni":
			case "esporta_transazioni_frontend":
				setlocale(LC_TIME,"it_IT");
				
				$dal = $_POST["dal"];
				if( empty($dal) ){
					$dal = time();
				}else{
					$dal = strtotime( str_replace("/","-",$dal) );	
				}
				$al = $_POST["al"];
				if( empty($al) ){
					$al = time();
				}else{
					$al = strtotime( str_replace("/","-",$al) );	
				}
				
				if( $dal > $al ){
					$al = $dal;
				}
				
				$id_negozio = $_POST["id_negozio"];
				$tipo = $_POST["tipo"];
				$q = "SELECT
					o.id AS id_ordine, n.nome AS negozio, o.nome, o.icard, o.card_number, o.timestamp AS data_ordine,
					c.punti_caricati, c.id_prodotto, c.qta, c.prezzo
					
					FROM ordini o INNER JOIN carrello c ON o.id = c.id_ordine
					INNER JOIN negozi n ON n.id_admin_cassa = c.id_admin_cassa
					WHERE 1";
				
				$q .= " AND '" . date("Y-m-d 00:00:00", $dal) ."' <= o.`timestamp` AND o.`timestamp` <= '" . date("Y-m-d 23:59:59", $al) ."'";
				
				if( $id_negozio>0 ){
					$q .= " AND n.id = {$id_negozio}";
				}
				switch($tipo){
					case "carico":
						$q .= " AND c.is_premio=0 AND id_prodotto>0";
						break;
					case "richiesta_premi":
						$q .= " AND c.is_premio=1";
						break;
					case "punti_bonus":
						$q .= " AND c.is_premio=0 AND id_prodotto IS NULL AND punti_premio>0";
						break;
				}
				
				$q .= " ORDER BY negozio ASC, data_ordine ASC";
				
				$this->cn->Q($q);
				$n = $this->cn->n;
				
				$ret = array(
					"status"=>1,
					"n"=>$n,
					"dal"=>date("d/m/Y", $dal),
					"al"=>date("d/m/Y",$al),
					"qclear"=>$q,
					"q"=>rawurlencode( $q ) //si passa la query generata all'iframe per la generazione del file xls o csv
				);
				
				break;
			case "translate_save":
				$lang = $_POST["lang"];
				foreach($_POST as $k=>$v){
					list($a,$iRow) = explode("-",$k);
					if($a=="idlabel"){
						$q = "UPDATE `{$this->_dp}labels` SET
							`label` = '" . addslashes( $_POST["label-" . $iRow] ) . "'
							WHERE id = {$v}";
						$this->cn->Q($q);
					}
				}
				
				$ret = array("status"=>1,"msg"=>"Dati salvati","fields"=>"");
				
				break;
			case "negozio_save":
				$id = $_POST["id"];
				$Load = $_POST["load"]==1 ? true : false;
				
				$status = 0;
				$msg = "";
				
				if( !$Load ){
					$errors = array();
					
					if( count($errors)>0 ){
						$msg = $this->toMsg($errors);
					}else{
						if( $id==0 ){
							$q = "INSERT INTO {$this->_dp}stores () VALUES ()";
							$this->cn->Q($q);
							$id = $this->cn->last_id;
						}
						
						$enable = isset($_POST["enable"]) ? 1 : 0;
						$q = "UPDATE `{$this->_dp}stores` SET
							enable = {$enable},
							html = '".addslashes($_POST["html"])."',
							pointValue = '{$_POST['pointValue']}',
							moneyValue = '{$_POST['moneyValue']}',
							terminal_username = '".addslashes($_POST["terminal_username"])."',
							terminal_password = '".addslashes($_POST["terminal_password"])."',
							terminal_sn = '".addslashes($_POST["terminal_sn"])."',
							note_internal = '".addslashes($_POST["note_internal"])."'
							
							WHERE id = {$id}
							";
						$this->cn->Q($q);
						
						$q = "DELETE FROM `{$this->_dp}stores_hours` WHERE id_store = {$id}";
						$this->cn->Q($q);
						for($i=1;$i<=7;$i++){
							if( strlen($_POST["timetable{$i}"])>0 ){
								$q = "INSERT INTO {$this->_dp}stores_hours (id_store,`day`,`timetable`) VALUES ({$id},{$i},'" . addslashes($_POST["timetable{$i}"]) . "')";
								$this->cn->Q($q);
							}
						}
						
						//IMMAGINI
						$id_images = array();
						foreach($_POST as $k=>$v){
							if( strpos($k,"id_file")===0 && $v>0){
								$q = "SELECT * FROM `{$this->_dp}stores_gallery` WHERE id_file = {$v}";
								$this->cn->Q($q);
								$id_images[] = $v;
								if( $this->cn->n==0 ){
									$q = "SELECT `order` FROM `{$this->_dp}stores_gallery` WHERE id_store = {$id} ORDER BY `order` DESC LIMIT 1";
									$order = $this->cn->OF($q);
									$order = $order>0 ? $order+1 : 1;
									
									$q = "INSERT INTO `{$this->_dp}stores_gallery` (id_store,id_file,`order`) VALUES ({$id},{$v},{$order})";
									$this->cn->Q($q);
								}
							}
						}
						//CERCA IMMAGINI DA CANCELLARE
						$q = "DELETE FROM `{$this->_dp}stores_gallery` WHERE id_store = {$id}";
						if( count($id_images)>0 ){
							$q .= " AND id_file NOT IN (" . implode(",",$id_images) . ")";
						}
						$this->cn->Q($q);
						
						$status = 1;
					}
				}else{
					$status = 1;
				}
				
				$dati = false;
				if( $Load && $id>0 || $id>0 ){
					$q = "SELECT * FROM {$this->_dp}stores WHERE id = {$id}";
					$dati = $this->cn->OQ( $q );
					$q = "SELECT * FROM {$this->_dp}stores_hours WHERE id_store = {$id} ORDER BY `day` ASC";
					$hours = $this->cn->Q($q,true);
					foreach($hours as $v){
						$dati["timetable" . $v["day"] ] = $v["timetable"];	
					}
					$q = "SELECT id_file FROM {$this->_dp}stores_gallery WHERE id_store = {$id} ORDER BY `order` ASC";
					$files = $this->cn->Q($q,true);
					$index=0;
					foreach($files as $f){
						$dati["id_file" . $index] = $f["id_file"];
						$index++;	
					}
				}
				
				$ret = array("status"=>$status,"msg"=>$msg,"fields"=>$dati);
				
				break;
			case "negozi_list":
				if( $_POST["delete"]==1 ){
					$ids = array();
					foreach($_POST as $k=>$v){
						if( strpos($k,"delete_id")===0 && $v>0){
							$ids[] = $v;
						}
					}
					if( count($ids)>0 ){
						$q = "UPDATE {$this->_dp}stores SET deleted = NOW() WHERE id IN (".implode(",",$ids).")";
						$this->cn->Q($q);
						$ret = array("status"=>1);
					}else{
						$ret = array("status"=>0);
					}
				}else{
					$order = $_POST["order"];
					$order_dir = $_POST["order_dir"];
	
					$q = "SELECT * FROM {$this->_dp}stores WHERE 1 AND deleted IS NULL";
										
					$q .= " ORDER BY {$order} {$order_dir}";
					
					$ris = $this->cn->Q($q,true);
					$n = $this->cn->n;
					
					$list = array();
					foreach($ris as $r){
						$row = array();
						$row[] = "<tr class=\"Clickable\" data-url=\"sezioni/negozi/scheda?id={$r['id']}\">";
						$row[] = "<td><div class=\"center\">{$r['id']}</div></td>";
						$row[] = "<td>{$r['fn_description']}<br>({$r['fn_companyName']})</td>";
						$attivo = $r["enable"]==1 ? "check.png" : "del.png";
						$row[] = "<td><div class=\"center\"><img src=\"_ext/img/icon/{$attivo}\" width=\"24\"></div></td>";
						//$row[] = "<td><div class=\"left\">{$r['email']}</div></td>";
						//$row[] = "<td class=\"noClickable\"><div class=\"center\"><input type=\"checkbox\" name=\"delete_id{$r['id']}\" value=\"{$r['id']}\"></div></td>";
						$row[] = '</tr>';
						
						$list[] = implode("",$row);
					}
					
					
					$ret = array("status"=>1,
						"n"=>$n,
						"list"=>$list,
						"order"=>$order,
						"order_dir"=>$order_dir
					);
				
					self::delay();
				}
				break;
			case "polls_save":
				$id = $_POST["id"];
				$Load = $_POST["load"]==1 ? true : false;
				
				$status = 0;
				$msg = "";
				
				if( !$Load ){
					$errors = array();
					
					if( empty($_POST["question"]) ){
						$errors[] = "Il campo Domanda è necessario";
					}
					if( strlen($_POST["from"])==0 ){
						$errors[] = "Inserire un range di data valido";
					}
					if( strlen($_POST["to"])==0 ){
						$errors[] = "Inserire un range di data valido";
					}
					if( empty($_POST["answer0"]) ){
						$errors[] = "La risposta 1 è necessaria";
					}
					if( empty($_POST["answer1"]) ){
						$errors[] = "La risposta 2 è necessaria";
					}
					
					if( count($errors)>0 ){
						$msg = $this->toMsg($errors);
					}else{
						if( $id==0 ){
							$q = "INSERT INTO {$this->_dp}polls () VALUES ()";
							$this->cn->Q($q);
							$id = $this->cn->last_id;
						}
						
						$enable_on = isset($_POST["enable_on"]) ? 1 : 0;
						$from = strtotime( str_replace("/","-",$_POST["from"]) );
						$to = strtotime( str_replace("/","-",$_POST["to"]) );
						$q = "UPDATE {$this->_dp}polls SET
							question = '" . addslashes($_POST["question"]) . "',
							`description` = '" . addslashes($_POST["description"]) . "',
							enable_on = {$enable_on},
							`from` = '" . date("Y-m-d", $from) . "',
							`to` = '" . date("Y-m-d", $to) . "'
							
							WHERE id = {$id}
							";
						$this->cn->Q($q);
						
						for($i=0;$i<$_POST["n_answers"];$i++){
							$q = "SELECT * FROM `{$this->_dp}polls_answers` WHERE id_poll = {$id} AND `order`={$i} LIMIT 1";
							$x = $this->cn->OQ($q);
							if( $x==-1 ){
								if( strlen($_POST["answer".$i])>0 ){
									$q = "INSERT INTO `{$this->_dp}polls_answers` (id_poll) VALUES ({$id})";	
									$this->cn->Q($q);
									$id_answer = $this->cn->last_id;
								}else{
									$id_answer = false;
								}
							}else{
								$id_answer = $x["id"];
							}
							
							if( $id_answer ){							
								$q = "UPDATE `{$this->_dp}polls_answers` SET
									`answer` = '".addslashes($_POST["answer".$i])."',
									`order` = {$i}
									WHERE id = {$id_answer}";
								$this->cn->Q($q);
							}
						}
						$status = 1;
					}
				}else{
					$status = 1;
				}
				
				$dati = false;
				if( $Load && $id>0 || $id>0 ){
					$q = "SELECT * FROM {$this->_dp}polls WHERE id = {$id}";
					$dati = $this->cn->OQ( $q );
					$dati["from"] = date("d/m/Y", strtotime($dati["from"]) );
					$dati["to"] = date("d/m/Y", strtotime($dati["to"]) );
					
					$q = "SELECT * FROM {$this->_dp}polls_answers WHERE id_poll = {$id} ORDER BY `order` ASC";
					$answers = $this->cn->Q($q,true);
					$tot_polls = 0;
					foreach($answers as $v){
						$dati["answer" . $v["order"] ] = $v["answer"];
						//Calcola numero voti
						$q = "SELECT COUNT(DISTINCT id_user) AS n FROM `{$this->_dp}polls_answers_rate` WHERE id_answer = {$v['id']}";
						$n_polls = $this->cn->OF($q);
						$dati["answer" . $v["order"] . "_polls"] = $n_polls>0 ? "(Voti: {$n_polls})" : '';
						$tot_polls += $n_polls;
					}
					$dati["tot_polls"] = $tot_polls>0 ? $tot_polls : '';
				}
				
				$ret = array("status"=>$status,"msg"=>$msg,"fields"=>$dati);
				
				break;
			case "polls_list":
				if( $_POST["delete"]==1 ){
					$ids = array();
					foreach($_POST as $k=>$v){
						if( strpos($k,"delete_id")===0 && $v>0){
							$ids[] = $v;
						}
					}
					if( count($ids)>0 ){
						$q = "UPDATE `{$this->_dp}polls` SET deleted = NOW() WHERE id IN (".implode(",",$ids).")";
						$this->cn->Q($q);
						$ret = array("status"=>1);
					}else{
						$ret = array("status"=>0);
					}
				}else{
					$order = $_POST["order"];
					$order_dir = $_POST["order_dir"];
	
					$q = "SELECT *, DATE_FORMAT(`from`, '%d-%m-%Y') AS from_text, DATE_FORMAT(`to`, '%d-%m-%Y') AS to_text FROM `{$this->_dp}polls` WHERE 1 AND deleted IS NULL";
										
					$q .= " ORDER BY {$order} {$order_dir}";
					
					$ris = $this->cn->Q($q,true);
					$n = $this->cn->n;
					
					$list = array();
					foreach($ris as $r){
						$row = array();
						$row[] = "<tr class=\"Clickable\" data-url=\"sezioni/sondaggi/scheda?id={$r['id']}\">";
						$row[] = "<td><div class=\"center\">{$r['id']}</div></td>";
						$row[] = "<td>{$r['question']}</td>";
						$row[] = "<td><div class=\"center\">{$r['from_text']}</div></td>";
						$row[] = "<td><div class=\"center\">{$r['to_text']}</div></td>";
						$attivo = $r["enable_on"]==1 ? "check.png" : "del.png";
						$row[] = "<td><div class=\"center\"><img src=\"_ext/img/icon/{$attivo}\" width=\"24\"></div></td>";
						$row[] = "<td class=\"noClickable\"><div class=\"center\"><input type=\"checkbox\" name=\"delete_id{$r['id']}\" value=\"{$r['id']}\"></div></td>";
						$row[] = '</tr>';
						
						$list[] = implode("",$row);
					}
					
					
					$ret = array("status"=>1,
						"n"=>$n,
						"list"=>$list,
						"order"=>$order,
						"order_dir"=>$order_dir
					);
				
					self::delay();
				}
				break;
			case "users_gallery_save":
				$id = $_POST["id"];
				$Load = $_POST["load"]==1 ? true : false;
				
				$status = 0;
				$msg = "";
				
				if( !$Load ){
					$errors = array();
					
					if( count($errors)>0 ){
						$msg = $this->toMsg($errors);
					}else{
						$approved = $_POST["approved"]>0 ? "NOW()" : "NULL";
						$q = "UPDATE {$this->_dp}users_gallery SET
							`approved` = {$approved}
							
							WHERE id = {$id}
							";
						$this->cn->Q($q);
						
						$status = 1;
					}
				}else{
					$status = 1;
				}
				
				$dati = false;
				
				$ret = array("status"=>$status,"msg"=>$msg,"fields"=>$dati);
				
				break;
			case "users_gallery_list":
				if( $_POST["delete"]==1 ){
					/*$ids = array();
					foreach($_POST as $k=>$v){
						if( strpos($k,"delete_id")===0 && $v>0){
							$ids[] = $v;
						}
					}
					if( count($ids)>0 ){
						$q = "UPDATE {$this->_dp}news_promo SET deleted = NOW() WHERE id IN (".implode(",",$ids).")";
						$this->cn->Q($q);
						$ret = array("status"=>1);
					}else{
						$ret = array("status"=>0);
					}*/
				}else{
					$order = $_POST["order"];
					$order_dir = $_POST["order_dir"];
	
					$q = "SELECT
						*
						FROM `{$this->_dp}users_gallery` WHERE 1";
					
					switch($_POST["s"]){
						case "new":
							$q .= " AND new=1";
							break;
						case "approvati":
							$q .= " AND approved IS NOT NULL";
							break;
						case "respinti":
							$q .= " AND approved IS NULL AND new=0";
							break;
					}					
										
					$q .= " ORDER BY {$order} {$order_dir}";
					
					$ris = $this->cn->Q($q,true);
					$n = $this->cn->n;
					
					$list = array();
					foreach($ris as $r){
						$row = array();
						$row[] = "<tr class=\"Clickable\" data-url=\"sezioni/users_gallery/scheda?id={$r['id']}\">";
						$row[] = "<td><div class=\"center\">{$r['id']}</div></td>";
						
						$q = "SELECT * FROM `{$this->_dp}file` WHERE id = {$r['id_file']}";
						$F = $this->cn->OQ($q);
						$filepath = "../../../_public/file/" . substr($r["id_file"],0,1) . "/{$r['id_file']}.{$F['ext']}";
						$row[] = "<td><div class=\"left\"><img src=\"{$filepath}\" style=\"height: 80px;\"></div></td>";
						
						if( !empty($r["approved"]) ){
							$status = "Approvata";
						}else if($r["new"]==1){
							$status = "Da visionare";
						}else{
							$status = "Rifiutata";
						}
						
						$row[] = "<td>{$status}</td>";
						//$row[] = "<td class=\"noClickable\"><div class=\"center\"><input type=\"checkbox\" name=\"delete_id{$r['id']}\" value=\"{$r['id']}\"></div></td>";
						$row[] = '</tr>';
						
						$list[] = implode("",$row);
					}
					
					
					$ret = array("status"=>1,
						"n"=>$n,
						"list"=>$list,
						"order"=>$order,
						"order_dir"=>$order_dir
					);
				
					self::delay();
				}
				break;
			case "downloads_save":
				$id = $_POST["id"];
				$Load = $_POST["load"]==1 ? true : false;
				
				$status = 0;
				$msg = "";
				$type = $_POST["type"];
				
				if( !$Load ){
					$errors = array();
					
					if( empty($_POST["title"]) ){
						//$errors[] = $this->S->L('Il campo Titolo è necessario');	
					}
					
					if( count($errors)>0 ){
						$msg = $this->toMsg($errors);
					}else{
						if( $id==0 ){
							$q = "INSERT INTO {$this->_dp}downloads () VALUES ()";
							$this->cn->Q($q);
							$id = $this->cn->last_id;
						}
						
						$id_file = $_POST["id_file"]>0 ? $_POST["id_file"] : "NULL";
						$id_file_pdf = $_POST["id_file_pdf"]>0 ? $_POST["id_file_pdf"] : "NULL";
						
						$option = '';
						if( in_array($type,array("cori","suonerie")) ){
							$q = "SELECT * FROM `{$this->dp}files` WHERE id = {$id_file}";
							$x = $this->cn->OQ($q);
							$option = "{$_POST['tipo']}|{$x['name']}";
						}else if( in_array($type,array("videopremium")) ){
							$option = $_POST["option"];
						}
						
						$order = $_POST["order"]>0 ? $_POST["order"] : "NULL";
						$q = "UPDATE {$this->_dp}downloads SET
							`id_file` = {$id_file},
							`id_file_pdf` = {$id_file_pdf},
							`label` = '".addslashes($_POST["label"])."',
							`type` = '{$_POST['type']}',
							`option` = '" . addslashes($option) . "',
							`order` = {$order}
							
							WHERE id = {$id}
							";
						$this->cn->Q($q);
						
						$status = 1;
					}
				}else{
					$status = 1;
				}
				
				$dati = false;
				if( $Load && $id>0 || $id>0 ){
					$q = "SELECT *
					FROM `{$this->_dp}downloads` WHERE id = {$id}";
					$dati = $this->cn->OQ( $q );
				}
				
				$ret = array("status"=>$status,"msg"=>$msg,"fields"=>$dati);
				
				break;
			case "downloads_wallpapers_save":
				$id = $_POST["id"];
				$Load = $_POST["load"]==1 ? true : false;
				
				$status = 0;
				$msg = "";
				$type = $_POST["type"];
				
				if( !$Load ){
					$errors = array();
					
					if( empty($_POST["title"]) ){
						//$errors[] = $this->S->L('Il campo Titolo è necessario');	
					}
					for($flag=false,$i=0;$i<4;$i++){
						if(!($_POST["id_file{$i}"]>0)){
							$flag = true;	
						}
					}
					if($flag){
						$errors[] = $this->S->L('Sono necessarie tutte le risoluzioni');
					}
					
					if( count($errors)>0 ){
						$msg = $this->toMsg($errors);
					}else{
						$ids = array();
						if( $id==0 ){
							//Trova l'ultimo gruppo
							$q = "SELECT CONVERT(`group`, UNSIGNED INTEGER) AS group_number FROM `{$this->_dp}downloads` WHERE type = 'wallpapers' ORDER BY `group_number` DESC LIMIT 1";
							$group = $this->cn->OF($q);
							$group++;
							$res = array("1024x768","1280x1050","1680x1050","1920x1200");
							foreach($res as $r){
								$q = "INSERT INTO `{$this->_dp}downloads` (`type`,`option`,`group`) VALUES ('wallpapers','{$r}','{$group}')";
								$this->cn->Q($q);
								$ids[] = $this->cn->last_id;
							}
						}else{
							$q = "SELECT `group` FROM `{$this->_dp}downloads` WHERE id={$id}";
							$group = $this->cn->OF($q);
							$q = "SELECT id FROM `{$this->_dp}downloads` WHERE type='wallpapers' AND `group`='{$group}' ORDER BY id ASC";
							$list = $this->cn->Q($q,true);
							foreach($list as $l){
								$ids[] = $l["id"];
							}
						}
						
						$order = $_POST["order"]>0 ? $_POST["order"] : "NULL";
						for($i=0;$i<count($ids);$i++){
							$id_file = $_POST["id_file{$i}"]>0 ? $_POST["id_file{$i}"] : "NULL";
							$q = "UPDATE `{$this->_dp}downloads` SET
								`id_file` = {$id_file},
								`label` = '".addslashes($_POST["label"])."',
								`order` = {$order}
								
								WHERE id = {$ids[$i]}
								";
							$this->cn->Q($q);	
						}
						
						$status = 1;
					}
				}else{
					$status = 1;
				}
				
				$dati = false;
				if( $Load && $id>0 || $id>0 ){
					$q = "SELECT *, id_file AS id_file0
					FROM `{$this->_dp}downloads` WHERE id = {$id}";
					$dati = $this->cn->OQ( $q );
					$q = "SELECT id_file AS id_file{$i} FROM `{$this->_dp}downloads` WHERE type='wallpapers' AND `group` = '{$dati['group']}' AND id!={$dati['id']} ORDER BY id ASC";
					$list = $this->cn->Q($q,true);
					for($i=1;$i<4;$i++){
						$dati["id_file{$i}"] = $list[$i-1]["id_file"];
					}
					
				}
				
				$ret = array("status"=>$status,"msg"=>$msg,"fields"=>$dati);
				
				break;
			case "downloads_list":
				if( $_POST["delete"]==1 ){
					$ids = array();
					foreach($_POST as $k=>$v){
						if( strpos($k,"delete_id")===0 && $v>0){
							$ids[] = $v;
						}
					}
					if( count($ids)>0 ){
						$q = "UPDATE `{$this->_dp}downloads` SET deleted = NOW() WHERE id IN (".implode(",",$ids).")";
						$this->cn->Q($q);
						$ret = array("status"=>1);
					}else{
						$ret = array("status"=>0);
					}
				}else{
					$order = $_POST["order"];
					$order_dir = $_POST["order_dir"];
	
					$q = "SELECT
						*
						FROM {$this->_dp}downloads WHERE 1 AND deleted IS NULL";
					
					if( empty($_POST["type"]) ){
						$q .= " AND `type`!='wallpapers'";
					}else if($_POST["type"]=='wallpapers'){
						$q .= " AND `type` = 'wallpapers' AND `option`='1024x768'";	
					}else{
						$q .= " AND `type` = '{$_POST['type']}'";	
					}
										
					$q .= " ORDER BY {$order} {$order_dir}";
					
					$ris = $this->cn->Q($q,true);
					$n = $this->cn->n;
					
					$list = array();
					foreach($ris as $r){
						$row = array();
						$final_url = $r["type"]=="wallpapers" ? 'scheda_wallpaper' : 'scheda_download';
						$row[] = "<tr class=\"Clickable\" data-url=\"sezioni/area_download/{$final_url}?id={$r['id']}\">";
						$row[] = "<td><div class=\"center\">{$r['id']}</div></td>";
						
						$img = '';
						if( !in_array($r["type"],array("cori","suonerie")) && $r["id_file"]>0 ){
							$q = "SELECT * FROM `{$this->_dp}file` WHERE id = {$r['id_file']}";
							$F = $this->cn->OQ($q);
							$filepath = "../../../img/" . substr($r["id_file"],0,1) . "-" . md5($r["id_file"]) . "." . $r["ext"] . "/w_100/h_100/m_square/bg_fff/f_file/" . substr($r["id_file"],0,1) . "/{$r['id_file']}.{$F['ext']}";
							$img = "<img src=\"{$filepath}\" style=\"height: 80px;\">";
						}
						
						$row[] = "<td><div class=\"left\">{$img}</div></td>";
						
						
						$row[] = "<td>{$r['label']}</td>";
						$row[] = "<td>{$r['type']}</td>";
						$row[] = "<td class=\"noClickable\"><div class=\"center\"><input type=\"checkbox\" name=\"delete_id{$r['id']}\" value=\"{$r['id']}\"></div></td>";
						$row[] = '</tr>';
						
						$list[] = implode("",$row);
					}
					
					$ret = array("status"=>1,
						"n"=>$n,
						"list"=>$list,
						"order"=>$order,
						"order_dir"=>$order_dir
					);
				
					self::delay();
				}
				break;
			case "video_ads_save":
				$id = $_POST["id"];
				$Load = $_POST["load"]==1 ? true : false;
				
				$status = 0;
				$msg = "";
				
				if( !$Load ){
					$errors = array();
					
					if( $_POST['type']=='image' ){
						if( !($_POST["id_file"]>0) ) {
							$errors[] = $this->S->L('E\' necessario caricare un\'immagine');
						}else{
							$id_image = $_POST['id_file'];	
						}
						$ext_code = '';
					}else{
						$id_image = "NULL";	
						$_POST['timing'] = $_POST['timing']==0 ? 30 : $_POST['timing'];
						if( empty($_POST['ext_code']) ){
							$errors[] = $this->S->L('Inserire il codice di embed');	
						}else{
							$ext_code = addslashes($_POST['ext_code']);
							$ext_code = str_replace('width:640px' , 'width:766px', $ext_code);
							$ext_code = str_replace('height:480px' , 'height:432px', $ext_code);;
							$ext_code = str_replace('noSkin: false' , 'noSkin: true', $ext_code);
							$ext_code = str_replace('autoPlay: false' , 'autoPlay: true', $ext_code);
							/*
							4ME Code:
							<script type="text/javascript" src="//acffiorentina-4me.weebo.it/static/player/4x/scripts/embedscript-min.js"></script>
<div id="ekasi" style="width:640px; height:480px"></div>
<script>//<![CDATA[
var params={trackingInfo: {},
embedCodeId: "SpILAs",
displayLinked: "open",
displayDownload: "open",
noSkin: false,
autoPlay: false};
embedVideo("//acffiorentina-view.4me.it/api/xcontents/resources/delivery/getContentDetail?clientId=acffiorentina&xcontentId=534159b7-8321-4f04-9288-ed2badb64ba8&pkey=zk3ecL","ekasi",params, ["UA-44338784-21","UA-5075316-3"]);
//]]></script>
						*/							
						}
					}
					
					if( empty($_POST["section"]) ){
						$errors[] = $this->S->L('E\' necessario scegliere il box sul sito');
					}
					
					if( count($errors)>0 ){
						$msg = $this->toMsg($errors);
					}else{
						if( $id==0 ){
							$q = "INSERT INTO `{$this->_dp}video_ads` () VALUES ()";
							$this->cn->Q($q);
							$id = $this->cn->last_id;
						}
						
						$order = $_POST["order"]>0 ? $_POST["order"] : "NULL";
						
						$timing = intval( $_POST['timing_sec'] );
						$timing += intval( $_POST['timing_min'] )*60;
						
						$q = "UPDATE `{$this->_dp}video_ads` SET
							`title` = '" . addslashes($_POST["title"]) . "',
							id_file = {$id_image},
							`section` = '" . addslashes($_POST["section"]) . "',
							`type` = '{$_POST['type']}',
							`ext_code` = '{$ext_code}',
							`url` = '" . addslashes($_POST["url"]) ."',
							`blank` = {$_POST['blank']},
							`timing` = {$timing},
							`show_from` = " . $this->toDate($_POST["show_from"]) .",
							`show_to` = " . $this->toDate($_POST["show_to"]) ."
							
							WHERE id = {$id}
							";
						$this->cn->Q($q);
						
						$status = 1;
					}
				}else{
					$status = 1;
				}
				
				$dati = false;
				if( $Load && $id>0 || $id>0 ){
					$q = "SELECT *
					FROM `{$this->_dp}video_ads` WHERE id = {$id}";
					$dati = $this->cn->OQ( $q );
					
					$dati["timing_min"] = intval( $dati["timing"] / 60 );
					$dati["timing_sec"] = $dati["timing"] % 60;
				}
				
				$ret = array("status"=>$status,"msg"=>$msg,"fields"=>$dati);
				
				break;
			case "video_ads_list":
				if( $_POST["delete"]==1 ){
					$ids = array();
					foreach($_POST as $k=>$v){
						if( strpos($k,"delete_id")===0 && $v>0){
							$ids[] = $v;
						}
					}
					if( count($ids)>0 ){
						$q = "UPDATE `{$this->_dp}video_ads` SET deleted = NOW() WHERE id IN (".implode(",",$ids).")";
						$this->cn->Q($q);
						$ret = array("status"=>1);
					}else{
						$ret = array("status"=>0);
					}
				}else{
					$order = $_POST["order"];
					$order_dir = $_POST["order_dir"];
	
					$q = "SELECT
						*
						FROM `{$this->_dp}video_ads` WHERE 1 AND deleted IS NULL";
										
					$q .= " ORDER BY {$order} {$order_dir}";
					
					$ris = $this->cn->Q($q,true);
					$n = $this->cn->n;
					
					$list = array();
					foreach($ris as $r){
						$row = array();
						$row[] = "<tr class=\"Clickable\" data-url=\"sezioni/video_ads/scheda?id={$r['id']}\">";
						$row[] = "<td><div class=\"center\">{$r['id']}</div></td>";
						if($r["id_file"]>0 ){
							$pathfile = $this->pathFile($r["id_file"]);
							$row[] = "<td><img src=\"{$pathfile}\" width=\"100\" class=\"img-polaroid\"></td>";
						}else{
							$row[] = "<td>Video</td>";
						}
						$row[] = "<td><div class=\"left\">{$r['title']}</div></td>";
						$row[] = "<td class=\"noClickable\"><div class=\"center\"><input type=\"checkbox\" name=\"delete_id{$r['id']}\" value=\"{$r['id']}\"></div></td>";
						$row[] = '</tr>';
						
						$list[] = implode("",$row);
					}
					
					
					$ret = array("status"=>1,
						"n"=>$n,
						"list"=>$list,
						"order"=>$order,
						"order_dir"=>$order_dir
					);
				
					self::delay();
				}
				break;
			case "rosa_save":
				$id = $_POST["id"];
				$Load = $_POST["load"]==1 ? true : false;
				
				$status = 0;
				$msg = "";
				
				if( !$Load ){
					$errors = array();
					
					if( empty($_POST["nome"]) ){
						$errors[] = $this->S->L('E&grave; necessario inserire il nome');
					}
					if( empty($_POST["cognome"]) ){
						$errors[] = $this->S->L('E&grave; necessario inserire il cognome');
					}
					
					if( count($errors)>0 ){
						$msg = $this->toMsg($errors);
					}else{
						if( $id==0 ){
							$q = "INSERT INTO `{$this->_dp}gf_rosa` () VALUES ()";
							$this->cn->Q($q);
							$id = $this->cn->last_id;
						}
						
						$id_foto = $_POST["id_foto"]>0 ? $_POST["id_foto"] : "NULL";
						$id_foto_over = $_POST["id_foto_over"]>0 ? $_POST["id_foto_over"] : "NULL";	
						$portiere = isset($_POST['portiere']) ? 1 : 0;
						$attivo = isset($_POST['attivo']) ? 1 : 0;
						$order = $_POST["order"]>0 ? $_POST["order"] : "NULL";
						$q = "UPDATE `{$this->_dp}gf_rosa` SET
							`nome` = '" . addslashes($_POST["nome"]) . "',
							`cognome` = '" . addslashes($_POST["cognome"]) . "',
							id_foto = {$id_foto},
							id_foto_over = {$id_foto_over},
							`numero` = '" . intval($_POST["numero"]) . "',
							portiere = {$portiere},
							attivo = {$attivo}
							
							WHERE id = {$id}
							";
						$this->cn->Q($q);
						
						$status = 1;
					}
				}else{
					$status = 1;
				}
				
				$dati = false;
				if( $Load && $id>0 || $id>0 ){
					$q = "SELECT *
					FROM `{$this->_dp}gf_rosa` WHERE id = {$id}";
					$dati = $this->cn->OQ( $q );
				}
				
				$ret = array("status"=>$status,"msg"=>$msg,"fields"=>$dati);
				self::delay();
				break;
			case "rosa_list":
				if( $_POST["delete"]==1 ){
					$ids = array();
					foreach($_POST as $k=>$v){
						if( strpos($k,"delete_id")===0 && $v>0){
							$ids[] = $v;
						}
					}
					if( count($ids)>0 ){
						$q = "UPDATE `{$this->_dp}gf_rosa` SET deleted = NOW() WHERE id IN (".implode(",",$ids).")";
						$this->cn->Q($q);
						$ret = array("status"=>1);
					}else{
						$ret = array("status"=>0);
					}
				}else{
					$order = $_POST["order"];
					$order_dir = $_POST["order_dir"];
	
					$q = "SELECT
						*
						FROM `{$this->_dp}gf_rosa` WHERE 1";
										
					$q .= " ORDER BY {$order} {$order_dir}";
					
					$ris = $this->cn->Q($q,true);
					$n = $this->cn->n;
					
					$list = array();
					foreach($ris as $r){
						$row = array();
						$row[] = "<tr class=\"Clickable\" data-url=\"sezioni/giochi/rosa_scheda?id={$r['id']}\">";
						$row[] = "<td><div class=\"center\">{$r['id']}</div></td>";
						if($r["id_foto"]>0 ){
							$pathfile = $this->pathFile($r["id_foto"]);
							$row[] = "<td><img src=\"{$pathfile}\" width=\"100\" class=\"img-polaroid\"></td>";
						}else{
							$row[] = "<td>-</td>";
						}
						$row[] = "<td><div class=\"left\">{$r['nome']} {$r['cognome']}</div></td>";
						$attivo = $r["attivo"]==1 ? "check.png" : "del.png";
						$row[] = "<td><div class=\"center\"><img src=\"_ext/img/icon/{$attivo}\" width=\"24\"></div></td>";
						//$row[] = "<td class=\"noClickable\"><div class=\"center\"><input type=\"checkbox\" name=\"delete_id{$r['id']}\" value=\"{$r['id']}\"></div></td>";
						$row[] = '</tr>';
						
						$list[] = implode("",$row);
					}
					
					
					$ret = array("status"=>1,
						"n"=>$n,
						"list"=>$list,
						"order"=>$order,
						"order_dir"=>$order_dir
					);
				
					self::delay();
				}
				break;
			case "stagioni_save":
				$id = $_POST["id"];
				$Load = $_POST["load"]==1 ? true : false;
				
				$status = 0;
				$msg = "";
				
				if( !$Load ){
					$errors = array();
					
					if( empty($_POST["nome"]) ){
						$errors[] = $this->S->L('E&grave; necessario inserire il nome');
					}
					
					if( count($errors)>0 ){
						$msg = $this->toMsg($errors);
					}else{
						if( $id==0 ){
							$q = "INSERT INTO `{$this->_dp}gf_stagioni` () VALUES ()";
							$this->cn->Q($q);
							$id = $this->cn->last_id;
						}
						
						$q = "UPDATE `{$this->_dp}gf_stagioni` SET
							`nome` = '" . addslashes($_POST["nome"]) . "'
							
							WHERE id = {$id}
							";
						$this->cn->Q($q);
						
						$status = 1;
					}
				}else{
					$status = 1;
				}
				
				$dati = false;
				if( $Load && $id>0 || $id>0 ){
					$q = "SELECT *
					FROM `{$this->_dp}gf_stagioni` WHERE id = {$id}";
					$dati = $this->cn->OQ( $q );
				}
				
				$ret = array("status"=>$status,"msg"=>$msg,"fields"=>$dati);
				self::delay();
				break;
			case "stagioni_list":
				if( $_POST["delete"]==1 ){
					$ids = array();
					foreach($_POST as $k=>$v){
						if( strpos($k,"delete_id")===0 && $v>0){
							$ids[] = $v;
						}
					}
					if( count($ids)>0 ){
						$q = "UPDATE `{$this->_dp}gf_stagioni` SET deleted = NOW() WHERE id IN (".implode(",",$ids).")";
						$this->cn->Q($q);
						$ret = array("status"=>1);
					}else{
						$ret = array("status"=>0);
					}
				}else{
					$order = $_POST["order"];
					$order_dir = $_POST["order_dir"];
	
					$q = "SELECT
						*
						FROM `{$this->_dp}gf_stagioni` WHERE 1";
										
					$q .= " ORDER BY {$order} {$order_dir}";
					
					$ris = $this->cn->Q($q,true);
					$n = $this->cn->n;
					
					$list = array();
					foreach($ris as $r){
						$row = array();
						$row[] = "<tr class=\"Clickable\" data-url=\"sezioni/giochi/stagioni_scheda?id={$r['id']}\">";
						$row[] = "<td><div class=\"center\">{$r['id']}</div></td>";
						$row[] = "<td><div class=\"left\">{$r['nome']}</div></td>";
						//$row[] = "<td class=\"noClickable\"><div class=\"center\"><input type=\"checkbox\" name=\"delete_id{$r['id']}\" value=\"{$r['id']}\"></div></td>";
						$row[] = '</tr>';
						
						$list[] = implode("",$row);
					}
					
					
					$ret = array("status"=>1,
						"n"=>$n,
						"list"=>$list,
						"order"=>$order,
						"order_dir"=>$order_dir
					);
				
					self::delay();
				}
				break;
			case "formazione_save":
				set_time_limit(0);
				$id = $_POST["id"];
				$Load = $_POST["load"]==1 ? true : false;
				
				$status = 0;
				$msg = "";
				
				if( !$Load ){
					$errors = array();
					
					if( empty($_POST["data"]) ){
						$errors[] = $this->S->L('E&grave; necessario inserire la data');
					}else{
						list($data,$ora) = explode(" ",$_POST['data']);
						$data = $this->toDate($data);
						$data = str_replace("'","",$data);
						$ora .= ":00";
					}
                    if( !isset($_POST['conferma_salvataggio']) ){
                        $errors[] = $this->S->L('Conferma il salvataggio dei dati per proseguire    ');
                    }
					/*if( empty($_POST["avversario"]) ){
						$errors[] = $this->S->L('E&grave; necessario inserire il nome dell\'avversario');
					}*/
                    if( !($_POST["id_avversario"]>0) ){
                        $errors[] = $this->S->L('E&grave; necessario selezionare l\'avversario');
                    }

					
					if( $_POST["id_formazione"]>0 ){
						$arr = array();
						for($i=1;$i<=11;$i++){
							if( !($_POST["id_giocatore{$i}"]>0) ){
								$errors[] = "Non &egrave; stato selezionato il giocatore n. $i";	
							}else if( in_array($_POST["id_giocatore{$i}"],$arr) ){
								$errors[] = "Il giocatore {$i} &egrave; gi&agrave; stato selezionato";
							}
							$arr[] = $_POST["id_giocatore{$i}"];
						}
					}
					
					if( count($errors)>0 ){
						$msg = $this->toMsg($errors);
					}else{
						if( $id==0 ){
							$q = "INSERT INTO `{$this->_dp}gf_match` () VALUES ()";
							$this->cn->Q($q);
							$id = $this->cn->last_id;
						}
						
						//Gioco indovina la formazione
						$q = "SELECT * FROM `{$this->_dp}gf_formazione_match` WHERE id_match = {$id} AND id_utente IS NULL";
						$this->cn->Q($q);
						if( $this->cn->n==0 ){
							$q = "INSERT INTO `{$this->_dp}gf_formazione_match` (id_match) VALUES ({$id})";
							$this->cn->Q($q);
						}
						$q = "SELECT * FROM `{$this->_dp}gir_risultati` WHERE id_match = {$id} AND id_utente IS NULL";
						$this->cn->Q($q);
						if( $this->cn->n==0 ){
							$q = "INSERT INTO `{$this->_dp}gir_risultati` (id_match) VALUES ({$id})";//Gioco indovina la formazione
							$this->cn->Q($q);
						}
						$q = "SELECT * FROM `{$this->_dp}gim_risultati` WHERE id_match = {$id} AND id_utente IS NULL";
						$this->cn->Q($q);
						if( $this->cn->n==0 ){
							$q = "INSERT INTO `{$this->_dp}gim_risultati` (id_match) VALUES ({$id})";//Gioco indovina la formazione
							$this->cn->Q($q);
						}
						
						
						$attiva = isset($_POST["attiva"]) ? 1 : 0;
						$in_casa = isset($_POST["in_casa"]) ? 1 : 0;
						$order = $_POST["order"]>0 ? $_POST["order"] : "NULL";
						$q = "UPDATE `{$this->_dp}gf_match` SET
							id_banner = {$_POST['id_banner']},
							id_stagione = {$_POST['id_stagione']},
							in_casa = {$in_casa},
							`data` = '{$data} {$ora}',
							`id_avversario` = {$_POST['id_avversario']},
							`description` = '" . addslashes($_POST["description"]) . "',
							attiva = {$attiva}
							
							WHERE id = {$id}
							";
						$this->cn->Q($q);
						
						if( site=='live' ){
                            require("../services/fnet3/inviola/ws_data/web.inc.php");
							//id negozio = 3446
							//serial nr terminal= 403344601
							$Dati['Terminal']["terminal_sn"] = $Dati['Terminal']["foreignID"] = '403344601';
                            require("../services/fnet3/FNET3.class.php");
						}else{
							require("../services/fnet3/inviola/ws_data/web-dev.inc.php");
							//id negozio 1467
							//serial terminale 282146701
							$Dati['Terminal']["terminal_sn"] = $Dati['Terminal']["foreignID"] = '282146701';
							require("../services/fnet3/FNET3-dev.class.php");
						}

                        //GIOCO INDOVINA FORMAZIONE
						$q = "UPDATE `{$this->_dp}gf_formazione_match` SET
						id_formazione = {$_POST['id_formazione']}";
						
						for($i=1,$flag=false;$i<=11;$i++){
							$q .= ", `id_giocatore{$i}` = " . $_POST['id_giocatore' . $i];
							if( !($_POST["id_giocatore".$i]>0) ){
								$flag = true;
							}
						}
						$q .= " WHERE id_match = {$id} AND id_utente IS NULL";
						$this->cn->Q($q);

						$q = "SELECT punti_assegnati FROM `{$this->_dp}gf_formazione_match` WHERE id_match = {$id} AND id_utente IS NULL";
						$punti_assegnati = $this->cn->OF($q);
						if( $_POST['id_formazione']>0 && !$flag && !$punti_assegnati){ //Formazione inserita
							//Controllo i vincitori
							$q = "SELECT id_utente FROM `{$this->_dp}gf_formazione_match` WHERE id_match = {$id} AND id_formazione = {$_POST['id_formazione']} AND id_utente>0";
							for($i=1;$i<=11;$i++){
								$q .= " AND id_giocatore{$i} = " . $_POST["id_giocatore{$i}"];	
							}
							$q .= " GROUP BY id_utente";
							$id_winners = $this->cn->Q($q,true);
							
							if( $this->cn->n>0 ){//Assegna punti ai vincitori
								$FNET = new FNET3('inviola',$Dati);
								
								$punti_vinti = 100;
								foreach($id_winners as $id_win){
									$q = "SELECT customer_id FROM `utenti` WHERE id = {$id_win['id_utente']}";
									$c_id = $this->cn->OF($q);
									if($c_id>0){
										$x = $FNET->ChargePoints(false,$c_id, array("points"=>$punti_vinti,"kind"=>1,"notes"=>'Bonus vincita gioco "Indovina la formazione".') );
										if( $x->answerCode!="0" ){
											$fp = "logs/gioco-formazione-error.txt";
											fwrite($fp,"Utente: {$c_id} ha partecipato alla formazione ID: {$_POST['id_formazione']}. I punti non gli sono stati assegnati per il seguente problema: AnswerCode: {$x->answerCode}");
											fclose($fp);
										}
									}
								}
								$q = "UPDATE `{$this->_dp}gf_formazione_match` SET punti_assegnati = 1, punti_vinti={$punti_vinti} WHERE id_match = {$id} AND id_utente IS NULL";
								$this->cn->Q($q);
								unset($FNET);
							}
						}
                        //FINE GIOCO INDOVINA FORMAZIONE

                        //GIOCO INDOVINA RISULTATO
                        $q = "UPDATE `{$this->_dp}gir_risultati` SET
                          goal_fiorentina = {$_POST['goal_fiorentina']},
                          goal_avversario = {$_POST['goal_avversario']}
                        WHERE id_match = {$id} AND id_utente IS NULL";
                        $this->cn->Q($q);

                        $q = "SELECT punti_assegnati FROM `{$this->_dp}gir_risultati` WHERE id_match = {$id} AND id_utente IS NULL";
                        $punti_assegnati = $this->cn->OF($q);
                        if( $_POST['id_formazione']>0 && !$punti_assegnati){ //Formazione inserita
                            //Controllo i vincitori
                            $q = "SELECT id_utente FROM `{$this->_dp}gir_risultati` WHERE id_match = {$id} AND id_utente>0 AND goal_fiorentina = {$_POST['goal_fiorentina']} AND goal_avversario = {$_POST['goal_avversario']}";
                            $q .= " GROUP BY id_utente";
                            $id_winners = $this->cn->Q($q,true);

                            if( $this->cn->n>0 ){//Assegna punti ai vincitori
                                $FNET = new FNET3('inviola',$Dati);

                                $punti_vinti = 40;
                                foreach($id_winners as $id_win){
                                    $q = "SELECT customer_id FROM `utenti` WHERE id = {$id_win['id_utente']}";
                                    $c_id = $this->cn->OF($q);
                                    if($c_id>0){
                                        $x = $FNET->ChargePoints(false,$c_id, array("points"=>$punti_vinti,"kind"=>1,"notes"=>'Bonus vincita gioco "Indovina il risultato".') );
                                        if( $x->answerCode!="0" ){
                                            $fp = "logs/gioco-indovina-risultato-error.txt";
                                            fwrite($fp,"Utente: {$c_id} ha partecipato al gioco indovina il risultato: {$_POST['id_match']}. I punti non gli sono stati assegnati per il seguente problema: AnswerCode: {$x->answerCode}");
                                            fclose($fp);
                                        }
                                    }
                                }
                                $q = "UPDATE `{$this->_dp}gir_risultati` SET punti_assegnati = 1, punti_vinti={$punti_vinti} WHERE id_match = {$id} AND id_utente IS NULL";
                                $this->cn->Q($q);
                                unset($FNET);
                            }
                        }
                        //FINE GIOCO INDOVINA RISULTATO

                        //GIOCO INDOVINA PRIMO MARCATORE
						  $q = "UPDATE `{$this->_dp}gim_risultati` SET
                          id_giocatore = {$_POST['id_primo_marcatore']}
                        WHERE id_match = {$id} AND id_utente IS NULL";
                        $this->cn->Q($q);

                        $q = "SELECT punti_assegnati FROM `{$this->_dp}gim_risultati` WHERE id_match = {$id} AND id_utente IS NULL";
                        $punti_assegnati = $this->cn->OF($q);
                        if( $_POST['id_formazione']>0 && !$punti_assegnati){ //Formazione inserita
                            //Controllo i vincitori
                            $q = "SELECT id_utente FROM `{$this->_dp}gim_risultati` WHERE id_match = {$id} AND id_utente>0 AND id_giocatore = {$_POST['id_primo_marcatore']}";
                            $q .= " GROUP BY id_utente";
                            $id_winners = $this->cn->Q($q,true);

                            if( $this->cn->n>0 ){//Assegna punti ai vincitori
                                $FNET = new FNET3('inviola',$Dati);

                                $punti_vinti = 30;
                                foreach($id_winners as $id_win){
                                    $q = "SELECT customer_id FROM `utenti` WHERE id = {$id_win['id_utente']}";
                                    $c_id = $this->cn->OF($q);
                                    if($c_id>0){
                                        $x = $FNET->ChargePoints(false,$c_id, array("points"=>$punti_vinti,"kind"=>1,"notes"=>'Bonus vincita gioco "Indovina il primo marcatore".') );
                                        if( $x->answerCode!="0" ){
                                            $fp = "logs/gioco-primo-marcatore-error.txt";
                                            fwrite($fp,"Utente: {$c_id} ha partecipato al gioco indovina il primo marcatore: {$_POST['id_match']}. I punti non gli sono stati assegnati per il seguente problema: AnswerCode: {$x->answerCode}");
                                            fclose($fp);
                                        }
                                    }
                                }	
                                $q = "UPDATE `{$this->_dp}gim_risultati` SET punti_assegnati = 1, punti_vinti={$punti_vinti} WHERE id_match = {$id} AND id_utente IS NULL";
                                $this->cn->Q($q);
								
                                unset($FNET);
                            }
                        } 
						
                        //FINE GIOCO INDOVINA PRIMO MARCATORE
						
						$status = 1;
					}
				}else{
					$status = 1;
				}
				
				$dati = false;
				if( $Load && $id>0 || $id>0 ){
					$q = "SELECT *, DATE_FORMAT(data,'%d/%m/%Y %H:%i') AS data
					FROM `{$this->_dp}gf_match` WHERE id = {$id}";
					$dati = $this->cn->OQ( $q );
					
					$MatchClose = false;
					
					//Gioco indovina formazione
					$q = "SELECT id_formazione";
					for($i=1;$i<=11;$i++){
						$q .= ", id_giocatore{$i}";	
					}
					$q .= "	FROM `{$this->_dp}gf_formazione_match` WHERE id_match = {$id} AND id_utente IS NULL LIMIT 1";
					$MatchWinner = $this->cn->OQ($q);
					$dati["nr_partecipanti"] = $dati["nr_vincitori"] = $dati["perc_vincitori"] = "-";	
					
					//Statistiche
					if( is_array($MatchWinner) ){
						$dati = array_merge($dati,$MatchWinner);
					
						$q = "SELECT COUNT(DISTINCT id) AS n FROM `{$this->_dp}gf_formazione_match` WHERE id_match = {$id} AND id_utente IS NOT NULL";
						$dati["nr_partecipanti"] = $this->cn->OF($q);
						$MatchClose = $MatchWinner["id_formazione"]>0 ? true : false;
						if( $MatchWinner["id_formazione"]>0 &&
							$MatchWinner["id_giocatore1"]>0 && $MatchWinner["id_giocatore2"]>0 && $MatchWinner["id_giocatore3"]>0 && $MatchWinner["id_giocatore4"]>0 && $MatchWinner["id_giocatore5"]>0 && $MatchWinner["id_giocatore6"]>0 && $MatchWinner["id_giocatore7"]>0 && $MatchWinner["id_giocatore8"]>0 && $MatchWinner["id_giocatore9"]>0 && $MatchWinner["id_giocatore10"]>0 && $MatchWinner["id_giocatore11"]>0
						){
								
							$q = "SELECT COUNT(DISTINCT id) AS n FROM `{$this->_dp}gf_formazione_match` WHERE id_match = {$id} AND id_utente IS NOT NULL AND id_formazione = {$MatchWinner['id_formazione']}";
							for($i=1;$i<=11;$i++){
								$q .= " AND id_giocatore{$i} = " . $MatchWinner["id_giocatore{$i}"];
							}
							$q .= " GROUP BY id_utente";
							$this->cn->Q($q);
							$dati["nr_vincitori"] = $this->cn->n;
							$dati["perc_vincitori"] = $dati["nr_vincitori"]*100/$dati["nr_partecipanti"];
							$dati["perc_vincitori"] = (round($dati["perc_vincitori"]*100)/100) . "%";
						}
					}
					unset($MatchWinner);
					
					//Gioco indovina primo marcatore
					$q = "SELECT * FROM `{$this->_dp}gim_risultati` WHERE id_match = {$id} AND id_utente IS NULL LIMIT 1";
					$MatchWinner = $this->cn->OQ($q);
					$dati["nr_partecipanti_marcatore"] = $dati["nr_vincitori_marcatore"] = $dati["perc_vincitori_marcatore"] = "-";	
					//Statistiche
					if( is_array($MatchWinner) ){
						$dati['id_primo_marcatore'] = $MatchWinner['id_giocatore'];
						
						$q = "SELECT COUNT(DISTINCT id) AS n FROM `{$this->_dp}gim_risultati` WHERE id_match = {$id} AND id_utente IS NOT NULL";
						$dati["nr_partecipanti_marcatore"] = $this->cn->OF($q);
						if( is_array($MatchWinner) && $MatchClose){
							$q = "SELECT COUNT(DISTINCT id) AS n FROM `{$this->_dp}gim_risultati` WHERE id_match = {$id} AND id_utente IS NOT NULL AND id_giocatore = {$MatchWinner['id_giocatore']} GROUP BY id_utente";
							$this->cn->Q($q);
							$dati['nr_vincitori_marcatore'] = $this->cn->n;
							$dati["perc_vincitori_marcatore"] = $dati["nr_vincitori_marcatore"]*100/$dati["nr_partecipanti_marcatore"];
							$dati["perc_vincitori_marcatore"] = (round($dati["perc_vincitori_marcatore"]*100)/100) . "%";
						}
					}
					unset($MatchWinner);
					
					//Gioco indovina risultato
					$q = "SELECT * FROM `{$this->_dp}gir_risultati` WHERE id_match = {$id} AND id_utente IS NULL LIMIT 1";
					$MatchWinner = $this->cn->OQ($q);
					$dati["nr_partecipanti_risultato"] = $dati["nr_vincitori_risultato"] = $dati["perc_vincitori_risultato"] = "-";	
					
					if( is_array($MatchWinner) ){
						$dati['goal_fiorentina'] = $MatchWinner['goal_fiorentina'];
						$dati['goal_avversario'] = $MatchWinner['goal_avversario'];
						
						$q = "SELECT COUNT(DISTINCT id) AS n FROM `{$this->_dp}gir_risultati` WHERE id_match = {$id} AND id_utente IS NOT NULL";
						$dati["nr_partecipanti_risultato"] = $this->cn->OF($q);
						
						if( $MatchClose ){
							$q = "SELECT COUNT(DISTINCT id) AS n FROM `{$this->_dp}gir_risultati` WHERE id_match = {$id} AND id_utente IS NOT NULL AND goal_fiorentina = {$MatchWinner['goal_fiorentina']} AND goal_avversario = {$MatchWinner['goal_avversario']} GROUP BY id_utente";
							$this->cn->Q($q);
							$dati['nr_vincitori_risultato'] = $this->cn->n;
							$dati["perc_vincitori_risultato"] = $dati["nr_vincitori_risultato"]*100/$dati["nr_partecipanti_risultato"];
							$dati["perc_vincitori_risultato"] = (round($dati["perc_vincitori_risultato"]*100)/100) . "%";
						}
					}
					unset($MatchWinner);
				}
				
				$ret = array("status"=>$status,"msg"=>$msg,"fields"=>$dati);
				self::delay();
				break;
			case "formazione_list":
				if( $_POST["delete"]==1 ){
					$ids = array();
					foreach($_POST as $k=>$v){
						if( strpos($k,"delete_id")===0 && $v>0){
							$ids[] = $v;
						}
					}
					if( count($ids)>0 ){
						$q = "UPDATE `{$this->_dp}gf_match` SET deleted = NOW() WHERE id IN (".implode(",",$ids).")";
						$this->cn->Q($q);
						$ret = array("status"=>1);
					}else{
						$ret = array("status"=>0);
					}
				}else{
					$order = $_POST["order"];
					$order_dir = $_POST["order_dir"];
	
					$q = "SELECT
						main.*,
						IF(main.id_avversario>0, teams.nome, main.avversario) AS avversario,
						DATE_FORMAT(main.data, '%d/%m/%Y %H:%i') AS data_text,
						IF( main.data <= NOW(), 0, 1) AS eliminable
						FROM
						`{$this->_dp}gf_match` main
						LEFT JOIN `{$this->_dp}gf_teams` teams ON main.id_avversario = teams.id
						WHERE main.deleted IS NULL";
					
					if($_POST["id_stagione"]>0){
						$q .= " AND main.id_stagione = {$_POST['id_stagione']}";
					}
										
					$q .= " ORDER BY {$order} {$order_dir}";
					
					$ris = $this->cn->Q($q,true);
					$n = $this->cn->n;
					
					$list = array();
					foreach($ris as $r){
						$row = array();
						$row[] = "<tr class=\"Clickable\" data-url=\"sezioni/giochi/formazione_scheda?id={$r['id']}\">";
						$row[] = "<td><div class=\"center\">{$r['id']}</div></td>";
						$row[] = "<td><div class=\"left\">{$r['data_text']}</div></td>";
						$partita = $r["in_casa"]==1 ? "Fiorentina - {$r['avversario']}" : "{$r['avversario']} - Fiorentina";
						$row[] = "<td><div class=\"left\">{$partita}</div></td>";
						$attivo = $r["attiva"]==1 ? "check.png" : "del.png";
						$row[] = "<td><div class=\"center\"><img src=\"_ext/img/icon/{$attivo}\" width=\"24\"></div></td>";
						$disabled = $r["eliminable"]==1 ? '' : ' disabled';
						$row[] = "<td class=\"noClickable\"><div class=\"center\"><input type=\"checkbox\" name=\"delete_id{$r['id']}\" value=\"{$r['id']}\"{$disabled}></div></td>";
						$row[] = '</tr>';
						
						$list[] = implode("",$row);
					}
					
					
					$ret = array("status"=>1,
						"n"=>$n,
						"list"=>$list,
						"order"=>$order,
						"order_dir"=>$order_dir
					);
				
					self::delay();
				}
				break;
            case "categorie_teams_save":
                $id = $_POST["id"];
                $Load = $_POST["load"]==1 ? true : false;

                $status = 0;
                $msg = "";

                if( !$Load ){
                    $errors = array();

                    if( empty($_POST["nome"]) ){
                        $errors[] = $this->S->L('E&grave; necessario inserire il nome');
                    }

                    if( count($errors)>0 ){
                        $msg = $this->toMsg($errors);
                    }else{
                        if( $id==0 ){
                            $q = "INSERT INTO `{$this->_dp}gf_teams_categorie` () VALUES ()";
                            $this->cn->Q($q);
                            $id = $this->cn->last_id;
                        }

                        $q = "UPDATE `{$this->_dp}gf_teams_categorie` SET
							`nome` = '" . addslashes($_POST["nome"]) . "'

							WHERE id = {$id}
							";
                        $this->cn->Q($q);

                        $status = 1;
                    }
                }else{
                    $status = 1;
                }

                $dati = false;
                if( $Load && $id>0 || $id>0 ){
                    $q = "SELECT *
					FROM `{$this->_dp}gf_teams_categorie` WHERE id = {$id}";
                    $dati = $this->cn->OQ( $q );
                }

                $ret = array("status"=>$status,"msg"=>$msg,"fields"=>$dati);
                self::delay();
                break;
            case "categorie_teams_list":
                if( $_POST["delete"]==1 ){
                    $ids = array();
                    foreach($_POST as $k=>$v){
                        if( strpos($k,"delete_id")===0 && $v>0){
                            $ids[] = $v;
                        }
                    }
                    if( count($ids)>0 ){
                        $q = "DELETE FROM `{$this->_dp}gf_teams_categorie` WHERE id IN (".implode(",",$ids).")";
                        $this->cn->Q($q);
                        $ret = array("status"=>1);
                    }else{
                        $ret = array("status"=>0);
                    }
                }else{
                    $order = $_POST["order"];
                    $order_dir = $_POST["order_dir"];

                    $q = "SELECT
						*
						FROM `{$this->_dp}gf_teams_categorie` WHERE 1";

                    $q .= " ORDER BY {$order} {$order_dir}";

                    $ris = $this->cn->Q($q,true);
                    $n = $this->cn->n;

                    $list = array();
                    foreach($ris as $r){
                        $row = array();
                        $row[] = "<tr class=\"Clickable\" data-url=\"sezioni/giochi/categorie_teams_scheda?id={$r['id']}\">";
                        $row[] = "<td><div class=\"center\">{$r['id']}</div></td>";
                        $row[] = "<td><div class=\"left\">{$r['nome']}</div></td>";
                        $row[] = "<td class=\"noClickable\"><div class=\"center\"><input type=\"checkbox\" name=\"delete_id{$r['id']}\" value=\"{$r['id']}\"></div></td>";
                        $row[] = '</tr>';

                        $list[] = implode("",$row);
                    }


                    $ret = array("status"=>1,
                        "n"=>$n,
                        "list"=>$list,
                        "order"=>$order,
                        "order_dir"=>$order_dir
                    );

                    self::delay();
                }
                break;
            case "teams_save":
                $id = $_POST["id"];
                $Load = $_POST["load"]==1 ? true : false;

                $status = 0;
                $msg = "";

                if( !$Load ){
                    $errors = array();

                    if( empty($_POST["nome"]) ){
                        $errors[] = $this->S->L('E&grave; necessario inserire il nome');
                    }

                    if( count($errors)>0 ){
                        $msg = $this->toMsg($errors);
                    }else{
                        if( $id==0 ){
                            $q = "INSERT INTO `{$this->_dp}gf_teams` () VALUES ()";
                            $this->cn->Q($q);
                            $id = $this->cn->last_id;
                        }

                        $id_logo = $_POST["id_logo"]>0 ? $_POST["id_logo"] : "NULL";
                        $attivo = isset($_POST['attivo']) ? 1 : 0;
                        $order = $_POST["order"]>0 ? $_POST["order"] : "NULL";
                        $q = "UPDATE `{$this->_dp}gf_teams` SET
							`nome` = '" . addslashes($_POST["nome"]) . "',
							`id_categoria` = {$_POST['id_categoria']},
							id_logo = {$id_logo},
							attivo = {$attivo}

							WHERE id = {$id}
							";
                        $this->cn->Q($q);

                        $status = 1;
                    }
                }else{
                    $status = 1;
                }

                $dati = false;
                if( $Load && $id>0 || $id>0 ){
                    $q = "SELECT *
					FROM `{$this->_dp}gf_teams` WHERE id = {$id}";
                    $dati = $this->cn->OQ( $q );
                }

                $ret = array("status"=>$status,"msg"=>$msg,"fields"=>$dati);
                self::delay();
                break;
            case "teams_list":
                if( $_POST["delete"]==1 ){
                    $ids = array();
                    foreach($_POST as $k=>$v){
                        if( strpos($k,"delete_id")===0 && $v>0){
                            $ids[] = $v;
                        }
                    }
                    if( count($ids)>0 ){
                        $q = "UPDATE `{$this->_dp}gf_teams` SET deleted = NOW() WHERE id IN (".implode(",",$ids).")";
                        $this->cn->Q($q);
                        $ret = array("status"=>1);
                    }else{
                        $ret = array("status"=>0);
                    }
                }else{
                    $order = $_POST["order"];
                    $order_dir = $_POST["order_dir"];

                    $q = "SELECT
						*
						FROM `{$this->_dp}gf_teams` WHERE 1";

                    $q .= " ORDER BY {$order} {$order_dir}";

                    $ris = $this->cn->Q($q,true);
                    $n = $this->cn->n;

                    $list = array();
                    foreach($ris as $r){
                        $row = array();
                        $row[] = "<tr class=\"Clickable\" data-url=\"sezioni/giochi/teams_scheda?id={$r['id']}\">";
                        $row[] = "<td style=\"vertical-align: middle;\"><div class=\"center\">{$r['id']}</div></td>";
                        if($r["id_logo"]>0 ){
                            $pathfile = $this->pathFile($r["id_logo"]);
                            $row[] = "<td><div class=\"center\"><img src=\"{$pathfile}\" width=\"100\"></div></td>";
                        }else{
                            $row[] = "<td><div class=\"center\">-</div></td>";
                        }
                        $row[] = "<td style=\"vertical-align: middle;\"><div class=\"left\">{$r['nome']}</div></td>";
                        $attivo = $r["attivo"]==1 ? "check.png" : "del.png";
                        $row[] = "<td style=\"vertical-align: middle;\"><div class=\"center\"><img src=\"_ext/img/icon/{$attivo}\" width=\"24\"></div></td>";
                        $row[] = "<td style=\"vertical-align: middle;\" class=\"noClickable\"><div class=\"center\"><input type=\"checkbox\" name=\"delete_id{$r['id']}\" value=\"{$r['id']}\"></div></td>";
                        $row[] = '</tr>';

                        $list[] = implode("",$row);
                    }


                    $ret = array("status"=>1,
                        "n"=>$n,
                        "list"=>$list,
                        "order"=>$order,
                        "order_dir"=>$order_dir
                    );

                    self::delay();
                }
                break;
			//last
			//old
			case "promozioni_save":
				$id = $_POST["id"];
				$Load = $_POST["load"]==1 ? true : false;
				
				$status = 0;
				$msg = "";
				
				if( !$Load ){
					$errors = array();
					if( strlen($_POST["titolo"])==0 ){
						$errors[] = "Il campo Titolo è obbligatorio";
					}
					if( empty($_POST["dal"]) || empty($_POST["al"]) ){
						$errors[] = "E' necessario compilare il periodo";
					}else{
						list($a,$b,$c) = explode("/",$_POST["dal"]);
						$dal = mktime(0,0,0,$b,$a,$c);
						list($a,$b,$c) = explode("/",$_POST["al"]);
						$al = mktime(0,0,0,$b,$a,$c);
						if($dal>$al){
							$al = $dal;
						}
					}
					
					if( count($errors)>0 ){
						$msg = $this->toMsg($errors);
					}else{
						if( $id==0 ){
							$q = "INSERT INTO promozioni () VALUES ()";
							$this->cn->Q($q);
							$id = $this->cn->last_id;
						}
						
						$attivo = $_POST["attivo"]==1 ? 1 : 0;
						$dal_ms = strftime("%Y-%m-%d", $dal);
						$al_ms = strftime("%Y-%m-%d", $al);
						$q = "UPDATE promozioni SET
							id_ccn = {$_POST[id_ccn]},
							id_negozio = {$_POST[id_negozio]},
							titolo = '".addslashes($_POST["titolo"])."',
							descr = '".addslashes($_POST["descr"])."',
							dal = '{$dal}',
							al = '{$al}',
							attiva = {$attivo}
							
							WHERE id = {$id}
							";
						$this->cn->Q($q);
						
						$status = 1;
					}
				}else{
					$status = 1;
				}
				
				$dati = false;
				if( $Load && $id>0 || $id>0 ){
					$q = "SELECT * FROM promozioni WHERE id = {$id}";
					$dati = $this->cn->OQ( $q );
				}else if($id==0){
					$x = date("w", time());
					$x = $x==0 ? 6 : $x-1;
					$lunedi = time()-($x*24*60*60);
					$dal = mktime(0,0,0,date("m",$lunedi),date("d",$lunedi),date("Y",$lunedi));
					$al = $dal+(4*24*60*60);
					$dati = array("dal"=>strftime("%d/%m/%Y",$dal), "al"=>strftime("%d/%m/%Y",$al));
				}

				$ret = array("status"=>$status,"msg"=>$msg,"fields"=>$dati);
				
				break;
			case "promozioni_list":
				$order = $_POST["order"];
				$order_dir = $_POST["order_dir"];

				$q = "SELECT p.*,
					DATE_FORMAT('%d//%m-%Y', p.dal) AS dal_f,
					DATE_FORMAT('%d/%m/%Y', p.al) AS al_f,
					IF(p.id_negozio=0,'TUTTI', n.nome) AS negozio,
					CONCAT(g.nome,' - ', ccn.nome) AS circuito FROM
					promozioni p INNER JOIN ccn ON p.id_ccn = ccn.id
					INNER JOIN gruppi g ON g.id = ccn.id_gruppo
					LEFT JOIN negozi n ON p.id_negozio = n.id
					
					WHERE 1";
									
				$q .= " ORDER BY {$order} {$order_dir}";
				
				$ris = $this->cn->Q($q,true);
				$n = $this->cn->n;
				
				$list = array();
				foreach($ris as $r){
					$row = array();
					$row[] = "<tr class=\"Clickable\" data-url=\"sezioni/promozioni/scheda?id={$r[id]}\">";
					$row[] = "<td><div class=\"center\">{$r[id]}</div></td>";
					$row[] = "<td>{$r[titolo]}</td>";
					$row[] = "<td>{$r[dal_f]} - {$r[dal_f]}</td>";
					$row[] = "<td>{$r[negozio]}</td>";
					$row[] = "<td>{$r[circuito]}</td>";
					$attivo = $r["attiva"]==1 ? "check.png" : "del.png";
					$row[] = "<td><div class=\"center\"><img src=\"_ext/img/icon/{$attivo}\" width=\"24\"></div></td>";
					//$row[] = "<td class=\"noClickable\"><div class=\"center\"><input type=\"checkbox\" name=\"delete_id{$r[id]}\" value=\"{$r[id]}\"></div></td>";
					$row[] = '</tr>';
					
					$list[] = implode("",$row);
				}
				
				
				$ret = array("status"=>1,
					"n"=>$n,
					"list"=>$list,
					"order"=>$order,
					"order_dir"=>$order_dir
				);
			
				self::delay();

				break;
				
				
				
			case "prodotti_list":
				if( $_POST["delete"]==1 ){
					$ids = array();
					foreach($_POST as $k=>$v){
						if( strpos($k,'delete_id')===0 && $v>0){
							$ids[] = $v;
						}
					}
					if( count($ids)>0 ){
						$q = "UPDATE prodotti SET eliminato = NOW() WHERE id IN (".implode(",",$ids).")";
						$this->cn->Q($q);
						$ret = array("status"=>1);
					}else{
						$ret = array("status"=>0);
					}
				}else{
					$order = $_POST["order"];
					$order_dir = $_POST["order_dir"];
	
					$q = "SELECT
						p.*,
						c.nome AS categoria,
						(SELECT prezzo FROM prodotti_prezzi WHERE id_prodotto = p.id ORDER BY `timestamp` DESC LIMIT 1) AS prezzo,
						(SELECT punti FROM prodotti_prezzi WHERE id_prodotto = p.id ORDER BY `timestamp` DESC LIMIT 1) AS punti,
						(SELECT punti_premio FROM prodotti_prezzi WHERE id_prodotto = p.id ORDER BY `timestamp` DESC LIMIT 1) AS punti_premio
					
						FROM prodotti p INNER JOIN categorie c ON p.id_categoria = c.id
					WHERE 1 AND eliminato IS NULL";
					$having = " HAVING 1";
					
					if( !empty($_POST["nome"]) ){
						$q .= " AND p.nome LIKE '%{$_POST[nome]}%'";	
					}
					if( !empty($_POST["codice"]) ){
						$q .= " AND p.codice LIKE '%{$_POST[codice]}%'";	
					}
					if( !empty($_POST["categoria"]) ){
						$q .= " AND c.nome LIKE '%{$_POST[categoria]}%'";	
					}
					if( !empty($_POST["prezzo_da"]) ){
						$having .= " AND '{$_POST[prezzo_da]}' <= prezzo";	
					}
					if( !empty($_POST["prezzo_a"]) ){
						$having .= " AND prezzo <= '{$_POST[prezzo_a]}'";
					}
					if( !empty($_POST["punti_da"]) ){
						$having .= " AND '{$_POST[punti_da]}' <= punti";	
					}
					if( !empty($_POST["punti_a"]) ){
						$having .= " AND punti <= '{$_POST[punti_a]}'";
					}
					if( !empty($_POST["punti_premio_da"]) ){
						$having .= " AND '{$_POST[punti_premio_da]}' <= punti_premio";
					}
					if( !empty($_POST["punti_premio_a"]) ){
						$having .= " AND punti_premio <= '{$_POST[punti_premio_a]}'";
					}
					
					$q .= $having;
										
					$q .= " ORDER BY {$order} {$order_dir}";
					
					$ris = $this->cn->Q($q,true);// or self::Error($q);
					$n = $this->cn->n;
					
					$list = array();
					
					$valuta = $this->S->P('valuta_html',$_POST["id_gruppo"]);
					foreach($ris as $r){
						$row = array();
						$row[] = "<tr class=\"Clickable\" data-url=\"sezioni/prodotti/scheda?id={$r[id]}\">";
						$row[] = "<td><div class=\"center\">{$r[id]}</div></td>";
						$row[] = "<td>{$r[nome]}</td>";
						$row[] = "<td>{$r[categoria]}</td>";
						$attivo = $r["attivo"]==1 ? "check.png" : "del.png";
						$row[] = "<td><div class=\"center\"><img src=\"_ext/img/icon/{$attivo}\" width=\"24\"></div></td>";
						
						$row[] = "<td>{$valuta} ".number_format($r["prezzo"],2,",",".")."</td>";
						$row[] = "<td><div class=\"center\">{$r[punti]}</div></td>";
						$row[] = "<td><div class=\"center\">{$r[punti_premio]}</div></td>";
						
						$row[] = "<td class=\"noClickable\"><div class=\"center\"><input type=\"checkbox\" name=\"delete_id{$r[id]}\" value=\"{$r[id]}\"></div></td>";
						$row[] = '</tr>';
						
						$list[] = implode("",$row);
					}
					
					
					$ret = array("status"=>1,
						"n"=>$n,
						"list"=>$list,
						"order"=>$order,
						"order_dir"=>$order_dir
					);
				
					self::delay();
				}
				break;
			case "prodotti_save":
				$id = $_POST["id"];
				$Load = $_POST["load"]==1 ? true : false;
				
				$status = 0;
				$msg = "";
				
				if( !$Load ){
					//Controllo campi
					
					$errors = array();
					if( strlen($_POST["nome"])==0 ){
						$errors[] = $this->S->L("Il campo Nome è necessario");
					}
					if( strlen($_POST["nome_pulsante"])==0 ){
						$errors[] = $this->S->L("Il campo Testo su pulsante è necessario");
					}
					if( !($_POST["punti_premio"]>0) && empty($_POST["prezzo"]) ){
						$errors[] = $this->S->L("Il campo Prezzo è necessario");
					}
					
					if( count($errors)>0 ){
						$msg = $this->toMsg($errors);
					}else{
						
						$q = "SELECT * FROM categorie WHERE id = {$_POST[id_categoria]}";
						$cat = $this->cn->OQ($q);
						if( $cat["tipo"]=="bundle" ){
							$punti = $_POST["punti"];
						}else{
							$punti = 0;
						}
					
						if( $id==0 ){
							$q = "INSERT INTO prodotti (id_gruppo,id_categoria) VALUES ({$_POST[id_gruppo]},{$_POST[id_categoria]})";
							$this->cn->Q($q);
							$id = $this->cn->last_id;
						}
						
						$attivo = isset($_POST["attivo"]) ? 1 : 0;
						$q = "UPDATE prodotti SET
							nome = '".addslashes($_POST["nome"])."',";
							
						if( isset($_POST["nome_scontrino"]) ){
							$q .= "nome_scontrino = '".addslashes($_POST["nome_scontrino"])."',";	
						}
						
						$is_premio = $_POST["is_premio"]==1 ? 1 : 0;
						$q .= "nome_pulsante = '".addslashes($_POST["nome_pulsante"])."',
							codice = '".addslashes($_POST["codice"])."',
							codice_iva = '{$_POST[codice_iva]}',
							id_categoria = {$_POST[id_categoria]},
							attivo = {$attivo},
							is_premio = {$is_premio}
							
							WHERE id = {$id}";
						$this->cn->Q($q);
						
						$prezzo = str_replace(",",".",$_POST["prezzo"]);
						$q = "INSERT INTO prodotti_prezzi
							(id_prodotto,prezzo,punti,punti_premio) VALUES ({$id},'{$prezzo}','{$punti}','{$_POST[punti_premio]}')";
						$this->cn->Q($q);
						
						$status=1;
					}
				}else{
					$status=1;
				}
								
				$dati = false;
				if( $Load && $id>0 || $id>0 ){
					$q = "SELECT * FROM prodotti WHERE id = {$id}";
					$dati = $this->cn->OQ( $q );
					$q = "SELECT * FROM prodotti_prezzi WHERE id_prodotto = {$id} ORDER BY `timestamp` DESC LIMIT 1";
					$dati_add = $this->cn->OQ($q);
					$dati = array_merge($dati,$dati_add);
				}
				
				$ret = array("status"=>$status,"msg"=>$msg,"fields"=>$dati);
				
				break;
			case "esporta_transazioni":
				setlocale("LC_TIME","it_IT");
				$id_gruppo = $_POST["id_gruppo"];
				
				$dal = $_POST["dal"];
				if( empty($dal) ){
					$dal = time();
				}else{
					$dal = strtotime( str_replace("/","-",$dal) );	
				}
				$al = $_POST["al"];
				if( empty($al) ){
					$al = time();
				}else{
					$al = strtotime( str_replace("/","-",$al) );	
				}
				
				if( $dal > $al ){
					$al = $dal;
				}
				
				$id_negozio = $_POST["id_negozio"];
				
				$q = "SELECT
					o.id AS id_ordine, n.nome AS negozio, o.nome, o.icard, o.timestamp AS data_ordine,
					c.punti_caricati, IF(c.id_prodotto>0, p.nome, '-') AS nome_prodotto, c.qta, c.prezzo
					
					FROM ordini o INNER JOIN carrello c ON o.id = c.id_ordine
					INNER JOIN negozi n ON n.id_admin_cassa = c.id_admin_cassa
					LEFT JOIN prodotti p ON c.id_prodotto = p.id
					WHERE o.id_gruppo = {$id_gruppo}";
				$q = "SELECT
					o.id AS id_ordine, n.nome AS negozio, o.nome, o.icard, o.timestamp AS data_ordine,
					c.punti_caricati, c.id_prodotto, c.qta, c.prezzo
					
					FROM ordini o INNER JOIN carrello c ON o.id = c.id_ordine
					INNER JOIN negozi n ON n.id_admin_cassa = c.id_admin_cassa
					WHERE o.id_gruppo = {$id_gruppo}";
				
				$q .= " AND '" . date("Y-m-d", $dal) ."' <= o.`timestamp` AND o.`timestamp` <= '" . date("Y-m-d", $al) ."'";
				
				if( $id_negozio>0 ){
					$q .= " AND n.id = {$id_negozio}";
				}
				
				$q .= " ORDER BY negozio ASC, data_ordine ASC";
				
				$this->cn->Q($q);
				$n = $this->cn->n;
				
				$ret = array(
					"status"=>1,
					"n"=>$n,
					"dal"=>date("d/m/Y", $dal),
					"al"=>date("d/m/Y",$al),
					"qclear"=>$q,
					"q"=>base64_encode( $q ) //si passa la query generata all'iframe per la generazione del file xls o csv
				);
				
				break;
			//LAST
			default:
				$ret = array("status"=>1);
				break;
		}
		
		switch($return){
			default:
			case "json_header":
				header('Cache-Control: no-cache, must-revalidate');
				header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
				header('Content-type: application/json');
			case "json":
				echo json_encode($ret);
				break;
			case "echo":
				echo $ret;
				break;
			case "return":
				return $ret;
				break;
		}
	}
	
	private function parseURL($url){
		$validechars = "abcdefghijklmnopqrstuvwxyz";
		$validechars .= strtoupper($validechars);
		$validechars .= '0123456789-_|!"£$%&/()=?^';
		
		$url = str_replace(" ","-",$url);
		$validechars = str_split($validechars);
		$url_arr = str_split($url);
		$newurl = array();
		foreach($url_arr as $c){
			if(in_array($c,$validechars)){
				$newurl[] = $c;
			}
		}
		$url = implode("",$newurl);
		return $url;
	}
	
	public function toDate($date,$type=1){
		if( strlen($date)==10 ){
			list($d,$m,$y) = explode("/",$date);
			$t = mktime(0,0,0,$m,$d,$y);
			$ret = strftime("%Y-%m-%d", $t);
		}
		
		if( $type==1 ){
			if( empty($date) ){
				return "NULL";
			}else{
				return "'{$ret}'";	
			}
		}else if($type==2){
			if( empty($date) ){
				return "NOW()";
			}else{
				return "'{$ret}'";	
			}
		}
	}
	
	private function pathFile($id_file,$full=true){
		$q = "SELECT * FROM {$this->_dp}file WHERE id = {$id_file}";
		$f = $this->cn->OQ($q);
		if( $f==-1 ){
			return false;
		}else{
			$pathfile = $full ? path_webroot : "";
			$subdir = substr($id_file,0,1);
			$pathfile .= "_public/file/{$subdir}/{$id_file}";
			$pathfile .= empty($f["ext"]) ? "" : ".{$f[ext]}";
			return $pathfile;
		}
	}
	
	private function toMsg($errors){
		$new = array();
		foreach($errors as $e){
			$new[] = "<li>{$e}</li>";
		}
		return "<ul class=\"errorList\">" . implode("",$new) . "</ul>";
	}
	
	private function Error($q){
		$ret = array("status"=>-1,"msg"=>"Errore di sistema","q"=>$q);
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		echo json_encode($ret);
		die();
	}
	
	private function delay(){
		usleep( 500000 );
	}
}
?>