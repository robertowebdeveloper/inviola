<?php
session_start();
$_sdkpath = '../sdk/src/';
$_sdkpath = '';

require_once( $_sdkpath . 'Facebook/HttpClients/FacebookHttpable.php' );
require_once( $_sdkpath . 'Facebook/HttpClients/FacebookCurl.php' );
require_once( $_sdkpath . 'Facebook/HttpClients/FacebookCurlHttpClient.php' );

//require_once( $_sdkpath . 'Facebook/Entities/SignedRequest.php' );
require_once( $_sdkpath . 'Facebook/FacebookSession.php' );
require_once( $_sdkpath . 'Facebook/FacebookRedirectLoginHelper.php' );
require_once( $_sdkpath . 'Facebook/FacebookRequest.php' );
require_once( $_sdkpath . 'Facebook/FacebookResponse.php' );
require_once( $_sdkpath . 'Facebook/FacebookSDKException.php' );
require_once( $_sdkpath . 'Facebook/FacebookRequestException.php' );
require_once( $_sdkpath . 'Facebook/FacebookAuthorizationException.php' );
require_once( $_sdkpath . 'Facebook/GraphObject.php' );

use Facebook\HttpClients\FacebookHttpable;
use Facebook\HttpClients\FacebookCurl;
use Facebook\HttpClients\FacebookCurlHttpClient;

use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
?>