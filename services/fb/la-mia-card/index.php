<?php
//https://www.facebook.com/dialog/pagetab?app_id=656445511112224&redirect_uri=https://inviola.violachannel.tv/services/fb/la-mia-card/
define('fb_app',1);
$relative_path = "../../../";
require_once("{$relative_path}_ext/scripts/main.inc.php");
//require("fb_bootstrap.inc.php");
require_once("../sdk/3/src/facebook.php");

$config = array(
	"appId"					=>	'656445511112224',
	"secret"				=>	'b8f805f69008d46fa439b9e98fc65a39',
	"fileUpload"			=>	false,
	"allowSignedRequest"	=>	true,
	"cookie"				=>	true
);

$fb = new Facebook($config);
$user_id = $fb->getUser();

$S->Start();
?><!doctype html>
<html>
<head>
<meta charset="UTF-8">
<?=$S->Head(); ?>
<link href="{{theme}}layout.css" rel="stylesheet" type="text/css">
<link href="fb.css" rel="stylesheet" type="text/css">
</head>

<body>
<?php if(!$user_id){
	$login_url = $fb->getLoginUrl(array(
		'scope'	=>	'email,public_profile,user_birthday',
		//'scope'	=>	'email,public_profile',
		'redirect_uri'	=>	'https://www.facebook.com/pages/Card-Fi-Demo/157146297790649?sk=app_656445511112224'
	));
	?>
	<script type="text/javascript"><!--
    $(document).ready(function(e) {
        window.top.location = '<?=$login_url; ?>';
    });
    --></script>
<?php }else{
	$user = $fb->api('/me','GET');
	if( isset($_REQUEST["signed_request"]) ){
		$_SESSION["signed_request"] = $_REQUEST["signed_request"];
	}
	$_REQUEST["signed_request"] = $_SESSION["signed_request"];
	$info = $fb->getSignedRequest();
	//echo "<pre>";print_r($info);
	$oauth_token = $info["oauth_token"];
	$is_fan = $info["page"]["liked"]==1 ? true : false;
	?>
    <div id="Main">
        
        <header id="Header">
            <div id="Top"></div>
            <?php if($is_fan){ ?><nav><ul>
            	<?php
				$arr = array("La mia card"=>"lamiacard","Profilo"=>"profilo","I miei premi"=>"imieipremi","Wish list"=>"wishlist","Trasferisci i punti"=>"trasferiscipunti","Punti InViola"=>"negozi","Catalogo Premi"=>"elencopremi");
				foreach($arr as $label=>$page){ ?>
                	<li><a href="?p=<?=$page; ?>"><?=$label; ?></a></li>
               <?php } ?>
            </ul></nav><?php } ?>
        </header>
        
        <?php
        if( $is_fan ){
			
			//Verifica se l'id facebook è collegato ad una card o meno
			if( false ){
				
			}else{ 			
				switch($_GET["p"]){
					default:
					case "lamiacard":
						$p_inc = 'card_la-mia-card.inc.php';
						break;
					case "profilo":
						$p_inc = 'card_profilo.inc.php';
						break;
					case "imieipremi":
						$p_inc = 'card_i-miei-premi.inc.php';
						break;
					case "wishlist":
						$p_inc = 'card_wishlist.inc.php';
						break;
					case "trasferiscipunti":
						$p_inc = 'card_trasferiscipunti.inc.php';
						break;
					case "negozi":
						$p_inc = 'negozi.inc.php';
						break;
					case "elencopremi":
						$p_inc = 'catalogo-premi.inc.php';
						break;
				}
			}
			
			echo '<div class="container">';
			include( "{$relative_path}_ext/pages/{$p_inc}");
			echo '</div>';
			
		}else{ include("no-like.inc.php"); }
		?>
        
    </div>
    
    <?=$S->EndScript(); ?>
    <script type="text/javascript" src="{{theme}}main.js"></script>
<?php } ?>
</body>
</html>
<?php
$S->End();
$cn->Close();
unset($cn,$S);
?>