<?php
include("_ext/scripts/setuser.inc.php");

$postdata = array("az"=>'infocardByCustomer','customer_id'=>$_SESSION["customer_id"]);
$user = $S->FNET($postdata);
$user = json_decode($user);

$postdata = array("az"=>'premi_utente','customer_id'=>$_SESSION["customer_id"]);
$premi = $S->FNET($postdata);

$premi = json_decode($premi);
//echo "<pre>";print_r($premi);//exit;
$premi = $premi->data->richiesti;
$item = false;
foreach($premi as $x){
	if($x->prize->prize->id==$_GET["id"]){
		$item = $x;	
	}
}

$data_richiesta = false;
if( strlen($item->prize->exchangeDate)>0 ){
	list($tmp) = explode("+",$item->prize->exchangeDate);
	list($y,$m,$d) = explode("-",$tmp);
	$data_richiesta = "{$d}/{$m}/{$y}";
}
//print_r($_SESSION);
//echo "<pre>";print_r($item);
//echo "<pre>";print_r($user);
?><!doctype html>
<html lang="<?=$S->_lang; ?>">
<head>
<meta charset="UTF-8">
<?=$S->Head(); ?>
<link href='http://fonts.googleapis.com/css?family=Pathway+Gothic+One' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>

<link href="{{root}}_ext/card/style.css" rel="stylesheet" type="text/css">
<link href="{{theme}}layout.css" rel="stylesheet" type="text/css">
<link href="{{theme}}layout-ritiropremio.css" rel="stylesheet" type="text/css">
<style type="text/css" media="print"><!--
	.noPrint{
		display: none;
	}
--></style>
<style type="text/css" media="screen"><!--
	.Print{
		display: none;
	}
	.noPrint > a{
		font-size: 30px;
		display: block;
		color: #666;
	}
--></style>
</head>

<body<?=empty($S->Page["css_class"]) ? "" : ' class="' . $S->Page["css_class"] . '"'; ?>>
<div id="RP_wrapper">
	<br><br>
    
	<div class="center noPrint"><img src="{{theme}}img/logo-inviola-card.png" width="350"><br><br></div>
    <div class="Box center Print"><img src="{{theme}}img/logo-inviola-card-viola.png" width="350"><br><br></div>
    
    <div class="Box">
    	<table width="100%" cellpadding="0" cellspacing="0" border="0">
        	<tr>
            	<td>
                	<b><?="{$_SESSION[name]} {$_SESSION[surname]}"; ?></b>
                    <br>
                    Card: <b><?=$user->data->fidelyCode=="0" ? $user->data->card : $user->data->fidelyCode; ?></b>
                </td>
                <td align="right"><?=$data_richiesta ? "Data richiesta: <b>{$data_richiesta}</b>" : ""; ?></td>
            </tr>
        </table>
    </div>
    
    <div class="Box">
    	<img src="<?=$item->image; ?>" alt="" width="250" style="vertical-align: middle;">
        <h1 style="display: inline-block;"><?=$item->prize->prize->name; ?></h1>
    </div>
    <div class="Box center">
    	<img src="<?=$item->exchangeImg; ?>" alt="<?=$item->code; ?>" width="350">
    </div>
    <div class="Box">
    	<?php
		$html = array();
		foreach($item->ritiro as $row){
			if( $row->bold ){
				$html[] = "<b>{$row->msg}</b>";
			}else if( $row->email ){
				$html[] = "<a href=\"mailto:{$row->email}\">{$row->msg}</a>";
			}else{
				$html[] = $row->msg;
			}
		}
		echo implode("<br>",$html);
		?>
    </div>
    <div class="Box center noPrint"><a href="#" onclick="javascript:window.print();">STAMPA</a></div>
</div>

<?=$S->EndScript(); ?>
<script type="text/javascript" src="{{theme}}main.js"></script>
</body>
</html>