<?php
$premioRichiesto = false;
if( isset($_POST["richiestaPremio"]) && $_POST["richiestaPremio"]==1 && $_POST["prize_id"]>0 && $_POST["prize_code"]>0 && $_POST["qty"]>0 ){
	$postdata = array("az"=>'richiediPremio',"customer_id"=>$_SESSION["customer_id"],"prize_id"=>$_POST["prize_id"],"prize_code"=>$_POST["prize_code"],"qty"=>$_POST["qty"]);
	$ris = $S->FNET($postdata);
	$ris = json_decode($ris);
	//print_r($ris);
	$premioRichiesto = true;
}
$postdata = array(
	"az" => 'premi_utente',
	"customer_id" => $S->_customer_id
);
$list = $S->FNET($postdata);
$list = json_decode( $list );

$richiesti = $list->data->richiesti;
$ritirati = $list->data->ritirati;

//echo "<pre>";print_r($list);
?>
<div class="bgWhite">
	<?php //print_r($S->Page); ?>
    <div class="row"><div class="col-md-10 col-sm-10 col-xs-10 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
        <h1><?=$S->Page["name"]; ?></h1>
        
        <div class="row"><div class="col-md-12 col-sm-12 col-xs-12">
        	<?php if( count($richiesti)==0 ){ ?>
           		<span class="Title Red"><# Nessun premio richiesto #></span> 
           <?php }else{ ?>
        		<div class="TitleGray"><# Premi Richiesti #></div>
               
               <?php foreach($richiesti as $row){
				   //echo "<pre>";print_r($row);
				   ?>
               	<div class="row">
                		<div class="col-md-3 col-sm-3 col-xs-3">
                       	<div class="fotoPremio">
                        		<div class="foto"><a href="<?=$S->getUrl('catalogo-premi',$row->prize->prize->id); ?>"><img src="<?=$row->image; ?>" alt="<?=$row->prize->prize->name; ?>" class="img-responsive"></a></div>
                              <div class="shadow"><img src="{{theme}}img/shadow_premi.png" alt="" class="img-responsive"></div>
                        	</div>
                       </div>
                       <div class="col-md-6 col-sm-6 col-xs-6">
                       	<h3 style="margin-top:0;">
								<?=$row->prize->prize->name; ?>
                              <?php if( $premioRichiesto && $row->prize->id==$_POST["prize_id"]){ ?> - <span class="Red"><# Aggiunto #></span><?php } ?>
                        	</h3>
                          <br><br>
                        	<?=$row->prize->prize->description; ?>
                       </div>
                       <div class="col-md-3 col-sm-3 col-xs-3"><div class="center">
                       	<img src="{{theme}}img/icons/print.png" class="img-responsive" style="margin: 0 auto;" alt="<# Codice di ritiro #>">
                        	<br>
                          <a href="{{url codice-ritiro-premio-stampa}}?id=<?=$row->prize->prize->id; ?>" target="_blank" class="Button"><# Codice di ritiro #></a>
                       </div></div>
                	</div>
                   <hr>
               <?php } ?>
           <?php } ?>           
        </div></div>
        
        <?php if( count($ritirati)>0 ){ ?>        
	        <div class="row">
            	<div class="col-md-12 col-sm-12 col-xs-12"><div class="TitleGray"><# Premi Ritirati #></div></div>
               <?php foreach($ritirati as $row){ ?>
               	<div class="col-md-3 col-sm-4 col-xs-4">
                		<div class="fotoPremio">
                       	<div class="foto">
                        		<a href="<?=$S->getUrl('catalogo-premi',$row->prize->prize->id); ?>"><img src="<?=$row->image; ?>" alt="<?=$row->prize->prize->name; ?>" class="img-responsive"></a>
                              <?=$row->prize->prize->name; ?>
                          </div>
                          <div class="shadow"><img src="{{theme}}img/shadow_premi.png" alt="" class="img-responsive"></div>
                       </div>
                	</div>
               <?php } ?>
	        </div>
            
		<?php } ?>
        
    </div><div class="col-md-1 col-sm-1 col-xs-1"></div></div>
	<br /><br />
</div>