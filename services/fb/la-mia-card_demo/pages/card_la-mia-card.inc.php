<?php
$postdata = array("az"=>'movimenti',"customer_id"=>$_SESSION["customer_id"],"order_criterial"=>true);
$movimenti = $S->FNET($postdata);
$movimenti = json_decode($movimenti);
$data_attivazione = false;

if( is_array($movimenti->data->movimenti) && $movimenti->data->movimenti->kind=="1" ){
	$data_attivazione = $movimenti->data->movimenti->data;
}else{
	foreach($movimenti->data->movimenti as $v){
		if( $v->kind=="1" && !$data_attivazione){
			$data_attivazione = $v->data;
		}
	}
}
?>
<script type="text/javascript">
var updateMovements = function(dir,restart){
	var el = $("#movimentiForm").find("input[name='page']");
	var p = parseInt( el.val() );
	if( restart ){
		p = 0;
	}else{
		p += dir;
	}
	el.val(p);
	
	$("#movimentiForm").submit();
};
</script>
<div class="bgWhite">
	<?php //print_r($S->Page); ?>
    <div class="row"><div class="col-md-10 col-sm-10 col-xs-10 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
        <?php if( isset($_REQUEST["new_user"]) ){ ?>
            <br><br>
            <div class="alert alert-success Title2">
                <b>Complimenti!</b><br>
                La tua registrazione è stata completata con successo. Riceverai una mail con il numero della tua InViola E-Card.
            </div>
		<?php } ?>
        
        <h1><?=$S->Page["name"]; ?></h1>
        
        <div class="row">
        	<div class="col-md-4 col-sm-4 col-xs-4">
            	<div class="infoLaMiaCard">
                	<img src="{{theme}}img/card/card.png" alt="">
	               <# Numero di Card #><br>
                   <?php
				    $nr_card = strlen($S->_infoUser->data->fidelyCode)>1 ? $S->_infoUser->data->fidelyCode : $S->_infoUser->data->card;
					$nr_card = strlen($nr_card)<12 ? str_pad($nr_card,12,"0",STR_PAD_LEFT) : $nr_card;
				   	?>
                   <h4><?=$nr_card; ?></h4>
               </div>
            </div>
        	<div class="col-md-4 col-sm-4 col-xs-4">
            	<div class="infoLaMiaCard">
                	<img src="{{theme}}img/card/data-attivazione.png" class="img-responsive" alt="">               
	               <# Data di attivazione #><br>
                   <h4><?=$data_attivazione ? $data_attivazione : '-'; ?></h4>
               </div>
            </div>
        	<div class="col-md-4 col-sm-4 col-xs-4">
            	<div class="infoLaMiaCard">
                	<img src="{{theme}}img/card/punti.png" class="img-responsive" alt="">               
	               <# Saldo Punti #><br>
                   <h4><?=number_format($S->_infoUser->data->balanceData->balance_points,0,",","."); ?></h4>
               </div>
            </div>
        </div>
        
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
            	<br><br>
               <form id="movimentiForm" enctype="application/x-www-form-urlencoded" method="get" action="{{url la-mia-card}}">
               	<?php
                	$page = $_GET["page"]>0 ? $_GET["page"] : 0;
					$rowCount = isset($_GET["rC"]) && $_GET["rC"]>0 ? $_GET["rC"] : 10;
					?>
                	<input type="hidden" name="order" value="data">
                	<input type="hidden" name="orderBy" value="DESC">
                   <input type="hidden" name="page" value="<?=$page; ?>">
                   <div class="tableCard"><table class="table table-striped table-bordered">
                    	<thead>
                       	<tr>
                        		<th><# Data #></th>
                              <th><# Ora #></th>
                              <th><# Negozio #></th>
                              <th><# Operazione #></th>
                              <th><# Punti Caricati #></th>
                              <th><# Punti Scaricati #></th>
                        	</tr>
                       </thead>
                       <?php
						$postdata = array(
							"az" => "movimenti",
							"customer_id" => $S->_customer_id,
							"page" => $page,
							"rowCount" => $rowCount
						);
						$res = $S->FNET($postdata);
						$res = json_decode($res);
						//echo "<pre>";print_r($res);
						if( is_array($res->data->movimenti) && $res->data->movimenti->kind ){
							$arr = array();
							$arr[] = $res->data->movimenti;
							$res->data->movimenti = $arr;
						}
						foreach($res->data->movimenti as $row){
						?>
                      	<tr>
                        	<td><?=$row->data; ?></td>
                        	<td><?=$row->ora; ?></td>
                        	<td><?=$row->shop_name; ?></td>
                        	<td><?=$row->tipo_operazione; ?></td>
                        	<td><?=$row->caricati>0 ? $row->caricati : "-"; ?></td>
                        	<td><?=$row->scaricati>0 ? $row->scaricati : "-"; ?></td>
                       </tr>
                       <?php } ?>
                       <tfoot>
                       	<tr>
                        		<td colspan="6" valign="middle">
                                	<div class="row">
                                    	<div class="col-md-3 col-sm-3 col-xs-3">
                                        <select name="rC" onchange="updateMovements(0,true);">
                                                <?php
                                                $arr = array(10,25,50,100,250,500,1000);
                                                foreach($arr as $v){
                                                    $sel = $v==$rowCount ? ' selected' : '';
                                                ?>
                                                <option value="<?=$v; ?>"<?=$sel; ?>><?=$v; ?></option>
                                              <?php } ?>
                                            </select>
										</div>
                                      <div class="col-md-9 col-sm-9 col-xs-9"><div class="right">
                                      	<?php if($page>0){ ?><a href="#" onclick="updateMovements(-1,false);" class="btn btn-default">&laquo; <# Precedente #></a><?php } ?>
                                         <?php if(!$res->data->stopScroll){ ?><a href="#" onclick="updateMovements(1,false);" class="btn btn-default"><# Successivo #> &raquo;</a><?php } ?>
                                      </div></div>
                                  </div>
                              </td>
                        	</tr>
                       </tfoot>
                   </table></div>
               </form>
            </div>
        </div>
        
    </div><div class="col-md-1 col-sm-1 col-xs-1"></div></div>
	<br /><br />
</div>