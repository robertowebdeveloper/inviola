<?php
$postdata = array(
	"az" => 'infocardByCustomer',
	"customer_id" => $_SESSION["customer_id"]
);
$user = $S->FNET($postdata);
$user = json_decode( $user );

//echo "<pre>";print_r($user);
?>
<div class="bgWhite">
	<?php //print_r($S->Page); ?>
    <div class="row"><div class="col-md-10 col-sm-10 col-xs-10 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
        <h1 style="margin-bottom: 0;"><?=$S->Page["name"]; ?></h1>
        <h3 class="Gray"><# Inserire i dati della card su cui trasferire i punti #></h3>
        
        <script type="text/javascript"><!--
		var TP = {
			saldo: <?=$user->data->balanceData->balance_points; ?>,
			Add: function(punti_add){
				this.saldo = parseInt( this.saldo );
				var punti = parseInt( $("#punti_trasferire").val() );
				punti = punti+punti_add > this.saldo ? this.saldo : (punti+punti_add);
				$("#punti_trasferire").val( punti );
				$("#punti").val( punti );
			},
			
			Cancel: function(){
				$("#punti_trasferire").val(0);
				$("#punti").val('');	
			},
			
			Transfer: function(){
				var This = this;
				$("#TP_loader").removeClass('hide');
				$("#TP_msg").html('');
				$.ajax({
					url: System.sp,
					type: 'POST',
					timeout: System.timeout,
					data: $("#trasferisciPunti").serialize(),
					success: function(data){
						console.log(data);
						$("#TP_loader").addClass('hide');
						data = $.parseJSON(data);
						if( data.status ){
							$("#TP_msg").removeClass('Red').html( data.msg );
						}else{
							$("#TP_msg").addClass('Red').html( data.msg );
						}
					}
				});
			}
		};
		--></script>
        
        <form id="trasferisciPunti" enctype="application/x-www-form-urlencoded" method="post" action="{{url trasferisci-punti}}" role="form">
        	<input type="hidden" name="az" value="fnet">
            <input type="hidden" name="sub_az" value="trasferisciPunti">
            <input type="hidden" name="customer_id" value="<?=$_SESSION["customer_id"]; ?>">
            <!--div class="row">
            	<div class="col-md-5 col-sm-5 col-xs-12">
                	<div class="form-group">
                    	<br><br>
                		<h3 class="Viola"><# Numero card #></h3>
	                   <input type="text" name="cardReceiver" class="form-control" placeholder="<# INSERISCI IL NUMERO #>">
    	            </div>
        	    </div>
                <div class="col-md-7 col-sm-7 hidden-xs">
                	<img src="{{theme}}img/card-member-fff.jpg" alt="" class="img-responsive pull-right">
                </div>
            </div-->
            <div class="row">
            	 <div class="col-md-6 col-sm-6 col-xs-12">
                 	<br><br>
                 	<h3 class="Viola"><# Numero card #></h3>
                   <input type="text" name="cardReceiver" class="form-control" placeholder="<# INSERISCI IL NUMERO #>">
                 	<br><br>
                </div>
                <div class="col-md-6 col-sm-6 xs-hidden"></div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h3 class="Viola uppercase"><# Punti da trasferire #></h3>
                    <span class="Gray"><# Cliccare una o più volte sui bottoni per selezionare la quantità di punti da trasferire #></span>
                    <br><br>
                </div>
			</div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                	<a class="Button" href="#" onclick="TP.Add(1000);">+1.000</a>
                    &nbsp;&nbsp;
                    <a class="Button" href="#" onclick="TP.Add(5000);">+5.000</a>
                    &nbsp;&nbsp;
                    <input type="hidden" id="punti_trasferire" name="points" value="0">
                    <input type="text" class="form-control" id="punti" name="punti" disabled="disabled" placeholder="<# PUNTI PARZIALI DA TRASFERIRE #>" style="width: 300px; display: inline-block;">
                    &nbsp;&nbsp;
                    <a class="Button" href="#" onclick="TP.Transfer();"><# Trasferisci i punti #></a>
                    &nbsp;&nbsp;
                    <a class="Button Gray" href="#" onclick="TP.Cancel();"><# Annulla #></a>
                    &nbsp;&nbsp;
                    <img id="TP_loader" src="{{theme}}img/loaders/3.gif" alt="" class="hide">
                </div>
            </div>
            <div class="row"><div class="col-md-12 col-sm-12 col-xs-12"><div class="center">
            	<br>
            	<span id="TP_msg"></span>
            </div></div></div>
		</form>        
    </div><div class="col-md-1 col-sm-1 col-xs-1"></div></div>
	<br /><br />
</div>