<?php
$postdata = array(
	"az" => 'wishlist',
	"customer_id" => $S->_customer_id
);
$list = $S->FNET($postdata);
$list = json_decode( $list );
$list = $list->data;

//echo "<pre>";print_r($list);
?>
<div class="bgWhite">
	<?php //print_r($S->Page); ?>
    <div class="row"><div class="col-md-10 col-sm-10 col-xs-10 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
        <h1><?=$S->Page["name"]; ?></h1>
        
        <div class="row"><div class="col-md-12 col-sm-12 col-xs-12">
        	<?php if( count($list)==0 ){ ?>
           		<span class="Title Red"><# Nessun premio in elenco #></span> 
           <?php }else{ ?>               
               <?php foreach($list as $row){ ?>
               	<div class="row">
                		<div class="col-md-3 col-sm-3 col-xs-3">
                       	<div class="fotoPremio">
                        		<div class="foto"><a href="<?=$S->getUrl('catalogo-premi',$row->id); ?>"><img src="<?=$row->image; ?>" alt="<?=$row->name; ?>" class="img-responsive"></a></div>
                              <div class="shadow"><img src="{{theme}}img/shadow_premi.png" alt="" class="img-responsive"></div>
                        	</div>
                       </div>
                       <div class="col-md-9 col-sm-9 col-xs-9"><div class="relative">
                       	<b class="uppercase"><?=$row->name; ?></b>
                          <br>
                        	<?=$row->description; ?>
                          
                          <?php //$row->saldo = 500; //for test ?>
                          <div class="premioBarInfo">
                          		<div class="punti uppercase"><?=$row->points; ?> <# Punti #></div>
						  		<?php if( $row->saldo >= $row->points ){
									echo $S->FormRichiestaPremio($row->id,$row->prizeCode);
									?>
                                  <a href="#" onclick="System.richiediPremio(<?=$row->id; ?>);" target="_blank" class="Button Big btnRichiestaPremio"><# Richiedi Premio #></a>
                                  
                              <?php }else{
								  	$mancano = $row->points - $row->saldo;
									$perc = round( $row->saldo*100/$row->points );
									$perc = $perc>100 ? 100 : $perc;
									$perc = round($perc);
								  	?>
                              	<div class="barPunti">
                                		<div class="barPuntiCont" style="width: <?=$perc; ?>%;"></div>
                                      <span>Ti mancano <?=$mancano; ?> punti</span>
                                	</div>
                              <?php } ?>
                          </div>
                       </div></div>
                	</div>
                   <hr>
               <?php } ?>
           <?php } ?>           
        </div></div>
        
    </div><div class="col-md-1 col-sm-1 col-xs-1"></div></div>
	<br /><br />
</div>