<?php
$subtext = false;
switch($S->Page["k"]){
	case "disegni-per-bambini":
		$type = 'disegni-bambini';
		$img_arr = array("w"=>310,"h"=>200,"m"=>"square");
		break;
	case "cruci-viola":
		$type = 'cruci';
		$img_arr = array("w"=>300,"h"=>160,"m"=>"square");
		break;
	case "trova-differenze":
		$type = 'trova-differenze';
		$subtext = "(10 a disegno)";
		$img_arr = array("w"=>300,"h"=>100,"m"=>"square");
		break;
	case "trova-parole":
		$type = 'trova-parole';
		$img_arr = array("w"=>300,"h"=>300,"m"=>"square");
		break;
	case "unisci-i-punti":
		$type = 'unisci-i-punti';
		$img_arr = array("w"=>215,"h"=>215,"m"=>"square");
		break;
}
?>
<div class="bgWhite">
	<?php //print_r($S->Page); ?>
    <div class="row"><div class="col-md-10 col-sm-10 col-xs-10 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
        <h1<?=$subtext ? ' class="nmb"' : ''; ?>><?=$S->Page["name"]; ?></h1>
        <?php if( $subtext ){ ?>
        	<h3 class="Gray uppercase"><?=$S->W($subtext); ?></h3>
            <br>
        <?php } ?>
        
        <div class="row">
        	<?php
			$q = "SELECT * FROM `{$S->_db_prefix}downloads` WHERE type='{$type}' AND deleted IS NULL";
			$list = $S->cn->Q($q,true);
			foreach($list as $v){
				$file = $S->pathFile($v["id_file"]);
				$img = $S->Img($v["id_file"],$img_arr);
				
				if( $S->Page["k"]=="unisci-i-punti" ){
					list($tmp) = explode(".",$file);
					$thumb = $tmp . "-thumb.png";
					?>
                	<div class="col-md-3 col-sm-3 col-xs-3"><div class="fbCoverItem">
	                	 <div class="preview" style="border: 1px solid #ccc;"><img src="<?=$thumb; ?>" class="img-responsive"></div>
                        <div class="shadow"><img src="{{theme}}img/shadow_premio_img.png" alt="" class="img-responsive"></div>
                        <br>
                       <a href="<?=$file; ?>" target="_blank" class="Button"><# Download #></a>
                   </div></div>
               <?php }else{ ?>
                    <div class="col-md-4 col-sm-4 col-xs-12"><div class="fbCoverItem">
                        <div class="preview"><img src="<?=$img; ?>" class="img-responsive"></div>
                        <div class="shadow"><img src="{{theme}}img/shadow_premio_img.png" alt="" class="img-responsive"></div>
                        <br>
                        <a href="<?=$file; ?>" target="_blank" class="Button"><# Download #></a>
                        <!--table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                            <td rowspan="2" width="70%" valign="middle" align="center"><span style="color:#909090;"></span></td>
                               <td width="30%"><a href="<?=$file; ?>" target="_blank" class="Button"><# Download #></a></td>
                            </tr>
                        </table-->
                        <br><br>
                    </div></div>
           <?php
			   }
			}
			?>
        </div>
        
    </div><div class="col-md-1 col-sm-1 col-xs-1"></div></div>
	<br /><br />
</div>