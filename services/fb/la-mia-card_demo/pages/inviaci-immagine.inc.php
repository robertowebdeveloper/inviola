<?php
$img_upload = false;
if( isset($_FILES["foto"]) && strlen($_FILES["foto"]["tmp_name"])>0 ){
	$img_upload = true;
	
	$info = getimagesize($_FILES["foto"]["tmp_name"]);
	if( $info[2]==1 || $info[2]==2 || $info[2]==3 ){ //1=>GIF, 2=>JPG, 3=>PNG
		$tmp = explode(".",$_FILES["foto"]["name"]);
		$ext = strtolower( array_pop($tmp) );
		
		$q = "INSERT INTO `{$S->_db_prefix}file` (`name`,`size`,`type`,`ext`,`temp`) VALUES ('{$_FILES[foto][name]}',{$_FILES[foto][size]},'{$_FILES[foto][type]}','{$ext}',1)";
		$S->cn->Q($q);
		$id_file = $S->cn->last_id;
		$name = "{$id_file}.{$ext}";
		
		$subdir = substr($name,0,1);
		$pathdir = "_public/file/{$subdir}";
		$pathdir_here = "{$pathdir}";
		if( !file_exists($pathdir_here) ){
			@mkdir( $pathdir_here );	
		}
		
		if( !move_uploaded_file($_FILES["foto"]["tmp_name"], "{$pathdir_here}/{$name}" ) ){
			$q = "DELETE * FROM `{$S->_db_prefix}file` WHERE id = {$id_file}";
			$S->cn->Q($q);
			$foto_upload_status = false;
			$foto_msg = "Errore nel caricamento dell'immagine";
		}else{
			$q = "SELECT id FROM utenti WHERE customer_id = {$_SESSION[customer_id]} LIMIT 1";
			$id_utente = $S->cn->OF($q);
			
			$q = "INSERT INTO `{$S->_db_prefix}users_gallery` (id_file,id_user) VALUES ({$id_file},{$id_utente})";
			$S->cn->Q($q);
			$foto_upload_status = true;
		}	
	}else{
		$foto_upload_status = false;
		$foto_msg = "Formato immagine non valido";
	}
}
?>
<div class="bgWhite" style="background:url({{root}}_ext/themes/default/img/logo_big_background2.png) right center no-repeat #FFF;"><div class="row"><div class="col-md-1 col-sm-1 col-xs-1"></div><div class="col-md-10 col-sm-10 col-xs-10">
	<h1>Inviaci le tue foto</h1>
    
    <form id="inviaciFoto" enctype="multipart/form-data" method="post" action="{{url inviaci-foto}}" class="form-horizontal" role="form">
    	<input type="hidden" name="s" value="1">
    	<div class="Title2"><# Carica una immagine in formato JPG,PNG o GIF. #></div>
        <div class="Gray"><# Dimensione massima accettata 2MB #></div>
        <br>
        <!--div class="row">
        	<div class="col-md-1 col-sm-2 col-xs-6">
            	<label class="Title2">
                	<input type="radio" name="privacy" value="1" style="vertical-align: middle; margin-right: 5px;" onclick="Upload.Status(true);">
                    <span style="vertical-align: middle;"><# SI #></span>
                </label>
             </div>
             <div class="col-md-1 col-sm-2 col-xs-6">
            	<label class="Title2">
                	<input type="radio" name="privacy" value="2" style="vertical-align: middle; margin-right: 5px;" onclick="Upload.Status(false);">
                    <span style="vertical-align: middle;"><# NO #></span>
                </label>
            </div>
            <div class="col-md-10 col-sm-8"></div>
		</div-->
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12"><div class="relative">
            	 <input type="button" class="Button Big" value="<# Carica Immagine #>" onclick="Upload.Error();">
                <img id="uploadLoader" class="hide" style="vertical-align: middle;" src="{{theme}}img/loaders/3.gif" alt="">
                <input type="file" id="uploadFotoBtn" name="foto" onchange="Upload.Start();" />
				<script type="text/javascript"><!--
                var Upload = {
					 Start: function(){
						 $('#msg_privacy').addClass('hide');
						 $("#uploadLoader").removeClass('hide');
						 $("#inviaciFoto").submit();
					 },
    	            Error: function(){
        	            $('#msg_privacy').removeClass('hide');
	                },
					 Status: function(show){
						 $('#msg_privacy').addClass('hide');
						if( show ){
							$("input[name='foto']").css('display','block');
						}else{
							$("input[name='foto']").css('display','none');
						}
					 }
                };
                --></script>
                <span id="msg_privacy" class="Red hide"><# &Egrave; necessario accettare la normativa privacy #></span>
                <?php if($img_upload){ ?>
                	<div>
                       <?php if($foto_upload_status){ ?><br><span class="uppercase"><# La tua foto &egrave; stata inviata correttamente e sarà visibile previa approvazione del nostro staff. #></span><br>
                       <?php }else{ ?><b class="Red"><?=$foto_msg; ?></b><?php } ?>
                   </div>
                <?php } ?>
            </div></div>
        </div>
    </form>
    <br>
</div><div class="col-md-1 col-sm-1 col-xs-1"></div></div></div>

<?php if($_GET["id"]>0 ){ //Pagina singolo utente
	//$avatar = $S->getAvatar(true,$_GET["id"]);
	$q = "SELECT id_avatar, customer_id FROM utenti WHERE id = {$_GET[id]}";
	$v = $S->cn->OQ($q);
	$c_id = $v["customer_id"];
	$id_avatar = $v["id_avatar"]>0 ? $v["id_avatar"] : false;
	$postdata = array("az"=>"infocardByCustomer","customer_id"=>$c_id);
	$user = $S->FNET($postdata);
	$user = json_decode($user);
	?>
	<div class="row">
    	<div class="col-md-1 col-sm-1 col-xs-1"></div>
        <div class="col-md-10 col-sm-10 col-xs-10">
        	 <div class="bread1"><a href="{{url inviaci-foto}}">&laquo; {{urlname inviaci-foto}}</a></div>
            <h1>
            	<?php if($id_avatar){ ?><img src="<?=$S->Img($id_avatar,array("w"=>90,"h"=>90,"m"=>"square") ); ?>" class="img-responsive pull-right" alt="">
                <?php }else{ ?><img src="{{theme}}img/avatar.png" class="img-responsive pull-right" alt=""><?php } ?>
                <# Le foto degli utenti #>: <span class="Red"><?=$user->data->personalInfo->name." ".$user->data->personalInfo->surname; ?></span>
           </h1>
           <br><br>
            <div class="row"><div class="col-md-12 col-sm-12 col-xs-12"><div class="center">
            	 <?php
				 $q = "SELECT * FROM `{$S->_db_prefix}users_gallery` WHERE id_user = {$_GET[id]} AND approved IS NOT NULL ORDER BY `timestamp` ASC";
				 $list = $S->cn->Q($q,true);
				 ?>
            	 <script type="text/javascript"><!--
                $(document).ready(function(e) {
					var controls_on = <?=count($list)>1 ? 'true' : 'false'; ?>;
					$("#UserGalleryCont").bxSlider({
						pager: false,
						controls: controls_on
					});
                });
                --></script>
                
                <div id="UserGallery">
                	<div id="UserGalleryCont"><?php
						foreach($list as $li){
						?>
					   <div class="center"><img src="<?=$S->Img($li["id_file"],array("w"=>930,"h"=>700,"m"=>'prop4',"bg"=>'eeeeee') ); ?>" alt="" style="margin: 0 auto;"></div>
                   <?php } ?></div>
                   <div class="shadow"><img src="{{theme}}img/shadow_store.png" class="img-responsive" alt=""></div>
                </div>
                
           </div></div></div>
        </div>
    	<div class="col-md-1 col-sm-1 col-xs-1"></div>
    </div>
<?php }else{ //Pagina raccoglitore 
	$q = "SELECT * FROM `{$S->_db_prefix}users_gallery` WHERE approved IS NOT NULL GROUP BY id_user ORDER BY approved ASC, RAND()";
	$list = $S->cn->Q($q,true);
	if( count($list)==0 ){
	?>
		<div style="width: 100%; height: 400px; background: url({{root}}_ext/themes/default/img/inviaci-foto.png) center center no-repeat;"></div>
	<?php }else{ ?>
		<div class="row">
			<div class="col-md-1 col-sm-1 col-xs-1"></div>
			<div class="col-md-10 col-sm-10 col-xs-10">
				<h1><# Le foto degli utenti #></h1>
				<div class="row">
					<?php foreach($list as $item){
						//$avatar = $S->getAvatar(true,$item["id_user"]);
						$q = "SELECT id_avatar, customer_id FROM utenti WHERE id = {$item[id_user]}";
						$v = $S->cn->OQ($q);
						$c_id = $v["customer_id"];
						$id_avatar = $v["id_avatar"]>0 ? $v["id_avatar"] : false;
						$postdata = array("az"=>"infocardByCustomer","customer_id"=>$c_id);
						$user = $S->FNET($postdata);
						$user = json_decode($user);
						?>
						<div class="col-md-3 col-sm-4 col-xs-6"><div class="fotoUtentiItem">
						<div class="foto"><a href="<?=$S->getUrl('inviaci-foto',$item["id_user"]); ?>"><img src="<?=$S->Img($item["id_file"],array( "w"=>230,"h"=>130,"m"=>"square","bg"=>"FFF" ) ); ?>" alt="" class="img-responsive"></a></div>
							<div class="shadow"><img src="{{theme}}img/shadow_premio_img.png" class="img-responsive" alt=""></div>
						   <table width="100%" cellspacing="0" cellpadding="0">
								<tbody>
									<tr>
										<td width="20%">
										<?php if($id_avatar){ ?>
												<img src="<?=$S->Img($id_avatar,array("w"=>45,"h"=>45,"m"=>"square") ); ?>" class="img-responsive" alt="">
											<?php }else{ ?>
												<img src="{{theme}}img/avatar.png" class="img-responsive" alt="">
										  <?php } ?>  
									  </td>
									  <td width="5%">&nbsp;</td>
										<td width="75%" valign="middle" bgcolor="#eee" align="left">
											<# LE FOTO DI #><br>
										  <span class="Red uppercase"><?=$user->data->personalInfo->name." ".$user->data->personalInfo->surname; ?></span>
										</td>
									</tr>
									<tr>
								</tbody>
							</table>
                          <br><br>
					   </div></div>
				   <?php } ?>
				</div>
			</div>
			<div class="col-md-1 col-sm-1 col-xs-1"></div>
		</div>
		<br>
	<?php }
} ?>