<?php
$service_customer_id = $S->P('service-card-customer-id');
$Store = false;
if( isset($_GET["id"]) && $_GET["id"]>0 ){
	$id = $_GET["id"];
	$q = "SELECT * FROM `{$S->_db_prefix}stores` WHERE enable=1 AND fn_id={$id} LIMIT 1";
	$Store = $S->cn->OQ($q);
}

if( $Store ){ //Scheda negozio singolo
	$postdata = array("az"=>'schedaNegozio',"customer_id"=>$service_customer_id,"shop_id"=>$Store["fn_id"]);
	$Scheda = $S->FNET($postdata);
	$Scheda = json_decode( $Scheda );
	$Scheda = $Scheda->data;
	//echo "<pre>";print_r($Scheda);
?>
	<div class="bgWhite"><div class="row">
    	<div class="col-md-1 col-sm-1 col-xs-1"></div>
    	<div class="col-md-10 col-sm-10 col-xs-10"><div class="NegozioScheda">
        	<div class="bread1"><a href="{{url negozi}}">&laquo; {{urlname negozi}}</a></div>
        	<br>
	    	<h2><?=$Scheda->name; ?></h2>
           <?php
          $fotoBig = false; 
		   if( file_exists("_public/stores_img/" . $Scheda->id . ".jpg") ){
			   $fotoBig = true;
			   ?>
               <br>
           		<div class="foto">
                	<img src="_public/stores_img/<?=$Scheda->id; ?>.jpg" alt="" class="img-responsive">
                   <div class="shadow"><img src="{{theme}}img/shadow_store.png" alt="" class="img-responsive"></div>
				</div>
           <?php }else{ ?>
           		<div><br><img src="<?=$Scheda->image_url; ?>" class="img-responsive" alt=""><br></div>
           <?php } ?>
           <div class="clearfix"></div>
           
           
           <div class="row">
           		<div class="col-md-6 col-sm-6 col-xs-12"><div class="NegozioScheda_BoxAddress">
               	<h3 class="uppercase"><span class="Red"><# Indirizzo #>:</span> <span><?=$Scheda->address_full; ?></span></h3>
               	<h3 class="uppercase"><span class="Red"><# Telefono #>:</span> <span><?=strlen($Scheda->telephone)>0 ? $Scheda->telephone : '-'; ?></span></h3>
               	<h3 class="uppercase"><span class="Red"><# E-mail #>:</span> <span><?php
                		$email = strlen($Scheda->email_address)>0 ? $Scheda->email_address : false;
						if( $email ){ ?><a href="mailto:<?=$email; ?>"><?=$email; ?></a><?php }else{ ?>-<?php } ?>
					</span></h3>
               </div></div>
               <div class="col-md-6 col-sm-6 col-xs-12"><div class="NegozioScheda_BoxPonderazione">
               	<div class="center uppercase Red"><# Ponderazione #>:</div>
                	<h3 class="center uppercase"><span class="Red"><?=$Scheda->weightChargePointMoney; ?> &euro;</span> <span><# corrisponde a #></span> <span class="Red"><?=$Scheda->weightChargePointPoints; ?> punti</span></h3>
               </div></div>
           </div>
           
           <br><br>
           
           <div class="row">
               <div class="col-md-12 col-sm-12 col-xs-12"><div class="Descr">
                   <?=empty($Store["html"]) ? $Scheda->description : $Store["html"]; ?>
               </div></div>
           </div>
           <br><br>
           
		</div></div>
    	<div class="col-md-1 col-sm-1 col-xs-1"></div>
    </div></div>
<?php
}else{ //Elenco negozi
	$postdata = array("az"=>'elencoNegozi',"customer_id"=>$service_customer_id);
	$list = $S->FNET($postdata);
	$list  = json_decode($list);
	$Negozi = $list->data->shops;
	//echo "<pre>";print_r($list);exit;
	$Categorie = $list->data->categorie;
	//echo "<pre>";print_r($Categorie);exit;
	
	foreach($Negozi as $item){
		$q = "SELECT id FROM `{$S->_db_prefix}stores` WHERE fn_id = {$item->id} LIMIT 1";
		$tmp_id = $S->cn->OF($q);
		if( !$tmp_id ){
			$q = "INSERT INTO `{$S->_db_prefix}stores` (fn_id,is_new) VALUES ({$item->id},1)";
			$S->cn->Q($q);
			$tmp_id = $S->cn->last_id;
		}
		$q = "UPDATE `{$S->_db_prefix}stores` SET
			fn_description = '" . addslashes($item->description) . "',
			fn_imageURL = '" . addslashes($item->imageURL) ."',
			fn_companyName = '" . addslashes($item->companyName) ."',
			fn_addressPrefix = '" . addslashes($item->addressPrefix) ."',
			fn_country = {$item->country},
			fn_geoLevel1 = {$item->geoLevel1},
			fn_geoLevel2 = {$item->geoLevel2},
			fn_geoLevel3 = {$item->geoLevel3},
			fn_geoLevel4 = {$item->geoLevel4},
			fn_geoLevel5 = {$item->geoLevel5},
			fn_address = '" . addslashes($item->address) ."',
			fn_geoLat = '" . addslashes($item->geoLat) . "',
			fn_geoLong = '" . addslashes($item->geoLong) . "',
			fn_contactName = '" . addslashes($item->contactName) . "',
			fn_localCurrency = {$item->localCurrency},
			fn_addressNumber = '" . addslashes($item->addressNumber) ."',
			fn_zip = '" . addslashes( $item->zip ) . "',
			fn_taxNumber = '" . addslashes( $item->taxNumber ) . "',
			fn_mail = '" . addslashes( $item->mail ) . "',
			fn_telephone = '" . addslashes( $item->telephone ) . "',
			fn_contactEmail = '" . addslashes( $item->contactEmail ) . "',
			fn_contactMobile = '" . addslashes( $item->contactMobile ) . "',
			fn_contactTelephone = '" . addslashes( $item->contactTelephone ) . "',
			fn_shopCategory1 = '" . addslashes( $item->shopCategory1 ) . "',
			fn_shopCategory2 = '" . addslashes( $item->shopCategory2 ) . "'
			
			WHERE id = {$tmp_id}
		";
		$S->cn->Q($q);
	}
	
	?>
	<script type="text/javascript"><!--
	$(document).ready(function(e) {
		<?php if( isset($_GET["t"]) && $_GET["t"]=="list" ){ ?>
			$("#FiltriEserciziList").click();
		<?php }else{ ?>
			$("#FiltriEserciziMap").click();
		<?php } ?>
	});
	var FiltriEsercizi = function(w){
		$(".FiltriEsercizi_a").removeClass('fired');
		$("#EserciziMap").addClass('hide');
		$("#EserciziList").addClass('hide');
		
		switch(w){
			case 'map':
				$("#FiltriEserciziMap").addClass("fired");
				$('#EserciziMap').removeClass('hide');
				break;
			case 'list':
				$("#FiltriEserciziList").addClass("fired");
				$('#EserciziList').removeClass('hide');
				break;
		}
	}
	
	function initialize() {
		var mapOptions = {
		  center: new google.maps.LatLng(43.7794353,11.2845402),
		  zoom: 15,
		  mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		var map = new google.maps.Map(document.getElementById("map"),
			mapOptions);
		var geocoder = new google.maps.Geocoder();
		
		var img = '{{root}}_ext/themes/default/img/geotag.png';
		<?php	
		foreach($Negozi as $item){
			if( $item->geoLat && $item->geoLong && ( !($_GET["cat"]>0) || $_GET["cat"]==$item->shopCategory1 || $_GET["cat"]==$item->shopCategory2 ) ){
			?>
			var marker_<?=$item->id; ?> = new google.maps.Marker({
				position: new google.maps.LatLng(<?=$item->geoLat; ?>,<?=$item->geoLong; ?>),
				title: ' ',
				clickable: true,
				icon: img
			});
	
			marker_<?=$item->id; ?>.setMap( map );
	
			var infoWindow = new google.maps.InfoWindow({
				content: "<div><b><?=$item->name; ?></b> - <a href=\"<?=$S->getUrl('negozi',$item->id); ?>\">&raquo;</a></div>"
			});
			 
			// Add listner for marker. You can add listner for any object. It is just an example in which I am specifying that infowindow will be open on marker mouseover
			google.maps.event.addListener(marker_<?=$item->id; ?>, "click", function() {
				infoWindow.open(map, marker_<?=$item->id; ?>);
			});			
			
			<?php }else if( false && strlen($item->address)>0 && ( strlen($item->zip)==5 ) ){ ?>
				geocoder.geocode( {'address': '<?="{$item[indirizzo]}, {$item[cap]} {$item[citta]}"; ?>'}, function(results,status) {
					if (status == google.maps.GeocoderStatus.OK){
						$.ajax({
							type: 'post',
							url: '_ext/scripts/ajax_services.php',
							data: 'az=updateGeoLocationStore&id=<?=$item["id"]; ?>&location=' + results[0].geometry.location,
							success: function(data){
								console.log(data);
							}
						});
					} else {
						//alert("Problema nella ricerca dell'indirizzo: " + status);
					}
				});
			
			<?php
			}
		}
		?>
	}
	$(document).ready(function(e) {
	   google.maps.event.addDomListener(window, 'load', initialize);
	});
	var ShowMap = function(type){
		var field = $("input[name='t']").val( type );
		if( type=="map"){
			$("#listBtn").removeClass('ButtonViola').addClass('Button');
			$("#mapBtn").removeClass('Button').addClass('ButtonViola');
			$("#NegoziMap").slideDown();
		}else{
			$("#mapBtn").removeClass('ButtonViola').addClass('Button');
			$("#listBtn").removeClass('Button').addClass('ButtonViola');
			$("#NegoziMap").slideUp();
		}
	};
	--></script>
	
	<div class="bgWhite" style="background:url({{root}}_ext/themes/default/img/logo_big_background2.png) right center no-repeat #FFF;"><div class="row"><div class="col-md-1 col-sm-1 col-xs-1"></div><div class="col-md-10 col-sm-10 col-xs-10">
		<h1>Negozi Affiliati</h1>
		
		<form id="negoziCerca" enctype="application/x-www-form-urlencoded" method="get" action="{{url negozi}}" class="form-horizontal" role="form">
			<input type="hidden" name="s" value="1">
			<input type="hidden" name="t" value="list">
			
			<div class="row">
				<div class="col-md-2 col-sm-4 col-xs-4"><a id="listBtn" href="#" onclick="ShowMap('list');" class="Button Large Big" style="display: block;">LISTA</a></div>
				<div class="col-md-2 col-sm-4 col-xs-4"><a id="mapBtn" href="#" onclick="ShowMap('map');" class="ButtonViola Large Big" style="display: block;">MAPPA</a></div>
			</div>
			<div class="clearfix"><br /></div>
			<div class="row">
				<div class="col-md-4 col-sm-4 col-xs-6">
					<select name="cat" class="form-control" onchange="$('#negoziCerca').submit();">
						<option value="0"><# FILTRA PER CATEGORIA #></option>
						<?php
						 foreach($Categorie as $v){
							$sel = $_GET["cat"]==$v->id ? ' selected' : '';
							?><option value="<?=$v->id; ?>"<?=$sel; ?>><?=$v->description; ?></option><?php
						 }
						 ?>
					</select>
					<br>
				 </div>
				 <div class="col-md-8 col-sm-8 col-xs-6"></div>
			</div>
		</form>
		<br>
	</div><div class="col-md-1 col-sm-1 col-xs-1"></div></div></div>
	
	
	<div id="NegoziMap">
		<div class="shadow"></div>
		<div id="map"></div>
		<div class="shadow_ext"></div>
		
	</div>
	<br><br>
	
	<div id="NegoziList">
		<div class="row">
			<div class="col-md-1 col-sm-1 col-xs-1"></div>
			<div class="col-md-10 col-sm-10 col-xs-10"><div class="row">
				<?php
				 $iNegozi=0;
                foreach($Negozi as $item){
					if( !($_GET["cat"]>0) || $_GET["cat"]==$item->shopCategory1 || $_GET["cat"]==$item->shopCategory2 ){
					?>
					<div class="col-md-6 col-sm-6 col-xs-12"><a href="<?=$S->getUrl('negozi',$item->id); ?>" class="NegozioItem">
						<span class="row">
						   <span class="col-md-4 col-sm-4 col-xs-4"><img src="<?=$item->image; ?>" alt="" class="img-responsive"></span>
						   <span class="col-md-8 col-sm-8 col-xs-8">
                            <span class="nome"><?=$item->name; ?></span><br>
							  <span class="indirizzo"><?=$item->addressPrefix; ?> <?=$item->address; ?>, <?=$item->addressNumber; ?><br>
									<?=$item->zip; ?> <?=$item->city; ?>
							  </span>
							  
						   </span>
					   </span>
				   </a></div>
				<?php
					$iNegozi++;
                	}
				}
				if( $iNegozi==0 ){
				?>
	               <div class="Title2 Red"><# Nessun negozio trovato #></div>
               <?php } ?>
			</div></div>
			<div class="col-md-1 col-sm-1 col-xs-1"></div>
		</div>
	</div>
<?php } ?>