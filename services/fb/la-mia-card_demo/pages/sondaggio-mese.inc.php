<?php
$has_rate = false;
//$S->cn->query_error = true;
if( isset($_POST["id_utente"]) && $_POST["id_utente"]>0 ){
	$q = "SELECT * FROM `{$S->_db_prefix}polls_answers_rate` WHERE id_user={$_POST[id_utente]} AND id_answer = {$_POST[id_answer]}";
	$S->cn->Q($q);
	if( $S->cn->n==0 ){
		$q = "INSERT INTO `{$S->_db_prefix}polls_answers_rate` (id_user,id_answer) VALUES ({$_POST[id_utente]},{$_POST[id_answer]})";
		$S->cn->Q($q);
		$has_rate = true;
	}
}
?>
<div class="bgWhite" style="background:url({{root}}_ext/themes/default/img/logo_big_background2.png) right center no-repeat #FFF;"><div class="row"><div class="col-md-1 col-sm-1 col-xs-1"></div><div class="col-md-10 col-sm-10 col-xs-10">
	<h1>Il sondaggio del mese</h1>
    
    <?php if( $has_rate ){ ?>
    	<b class="Title Red"><# Grazie per aver partecipato al sondaggio! Il tuo voto &egrave; stato registrato. #></b>
        <br><br>
    <?php } ?>
    
    <?php
	$qBase = "SELECT * FROM `{$S->_db_prefix}polls` WHERE
		enable_on=1 AND deleted IS NULL
		AND
		`from` <= NOW()";
	$order = "ORDER BY `from` ASC LIMIT 1";
	
	$q = $qBase . " AND NOW() <= `to`" . $order;		
	$poll = $S->cn->OQ($q);
	if( $poll==-1 ){ //Se non trova niente prende l'ultime
		$q = $qBase . $order;
		$poll = $S->cn->OQ($q);
		$poll["scaduto"]=1;
	}else{
		$poll["scaduto"]=0;	
	}
	
	$answers = array();
	$user_rate = false;
	if( $poll!=-1 ){
		$q = "SELECT * FROM `{$S->_db_prefix}polls_answers` WHERE id_poll = {$poll[id]}";
		$answers = $S->cn->Q($q,true);
		
		//Cerca id utente e se ha già votato
		$q = "SELECT id FROM utenti WHERE customer_id = {$_SESSION[customer_id]}";
		$id_utente = $S->cn->OF($q);
		
		foreach($answers as $ans){		
			$q = "SELECT * FROM `{$S->_db_prefix}polls_answers_rate` WHERE id_answer={$ans[id]} AND id_user = {$id_utente}";
			$S->cn->Q($q);
			if( $S->cn->n>0 && !$user_rate){
				$user_rate = $ans["id"];
			}
		}
	}
	
	if( count($answers)>0 ){
		//calcolo totale voti
		$q = "SELECT * FROM `{$S->_db_prefix}polls_answers_rate` r INNER JOIN `{$S->_db_prefix}polls_answers` a ON r.id_answer = a.id WHERE a.id_poll = {$poll[id]}";
		$S->cn->Q($q);
		$tot_rates = $S->cn->n;
	?>
        <form id="sondaggioForm" enctype="application/x-www-form-urlencoded" method="post" action="{{url sondaggio-mese}}" class="form-horizontal" role="form">
        	<input type="hidden" name="id_utente" value="<?=$id_utente; ?>">
            <input type="hidden" name="id_poll" value="<?=$poll["id"]; ?>">
            <div class="Title2" style="color:#462a9b;"><?=$poll["question"]; ?></div>
            <br /><br />
            <div class="row">
            <?php foreach($answers as $ans){
				$q = "SELECT * FROM `{$S->_db_prefix}polls_answers_rate` r WHERE r.id_answer = {$ans[id]}";
				$S->cn->Q($q);
				$rates = $S->cn->n;
				$perc = round($rates*100/$tot_rates);
				?>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <input type="radio" name="id_answer" value="<?=$ans["id"]; ?>"<?=$user_rate || $poll["scaduto"] ? ' disabled' : ''; ?><?=$user_rate==$ans["id"] ? ' checked' : ''; ?>>
                    <span style="color:#747474; font-size: 20px; margin-left: 15px;"><?=$ans["answer"]; ?></span>
                    <div>
                        <div style=" width: 400px; display:inline-block; margin-left: 30px;height:40px; border: 1px solid #9a9a9a;">
                            <div style="width: <?=$perc; ?>%; <?=$perc>0 ? '' : ' display: none;'; ?> height: 38px; background-color: #462a9b;"></div>
                        </div>
                        <div style="display: inline-block; line-height: 40px; color:#462a9b; font-size: 30px; vertical-align: top; height: 40px; margin-left: 10px;"><?=$perc>0 ? "{$perc}%" : ''; ?></div>
                    </div>
                    <br />
                 </div>
            <?php } ?>
            </div>
            
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12"><div class="relative">
                	 <?php if($user_rate && !$has_rate){ ?>
                     	<span class="Title Red"><# Hai già espresso il tuo voto per questo sondaggio #></span>
                    <?php }else if(!$user_rate && !$poll["scaduto"]){ ?>
	                    <input type="submit" class="Button Big" value="<# Vota #>">
                    <?php } ?>
                </div></div>
            </div>
        </form>
	 <?php }else{ ?>
     	<h1 class="Red"><# Spiacente, nessun sondaggio presente #></h1>
    <?php } ?>
    <br>
</div><div class="col-md-1 col-sm-1 col-xs-1"></div></div></div>