<?php
$is_raccoglitore = true;

if( isset($_GET["id"]) && $_GET["id"]>0 ){
	$id = $_GET["id"];
	$q = "SELECT * FROM `{$S->_db_prefix}downloads` WHERE id = {$_GET[id]} AND type='videopremium' AND deleted IS NULL ORDER BY `order` ASC";
	$Scheda = $this->cn->OQ($q);
	
	$is_raccoglitore = $Scheda==-1 ? true : false;
}

if( $is_raccoglitore ){
	$q = "SELECT * FROM `{$S->_db_prefix}downloads` WHERE type='videopremium' AND deleted IS NULL ORDER BY `order` ASC";
	$list = $S->cn->Q($q,true);
?>
	<!--script type="text/javascript"><!--
	$(document).ready(function(e) {
		System.loadNews();
	});
	-></script-->
    
    <div class="bgWhite"><div class="row">
    	<div class="col-md-10 col-sm-10 col-xs-10 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
	        <h1><?=$S->Page["name"]; ?></h1>
    	    <!--div id="NewsCont" class="row" data-page="0"></div>
        	<div class="row"><div class="col-md-12"><div id="loaderNews" class="center"><img src="{{theme}}img/loaders/1.gif" alt=""></div></div>
	        <div class="row"><div class="col-md-12"><div id="NewsContNext" class="hide"><a class="Button Big" href="#" onclick="System.loadNews();"><# CARICA ALTRI #></a></div></div></div-->
            
            <?php
			if( count($list)==0 ){
			?>        
	        	<span class="Title uppercase"><# Al momento non è presente nessun video #></span>
            <?php }else{
				$iVideo=1;
				foreach($list as $video){ ?>
                	<div class="col-md-3 col-sm-3 col-xs-12"><div class="itemVideoPremium">
                   	<div class="itemVideoPremiumCont">
                    		<a href="<?=$S->getUrl('video-premium',$video["id"]); ?>">
                    			<img src="{{root}}_public/file/video/video<?=$iVideo; ?>.png" class="img-responsive">
                              <?php
							  		$text = substr($video["label"],0,120)."&hellip;";
								?>
                              <div class="descr" style="height: 7em;"><?=$text; ?></div>
                              <div class="bottom"><div>
                              	<span class="iconVideoPremium"></span>
                              	<?=strftime("%d.%m.%Y&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;%H:%M", strtotime( $video["timestamp"]) ); ?>
								</div></div>
                          </a></div>
                      		<div class="shadow"><img src="{{theme}}img/box_shadow_news.png" class="img-responsive">
                      </div>
                   </div></div>
                <?php
					$iVideo++;
			    }
			}
			?>
            
		    <br><br>
		</div>
        <div class="col-md-1 col-sm-1 col-xs-1"></div>
	</div></div>
    
<?php }else{ //VIDEO SINGOLO ?>
	<script type="text/javascript" src="//acffiorentina-4me.weebo.it/static/player/4x/scripts/embedscript-min.js"></script>
	<div class="bgWhite"><div class="row">
    	<div class="col-md-10 col-sm-10 col-xs-10 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
        	<div class="bread1"><a href="{{url video-premium}}">&laquo; {{urlname video-premium}}</a></div>
        	<div class="VideoPremium Scheda">
            	<hr>
               <span class="Title"><?=$Scheda["label"]; ?></span>
               <hr>
               <div class="video"><?=$Scheda["option"]; ?></div>
               <?php if(false){ ?><div class="social"><?=$S->SocialBar(0); ?></div><?php } ?>
               <br><br>
			</div>
		</div>
        <div class="col-md-1 col-sm-1 col-xs-1"></div>
	</div></div>
<?php } ?>