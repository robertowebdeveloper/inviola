<div class="row"><div class="col-md-10 col-sm-10 col-xs-10 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
	<h1>WALLPAPER</h1>
    
    <div class="row">
    	<?php
		$q = "SELECT * FROM `{$S->_db_prefix}downloads` WHERE type='wallpapers' AND `option`='1024x768' AND deleted IS NULL";
		$list = $S->cn->Q($q,true);
		foreach($list as $v){
			$file = $S->pathFile($v["id_file"]);
			$img = $S->Img($v["id_file"], array("w"=>300,"h"=>210,"m"=>"square"));
		?>
            <div class="col-md-4 col-sm-4 col-xs-12"><div class="fbCoverItem">
                <div class="preview"><img src="<?=$img; ?>" class="img-responsive"></div>
                <div class="shadow"><img src="{{theme}}img/shadow_premio_img.png" alt="" class="img-responsive"></div>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                    <td rowspan="2" width="40%" bgcolor="#eee" valign="middle" align="center"><span style="color:#909090;"><# DOWNLOAD #></span></td>
                       <td width="30%"><a href="<?=$file; ?>" target="_blank" class="button2">1024x768</a></td>
                       <?php
					   	 $q = "SELECT id_file FROM `{$S->_db_prefix}downloads` WHERE type='wallpapers' AND `option`='1680x1050' AND `group`='{$v[group]}' AND deleted IS NULL LIMIT 1";
						 $x = $S->cn->OF($q);
						 $x = $S->pathFile($x);
					   	 ?>
                       <td width="30%"><a href="<?=$x; ?>" target="_blank" class="button2">1680x1050</a></td>
                    </tr>
                    <tr>
                    	  <?php
					   	 $q = "SELECT id_file FROM `{$S->_db_prefix}downloads` WHERE type='wallpapers' AND `option`='1280x1050' AND `group`='{$v[group]}' AND deleted IS NULL LIMIT 1";
						 $x = $S->cn->OF($q);
						 $x = $S->pathFile($x);
					   	 ?>
                        <td width="30%"><a href="<?=$x; ?>" target="_blank" class="button2">1280x1050</a></td>
                        <?php
					   	 $q = "SELECT id_file FROM `{$S->_db_prefix}downloads` WHERE type='wallpapers' AND `option`='1920x1200' AND `group`='{$v[group]}' AND deleted IS NULL LIMIT 1";
						 $x = $S->cn->OF($q);
						 $x = $S->pathFile($x);
					   	 ?>
                        <td width="30%"><a href="<?=$x; ?>" target="_blank" class="button2">1920x1200</a></td>
                    </tr>
                </table>
                <br><br>
            </div></div>
        <?php } ?>
    </div>
    
</div><div class="col-md-1 col-sm-1 col-xs-1"></div></div>