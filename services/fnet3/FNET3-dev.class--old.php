<?php
//v. 0.5 06 mag 2014

class FNET3{
	private $x_WSDL = array(
		'BO' => 'https://v3.fidely.net/fnet3web/proxy/FNETBACKOFFICEWS_01_02_00_x.wsdl', //Back Office
		'CA' => 'https://v3.fidely.net/fnet3web/proxy/FNET3_CustomerArea_01_02_00.wsdl', //Customer Area
		'Terminal' => 'https://v3.fidely.net/fnet3web/FNETWS_01_04_00_x.wsdl'
	);
	public $_WSDL = array(
		'BO' => 'https://v3demo.fidely.net/fnet3web/proxy/FNETBACKOFFICEWS_01_02_00_x.wsdl', //Back Office
		'CA' => 'https://v3demo.fidely.net/fnet3web/proxy/FNET3_CustomerArea_01_02_00.wsdl', //Customer Area
		'Terminal' => 'https://v3demo.fidely.net/fnet3web/FNETWS_01_04_00_x.wsdl'
	);

	private $group = NULL;
	private $sessionID = array(
		'BO' => NULL,
		'CA' => NULL,
		'Terminal' => NULL
	);
	
	public $InfoUser = false;//dati utente dopo login a Customer Area
	
	public function __construct( $group, $Dati=NULL ){ //$group = 'Energas' (for example). Group is the name of log files
		$this->group = $group;

		$context = array(
			'ssl' => array('ciphers'=>'RC4-SHA')
		);
		$options = array(
			'stream_context' => stream_context_create($context)
		);

		try{
			$this->ws = new SoapClient( $this->_WSDL['Terminal'], $options );
		}catch(Exception $e){
			print_r($e);
		}
		try{
			$this->wsBO = new SoapClient( $this->_WSDL["BO"], $options );
		}catch(Exception $e){
			print_r($e);
		}
		try{
			$this->wsCA = new SoapClient( $this->_WSDL["CA"], $options );
		}catch(Exception $e){
			print_r($e);
		}

		if( $Dati ){
			$this->Dati = $Dati;
			if( $this->Dati["Terminal"]) {
				$this->Login('Terminal');
			}
			if( $this->Dati["BO"] ){
				$this->Login('BO');
			}
			//Il login alla customer area si fa con i dati utente
		}
	}
	
	public function __destruct(){
		$this->Logout();
		unset( $this->ws );	
	}
	
	public function Login($what){
		try{
			switch($what){
				default:
				case "Terminal":
					$response = $this->ws->SynchroAndLogin( array(
						"serialNumber"=>$this->Dati["Terminal"]["terminal_sn"],
						"username"=>$this->Dati["Terminal"]["user"],
						"password"=>$this->Dati["Terminal"]["pw"],
						"foreignID" => isset($this->Dati["Terminal"]["foreignID"]) ? $this->Dati["Terminal"]["foreignID"] : "1"
					) );
					//print_r($response);
					if( $response->answerCode=="0" ){
						$this->sessionID["Terminal"] = $response->sessionID;
						return true;
					}else{
						$this->Error( $response->answerCode, 'LoginTerminal' );
						return false;
					}
					break;
				case "BO":
					$response = $this->wsBO->Synchro( array(
						"kind" => $this->Dati["BO"]["kind"]
					) );
					//print_r($response);
					if( $response->answerCode=="0"){
						$this->sessionID["BO"] = $response->sessionId;
						
						$response = $this->wsBO->Login( array(
							"sessionId" => $this->sessionID["BO"],
							"username" => $this->Dati["BO"]["user"],
							"password" => $this->Dati["BO"]["pw"]							
						));
						//print_r($response);
						if($response->answerCode=="0"){
							return true;
						}else{
							$this->Error( $response->answerCode, 'LoginBO');	
						}
					}else{
						$this->Error( $response->answerCode, 'SynchroBO');	
					}
					break;
			}
		}catch(Exception $e){
			echo "<pre>";print_r($e);exit;
			return $e;
		}
		return false;
	}
	
	public function LoginCA($user,$pw,$campaignId,$kind=3){
		try{
			$response = $this->wsCA->Synchro( array(
				"kind" => $kind,
				"campaignId" => $campaignId
			) );
			//print_r($response);
			if( $response->answerCode=="0"){
				$this->sessionID["CA"] = $response->session;

				$responseLogin = $this->wsCA->Login( array(
					"session" => $this->sessionID["CA"],
					"username" => $user,
					"password" => $pw
				));
				//echo "<pre>";print_r($response);
				if($responseLogin->answerCode=="0"){
					$this->InfoUser = $responseLogin;
					return $response->session;
				}else{
					$this->Error( $responseLogin->answerCode, 'LoginCA');
					return $responseLogin;
				}
			}else{
				$this->Error( $response->answerCode, 'SynchroCA');
			}	
		}catch(Exception $e){
			return $e;
		}
	}
	
	public function Logout(){
		//print_r($this->sessionID);
		try{
			if( $this->sessionID["Terminal"] )
				$this->ws->Logout( array("sessionID"=>$this->sessionID["Terminal"]) );
			if( $this->sessionID["BO"] )
				$this->wsBO->Logout( array("sessionId"=>$this->sessionID["BO"]) );
			/*if( $this->sessionID["CA"] ) //Customer Area non ha logout?
				$this->wsCA->Logout( array("session"=>$this->sessionID["CA"]) );*/
		}catch(Exception $e){
			echo "<pre>";print_r($e);exit;	
		}
	}
	
	public function SearchCustomerByID($customer_id){
		try{
			$res = $this->ws->SearchCustomerByID( array(
				"sessionID" => $this->sessionID["Terminal"],
				"customerID" => $customer_id
			));
			return $res;
		}catch(Exception $e){
			return $e;	
		}
	}
	
	public function SearchCustomerByPersonalData($data){
		//$data [ name , surname , birthdate , birthdateDay , birthdateMonth , birthdateYear , email , cellphone , facebookId , initLimit ,  rowCount , orders ( criteria , columnName ) , recordsTotal , actualPage , totalPages
		try{
			$res = $this->ws->SearchCustomerByPersonalData( array(
				"sessionID" => $this->sessionID["Terminal"],
				"name" => $data["name"],
				"surname" => $data["surname"],
				"birthdate" => $data["birthdate"],
				"birthdateDay" => $data["birthdateDay"],
				"birthdateMonth" => $data["birthdateMonth"],
				"birthdateYear" => $data["birthdateYear"],
				"email" => $data["email"],
				"cellphone" => $data["cellphone"],
				"facebookId" => $data["facebookId"],
				"pagination" => array(
					"initLimit" => $data["initLimit"],
					"rowCount" => $data["rowCount"],
					"orders" => $data["orders"],
					"recordsTotal" => $data["recordsTotal"],
					"actualPage" => $data["actualPage"],
					"totalPages" => $data["totalPages"]
				)
			));
			return $res;
		}catch(Exception $e){
			return $e;	
		}
			
	}
	
	public function InterestAreas(){
		try{
			$res = $this->ws->GetInterestAreas( array(
				"sessionID" => $this->sessionID,
				"pagination" => array(
					"initLimit" => 0,
					"rowCount" => 0,
					"orders" => array(
						"criteria" => '',
						"columnName" => ''
					),
					"recordsTotal" => "",
					"actualPage" => '',
					"totalPages" => ''
				)
			));
			return $res;
		}catch(Exception $e){
			return $e;
		}
	}
	
	public function ChargePoints($card=false,$customer_id=false,$data){//$card o $customerId sono alternativi
		//$data [ $points, kind=1 , totalMoney='' , ticketID='' ,  $notes='' ]
		try{
			if( !$card && !$customer_id ){
				return false;
			}
			
			if( $customer_id ){
				$info = $this->SearchCustomerByID($customer_id);
				$card = $info->customer->fidelyCode;
			}
			$InfoCard = $this->Info($card);
			//print_r($InfoCard);
			try{
				$res = $this->ws->ChargePoints( array(
					"sessionID" => $this->sessionID['Terminal'],
					"campaignID" => $InfoCard->customer->campaignId,
					"customerID" => $InfoCard->customer->id,
					"card" => $InfoCard->customer->card,
					"chargedPoints" => $data["points"],
					"kindCharge" => isset($data["kind"]) ? $data["kind"] : 1,
					"totalMoney" => isset($data["totalMoney"]) ? $data["totalMoney"] : '',
					"ticketID" => isset($data["ticketID"]) ? $data["ticketID"] : '',
					"notes" => isset($data["notes"]) ? $data["notes"] : ''
				));
				return $res;
			}catch(Exception $e){
				return $e;	
			}
			
		}catch(Exception $e){
			return $e;
		}
	}
	
	public function ModifyCustomerCategory($campaignId, $customer_id, $new_category_id){
		try{
			$res = $this->ws->ModifyCustomerCategory( array(
				"sessionID" => $this->sessionID['Terminal'],
				"campaignID" => $campaignId,
				"customerID" => $customer_id,
				"newCategoryID" => $new_category_id
			));
			return $res;
		}catch(Exception $e){
			return $e;
		}
	}
	
	public function NewCard($card,$fidelyCode,$campaignId,$parentCustomerId=false,$mlmCustomerId=false,$identityCard=false,$name=false,$surname=false,$gender=false,$birthdate=false, $customer_notes=false, $userName=false,
		$privacy = array("flags"=>false, "usedForPromotions"=>false, "usedForStatistics"=>false, "usedByOthers"=>false, "canGetCurrentLocation"=>false, "canComunicaVerification"=>false),
		$mail=false, $cell=false, $tel=false, $fax=false, $address=false, $addressNumber=false, $addressPrefix=false, $zip=false, $country=false, $geoLevel1=false, $geoLevel2=false, $geoLevel3=false, $geoLevel4=false, $geoLevel5=false,
		$customerFields=false , $interestAreas = false, $facebookId = false, $notes = false ){
		
		
		$customerFields = $customerFields ? $customerFields : array("id"=>'',"value"=>'');
		$interestAreas = $interestAreas ? $interestAreas : array("id"=>"","active"=>"");
		
		try{
			$res = $this->ws->NewCard( array(
				"sessionID" => $this->sessionID['Terminal'],
				"customer" => array(
					"campaignId" => $campaignId,
					"card" => $card,
					"fidelyCode" => $fidelyCode,
					"parentCustomerId" => $parentCustomerId ? $parentCustomerId : '',
					"mlmCustomerId" => $mlmCustomerId ? $mlmCustomerId : '',
					"personalInfo" => array(
						"identityCard" => $identityCard ? $identityCard : '',
						"name" => $name ? $name : '',
						"surname" => $surname ? $surname : '',
						"gender" => $gender ? $gender : '',
						"birthdate" => $birthdate ? $birthdate : '',
						"notes" => $customer_notes ? $customer_notes : '',
						"userName" => $userName ? $userName : '',
						"privacy" => array(
							"flags" => isset($privacy["flags"]) ? $privacy["flags"] : '',
							"usedForPromotions" => isset($privacy["usedForPromotions"]) ? $privacy["usedForPromotions"] : '',
							"usedForStatistics" => isset($privacy["usedForStatistics"]) ? $privacy["usedForStatistics"] : '',
							"usedByOthers" => isset($privacy["usedByOthers"]) ? $privacy["usedByOthers"] : '',
							"canGetCurrentLocation" => isset($privacy["canGetCurrentLocation"]) ? $privacy["canGetCurrentLocation"] : '',
							"canComunicaVerification" => isset($privacy["canComunicaVerification"]) ? $privacy["canComunicaVerification"] : ''
						),
						"interestAreas" => '', //non è indicato il suo utilizzo
						"mailContactData" => $mail ? $mail : '',
						"mobileContactData" => $cell ? $cell : '',
						"telephoneContactData" => $tel ? $tel : '',
						"faxContactData" => $fax ? $fax : '',
						"address" => $address ? $address : '',
						"addressNumber" => $addressNumber ? $addressNumber : '',
						"addressPrefix" => $addressPrefix ? $addressPrefix : '',
						"zip" => $zip ? $zip : '',
						"country" => $country ? $country : '',
						"geoLevel1" => $geoLevel1 ? $geoLevel1 : '',
						"geoLevel2" => $geoLevel2 ? $geoLevel2 : '',
						"geoLevel3" => $geoLevel3 ? $geoLevel3 : '',
						"geoLevel4" => $geoLevel4 ? $geoLevel4 : '',
						"geoLevel5" => $geoLevel5 ? $geoLevel5 : '',
						"facebookId" => $facebookId ? $facebookId : '',
						"customerDynamicFields" => $customerFields,
						"interestAreas" => $interestAreas
					)
				),
				"notes" => $notes ? $notes : ''
			));
			
			return $res;
		}catch(Exception $e){
			//$this->pr($e);	
			return $e;
		}
	}
	
	public function ModifyCustomer($card,$campaignCode_Id=false,$data){
		/*
		$data = [ status, name , surname , gender , birthdate , notes , userName ,
		privacy = array("flags"=>false, "usedForPromotions"=>false, "usedForStatistics"=>false, "usedByOthers"=>false, "canGetCurrentLocation"=>false, "canComunicaVerification"=>false),
		mail , cell, tel, fax, address, addressNumber, addressPrefix, zip, country, geoLevel1, geoLevel2, geoLevel3, geoLevel4, geoLevel5, facebookId
		$customerFields=false , $interestAreas = false,
		*/		
			
		if($campaignCode_Id){ //se false lo prende in automatico
			//campaignCode_Id => se inizia con "idXXX" è id altrimenti è codice
			if( substr($campaignCode_Id,0,2)=="id" ){
				$campaignId = intval( substr($campaignCode_Id,2) );
			}else{
				$campaignCode = $campaignCode_Id;
			}
		}

		//$InfoCard = $this->InfoCard( $card , $campaignCode );
		//$this->pr($InfoCard,false);
		$InfoCard = $this->Info( $card );

		//echo "<pre>";print_r($InfoCard);exit;
		//return $InfoCard;
		$customerFields = isset($data["customerFields"]) && $data["customerFields"] ? $data["customerFields"] : $InfoCard->customer->personalInfo->customerDynamicFields;
		$interestAreas = isset($data["interestAreas"]) && $data["interestAreas"] ? $data["interestAreas"] : array("id"=>"","active"=>"");
		$campaignId = isset($campaignId) ? $campaignId : $InfoCard->customer->campaignId;

		try{
			$res = $this->ws->ModifyCustomer( array(
				"sessionID" => $this->sessionID['Terminal'],
				"customer" => array(
					"id" => $InfoCard->customer->id,
					"campaignId" => $campaignId,
					"card" => $InfoCard->customer->card,
					"status" => isset($data["status"]) ? $data["status"] : $InfoCard->customer->status,
					"parentCustomerId" => 0,
					"mlmCustomerId" => $InfoCard->customer->mlmCustomerId,
					"fidelyCode" => $InfoCard->customer->fidelyCode,
					"personalInfo" => array(
						"identityCard" => isset($data["identityCard"]) ? $data["identityCard"] : $InfoCard->customer->identityCard,
						"name" => isset($data["name"]) ? $data["name"] : $InfoCard->customer->name,
						"surname" => isset($data["surname"]) ? $data["surname"] : $InfoCard->customer->surname,
						"gender" => isset($data["gender"]) ? $data["gender"] : $InfoCard->customer->gender,
						"birthdate" => isset($data["birthdate"]) ? $data["birthdate"] : $InfoCard->customer->birthdate,
						"notes" => isset($data["notes"]) ? $data["notes"] : $InfoCard->customer->notes,
						"userName" => isset($data["userName"]) ? $data["userName"] : $InfoCard->customer->userName,
						"privacy" => array(
							"flags" => ( isset($data["privacy"]) && $data["privacy"]["flags"]===false) || !isset($data["privacy"])  ? $InfoCard->customer->privacy->flags : $data["privacy"]["flags"],
							"usedForPromotions" => ( isset($data["privacy"]) && $privacy["usedForPromotions"]===false) || !isset($data["privacy"]) ? $InfoCard->customer->privacy->usedForPromotions : $data["privacy"]["usedForPromotions"],
							"usedForStatistics" => ( isset($data["privacy"]) && $privacy["usedForStatistics"]===false) || !isset($data["privacy"]) ? $InfoCard->customer->privacy->usedForStatistics : $data["privacy"]["usedForStatistics"],
							"usedByOthers" => ( isset($data["privacy"]) && $privacy["usedByOthers"]===false) || !isset($data["privacy"]) ? $InfoCard->customer->privacy->usedByOthers : $data["privacy"]["usedByOthers"],
							"canGetCurrentLocation" => ( isset($data["privacy"]) && $privacy["canGetCurrentLocation"]===false) || !isset($data["privacy"]) ? $InfoCard->customer->privacy->canGetCurrentLocation : $data["privacy"]["canGetCurrentLocation"],
							"canComunicaVerification" => ( isset($data["privacy"]) && $privacy["canComunicaVerification"]===false) || !isset($data["privacy"]) ? $InfoCard->customer->privacy->canComunicaVerification : $data["privacy"]["canComunicaVerification"]
						),
						"interestAreas" => '', //non è indicato il suo utilizzo
						"mailContactData" => isset($data["mail"]) ? $data["mail"] : $InfoCard->customer->mailContactData,
						"mobileContactData" => isset($data["cell"]) ? $data["cell"] : $InfoCard->customer->mobileContactData,
						"telephoneContactData" => isset($data["tel"]) ? $data["tel"] : $InfoCard->customer->telephoneContactData,
						"faxContactData" => isset($data["fax"]) ? $data["fax"] : $InfoCard->customer->faxContactData,
						"address" => isset($data["address"]) ? $data["address"] : $InfoCard->customer->address,
						"addressNumber" => isset($data["addressNumber"]) ? $data["addressNumber"] : $InfoCard->customer->addressNumber,
						"addressPrefix" => isset($data["addressPrefix"]) ? $data["addressPrefix"] : $InfoCard->customer->addressPrefix,
						"zip" => isset($data["zip"]) ? $data["zip"] : $InfoCard->customer->zip,
						"country" => isset($data["country"]) ? $data["country"] : $InfoCard->customer->country,
						"geoLevel1" => isset($data["geoLevel1"]) ? $data["geoLevel1"] : $InfoCard->customer->geoLevel1,
						"geoLevel2" => isset($data["geoLevel2"]) ? $data["geoLevel2"] : $InfoCard->customer->geoLevel2,
						"geoLevel3" => isset($data["geoLevel3"]) ? $data["geoLevel3"] : $InfoCard->customer->geoLevel3,
						"geoLevel4" => isset($data["geoLevel4"]) ? $data["geoLevel4"] : $InfoCard->customer->geoLevel4,
						"geoLevel5" => isset($data["geoLevel5"]) ? $data["geoLevel5"] : $InfoCard->customer->geoLevel5,
						"facebookId" => isset($data["facebookId"]) ? $data["facebookId"] : $InfoCard->customer->facebookId,
						"customerDynamicFields" => $customerFields,
						"interestAreas" => $interestAreas
					)
				)
			));
			
			return $res;
		}catch(Exception $e){
			$this->pr($e);
			return $e;
		}
	}
	
	public function UnlockCard( $data){
		//$data => array(campaignID,customerID,card,notes)
		try{
			$res = $this->ws->UnlockCard(array(
				"sessionID" => $this->sessionID["Terminal"],
				"campaignID" => $data["campaignID"],
				"customerID" => $data["customerID"],
				"card" => $data["card"],
				"notes" => empty($data["notes"]) ? '' : $data["notes"]
			));
			return $res;
		}catch(Exception $e){
			return $e;	
		}
		
	}
	
	public function GeoLevelsSelect( $level=0, $fatherId=3, $orders=array() ){
		$list = $this->GeoLevels( $level, $fatherId, 0,0,0,$orders);
		print_r($list);
	}
	
	public function GeoLevel($id,$level=1,$fatherId=3){
		$list = $this->GeoLevels($level,$fatherId);
		//echo "<pre>";print_r($list);
		$geoLevel = false;
		foreach($list->geoLevel as $v){
			if( $v->id==$id ){
				$geoLevel = $v;	
			}
		}
		unset($list,$id,$level,$fatherId);
		return $geoLevel;
	}
	
	public function GeoLevels( $level=1, $fatherId=3, $initLimit=0, $rowCount=0, $actualPage=0, $orders=array() ){
		$orders = $this->Orders($orders);
		try{
			$r = $this->ws->GetGeoLevels( array(
				"sessionID" => $this->sessionID['Terminal'],
				"level" => $level,
				"fatherId" => $fatherId,
				"countryId" => 3,//solo con vecchia WS
				"pagination" => array(
					"initLimit" => $initLimit,
					"rowCount" => $rowCount,
					"orders" => $orders,
					"recordsTotal" => "",
					"actualPage" => $actualPage,
					"totalPages" => ""
				)
			));

			if( $r->answerCode=="0" ){
				return $r;
			}else{
				$this->Error($r->answerCode, 'GeoLevels' );
				return $r->answerCode;
			}
		}catch(Exception $e){
			return $e;
		}
	}
	
	public function Info( $card ){
		try{
			$r = $this->ws->GetInfo( array("sessionID"=>$this->sessionID['Terminal'], "card"=>$card) );
			if( $r->answerCode=="0"){
				$birthday = $this->toTime( $r->customer->personalInfo->birthdate );
				$r->data_nascita = date( "d/m/Y", $birthday );
				
				//Regione/Provincia/Comune
				if( $r->customer->personalInfo->geoLevel1 > 0 ){
					$tmp = $this->GeoLevels(1,3);
					foreach($tmp->geoLevel as $v){
						if( $v->id==$r->customer->personalInfo->geoLevel1 ){
							$zona = $v->name;	
						}
					}
					$r->Regione = $zona;
				}else{
					$r->Regione = false;	
				}
				if( $r->customer->personalInfo->geoLevel2 > 0){
					//Usare id=100 per esempio
					$tmp = $this->GeoLevels( 2, $r->customer->personalInfo->geoLevel1 );
					foreach($tmp->geoLevel as $v){
						if( $v->id==$r->customer->personalInfo->geoLevel2 ){
							$zona = $v->name;	
						}
					}
					$r->Provincia = $zona;
				}else{
					$r->Provincia = false;	
				}
				if( $r->customer->personalInfo->geoLevel3 > 0 ){
					//Usare id=8302 per esempio
					$tmp = $this->GeoLevels( 3, $r->customer->personalInfo->geoLevel2 );
					if( is_array($tmp->geoLevel) ){
						foreach($tmp->geoLevel as $v){
							if( $v->id==$r->customer->personalInfo->geoLevel3 ){
								$zona = $v->name;	
							}
						}
						$r->Comune = $zona;
					}else{
						$r->Comune = '';
					}
				}else{
					$r->Comune = false;	
				}
				
				return $r;
			}else{
				$this->Error($r->answerCode, 'Info' );
				return $r;
			}
		}catch(Exception $e){
			return $e;
		}
	}
	
	public function InfoCard( $card , $campaignCode ){
		try{
			$r = $this->ws->GetInfoByCard( array("sessionID"=>$this->sessionID['Terminal'], "card"=>$card, "campaignCode"=>$campaignCode) );
			//print_r($r);
			if( $r->answerCode=="0"){
				$birthday = $this->toTime( $r->customer->personalInfo->birthdate );
				$r->data_nascita = date( "d/m/Y", $birthday );
				
				//Regione/Provincia/Comune
				if( $r->customer->personalInfo->geoLevel1 > 0 ){
					$tmp = $this->GeoLevels(1,3);
					foreach($tmp->geoLevel as $v){
						if( $v->id==$r->customer->personalInfo->geoLevel1 ){
							$zona = $v->name;	
						}
					}
					$r->Regione = $zona;
				}else{
					$r->Regione = false;	
				}
				if( $r->customer->personalInfo->geoLevel2 > 0){
					//Usare id=100 per esempio
					$tmp = $this->GeoLevels( 2, $r->customer->personalInfo->geoLevel1 );
					foreach($tmp->geoLevel as $v){
						if( $v->id==$r->customer->personalInfo->geoLevel2 ){
							$zona = $v->name;	
						}
					}
					$r->Provincia = $zona;
				}else{
					$r->Provincia = false;	
				}
				if( $r->customer->personalInfo->geoLevel3 > 0 ){
					//Usare id=8302 per esempio
					$tmp = $this->GeoLevels( 3, $r->customer->personalInfo->geoLevel2 );
					foreach($tmp->geoLevel as $v){
						if( $v->id==$r->customer->personalInfo->geoLevel3 ){
							$zona = $v->name;	
						}
					}
					$r->Comune = $zona;
				}else{
					$r->Comune = false;	
				}
				return $r;
			}else{
				$this->Error($r->answerCode, 'InfoCard' );
				return $r;
			}
		}catch(Exception $e){
			return $e;
		}
	}
	
	private function Orders( $orders ){
		if( count( $orders )>0 ){
			$new=array();
			foreach($orders as $v){
				$new[] = array("criteria" => $v["criteria"] , "columnName" => $v["columnName"] );
			}
			$orders = $new;
		}else{
			$orders = array("criteria"=>'',"columnName"=>'');	
		}
		return $orders;
	}
	
	public function CampaignDynamicFields( $campaignId ){
		$res = $this->ws->GetCampaignDynamicFields( array(
			"sessionID" => $this->sessionID,
			"campaignID" => $campaignId
		));
		return $res;
	}
	
	public function CampaignList($initLimit=0,$rowCount=0, $actualPage=0,$orders=array()){
		$orders = $this->Orders( $orders );		
		
		$r = $this->ws->GetCampaignList( array(
			"sessionID" => $this->sessionID,
			"pagination" => array(
				"initLimit" => $initLimit,
				"rowCount" => $rowCount,
				"orders" => $orders,
				"recordsTotal" => "",
				"actualPage" => $actualPage,
				"totalPages" => ""
			)
		));
		return $r;
	}
	
	public function Campaign( $id ){
		try{
			$r = $this->ws->GetCampaign( array(
				"sessionID" => $this->sessionID['Terminal'],
				"campaignID" => $id
			));
			if( $r->answerCode=="0" ){
				return $r;
			}else{
				$this->Error($r->answerCode, 'Campaign' );
				return false;
			}
		}catch(Exception $e){
			return $e;
		}
	}
	
	public function ModifyUsernameAndPassword($customer_id, $username, $pw ){
		try{
			$res = $this->ws->ModifyUsernameAndPassword( array(
				"sessionID" => $this->sessionID['Terminal'],
				"customerId" => $customer_id,
				"username" => $username,
				"password" => $pw
			));
			return $res;
			/*
			if( $r->answerCode=="0" ){
				return true;
			}else{
				return $r->answerCode;
			}*/
		}catch(Exception $e){
			return $e;	
		}
	}
	
	public function GetInfoShop(){
		try{
			$res = $this->ws->GetInfoShop( array(
				"sessionID" => $this->sessionID['Terminal']
			));
			return $res;
		}catch(Exception $e){
			return $e;	
		}
	}
	
	public function LastMovements( $card , $initLimit=0, $rowCount=0, $orders=array() ){
		//$InfoCard = $this->InfoCard( $card, $campaignCode );
		$InfoCard=$this->Info($card);
		if( !$InfoCard )
			return false;
			
		//pr( $InfoCard );
		
		$orders = $this->Orders( $orders );
		try{
			$r = $this->ws->GetLastMovements( array(
				"sessionID" => $this->sessionID,
				"customerID" => $InfoCard->customer->id,
				"campaignID" => $InfoCard->customer->campaignId,
				"pagination" => array(
					"initLimit" => $initLimit,
					"rowCount" => $rowCount,
					"orders" => $orders,
					"recordsTotal" => "",
					"actualPage" => "",
					"totalPages" => ""
				)
			) );
		}catch(Exception $e){
			echo "<pre>";
			print_r($e);
		}
		return $r;
	}
	
	public function LastMovementsByDate($card=false,$initialDate,$finalDate,$initLimit=0,$rowCount=0,$orders=array() ){ //utilizza un'altra WS
		//se card è false torna tutti i movimenti
		$ws = new SoapClient( $this->_WSDL_ethos );
		$res = $ws->Synchro( array(
			"kind"=>8
		));
		$sessionID = $res->sessionId;
		$res = $ws->Login( array(
			"sessionId" => $sessionID,
			"username" => $this->Dati["user"],
			"password" => $this->Dati["pw"]
		));
		//print_r($res);
		//echo "<pre>";
		try{
			$res = $ws->GetMovementsList( array(
				"sessionId" => $sessionID,
				"netId" => 859,//Per energas
				"initialDate"=> $initialDate,
				"finalDate" => $finalDate,
				"pagination" => array(
					"initLimit" => $initLimit,
					"rowCount" => $rowCount,
					"orders" => $this->Orders( $orders ),
					"recordsTotal" => '',
					"actualPage" => '',
					"totalPages" => ''
				)
			));

			if( $card ){
				$InfoCard = $this->Info($card);
				$n_card = $InfoCard->customer->card;
				for($i=0; $i<count($res->movements	);$i++ ){
					if( $res->movements[$i]->card!=$n_card ){
						//unset( $res->movements[$i] );
					}
				}
				echo "<pre>";
				print_r($res);
			}
			
			return $res;
		}catch(Exception $e){
			echo "<pre>";
			print_r($e);	
		}
	}
	
	public function ExchangePrizes($data){
		//$data [campaignID, customerID, card, notes, prizeId, prizeCode, quantity, ShopId 
		try{
			$res = $this->ws->ExchangePrizes( array(
				"sessionID" => $this->sessionID['Terminal'],
				"campaignID" => $data["campaignID"],
				"customerID" => $data["customerID"],
				"card" => $data["card"],
				"prizesList" => array(
					"prizeId" => $data["prizeId"],
					"prizeCode" => $data["prizeCode"],
					"quantity" => $data["quantity"]>0 ? $data["quantity"] : 1,
					"shopId" => $data["shopId"]
				),
				"notes" => isset($data["notes"]) ? $data["notes"] : ''
			));
			return $res;
		}catch(Exception $e){
			return $e;	
		}
	}
	
	public function TransferPoints($customer_id,$cardReceiver,$points,$notes=""){
		$InfoCard = $this->SearchCustomerByID($customer_id);
		//$this->pr($InfoCard,true);
		try{
			$_ret = $this->ws->TransferPoints( array(
				"sessionID" => $this->sessionID['Terminal'],
				"campaignID" => $InfoCard->customer->campaignId,
				"customerID" => $InfoCard->customer->id,
				"card" => $InfoCard->customer->card,
				"cardReceiver" => $cardReceiver,
				"birthdate" => "",
				"pincode" => '',
				"dischargedPoints" => $points,
				"notes" => ""
			));
			return $_ret;
		}catch(Exception $e){
			print_r($e);	
		}
	}
	
	//******* Metodi della BackofficeArea ***************
	public function BA_GetShopCategories($data){
		//$data [ languageId=2 , fatherId=0, initLimit=0 , rowCount=100 , orders = array() , recordsTotal='', actualPage='', totalPages=''
		try{
			$res = $this->wsBO->GetShopCategories( array(
				"sessionId" => $this->sessionID['BO'],
				"languageId" => $data["languageId"]>0 ? $data["languageId"] : 2,
				"fatherId" => $data["fatherId"]>0 ? $data["fatherId"] : 0,
				"pagination" => array(
					"initLimit" => isset($data["initLimit"]) ? $data["initLimit"] : 0,
					"rowCount" => isset($data["rowCount"]) ? $data["rowCount"] : 100,
					"orders" => isset($data["orders"]) ? $data["orders"] : array(),
					"recordsTotal" => isset($data["recordsTotal"]) ? $data["recordsTotal"] : '',
					"actualPage" => isset($data["actualPage"]) ? $data["actualPage"] : '',
					"totalPages" => isset($data["totalPages"]) ? $data["totalPages"] : ''
				)
			));
			return $res;
		}catch(Exception $e){
			return $e;
		}	
	}
	//******* Fine Metodi della BackofficeArea ***************
	
	//******* Metodi della CustomerArea *****************
	
	public function CA_GetCategory($data){
		//$data [ categoryId ]
		try{
			$res = $this->wsCA->GetCategory( array(
				"session" => $this->sessionID['CA'],
				"categoryId" => $data["categoryId"]
			));
			return $res;
		}catch(Exception $e){
			return $e;
		}
	}
	
	public function CA_GetExchangeCodes($data){ //Codici di scambio dei Premi richiesti
		//$data [ initLimit=0 , rowCount=100 , orders = array() , recordsTotal='', actualPage='', totalPages=''
		try{
			$res = $this->wsCA->GetExchangeCodes( array(
				"session" => $this->sessionID['CA'],
				"pagination" => array(
					"InitLimit" => $data["InitLimit"]>0 ? $data["InitLimit"] : 0,
					"rowCount" => $data["rowCount"]>0 ? $data["rowCount"] : 10,
					"orders" => isset($data["orders"]) ? $data["orders"] : array(),
					"recordsTotal" => $data["recordsTotal"]>0 ? $data["recordsTotal"] : '',
					"actualPage" => isset($data["actualPage"]) ? $data["actualPage"] : '',
					"totalPages" => isset($data["totalPages"]) ? $data["totalPages"] : ''
				)
			));
			return $res;
		}catch(Exception $e){
			return $e;
		}
	}
	
	public function CA_GetNotExchangedPrizes($data){//Premi ritirati
		//$data [ initLimit=0 , rowCount=100 , orders = array() , recordsTotal='', actualPage='', totalPages=''
		try{
			$res = $this->wsCA->GetNotExchangedPrizes( array(
				"session" => $this->sessionID['CA'],
				"pagination" => array(
					"InitLimit" => $data["InitLimit"]>0 ? $data["InitLimit"] : 0,
					"rowCount" => $data["rowCount"]>0 ? $data["rowCount"] : 10,
					"orders" => isset($data["orders"]) ? $data["orders"] : array(),
					"recordsTotal" => $data["recordsTotal"]>0 ? $data["recordsTotal"] : '',
					"actualPage" => isset($data["actualPage"]) ? $data["actualPage"] : '',
					"totalPages" => isset($data["totalPages"]) ? $data["totalPages"] : ''
				)
			));
			return $res;
		}catch(Exception $e){
			return $e;
		}
	}
	
	public function CA_GetExchangedPrizes($data){//Premi ritirati
		//$data [ initLimit=0 , rowCount=100 , orders = array() , recordsTotal='', actualPage='', totalPages=''
		try{
			$res = $this->wsCA->GetExchangedPrizes( array(
				"session" => $this->sessionID['CA'],
				"pagination" => array(
					"InitLimit" => $data["InitLimit"]>0 ? $data["InitLimit"] : 0,
					"rowCount" => $data["rowCount"]>0 ? $data["rowCount"] : 10,
					"orders" => isset($data["orders"]) ? $data["orders"] : array(),
					"recordsTotal" => $data["recordsTotal"]>0 ? $data["recordsTotal"] : '',
					"actualPage" => isset($data["actualPage"]) ? $data["actualPage"] : '',
					"totalPages" => isset($data["totalPages"]) ? $data["totalPages"] : ''
				)
			));
			return $res;
		}catch(Exception $e){
			return $e;
		}
	}
	
	public function CA_ExchangePrizes($prizeId,$prizeCode,$qty=1,$shopId=''){
		try{
			$res = $this->wsCA->ExchangePrizes( array(
				"session" => $this->sessionID['CA'],
				"exchangeablePrizes" => array(
					"prizeId" => $prizeId,
					"prizeCode" => $prizeCode,
					"quantity" => $qty,
					"shopId" => $shopId
				)
			));
			return $res;
		}catch(Exception $e){
			return $e;	
		}
	}
	
	public function CA_GetPrizes($catalogId,$categoryId=0,$InitLimit=0,$rowCount=10,$orders=array(), $recordsTotal='', $actualPage='', $totalPages=''){
		try{
			$res = $this->wsCA->GetPrizes( array(
				"session" => $this->sessionID['CA'],
				"catalogId" => $catalogId,
				"categoryId" => $categoryId,
				"pagination" => array(
					"InitLimit" => $InitLimit,
					"rowCount" => $rowCount,
					"orders" => $orders,
					"recordsTotal" => $recordsTotal,
					"actualPage" => $actualPage,
					"totalPages" => $totalPages
				)
			));
			return $res;
		}catch(Exception $e){
			return $e;	
		}
	}
	
	public function CA_GetShops($data){
		//$data [ shopCategory=0 , netId = 0 , InitLimit=0 , rowCount = 10 , orders=array() , recordsTotal='', actualPage='' , totalPages=''){
		try{
			/*$wsdl_tmp = 'https://v3demo.fidely.net/fnet3web/proxy/FNET3_CustomerArea_01_01_00.wsdl';
			$wsdl = new SoapClient( $wsdl_tmp, array("trace"=>true));
			$res = $wsdl->GetShops( array(
				"session" => $this->sessionID['CA'],
				"shopCategory" => isset($data["shopCategory"]) ? $data["shopCategory"] : 0,
				"netId" => isset($data["netId"]) ? $data["netId"] : 0,
				"pagination" => array(
					"InitLimit" => isset($data["InitLimit"]) ? $data["InitLimit"] : 0,
					"rowCount" => isset($data["rowCount"]) ? $data["rowCount"] : 10,
					"orders" => isset($data["orders"]) ? $data["orders"] : array(),
					"recordsTotal" => isset($data["recordsTotal"]) ? $data["recordsTotal"] : '',
					"actualPage" => isset($data["actualPage"]) ? $data["actualPage"] : '',
					"totalPages" => isset($data["totalPages"]) ? $data["totalPages"] : ''
				)
			));*/
			$res = $this->wsCA->GetShops( array(
				"session" => $this->sessionID['CA'],
				"shopCategory" => isset($data["shopCategory"]) ? $data["shopCategory"] : 0,
				"netId" => isset($data["netId"]) ? $data["netId"] : 0,
				"pagination" => array(
					"InitLimit" => isset($data["InitLimit"]) ? $data["InitLimit"] : 0,
					"rowCount" => isset($data["rowCount"]) ? $data["rowCount"] : 10,
					"orders" => isset($data["orders"]) ? $data["orders"] : array(),
					"recordsTotal" => isset($data["recordsTotal"]) ? $data["recordsTotal"] : '',
					"actualPage" => isset($data["actualPage"]) ? $data["actualPage"] : '',
					"totalPages" => isset($data["totalPages"]) ? $data["totalPages"] : ''
				)
			));
			return $res;
		}catch(Exception $e){
			return $e;	
		}
	}
	
	public function CA_GetMovements($InitLimit=0,$rowCount=10,$orders=array(), $recordsTotal='', $actualPage='', $totalPages=''){
		//eseguire prima LoginCA per settare la sessione
		try{
			$res = $this->wsCA->GetMovements( array(
				"session" => $this->sessionID['CA'],
				"pagination" => array(
					"InitLimit" => $InitLimit,
					"rowCount" => $rowCount,
					"orders" => $orders,
					"recordsTotal" => $recordsTotal,
					"actualPage" => $actualPage,
					"totalPages" => $totalPages
				)
			));
			//print_r($res);
			return $res;
		}catch(Exception $e){
			return $e;
		}
	}
	
	//******* FINE Metodi della CustomerArea *****************
	
	/*
	public function RegisterCard($card,$campaignId,$name,$surname,$gender="N",$birthdate="", $notes="", $userName="",
		$privacy = array("flags"=>false, "usedForPromotions"=>false, "usedForStatistics"=>false, "usedByOthers"=>false, "canGetCurrentLocation"=>false, "canComunicaVerification"=>false),
		$mail="", $cell="", $tel="", $fax="", $address="", $addressNumber="", $addressPrefix="", $zip="", $country=3, $geoLevel1=0, $geoLevel2=0, $geoLevel3=0, $geoLevel4=0, $geoLevel5=0, $customerFields=array(), $interestAreas=array() ){
		
		$customerFields = $customerFields ? $customerFields : array("id"=>"","value"=>"");
		
		try{
			$res = $this->ws->NewCard( array(
				 "sessionID" => $this->sessionID,
				 "customer" => array(
					"campaignId" => $campaignId,
					"card" => $card,
					"fidelyCode" => '',
					"parentCustomerId"=>0,
					"personalInfo" => array(
						"identityCard" => '',
						"name" => $name,
						"surname" => $surname,
						"gender" => $gender,
						"birthdate" => $birthdate,
						"notes" => $notes,
						"userName" => $userName,
						"privacy" => array(
							"flags" => $privacy["flags"],
							"usedForPromotions" => $privacy["usedForPromotions"],
							"usedForStatistics" => $privacy["usedForStatistics"],
							"usedByOthers" => $privacy["usedByOthers"],
							"canGetCurrentLocation" => $privacy["canGetCurrentLocation"],
							"canComunicaVerification" => false
						),
						"mailContactData" => $mail,
						"mobileContactData" => $cell,
						"telephoneContactData" => $tel,
						"faxContactData" => $fax,
						"address" => $address,
						"addressNumber" => $addressNumber,
						"addressPrefix" => $addressPrefix,
						"zip" => $zip,
						"country" => $country,
						"geoLevel1" => $geoLevel1,
						"geoLevel2" => $geoLevel2,
						"geoLevel3" => $geoLevel3,
						"geoLevel4" => $geoLevel4,
						"geoLevel5" => $geoLevel5,
						"customerDynamicFields" => $customerFields,
						"interestAreas" => $interestAreas
					)
				),
				"notes" => ''
			));
//			print_r($res);
			return $res;
		}catch(Exception $e){
			return $e;
		}
	}
	*/
	
	public function toTime( $FidelyTime ){
		//2013-06-06T20:37:35.944-03:00
		list($a,$b) = explode("T",$FidelyTime);
		list($Y,$m,$d) = explode("-",$a);
		$b = substr($b,0,8);
		list($h,$i,$s) = explode(":",$b);
		$time = mktime($h,$i,$s,$m,$d,$Y);
		return $time;
	}
	
	public function Time( $time="auto" ){
		$time = $time=="auto" ? time() : $time;
		
		//2013-06-06T20:37:35.944-03:00
		return date("Y-m-d", $time) . "T" . date("H:i:s.000P",$time);
	}
	
	private function Error( $error_code, $fn_name="" ){	
		$log = date("d/m/Y H:i") . " - Error: ({$error_code})";
		$e = $this->errorDescr($error_code);
		$log .= $e ? $e : "";
		$log .= "\r\n------------------------------------\r\n";
		
		$fp = fopen( $this->group . "/logs/error.log","a");
		fwrite($fp, $log );
		fclose( $fp );
	}
	
	private function fileName(){
		$name = strtolower( $this->group );
		$name = str_replace(" ","-",$name);
		return $name;	
	}
	
	private function pr($var,$exit_on=true){
		echo "<pre>";
		print_r( $var );
		echo "</pre>";
		if($exit_on)
			exit;
	}
	
	private function errorDescr( $code ){
		switch( $code ){
			default:
				return false;	
		}
	}
}
?>