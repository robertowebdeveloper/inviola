<?php
require("module/nmefw.php");
if( strlen($_POST["tipo"])>0 && strlen( $_FILES["file"]["tmp_name"])>0 ){
	$cn = new NE_mysql(0);
	$filename = $_FILES["file"]["tmp_name"];
	$file = file($filename);
	
	list($tipo,$category_id) = explode(",",$_POST["tipo"]);
	
	for($i=1,$importati=0;$i<count($file);$i++){
		list($numero,$fidelycode) = explode(",",$file[$i]);
		$numero = trim($numero);
		$fidelycode = trim($fidelycode);
		
		$q = "SELECT * FROM newcard WHERE fidelycode='{$fidelycode}'";
		$cn->Q($q);
		if( $cn->n==0 ){
			$q = "INSERT INTO newcard
				(tipo,category_id,numero,fidelycode)
				VALUES
				('{$tipo}','{$category_id}','{$numero}','{$fidelycode}')";
			$cn->Q($q);
			$importati++;	
		}
	}
	
	$h1 = "Importati: {$importati} su " . (count($file)-1);
	
	$cn->Close();
}
?><!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Importa nuove card</title>
</head>

<body>
<?php if(isset($h1)){ ?><h1><?=$h1; ?></h1><?php } ?>
<br><br>
<hr>
<form id="form" enctype="multipart/form-data" method="post" accept-charset="importa_card.php">
	File:
    <input type="file" name="file">
    <br>
    Tipo:
    <select name="tipo">
    	<option value="cartoncino,423">Cartoncino</option>
        <option value="fb,426">Virtuale: Facebook</option>
        <option value="web,425">Virtuale: Web</option>
        <option value="app,424">Virtuale: App</option>
    </select>
    <hr>
    <script type="text/javascript">
    var Send = function(el){
		el.disabled = true;
		document.getElementById('form').submit();
	};
    </script>
    <input type="button" value="Invia e processa" onclick="Send(this);">
</form>
</body>
</html>