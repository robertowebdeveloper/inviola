<?php
$Namae_FW = '1.0.1'; //15 mag 2013
/*****
Copyright Namae Studio: www.namaestudio.com
******/
if(!isset($NE_hostname)){
	include("config.inc.php");
}

class NE_mysql{
	public $db = false; //nome database selezionato
	public $db_default = false; //nome database di default 
	public $cn = false; //identificatore della connessione
	public $n = 0; //record trovati dell'ultima select eseguita;
	public $last_id = 0; //ultimo id inserito da un insert;
	public $ris = NULL;
	public $mysqli = false;
	public $query_error = false; //impostare a true se si vuole attivare die(mysql_error()) sulle chiamate a query
	public $default_charset = false; //acquisisce il charset di default da config.inc.php
	public $permanentConnect = false; //solo per connessioni di tipo mysql_
	
	public function __construct($database=NULL){
		global $NE_db_default_charset;
		if(isset($NE_db_default_charset)){
			$this->default_charset = $NE_db_default_charset;
		}
		
		if($database!==NULL){
			$this->OpenDf($database);
		}
	}
	
	public function __destruct(){
		unset($this->db,$this->db_default,$this->cn,$this->n,$this->last_id,$this->ris,$this->mysqli,$this->query_error);
	}
	
	public function OpenDf($database=NULL){ //apre la connessione e seleziona il db scelto (con null seleziona il db di default);
		$this->Open();
		$this->Sel($database);
	}

	public function Open(){
		//apre la connessione al database con l'utente di default
		//global $NM_Path;
		//require_once($NM_Path."config.inc.php");
		global $NE_hostname, $NE_username, $NE_password,$NE_db,$NE_socket,$NE_port;
		$this->db_default = $NE_db;
		if(function_exists("mysqli_connect")){
			if( $NE_port && $NE_socket ){
				$this->cn = mysqli_connect($NE_hostname,$NE_username,$NE_password,"",$NE_port,$NE_socket);
			}else{
				$this->cn = mysqli_connect($NE_hostname,$NE_username,$NE_password);
			}
			if( $this->cn ){
				$this->mysqli=true;
				return $this->cn;
			}
		}
		
		$host = $NE_hostname;
		if($NE_port && $NE_socket){
			$host = "{$host}:{$NE_port}{$NE_socket}";
		}
		if($this->permanentConnect){
			$this->cn = mysql_pconnect($NE_hostname, $NE_username, $NE_password);// or trigger_error(mysql_error(),E_USER_ERROR)
		}else{
			$this->cn = mysql_connect($NE_hostname, $NE_username, $NE_password);// or trigger_error(mysql_error(),E_USER_ERROR)
		}
		return $this->cn;
	}
	
	public function OpenSt($host,$user,$pw,$port=false,$socket=false){
		if(function_exists("mysqli_connect")){
			if($port && $socket){
				if($this->cn = mysqli_connect($host, $user, $pw,"",$port,$socket)){
					$this->mysqli=true;
					return $this->cn;
				}
			}else{
				if($this->cn = mysqli_connect($host, $user, $pw)){
					$this->mysqli=true;
					return $this->cn;
				}
			}
		}
		if($port && $socket){
			$host = "{$host}:{$port}{$socket}";
		}
		$this->cn = mysql_connect($host, $user, $pw);// or trigger_error(mysql_error(),E_USER_ERROR);
		return $this->cn;
	}
	
	public function Sel($database=NULL){//seleziona il database. Con $database = 0 o NULL seleziona il database di default
		if($this->cn){
			$database = empty($database) ? $this->db_default : $database;
			$this->db = $database;
			if($this->mysqli) $status = mysqli_select_db($this->cn,$database);
			else $status = mysql_select_db($database,$this->cn);
			if($status && $this->default_charset){
				$this->Charset($this->default_charset);
			}
			return $status;
		}else{
			die("Connection not created");
		}
	}
	
	public function Charset($charset="utf8"){ //SETTA IL CHARSET
		//possibili valori: utf8, sjis(per giapponese)
		$this->Q("SET NAMES {$charset}");
	}
	
	public function Q($query,$return_array=false){ //esegue una query
		if($this->db && $this->cn){
			if($this->mysqli){
				if($this->query_error) $ris = mysqli_query($this->cn,$query) or die(mysqli_error($this->cn)." :: QUERY => $query");
				else $ris = mysqli_query($this->cn,$query);
				
				//****
				//CERCA IL TIPO DI QUERY
				//****
				if(strpos( strtolower($query) ,"select")<=5 && strpos( strtolower($query) ,"select")!==false){
					$this->n = $ris ? mysqli_num_rows($ris) : 0;
					$this->ris = $ris;
					
					if($return_array){
						$arr = array();
						while($r=$this->R($ris)){
							$arr[] = $r;
						}
						@$this->F($ris);
						return $arr;
					}
					
				}else if(strpos( strtolower($query) ,"insert")<=5 && strpos( strtolower($query) ,"insert")!==false){
					$this->last_id=mysqli_insert_id($this->cn);
				}
				//substr(strtolower($query),0,6)=="insert") $this->last_id=mysqli_insert_id($this->cn);
			}else{
				if($this->query_error) $ris = mysql_query($query,$this->cn) or die(mysql_error()." :: QUERY->$query");
				else $ris = mysql_query($query,$this->cn);
				
				//****
				//CERCA IL TIPO DI QUERY
				//****
				if(strpos( strtolower($query) ,"select")<=5 && strpos( strtolower($query) ,"select")!==false){
					$this->n = $ris ? mysql_num_rows($ris) : 0;
					$this->ris = $ris;
				}else if(strpos( strtolower($query) ,"insert")<=5 && strpos( strtolower($query) ,"insert")!==false){
					$this->last_id = mysql_insert_id();
				}
			}
			return $ris;
		}
	}
	
	public function OQ($query){ //seleziona una singola scheda, funziona solo con query SELECT di tipo WHERE id = x
		if($this->mysqli){
			if($this->query_error) $ris = mysqli_query($this->cn,$query) or die(mysqli_error($this->cn)." :: QUERY->$query");
			else $ris = mysqli_query($this->cn,$query);
			if($ris && mysqli_num_rows($ris)==1){
				$r = mysqli_fetch_assoc($ris);
				mysqli_free_result($ris);
				return $r;
			}else return -1;
		}else{
			if($this->query_error) $ris = mysql_query($query,$this->cn) or die(mysql_error()." :: QUERY->$query");
			else $ris = mysql_query($query,$this->cn);
			if($ris && mysql_num_rows($ris)==1){
				$r = mysql_fetch_assoc($ris);
				mysql_free_result($ris);
				return $r;
			}else return -1;
		}
	}
	
	public function OF($query,$field=NULL){ //restituisce il campo della query specificato in "field", se vuoto restituisce il primo campo della query [ OF = One Field ]
		$row = $this->OQ($query);
		if($row==-1)
			return false;
		if(empty($field)){
			foreach($row as $e){
				return $e;
			}
		}else{
			return $row[$field];
		}
	}
	
	public function R($ris){
		if(empty($ris)) $ris = &$this->ris;
		if($this->mysqli) return mysqli_fetch_assoc($ris);
		return mysql_fetch_assoc($ris);
	}
	
	public function F($ris){
		if(empty($ris)) $ris = &$this->ris;
		if($this->mysqli){
			return mysqli_num_rows($ris)>0 ? mysqli_free_result($ris) : false;
		}else{
			return mysql_num_rows($ris)>0 ? mysql_free_result($ris) : false;
		}
	}
	
	public function enumValues($table,$field){ //ritorna un array con i valori preimpostati di un campo set/enum
		$row = $this->OQ("SHOW COLUMNS FROM `{$table}` LIKE '{$field}'");
		$type = $row[Type];
		$type = substr($type,strpos($type,"(")+1);
		$type = substr($type,0,strlen($type)-1);
		
		$arr = explode(",",$type);
		foreach($arr as $k=>$v){
			$v = trim($v);
			$v = substr($v,1);
			$v = substr($v,0,strlen($v)-1);
			$arr[$k] = $v;
		}
		return $arr;
	}
	
	private function Error($error){
		die($error);
	}
	
	public function Rewind($ris){
		$this->Seek($ris,0);
	}
	
	public function Seek($ris,$pos){
		if(empty($ris)) $ris = &$this->ris;
		if($this->mysqli) mysqli_data_seek($ris,$pos);
		else mysql_data_seek($ris,$pos);
	}
		
	public function Close(){
		if($this->cn){
			if($this->mysqli) return mysqli_close($this->cn);
			return mysql_close($this->cn);
		}
	}
}

class NE_sqlite{
	public $db = false; //nome database selezionato
	public $db_default = false; //nome database di default 
	public $cn = false; //identificatore della connessione
	public $n = 0; //record trovati dell'ultima select eseguita;
	public $last_id = 0; //ultimo id inserito da un insert;
	public $ris = NULL;
	public $query_error = false; //impostare a true se si vuole attivare  sulle chiamate a query
	public $default_charset = false; //acquisisce il charset di default da config.inc.php
	public $permanentConnect = false; //apre connessioni permanenti
	
	public function __construct($database=NULL){
		global $NE_sqlite;
		if(isset($NE_sqlite["default_charset"])){
			$this->default_charset = $NE_sqlite["default_charset"];
		}
		
		if($database!==NULL){
			$this->Open($database);
		}
	}
	
	public function __destruct(){
		unset($this->db,$this->db_default,$this->cn,$this->n,$this->last_id,$this->ris,$this->query_error);
	}

	public function Open($db_name=NULL,$mode=0x0666){
		//apre la connessione al database con l'utente di default
		//global $NM_Path;
		//require_once($NM_Path."config.inc.php");
		global $NE_sqlite;
		if($db_name==NULL){
			$db_name = $this->db_default = $NE_sqlite["db"];
			$mode = !empty($NE_sqlite["mode"]) ? $NE_sqlite["mode"] : 0x0666;
		}
		$this->db = $db_name;
		$error_msg = $this->query_error ? "Apertura SQLite fallita" : "";
		if($this->permanentConnect){
			$this->cn = sqlite_popen($db_name,$mode,$error_msg);
		}else{
			$this->cn = sqlite_open($db_name,$mode,$error_msg);
		}
		unset($db_name,$mode,$error_msg);
		return $this->cn;
	}
		
	public function Charset($charset="utf8"){ //SETTA IL CHARSET (da testare su sqlite)
		//possibili valori: utf8, sjis(per giapponese)
		$this->Q("SET NAMES {$charset}");
	}
	
	public function Q($query,$return_array=false){ //esegue una query
		if($this->db && $this->cn){
			if($this->query_error) $ris = $this->cn->query($query) or die($this->cn->lastError()." :: QUERY->$query");
			else $ris = $this->cn->query($query);
			
			//****
			//CERCA IL TIPO DI QUERY
			//****
			if(strpos( strtolower($query) ,"select")<=5 && strpos( strtolower($query) ,"select")!==false){
				$this->n=$ris->numRows();
				$this->ris = $ris;
				
				if($return_array){
					$arr = array();
					while($r=$this->R($ris)){
						$arr[] = $r;
					}
					@$this->F($ris);
					return $arr;
				}
				
			}else if(strpos( strtolower($query) ,"insert")<=5 && strpos( strtolower($query) ,"insert")!==false){
				$this->last_id = $ris->lastInsertRowid();
			}
			
			return $ris;
		}
	}
	
	public function OQ($query){ //seleziona una singola scheda, funziona solo con query SELECT di tipo WHERE id = x
		if($this->query_error) $ris = $this->cn->singleQuery($query) or die($this->cn->lastError()." :: QUERY->$query");
		else $ris = $this->cn->singleQuery($query);
		return count($ris)==0 ? -1 : $ris;
	}
	
	public function OF($query,$field=NULL){ //restituisce il campo della query specificato in "field", se vuoto restituisce il primo campo della query [ OF = One Field ]
		$row = $this->OQ($query);
		if($row==-1)
			return false;
		if(empty($field)){
			foreach($row as $e){
				return $e;
			}
		}else{
			return $row[$field];
		}
	}
	
	public function R($ris){
		if(empty($ris)) $ris = &$this->ris;
		return $ris->fetch();
	}
	
	private function Error($error){
		die($error);
	}
	
	public function Rewind($ris){
		$ris->rewind();
	}
	
	public function Seek($ris,$pos){
		if(empty($ris)) $ris = &$this->ris;
		$ris->seek($pos);
	}
		
	public function Close(){
		sqlite_close($this->cn);
	}
}

class NE_Login{
	public $cn=NULL;
	public $redirectError;
	public $updatePasswordAfterLogin_flag = false;
	
	public function __construct($redirectError=NULL){
		session_start();
		$this->redirectError = $redirectError;
	}
	
	public function __destruct(){
		unset($this->redirectError,$this->cn);
	}
	
	public function Check(){
		if(!isset($_SESSION["NE_UtenteLogin"])){
			if($this->redirectError!=NULL){
				header("Location: $this->redirectError");
			}else{
				return false;
			}
		}else{
			return true;
		}
	}
	
	public function updatePasswordAfterLogin($crypting_method,$campo_new_pw){
		$this->updatePasswordAfterLogin_flag = true;
		$this->updatePasswordAfterLogin_dati = array("method"=>$crypting_method,"new_pw"=>$campo_new_pw);
	}
	
	public function Accedi($admin_table,$campo_name,$campo_login,$campo_password, $campo_level=false, $where_query=false,$case_sensitive=true,$crypting_method=false,$db_type="mysql",$chiamata_ajax=false,$redirectOk=false,$lg,$pw){
		/*
		$admin_table : nome della tabella dove sono contenuti i dati per il login
		$campo_name : nome dell'admin (diversa da login name, se c'�, altrimenti false e prender� il login name come nome);
		$campo_login : nome del campo del login name
		$campo_password : nome del campo della password
		$campo_level : nome del campo dell'eventuale livello (o false se non c'�)
		$where_query : aggiunta alla clausola where se ce n'� bisogno (false o "" se non necessita)
		$case_sensitive : true|false indica se password e login devono essere case sensitive o no
		$crypting_method: false => nessuna criptazione della password, "md5", "sha1", "PASSWORD" e "OLD_PASSWORD" (metodi di MySql)
		$db_type: 'mysql'|'postgresql'
		$chiamata_ajax: true|false se la chiamata � ajax (allora risponder� con 1 o 0 a seconda del login riuscito o meno) se non � ajax proceder� al login
		$redirectOk : pagina dove redirectare in caso di successo (false se non va effettuato il redirect)
		$lg : login name
		$pw : password
		*/
		
		/***** CONTROLLI QUERY INJECTION *****/
		$lg = str_replace(" ","",$lg);
		$pw = str_replace(" ","",$pw);
		
		if($this->cn==NULL){
			switch($db_type){
				case "mysql":
					$this->cn = new NE_mysql();
					break;
				case "postgre":
					$this->cn = new NE_pgsql();
					break;
			}

			$this->cn->OpenDf();
			$this->cn->mysql_error=true;
		}
		
		$campo_name = $campo_name ? $campo_name : $campo_login;
		
		if(!$case_sensitive){
			$lg = strtolower($lg);
			$pw = strtolower($pw);
		}
		
		$w = !$where_query ? "" : $where_query;
		
		$pw_crypt = $this->Crypt($crypting_method,$pw);

		$q = "SELECT * FROM `$admin_table` WHERE ((`$campo_login` = '$lg') AND (`$campo_password` = $pw_crypt ))".$w;
		$utente = $this->cn->Q($q);
		
		if($this->cn->n==1){
			if($this->updatePasswordAfterLogin_flag){ //setta la password in nuovo cript al momento del login su un nuovo campo
				$new_pw_crypt = $this->Crypt( $this->updatePasswordAfterLogin_dati["method"], $pw);
				$q = "UPDATE `$admin_table` SET `".$this->updatePasswordAfterLogin_dati[new_pw]."` = $new_pw_crypt WHERE (`$campo_login` = '$lg')";
				$this->cn->Q($q);
			}

			$ut = $this->cn->R($utente);
			$this->cn->F($utente);
			$this->cn->Close();
			if($chiamata_ajax) echo "1";
			else{
				$_SESSION["NE_UtenteName"] = $ut[$campo_name];
				$_SESSION["NE_UtenteLogin"] = $lg;
				if($campo_level) $_SESSION["NE_UtenteLivello"] = $ut[$campo_level];
				$_SESSION["NE_OraLogin"] = time();
				if($redirectOk) header("Location: ".$redirectOk);
			}
		}else{
			if($this->cn->n>0) $this->cn->F($utente);
			$this->cn->Close();
			if($chiamata_ajax) echo "0";
			else{
				$this->resetSesVar();
				header("Location: ".$this->redirectError);
			}		
		}
	}
	
	private function Crypt($method,$pw){
		switch(strtolower($method)){
			case "md5":
				$pw = "'".md5($pw)."'";
				break;
			case "sha1":
				$pw = "'".sha1($pw)."'";
				break;
			case "sha256":
			case "sha384":
			case "sha512":
				$pw = "'".hash( strtolower($method), $pw)."'";
				break;
			case "password":
				$pw = "PASSWORD('$pw')";
				break;
			case "old_password":
				$pw = "OLD_PASSWORD('$pw')";
				break;
			default:
				$pw = "'$pw'";
				break;
		}
		return $pw;
	}
	
	public function Logout(){
		$this->resetSesVar();
	}
	
	private function resetSesVar(){
		unset($_SESSION["NE_UtenteName"],$_SESSION["NE_UtenteLogin"],$_SESSION["NE_OraLogin"]);
	}
}

class NE_Email{
	public function __construct(){
		$this->fontSize=1;
		$this->fontHeadSize=1;
		$this->fontPieSize=1;
		$this->returnPath = false; //indicare l'indirizzo e-mail per il Return Path
		$this->Charset = "UTF-8";
	}
	
	public function __destruct(){
		unset($this->fontSize,$this->fontHeadSize,$this->fontPieSize,$this->Charset);
	}
	
	public function setAllSize($size=2){
		$this->fontSize = $this->fontHeadSize = $this->fontPieSize = $size;
	}
	public function sendHtmlMail($title,$textHead,$textPie,$listaCampi,$to,$from,$soggetto,$bcc=""){
		if($this->checkMail($to)){
			$contenutoHtml = $this->createHtmlStandard($title, $textHead,$textPie,$listaCampi);
			$ret = $this->invioMail($to,$from,$soggetto,$contenutoHtml,$bcc);
			return $ret;
		}else{
			return false;
		}
	}
	
	public function createHtmlStandard($title, $textHead,$textPie,$listaCampi){//listaCampi deve essere un array misto coppia nome=>valore
		$Mail = "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=".$this->Charset."\"><title>".$title."</title></head><body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'><div style=\"margin-left: 20px;\"><table width='553' border='0' cellspacing='0' cellpadding='0'><tr><td>";
		
		$Mail .= "<font face=\"Verdana\" size=\"".$this->fontHeadSize."\" color=\"#000000\">".$textHead."</font>";
		
		if(count($listaCampi)>0){
			$Mail .= "<br><br>";
		
			foreach($listaCampi as $k=>$v){
				$str = "<font face=\"Verdana\" size=\"".$this->fontSize."\" color=\"#000000\"><b>".$k."</b>: ";
				$str .= strpos(strtolower($k),"mail")>0 ? '<a href="mailto:'.$v.'">'.$v.'</a>' : $v;
	
				$Mail .= $str."</font><br>";
			}
		}
		
		$Mail .= "</td></tr>";
		$Mail .= "<tr><td height=\"60\" valign=\"bottom\"><font face=\"Verdana\" size=\"".$this->fontPieSize."\">".$textPie."</font></td></tr>";
		$Mail .="</table></div></body></html>";
		return $Mail;		
	}
	
	public function invioMail($to, $from, $soggetto,$contenutoHTML,$bcc=""){
		$eol = "\n"; //utilizzando \r\n da qualche problema con outlook 2003 (non su tutti i computer (?!?!?))
		$contenutoTEXT=str_replace("<br>","\n",$contenutoHTML);
		$contenutoTEXT=str_replace("&egrave","è",$contenutoTEXT);
		$contenutoTEXT=str_replace("&agrave","à",$contenutoTEXT);
		$contenutoTEXT=str_replace("&ograve","ò",$contenutoTEXT);
		$contenutoTEXT=str_replace("&ugrave","ù",$contenutoTEXT);
		$contenutoTEXT=str_replace("&igrave","ì",$contenutoTEXT);
		$contenutoTEXT=str_replace("&eacute","é",$contenutoTEXT);
		$contenutoTEXT=strip_tags($contenutoTEXT);
	
		$boundary = md5(uniqid(microtime()));
		
		// INTESTAZIONI DELLA MAIL
		$mail_headers = "";
		if($this->returnPath){
			$mail_headers .= "Return-Path: <{$this->returnPath}>{$eol}";
		}
		$mail_headers .= "From: $from\nMIME-version: 1.0".$eol; 
		$mail_headers .= empty($bcc) ? "" : "Bcc: $bcc".$eol;
		$mail_headers .= "Content-Type: multipart/alternative; boundary=\"$boundary\"".$eol; 
		
		$mail_body = "--$boundary".$eol;
		$mail_body.= "Content-Type: text/plain; charset=\"".$this->Charset."\"".$eol;
		$mail_body.= "Content-Transfer-Encoding: 7bit".$eol.$eol;
	
		//Contenuto *solo testo* da modificare
		$mail_body .= $contenutoTEXT.$eol.$eol;
		
		$mail_body .= "--$boundary".$eol;
		$mail_body .= "Content-Type: text/html; charset=\"".$this->Charset."\"; Content-Transfer-Encoding: quoted-printable".$eol.$eol;// format=flowed\nContent-transfer-encoding: quoted-printable\n"; 
		
		//Contenuto HTML da modificare.
		$contenutoHTML = str_replace(">",">".$eol,$contenutoHTML);
		$contenutoHTML = str_replace(" "," ".$eol,$contenutoHTML);
		$mail_body .= $contenutoHTML;
		$mail_body .= $eol.$eol."--$boundary--";
			
		// INVIO DELLA MAIL
		return mail($to, $soggetto, $mail_body, $mail_headers);
	}

	public function checkMail($email,$multi=true){
		if($multi){
			$email = str_replace(";",",",$email);
			$arr = explode(",",$email);
			
			$ret = false;
			foreach($arr as $e){
				$ret = self::validateMail($e);
				if(!$ret)
					break;
			}
			return $ret;
		}else{
			return self::validateMail($email);
		}
	}
	public function validateMail($email){
		return eregi("^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-]{2,})+\.)+([a-zA-Z0-9]{2,})+$",$email);
	}
}

class NE_GF{
	public function globR($path,$exclude=NULL){
		$files = glob($path);
		$arr = array();
		foreach($files as $f){
			if( $f!=$exclude && !in_array($f,$exclude)){
				if(is_dir($f)){
					$tmp = NE_GF::globR($f."/*",$exclude);
					$arr = array_merge($arr,$tmp);
				}else{
					$arr[] = $f;
				}
			}
		}
		unset($files,$path,$tmp);
		return $arr;
	}
}

function NE_md2ts($data,$ora_legale=-1){ //converte una data in formato Mysql ad una data Unix timestamp , $ora_legale = (-1=>automatico, 0=>no,1=si)
	$anno = substr($data,0,4);
    $mese = substr($data,5,2);
    if(substr($mese,1,1)=="-"){
        $mese = substr($data,5,1);
        $giorno = substr($data,7,2);
    }else $giorno = substr($data,8,2);
	
	if( strlen($data)==strlen("0000-00-00 00:00:00") ){
		$H = substr($data,strlen("0000-00-00 "),2);
		$M = substr($data,strlen("0000-00-00 00:"),2);
		$S = substr($data,strlen("0000-00-00 00:00:"),2);
	}else{
		$H = $M = $S = 0;
	}
	
    return mktime($H,$M,$S,$mese,$giorno,$anno,$ora_legale);
}

function NE_checkMail($email){
	return NE_Email::checkMail($email); //per retrocompatibilita
}

function NE_XML($encoding="UTF-8",$standalone="yes",$version="1.0"){
	header("Content-type: application/xml");
	header("Cache-control: no-cache");
	return "<?xml version=\"{$version}\" encoding=\"{$encoding}\" standalone=\"{$standalone}\" ?>";
}
function NE_RandomCode($n_chars=8,$chars_type="aAnS"){
	$chars = array(); //a=>alfabetico minuscolo, A=>alfabeto maiuscolo, n=>numeri, S=>caratteri speciali
	$chars["a"] = "abcdefghijklmopqrstuvwxyz";
	$chars["A"] = strtoupper($chars["a"]);
	$chars["n"] = "0123456789";
	$chars["S"] = "$!£%&/()=?^,.;:#*+";
	
	$Chars = "";
	$chars_type_arr = str_split($chars_type);
	foreach($chars as $k=>$v){
		if(in_array($k,$chars_type_arr)){
			$Chars .= $v;
		}
	}

	$n_chars = $n_chars<0 ? 8 : $n_chars;
	for($i=0,$code="";$i<$n_chars;$i++){
		$code .= substr($Chars, rand(0,strlen($Chars)-1), 1);
	}
	return $code;
}

class NE_Excel{
	/**********
	Uso per esportazione Excel:
		NE_Excel::Header(nome del file)
		NE_Excel::BOF();
		NE_Excel::WriteNumber($row,$col,$val) -> per valore numerico
		NE_Excel::WriteLabel($row,$col,$val) -> per valore testuale
		NE_Excel::EOF();
	Uso per esportazione CSV:
		NE_Excel::Header(nome del file, "csv")
		
	***********/
	public function Header($filename="output", $type="xls", $disposition="attachment"){	//type => xls, csv | disposition=> attachment, inline
		switch($type){
			case "xls":
				header("Pragma: public");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
				header("Content-Type: application/force-download");
				header("Content-Type: application/octet-stream");
				header("Content-Type: application/download");;
				header("Content-Disposition: {$disposition};filename={$filename}");
				header("Content-Transfer-Encoding: binary ");
				break;
			case "csv":
				header("Content-Type: application/csv");
				header("Content-Disposition: {$disposition};filename={$filename}");
				break;
		}
	}
	public function BOF(){
		echo pack("ssssss", 0x809, 0x8, 0x0, 0x10, 0x0, 0x0);
		return;
	}
	public function EOF(){
		echo pack("ss",0x0A,0x00);
		return;
	}
	public function WriteNumber($Row,$Col,$Value){
		echo pack("sssss", 0x203, 14, $Row, $Col, 0x0); 
		echo pack("d", $Value);
		return;
	}
	public function WriteLabel($Row,$Col,$Value){
		$L = strlen($Value);
		echo pack("ssssss", 0x204, 8 + $L, $Row, $Col, 0x0, $L); 
		echo $Value;
		return;
	}
}

?>