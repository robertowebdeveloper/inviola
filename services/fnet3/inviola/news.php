<?php
setlocale(LC_TIME,"it_IT.utf8");
require_once("module/nmefw.php");
$cn = new NE_mysql(0);

$id = $_GET["id"];
$q = "SELECT * FROM `{$_db_prefix}news_promo` WHERE id = {$id}";
$News = $cn->OQ($q);
//print_r($News);
?><!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
<!--meta name="viewport" content="width=320, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=yes"-->
<title>InViola Card :: <?=$News["title"]; ?></title>
<!--<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">-->
<style type="text/css"><!--
html,body{
	margin: 0;
	padding: 0;
	width: 100%;
	height: 100%;
}
body{
	font-family: "Lucida Grande", "Lucida Sans Unicode", "Lucida Sans", "DejaVu Sans", Verdana, sans-serif;
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	font-size: 14px;
	line-height: 1em;
	color:#333;
}
h1{
	font-size: 32px;
	line-height: 1;
	color: #492f91;
	text-transform: uppercase;
}
hr{
	display: block;
	height: 0;
	border: none;
	background: none;
	padding: 0;
	margin: 10px 0;
	font-size: 1px;
	line-height: 0;
	border-top: 1px solid #ccc;
}
#Site{
	/*width: 900px;*/
	padding: 20px 15px;
}
.container{
	margin: 0 auto;
}
.justify{
	text-align: justify;
}
.FotoCont{
	margin: 20px 0;
	width: 100%;
	box-shadow: 0 0 15px rgba(0,0,0,.25);
	-webkit-box-shadow: 0 0 15px rgba(0,0,0,.25);
	-moz-box-shadow: 0 0 15px rgba(0,0,0,.25);
	border: 2px solid #f4f4f4;
}
.Foto{
	margin: 0 auto;
	display: block;
	max-width: 100%;
}
#Cont img, #Cont picture{
	display: none !important;
}
--></style>
</head>

<body>

<div id="Site" class="container">
	<?php //print_r($_GET); ?>
	<h1><?=$News["title"]; ?></h1>
    <?php if($News["id_file"]>0 ){
		$q = "SELECT * FROM `{$_db_prefix}file` WHERE id = {$News[id_file]}";
		$F = $cn->OQ($q);
		?>
        <div class="FotoCont"><img src="<?=path_webroot; ?>_public/file/<?=substr($F["id"],0,1); ?>/<?=$F["id"]; ?>.<?=$F["ext"]; ?>" class="Foto"></div>
    <?php } ?>

    <div id="Cont" class="justify">
    	<?=$News["description"]; ?>
    </div>
</div>

</body>
</html><?php
$cn->Close();
unset($cn);
?>