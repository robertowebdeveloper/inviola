<?php
require("../module/nmefw.php");
$cn = new NE_mysql(0);

if(isset($_GET["call"]) ){
	$_POST = $_GET;
}
	
$call = $_REQUEST["call"];
switch($call){
	case "update_merge":
		$return_arr = array();
		
		if( isset($_POST["old"]) && isset($_POST["new"]) ){
			$old = $_POST["old"];
			$new = $_POST["new"];
			
			$q = "SELECT * FROM utenti WHERE customer_id = {$old} LIMIT 1";
			$x = $cn->OQ($q);
			if( $x==-1 ){
				$return_arr[] = array("returncode"=>1002);
			}else{
				$q = "UPDATE utenti SET
					customer_id_premerge = {$old},
					customer_id = {$new}";
				if( strlen($_POST["card"])>0 ){
					$q .= ", card = '{$_POST['card']}'";
				}
				$q .= " WHERE id = {$x[id]}";
				$cn->Q($q);
				$return_arr[] = array("returncode"=>0);
				$fp = fopen("card_merge.txt","a");
				fwrite($fp, date("d-m-Y H:i:s") . " ------ {$q} ----- \r\n");
				fclose($fp);
			}
		}else{
			$return_arr[] = array("returncode"=>1003);
		}
		echo json_encode($return_arr);
		break;
}
$cn->Close();
unset($cn);
?>